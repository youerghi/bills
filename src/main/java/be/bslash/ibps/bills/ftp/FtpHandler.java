package be.bslash.ibps.bills.ftp;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

@Component
public class FtpHandler {

    private static final Logger LOGGER = LogManager.getLogger("BACKEND_LOGS");

    @Value("${ftp.host}")
    private String host;

    @Value("${ftp.port}")
    private Integer port;

    @Value("${ftp.username}")
    private String username;

    @Value("${ftp.password}")
    private String password;

    private FTPClient ftpClient;

    public FtpHandler() {
        ftpClient = new FTPClient();
    }

    public void openConnection() throws IOException {
        ftpClient.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
        int reply;
        ftpClient.connect(host, port);
        reply = ftpClient.getReplyCode();
        if (!FTPReply.isPositiveCompletion(reply)) {
            ftpClient.disconnect();
        }
        ftpClient.login(username, password);
        ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
        ftpClient.enterLocalPassiveMode();
    }

    public void uploadFile(File file)
            throws Exception {
        try (InputStream input = new FileInputStream(file)) {
            if (!this.ftpClient.isConnected()) {
                this.openConnection();
            }
            this.ftpClient.storeFile(file.getName(), input);
            this.disconnect();
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex), ex);
        }
    }

    public void disconnect() {
        if (this.ftpClient.isConnected()) {
            try {
                this.ftpClient.logout();
                this.ftpClient.disconnect();
            } catch (IOException f) {
                LOGGER.error(ExceptionUtils.getStackTrace(f), f);
                // do nothing as file is already saved to server
            }
        }
    }
}
