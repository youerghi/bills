package be.bslash.ibps.bills.exceptions;

import be.bslash.ibps.bills.configrations.Utf8ResourceBundle;

import java.util.Locale;

public class ValidationMessage {

    private String field;
    private String message;

    public ValidationMessage(String field, String messageKey, Locale locale) {
        String text = Utf8ResourceBundle.getString(messageKey, locale);
        this.field = field;
        this.message = text;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
