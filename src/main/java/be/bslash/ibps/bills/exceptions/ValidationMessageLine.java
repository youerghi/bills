package be.bslash.ibps.bills.exceptions;

import java.util.Locale;

public class ValidationMessageLine extends ValidationMessage {

    private Integer lineNumber;

    public ValidationMessageLine(String field, String messageKey, Integer lineNumber, Locale locale) {
        super(field, messageKey, locale);
        this.lineNumber = lineNumber;
    }

    public Integer getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Integer lineNumber) {
        this.lineNumber = lineNumber;
    }
}
