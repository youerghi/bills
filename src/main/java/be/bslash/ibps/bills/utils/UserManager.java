package be.bslash.ibps.bills.utils;

import be.bslash.ibps.bills.entities.EntityUser;
import be.bslash.ibps.bills.enums.Status;
import be.bslash.ibps.bills.exceptions.HttpServiceException;
import be.bslash.ibps.bills.security.JwtTokenProvider;
import be.bslash.ibps.bills.services.EntityUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

@Component
public class UserManager {

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private EntityUserService entityUserService;

    public EntityUser getUserEntity(HttpServletRequest httpServletRequest, Locale locale) throws HttpServiceException {

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser == null) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "inavllid_token", locale);
        }

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        return entityUser;
    }

}
