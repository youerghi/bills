package be.bslash.ibps.bills.utils;

import be.bslash.ibps.bills.configrations.Utf8ResourceBundle;
import be.bslash.ibps.bills.dto.response.BillReportEntityDtoRs;
import be.bslash.ibps.bills.entities.Bill;
import be.bslash.ibps.bills.entities.BillCustomField;
import be.bslash.ibps.bills.entities.BillItem;
import be.bslash.ibps.bills.entities.Entity;
import be.bslash.ibps.bills.entities.EntityField;
import be.bslash.ibps.bills.entities.EntityUser;
import be.bslash.ibps.bills.enums.AccountCategory;
import be.bslash.ibps.bills.enums.BillStatus;
import be.bslash.ibps.bills.enums.BillType;
import be.bslash.ibps.bills.enums.BooleanFlag;
import be.bslash.ibps.bills.enums.BulkBillStatus;
import be.bslash.ibps.bills.enums.EntityType;
import be.bslash.ibps.bills.enums.EntityUserType;
import be.bslash.ibps.bills.enums.IdIssueAuthority;
import be.bslash.ibps.bills.enums.IdType;
import be.bslash.ibps.bills.enums.PaymentMethod;
import be.bslash.ibps.bills.enums.TypeIdDocument;
import be.bslash.ibps.bills.enums.UserRole;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.util.ResourceUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.FileNotFoundException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Utils {

    private static final Logger LOGGER = LogManager.getLogger("BACKEND_LOGS");

    public static BigDecimal roundDecimalHalfDown(BigDecimal amount) {
        try {
            amount = amount.setScale(2, RoundingMode.HALF_DOWN);
            return amount;
        } catch (Exception ex) {
            return null;
        }
    }

    public static boolean isNullOrEmpty(Long value) {
        if (value == null || value.equals(0L)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isNullOrEmpty(BigDecimal value) {
        if (value == null || value.doubleValue() <= 0) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isNullOrEmpty(String value) {
        if (value == null || value.trim().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isNullOrEmpty(List value) {
        if (value == null || value.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public static BigDecimal calculateTotalAmountFromVatAndPreviousBalance(BigDecimal subAmount, String vat, BigDecimal previousBalance) {

        if (previousBalance == null) {
            previousBalance = new BigDecimal("0");
        }

        BigDecimal vatValue = null;
        if (vat == null || vat.equals("EXE") || vat.equals("NA")) {
            vatValue = new BigDecimal("0");
        } else {
            vatValue = new BigDecimal(vat);
        }

        BigDecimal vatAmount = subAmount.multiply(vatValue);
        return subAmount.add(vatAmount).add(previousBalance);
    }

    public static BigDecimal convertToBigDecimal(String number) {
        try {
            return new BigDecimal(number.trim());
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex), ex);
            return null;
        }
    }

    public static LocalDateTime getStartDayOfCurrentMonth() {
        LocalDate localDate = LocalDate.now();

        return localDate.withDayOfMonth(1).atStartOfDay();
    }

    public static LocalDateTime getCurrentDateTime() {
        return LocalDateTime.now();
    }

    public static String generateBillNumber(Entity entity) {

        String commercialRegisterName = entity.getCommercialNameEn();

        String commercialNameArray[] = commercialRegisterName.split("\\s+");

        String billNumber = "";

        if (commercialNameArray.length > 1) {
            billNumber = commercialNameArray[0].charAt(0) + "" + commercialNameArray[1].charAt(0);
        } else {
            billNumber = commercialNameArray[0].charAt(0) + "" + commercialNameArray[0].charAt(1);
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMdd");
        String dateTimeBillNumber = LocalDateTime.now().format(formatter);

        billNumber = billNumber + "" + dateTimeBillNumber + "-";

        return billNumber;
    }

    public static String formatMobileNumber(String phoneStr) {
        try {
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
            Phonenumber.PhoneNumber thePhoneNumber = null;

            thePhoneNumber = phoneUtil.parse(phoneStr.trim(), "SA");

            return phoneUtil.format(thePhoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean isMinimumPartialAmountAllowed(Bill bill) {

        if (bill.getEntity().getIsPartialAllowed().equals(BooleanFlag.NO)) {
            return true;
        }

        if (bill.getIsPartialAllowed() != null && bill.getIsPartialAllowed().equals(BooleanFlag.NO)) {
            return true;
        }

        BigDecimal minimumPartialAmount = bill.getMiniPartialAmount();
        LOGGER.info("minimumPartialAmount: " + minimumPartialAmount);

        BigDecimal minimumActivityAmount = bill.getEntityActivity().getTheLowestAmountPerUploadedBill();

        LOGGER.info("minimumActivityAmount: " + minimumActivityAmount);

        Integer compareResult = minimumPartialAmount.compareTo(minimumActivityAmount);

        if (compareResult == -1) {
            return false;
        }

        return true;
    }

    public static LocalDateTime parseDateFromString(String date, String pattern) {
        if (date == null || date.isEmpty()) {
            return null;
        }
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
            LocalDate localDate = LocalDate.parse(date.trim(), formatter);
            return localDate.atStartOfDay();
        } catch (Exception ex) {
            return null;
        }
    }

    public static boolean isNumber(String value) {//[0-9]+
        if (value == null) {
            return true;
        }
        String regex = "^[0-9 ]+$";

        Pattern p = Pattern.compile(regex);

        Matcher m = p.matcher(value);

        return m.matches();
    }

    public static boolean isValidTaxNumber(String taxNumber) {
        if (taxNumber == null || taxNumber.isEmpty()) {
            return true;
        }

        taxNumber = taxNumber.trim();

        if (!isNumber(taxNumber)) {
            return false;
        }

        if (taxNumber.length() != 15) {
            return false;
        }

        return true;
    }


    public static boolean isIdNumberValid(String id, IdType idType) {

        if (id == null || id.isEmpty()) {
            return true;
        }

        if (idType == null || idType.equals(IdType.PAS) || idType.equals(IdType.CRR)) {
            return true;
        }

        id = id.trim();

        if (!id.matches("[0-9]+")) {
            return false;//
        }
        if (id.length() != 10) {
            return false;//
        }
        int type = Integer.parseInt(id.substring(0, 1));
        if (type != 2 && type != 1) {
            return false;//
        }
        int sum = 0;
        for (int i = 0; i < 10; i++) {
            if (i % 2 == 0) {
                String ZFOdd = String.format("%02d", Integer.parseInt(id.substring(i, i + 1)) * 2);
                sum += Integer.parseInt(ZFOdd.substring(0, 1)) + Integer.parseInt(ZFOdd.substring(1, 2));
            } else {
                sum += Integer.parseInt(id.substring(i, i + 1));
            }
        }

        int result = (sum % 10 != 0) ? -1 : type;

        if (result == -1) {
            return false;//
        } else {
            return true;
        }
    }

    public static boolean isValidEmail(String emailStr) {
        if (emailStr == null || emailStr.isEmpty()) {
            return true;
        }
        final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr.trim());
        return matcher.find();
    }

    public static boolean isValidMobileNumber(String phoneStr) {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber thePhoneNumber = null;
        try {
            thePhoneNumber = phoneUtil.parse(phoneStr.trim(), "SA");
        } catch (NumberParseException e) {
            return false;
        }
        return phoneUtil.isValidNumber(thePhoneNumber);
    }

    public static Document convertStringToXmlDocument(String xml) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(xml));
            return builder.parse(is);
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex), ex);
            return null;
        }
    }

    public static String getElementValueByTagName(Document document, String tagName) {
        try {

            NodeList nodeList = document.getElementsByTagName(tagName);

            if (nodeList == null) {
                return "";
            }

            for (int index = 0; index < nodeList.getLength(); index++) {

                Node nodeItem = nodeList.item(index);

                if (nodeItem.getNodeType() == Node.ELEMENT_NODE) {

                    Element element = (Element) nodeItem;

                    return element.getTextContent();
                }
            }

            return "";
        } catch (Exception ex) {
            LOGGER.error(ExceptionUtils.getStackTrace(ex), ex);
            return "";
        }
    }

    public static String parseDateTime(LocalDateTime localDateTime, String pattern) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
            String formattedDateTime = localDateTime.format(formatter);
            return formattedDateTime;
        } catch (Exception ex) {
            return null;
        }

    }

    public static String roundDecimal(BigDecimal value) {
        if (value == null) {
            return "00.00";
        }
        DecimalFormat df = new DecimalFormat("0.00");
        return df.format(value.doubleValue());
    }

    public static boolean isValidSize(String value, Integer min, Integer max) {
        if (value == null) {
            return true;
        }

        Integer size = value.length();

        if (size >= min && size <= max) {
            return true;
        } else {
            return false;
        }
    }

    public static BigDecimal calculateSubAmountDiscount(BigDecimal subAmount, BigDecimal discount, String discountType) {
//
//        if (discountType == null || discountType.isEmpty()) {
//            discountType = "PERC";
//        }
//
//        BigDecimal discountAmount = null;
//        switch (discountType) {
//            case "PERC":
//                discountAmount = subAmount.multiply(discount);
//                break;
//            case "FIXED":
//                discountAmount = new BigDecimal(discount.toString());
//                break;
//            default:
//                discountAmount = subAmount.multiply(discount);
//        }
//
//        return subAmount.subtract(discountAmount).abs();
        return subAmount;
    }

    public static boolean isValidCustomField(EntityField entityField, String value) {
        if (entityField.getIsRequired().equals(BooleanFlag.YES) && isNullOrEmpty(value)) {
            return false;
        }

        return true;
    }


    public static String generateDocumentNumber(Entity entity) {

        String paymentNumber = "";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMddhhmmss");
        String dateTimeBillNumber = LocalDateTime.now().format(formatter);

        paymentNumber = entity.getCode() + "" + dateTimeBillNumber;

        return paymentNumber;
    }


    public static boolean isEnumValueExist(String value, Class enumClass) {

        if (isNullOrEmpty(value)) {
            return false;
        }

        try {
            switch (enumClass.getSimpleName()) {
                case "IdIssueAuthority":
                    return IdIssueAuthority.valueOf(value) != null;
                case "EntityType":
                    return EntityType.valueOf(value) != null;
                case "TypeIdDocument":
                    return TypeIdDocument.valueOf(value) != null;
                case "IdType":
                    return IdType.valueOf(value) != null;
                case "BillStatus":
                    return BillStatus.valueOf(value) != null;
                case "BillType":
                    return BillType.valueOf(value) != null;
                case "EntityUserType":
                    return EntityUserType.valueOf(value) != null;
                case "BulkBillStatus":
                    return BulkBillStatus.valueOf(value) != null;
                case "UserRole":
                    return UserRole.valueOf(value) != null;
                case "PaymentMethod":
                    return PaymentMethod.valueOf(value) != null;
                case "AccountCategory":
                    return AccountCategory.valueOf(value) != null;
                default:
                    return false;
            }
        } catch (Exception ex) {
            return false;
        }
    }

    public static Integer parseInt(String number) {
        try {
            return Integer.parseInt(number);
        } catch (Exception ex) {
            return null;
        }
    }

    public static JasperReport findReportResourceFile(String reportName, Locale locale) {
        try {
            if (locale.getISO3Language().equalsIgnoreCase("eng")) {
                return JasperCompileManager.compileReport(ResourceUtils.getFile("classpath:" + reportName + "En.jrxml").getPath());
            } else {
                return JasperCompileManager.compileReport(ResourceUtils.getFile("classpath:" + reportName + "En.jrxml").getPath());
            }
        } catch (FileNotFoundException e) {
            LOGGER.error(ExceptionUtils.getStackTrace(e), e);
            return null;
        } catch (JRException e) {
            LOGGER.error(ExceptionUtils.getStackTrace(e), e);
            return null;
        }
    }

    public static List<BillReportEntityDtoRs> fetchBillReportEntityList(List<Bill> billList, Locale locale) {
        List<BillReportEntityDtoRs> billReportEntityDtoRsList = new ArrayList<>();

        billList.forEach(bill -> {

            BillReportEntityDtoRs billReportEntityDtoRs = new BillReportEntityDtoRs();
            billReportEntityDtoRs.setBillNumber(bill.getBillNumber());
            billReportEntityDtoRs.setSadadNumber(bill.getSadadNumber());
            billReportEntityDtoRs.setIssueDate(bill.getIssueDate());
            billReportEntityDtoRs.setExpireDate(bill.getExpireDate());
            billReportEntityDtoRs.setCustomerFullName(bill.getCustomerFullName());
            billReportEntityDtoRs.setCustomerMobileNumber(bill.getCustomerMobileNumber());
            billReportEntityDtoRs.setTotalAmount(bill.getTotalAmount());
            billReportEntityDtoRs.setBillStatus(Utf8ResourceBundle.getString(bill.getBillStatus().name(), locale));

            billReportEntityDtoRsList.add(billReportEntityDtoRs);
        });

        return billReportEntityDtoRsList;
    }

    public static String fetchBillReportVolume(List<Bill> billList, Locale locale) {

        BigDecimal totalAmount = new BigDecimal(0);
        for (Bill bill : billList) {
            totalAmount = totalAmount.add(bill.getTotalAmount());
        }

        return totalAmount + " " + Utf8ResourceBundle.getString("SAR", locale);
    }

    public static JasperPrint fillReport(JasperReport jasperReport, Map parameters, JRBeanCollectionDataSource jrBeanCollectionDataSource) {
        try {
            return JasperFillManager.fillReport(jasperReport, parameters,
                    jrBeanCollectionDataSource);
        } catch (JRException e) {
            LOGGER.error(ExceptionUtils.getStackTrace(e), e);
            return null;
        }
    }

    public static Map<String, Object> fillDataForBillAdv(Bill bill, List<BillItem> billItemList, List<BillCustomField> billCustomFieldList, Map<String, Object> adMap, Map<String, Object> paymentDetailsMap) {


        Locale localeEn = new Locale("en");
        Locale localeAr = new Locale("ar");

        Map<String, Object> data = new HashMap<>();

        LOGGER.info("adMap: " + adMap);

        if (adMap.containsKey("adDetailsList")) {
            data.put("isClickable", adMap.get("isClickable"));
            data.put("isSwitchable", adMap.get("isSwitchable"));
            data.put("adTime", adMap.get("adTime"));
            data.put("adDetailsList", adMap.get("adDetailsList"));
            data.put("isEnable", "true");
        } else {
            data.put("isEnable", "false");
            data.put("isClickable", adMap.get("isClickable"));
            data.put("isSwitchable", adMap.get("isSwitchable"));
            data.put("adDetailsList", null);
            data.put("adTime", 0);
        }
        data.put("baseURL", adMap.get("baseURL"));
        data.put("entityLogo", bill.getEntity().getLogo());
        data.put("entityStamp", bill.getEntity().getStamp());
        data.put("commercialRegister", bill.getEntity().getIdNumber());
        data.put("entityTaxNumber", bill.getEntity().getVatNumber());
        data.put("customerTaxNumber", bill.getCustomerTaxNumber() == null ? ("-") : bill.getCustomerTaxNumber());
        data.put("customerName", bill.getCustomerFullName());
        data.put("customerAddress", bill.getCustomerAddress() == null ? ("-") : bill.getCustomerAddress());
        data.put("sadadNumber", bill.getSadadNumber());
        data.put("billNumber", bill.getBillNumber());
        data.put("issueDate", parseDateTime(bill.getIssueDate(), "yyyy-MM-dd"));
        data.put("brandNameEn", bill.getEntity().getBrandNameEn());
        data.put("brandNameAr", bill.getEntity().getBrandNameAr());
        data.put("entityCode", bill.getEntity().getCode());
        data.put("serviceName", bill.getServiceName());
        data.put("billerNameEn", bill.getEntity().getBrandNameEn());
        data.put("billerNameAr", bill.getEntity().getBrandNameAr());

        if (bill.getBillStatus().equals(BillStatus.PAID_BY_COMPANY)
                || bill.getBillStatus().equals(BillStatus.PAID_BY_SADAD)
                || bill.getBillStatus().equals(BillStatus.SETTLED_BY_SADAD)
                || bill.getBillStatus().equals(BillStatus.SETTLED_BY_COMPANY)) {
            data.put("statusAr", "مدفوعة");
            data.put("statusEn", "Paid");
            data.put("paymentFlag", "PAID");
        } else {
            data.put("statusAr", "غير مدفوعة");
            data.put("statusEn", "Unpaid");
            data.put("paymentFlag", "UNPAID");
        }

        data.put("customerIdNumber", bill.getCustomerIdNumber());
        if (bill.getCustomerIdType() != null) {
            data.put("customerIdTypeEn", Utf8ResourceBundle.getString(bill.getCustomerIdType().name(), localeEn));
            data.put("customerIdTypeAr", Utf8ResourceBundle.getString(bill.getCustomerIdType().name(), localeAr));
        }


        if (bill.getEntity().getCity() != null) {
            data.put("cityEn", bill.getEntity().getCity().getNameEn());
            data.put("cityAr", bill.getEntity().getCity().getNameAr());
        }
        if (bill.getEntity().getDistrict() != null) {
            data.put("districtEn", bill.getEntity().getDistrict().getNameEn());
            data.put("districtAr", bill.getEntity().getDistrict().getNameAr());
        }

        BigDecimal totalVatAmount = new BigDecimal(0);

        BigDecimal subAmount = new BigDecimal("0");

        if (bill.getBillType().equals(BillType.MASTER_BILL)) {
            Map<String, Object> detailsItem = new HashMap<>();

            BigDecimal vatItem = bill.getVat();
            BigDecimal subAmountItem = bill.getSubAmount();

            BigDecimal vatAmountItem = subAmountItem.multiply(vatItem);
            BigDecimal totalAmountItem = subAmountItem.add(vatAmountItem);

            String[] itemDetailArray = new String[6];
            itemDetailArray[0] = bill.getServiceName();
            itemDetailArray[1] = Utils.roundDecimal(subAmountItem);
            itemDetailArray[2] = "1";
            itemDetailArray[3] = Utils.roundDecimal(new BigDecimal("0"));

            if (bill.getVatNaFlag().equals(BooleanFlag.YES)) {
                String vatEn = Utf8ResourceBundle.getString("VAT_NA", localeEn);
                String vatAr = Utf8ResourceBundle.getString("VAT_NA", localeAr);
                itemDetailArray[4] = vatEn + "-" + vatAr;
            } else {
                if (bill.getVatExemptedFlag().equals(BooleanFlag.YES)) {
                    String vatEn = Utf8ResourceBundle.getString("VAT_EXE", localeEn);
                    String vatAr = Utf8ResourceBundle.getString("VAT_EXE", localeAr);
                    itemDetailArray[4] = vatEn + "-" + vatAr;
                } else {
                    itemDetailArray[4] = Utils.roundDecimal(vatAmountItem) + "(" + Utils.roundDecimal(vatItem.multiply(new BigDecimal(100))) + "%)";
                }
            }

            itemDetailArray[5] = Utils.roundDecimal(totalAmountItem);

            totalVatAmount = bill.getSubAmount().multiply(bill.getVat());

            subAmount = subAmount.add(bill.getSubAmount());

            detailsItem.put("01", itemDetailArray);
            data.put("detailedItem", detailsItem);

        } else {
            Map<Integer, Object> detailsItem = new HashMap<>();
            Integer line = 1;
            for (BillItem billItem : billItemList) {

                BigDecimal vatItem = billItem.getVat();
                BigDecimal unitPriceItem = billItem.getUnitPrice();
                BigDecimal itemPrice = unitPriceItem.multiply(billItem.getQuantity());

                subAmount = subAmount.add(itemPrice);

                String[] itemDetailArray = new String[6];
                itemDetailArray[0] = billItem.getName();
                itemDetailArray[1] = Utils.roundDecimal(unitPriceItem);
                itemDetailArray[2] = billItem.getQuantity().toString();

                BigDecimal discountAmount = null;
                switch (billItem.getDiscountType()) {
                    case "PERC":
                        BigDecimal discountPercent = billItem.getDiscount().divide(new BigDecimal(100));
                        discountAmount = itemPrice.multiply(discountPercent);
                        itemPrice = itemPrice.subtract(discountAmount);
                        itemDetailArray[3] = Utils.roundDecimal(discountAmount) + "(" + Utils.roundDecimal(billItem.getDiscount()) + "%)";
                        break;
                    case "FIXED":
                        BigDecimal discountFixed = billItem.getDiscount();
                        discountAmount = discountFixed;
                        itemPrice = itemPrice.subtract(discountAmount);
                        itemDetailArray[3] = Utils.roundDecimal(discountAmount);
                        break;
                    default:
                        BigDecimal discountDefault = billItem.getDiscount().divide(new BigDecimal(100));
                        discountAmount = itemPrice.multiply(discountDefault);
                        itemPrice = itemPrice.subtract(discountAmount);
                        itemDetailArray[3] = Utils.roundDecimal(discountAmount) + "(" + Utils.roundDecimal(billItem.getDiscount()) + "%)";
                        break;
                }

                BigDecimal vatAmount = itemPrice.multiply(vatItem);

                totalVatAmount = totalVatAmount.add(vatAmount);

                LOGGER.info("totalVatAmount: " + totalVatAmount);

                itemPrice = itemPrice.add(vatAmount);

                if (billItem.getVatNaFlag().equals(BooleanFlag.YES)) {
                    String vatEn = Utf8ResourceBundle.getString("VAT_NA", localeEn);
                    String vatAr = Utf8ResourceBundle.getString("VAT_NA", localeAr);
                    itemDetailArray[4] = vatEn + "-" + vatAr;
                } else {
                    if (billItem.getVatExemptedFlag().equals(BooleanFlag.YES)) {
                        String vatEn = Utf8ResourceBundle.getString("VAT_EXE", localeEn);
                        String vatAr = Utf8ResourceBundle.getString("VAT_EXE", localeAr);
                        itemDetailArray[4] = vatEn + "-" + vatAr;
                    } else {
                        itemDetailArray[4] = Utils.roundDecimal(vatAmount) + "(" + Utils.roundDecimal(vatItem.multiply(new BigDecimal(100))) + "%)";
                    }
                }

                itemDetailArray[5] = Utils.roundDecimal(itemPrice);

                detailsItem.put(line++, itemDetailArray);
            }
            data.put("detailedItem", detailsItem);

        }

        BigDecimal discountAmount = bill.getDiscount();
        BigDecimal totalAmountAfterDiscount = bill.getSubAmount().subtract(discountAmount);

        data.put("paymentId", bill.getPaymentId());
        data.put("subAmount", Utils.roundDecimal(subAmount));
        data.put("totalDiscountAmount", bill.getDiscount());
        data.put("totalVatAmount", Utils.roundDecimal(totalVatAmount));
        data.put("totalAmount", Utils.roundDecimal(bill.getTotalAmount()));
        data.put("previousBalance", Utils.roundDecimal(bill.getCustomerPreviousBalance()));
        data.put("totalAmountWithoutPreviousBalance", Utils.roundDecimal((bill.getTotalAmount().subtract(bill.getCustomerPreviousBalance())).abs()));
        data.put("isDiscountShown", (discountAmount.doubleValue() <= 0 ? false : true));
        data.put("isVatShown", true);

        data.put("statusAr", paymentDetailsMap.get("statusAr"));
        data.put("statusEn", paymentDetailsMap.get("statusEn"));
        data.put("paymentFlag", paymentDetailsMap.get("paymentFlag"));
        data.put("paymentDate", paymentDetailsMap.get("paymentDate"));
        data.put("paymentMethod", paymentDetailsMap.get("paymentMethod"));
        data.put("paymentMethodImg", paymentDetailsMap.get("paymentMethodImg"));

        if (billCustomFieldList == null || billCustomFieldList.isEmpty()) {
            Map<String, Object> customFields = new HashMap<>();
            data.put("billCustomFieldList", null);
        } else {
            Integer line = 1;
            Map<String, Object> customFields = new HashMap<>();
            for (BillCustomField billCustomField : billCustomFieldList) {
                String[] customArray = new String[3];
                customArray[0] = billCustomField.getEntityField().getFieldNameEn();
                customArray[1] = billCustomField.getEntityField().getFieldNameAr();
                customArray[2] = billCustomField.getValue();
                customFields.put((line++).toString(), customArray);
            }
            data.put("billCustomFieldList", customFields);
        }

        return data;
    }

    public static Long parseLong(String number) {
        try {
            return Long.parseLong(number);
        } catch (Exception ex) {
            return null;
        }
    }

    public static String getSmsBody(String layout,
                                    String entityNameAr,
                                    String entityNameEn,
                                    String totalAmount,
                                    String sadadNumber,
                                    String billNumberEncoded,
                                    String dateTime) {

        String body = layout.replaceAll("ENTITY_NAME_AR", entityNameAr)
                .replaceAll("ENTITY_NAME_EN", entityNameEn)
                .replaceAll("BILL_AMOUNT", totalAmount)
                .replaceAll("SADAD_NUMBER", sadadNumber)
                .replaceAll("BILL_NUMBER_ENCODED", billNumberEncoded)
                .replaceAll("DATE_TIME", dateTime);
        return body;
    }

    public static Long getCostSms(String responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody);
            String responseData = jsonObject.getString("data");
            String[] pointArray = responseData.split(",");
            return parseLong(pointArray[1]);
        } catch (Exception ex) {
            return null;
        }
    }

    public static String getSmsResponseCode(String responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody);
            String responseData = jsonObject.getString("data");
            return responseData;
        } catch (Exception ex) {
            return null;
        }
    }

    public static boolean isSendNotificationForRole(EntityUser issueEntityUser, EntityUser destinationEntityUser) {


        if (issueEntityUser.getId().equals(destinationEntityUser.getId())) {
            return true;
        }

        EntityUserType issueUserType = issueEntityUser.getEntityUserType();

        EntityUserType destinationUserType = destinationEntityUser.getEntityUserType();

        if (issueUserType.equals(EntityUserType.OFFICER) && !destinationUserType.equals(EntityUserType.OFFICER)) {
            return true;
        }

        if (issueUserType.equals(EntityUserType.SUPERVISOR)) {
            if (destinationUserType.equals(EntityUserType.SUPERVISOR_PLUS)
                    || destinationUserType.equals(EntityUserType.SUPER_ADMIN)
                    || destinationUserType.equals(EntityUserType.SUPER_ADMIN_PLUS)) {
                return true;
            }
        }

        if (issueUserType.equals(EntityUserType.SUPERVISOR_PLUS)) {
            if (destinationUserType.equals(EntityUserType.SUPER_ADMIN)
                    || destinationUserType.equals(EntityUserType.SUPER_ADMIN_PLUS)) {
                return true;
            }
        }

        if (issueUserType.equals(EntityUserType.SUPER_ADMIN)) {
            if (destinationUserType.equals(EntityUserType.SUPER_ADMIN_PLUS)) {
                return true;
            }
        }

        if (issueUserType.equals(EntityUserType.SUPER_ADMIN_PLUS)) {
            if (destinationUserType.equals(EntityUserType.SUPER_ADMIN_PLUS)) {
                return true;
            }
        }

        return false;
    }

    public static String generatePaymentNumber(Entity entity) {

        String paymentNumber = "";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMddhhmmssSSS");
        String dateTimeBillNumber = LocalDateTime.now().format(formatter);

        paymentNumber = entity.getCode() + "" + dateTimeBillNumber;

        return paymentNumber;
    }

    public static String generateOtp(int len) {
        String numbers = "0123456789";
        Random rndm_method = new Random();

        char[] otpArray = new char[len];

        for (int i = 0; i < len; i++) {
            otpArray[i] = numbers.charAt(rndm_method.nextInt(numbers.length()));
        }
        return String.valueOf(otpArray);
    }

    public static Boolean parseBoolean(String booleanValue) {
        try {
            return Boolean.parseBoolean(booleanValue);
        } catch (Exception ex) {
            return null;
        }
    }

    public static boolean isNull(BigDecimal value) {
        if (value == null) {
            return true;
        } else {
            return false;
        }
    }
}
