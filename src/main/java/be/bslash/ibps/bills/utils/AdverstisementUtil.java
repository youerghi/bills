package be.bslash.ibps.bills.utils;


import be.bslash.ibps.bills.entities.AdCampaign;
import be.bslash.ibps.bills.entities.AdDetails;
import be.bslash.ibps.bills.entities.AdSettings;
import be.bslash.ibps.bills.entities.Entity;
import be.bslash.ibps.bills.enums.AdApprovalType;
import be.bslash.ibps.bills.enums.Status;
import be.bslash.ibps.bills.repositories.AdCampaignRepo;
import be.bslash.ibps.bills.repositories.AddSettingsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class AdverstisementUtil {

    @Autowired
    AddSettingsRepo addSettingsRepo;

    @Autowired
    AdCampaignRepo adCampaignRepo;

    @Autowired
    private Environment environment;

    public Map<String, Object> getAdvertisment(Entity entity, Integer advId) {

        Map<String, Object> advCampaignInfo = new HashMap<String, Object>();
        AdSettings adSettings = addSettingsRepo.findByEntity(entity);
        AdCampaign adCampaign = null;
        List<AdCampaign> adCampaignList = null;
        Map<Integer, Object> adDetailsList = new HashMap<Integer, Object>();

        if (adSettings != null && adSettings.getCampaignActive().equals(Status.ACTIVE)) {
            if (advId != null && advId != 0 && !adSettings.getMasterAdvEnable().equals(Status.ACTIVE)) {
                adCampaign = adCampaignRepo.findByIdAndApprovalType(Long.valueOf(advId.toString()), AdApprovalType.APPROVED);
                if (adCampaign != null) {
                    populateMapObject(adCampaign, adDetailsList, adSettings.getMasterURLEnable(), adSettings.getMasterURL());
                    advCampaignInfo.put("adDetailsList", adDetailsList);
                }
            } else {
                if (adSettings.getMasterCampaignId() != null) {
                    //adCampaignList = adCampaignRepo.findAdCampaignByStatusAndEntityAndCategoryType(Status.ACTIVE,entity,AdCategoryType.DEFAULT);
                    adCampaign = adCampaignRepo.findByIdAndApprovalType(Long.valueOf(adSettings.getMasterCampaignId()), AdApprovalType.APPROVED);
                    if (adCampaign != null) {
                        populateMapObject(adCampaign, adDetailsList, adSettings.getMasterURLEnable(), adSettings.getMasterURL());
                        advCampaignInfo.put("adDetailsList", adDetailsList);

                    }
                }
            }

            if (adSettings.getAdSwitchEnable().equals(Status.ACTIVE)) {
                advCampaignInfo.put("adTime", adSettings.getAdSwitchDuration() != null ? adSettings.getAdSwitchDuration() : 0);
            } else {
                advCampaignInfo.put("adTime", adCampaign != null ? adCampaign.getDispalyTime() : 0);
            }
            advCampaignInfo.put("isClickable", adSettings.getUrlClickable().equals(Status.ACTIVE) ? true : false);
            advCampaignInfo.put("isSwitchable", true);
        } else {
            advCampaignInfo.put("adDetailsList", null);
            advCampaignInfo.put("isClickable", false);
            advCampaignInfo.put("isSwitchable", false);
            advCampaignInfo.put("adTime", 0);
        }

        advCampaignInfo.put("isClickable", false);

        advCampaignInfo.put("baseURL", environment.getProperty("base.url.hit.count"));
        return advCampaignInfo;
    }

    private void populateMapObject(AdCampaign adCampaign, Map<Integer, Object> adDetailsList, Status masterURLEnable, String masterURL) {

        String[] advItem = new String[3];
        Integer line = 0;

        for (AdDetails adDetails : adCampaign.getAdDetailsSet()) {

            if (adDetails.getStatus().equals(Status.DELETED)) {
                continue;
            }

            advItem = new String[3];
            advItem[0] = adDetails.getId().toString();

            advItem[1] = adDetails.getImageURL();
            if (masterURLEnable != null && masterURLEnable.equals(Status.ACTIVE)) {
                advItem[2] = masterURL != null ? masterURL : "";
            } else {
                advItem[2] = adDetails.getUrl();
            }
            adDetailsList.put(line++, advItem);
        }


    }
}


