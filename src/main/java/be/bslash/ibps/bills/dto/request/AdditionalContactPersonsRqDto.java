package be.bslash.ibps.bills.dto.request;

import be.bslash.ibps.bills.dto.DtoValidation;
import be.bslash.ibps.bills.exceptions.HttpServiceException;
import be.bslash.ibps.bills.exceptions.ValidationMessage;
import be.bslash.ibps.bills.utils.Utils;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AdditionalContactPersonsRqDto implements DtoValidation {


    private String additionalContactPersonName;
    private String additionalContactPersonNumber;
    private String additionalContactPersonEmail;

    public String getAdditionalContactPersonName() {
        return additionalContactPersonName;
    }

    public void setAdditionalContactPersonName(String additionalContactPersonName) {
        this.additionalContactPersonName = additionalContactPersonName;
    }

    public String getAdditionalContactPersonNumber() {
        return additionalContactPersonNumber;
    }

    public void setAdditionalContactPersonNumber(String additionalContactPersonNumber) {
        this.additionalContactPersonNumber = additionalContactPersonNumber;
    }

    public String getAdditionalContactPersonEmail() {
        return additionalContactPersonEmail;
    }

    public void setAdditionalContactPersonEmail(String additionalContactPersonEmail) {
        this.additionalContactPersonEmail = additionalContactPersonEmail;
    }

    @Override
    public void validate(Locale locale) {

        List<ValidationMessage> errorList = new ArrayList<>();


        if (!Utils.isNullOrEmpty(this.additionalContactPersonNumber) && !Utils.isValidMobileNumber(this.additionalContactPersonNumber)) {
            ValidationMessage validationMessage = new ValidationMessage("additionalContactPersonNumber", "Additional mobile number not correct", locale);
            errorList.add(validationMessage);
        }

        if (!Utils.isNullOrEmpty(this.additionalContactPersonEmail) && !Utils.isValidEmail(this.additionalContactPersonEmail)) {
            ValidationMessage validationMessage = new ValidationMessage("additionalContactPersonEmail", "Additional email not correct", locale);
            errorList.add(validationMessage);
        }

        if (!errorList.isEmpty()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request", errorList, locale);
        }
    }

}

