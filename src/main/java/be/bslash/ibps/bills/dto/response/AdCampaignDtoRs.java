package be.bslash.ibps.bills.dto.response;

import be.bslash.ibps.bills.dto.request.AdvReqDto;

import java.math.BigDecimal;
import java.util.List;


public class AdCampaignDtoRs {

    private Integer id;

    private String campaignCode;

    private String name;

    private String startDate;

    private String endDate;

    private String userType;

    private String advCategory;

    private String approvalType;

    private BigDecimal displayDuration;

    private Integer billerId;

    private String billerNameEn;

    private String billerNameAr;

    private String rejectedNote;

    private List<AdvReqDto> adList;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getAdvCategory() {
        return advCategory;
    }

    public void setAdvCategory(String advCategory) {
        this.advCategory = advCategory;
    }

    public BigDecimal getDisplayDuration() {
        return displayDuration;
    }

    public void setDisplayDuration(BigDecimal displayDuration) {
        this.displayDuration = displayDuration;
    }

    public String getApprovalType() {
        return approvalType;
    }

    public void setApprovalType(String approvalType) {
        this.approvalType = approvalType;
    }

    public List<AdvReqDto> getAdList() {
        return adList;
    }

    public void setAdList(List<AdvReqDto> adList) {
        this.adList = adList;
    }

    public Integer getBillerId() {
        return billerId;
    }

    public void setBillerId(Integer billerId) {
        this.billerId = billerId;
    }

    public String getBillerNameEn() {
        return billerNameEn;
    }

    public void setBillerNameEn(String billerNameEn) {
        this.billerNameEn = billerNameEn;
    }

    public String getRejectedNote() {
        return rejectedNote;
    }

    public void setRejectedNote(String rejectedNote) {
        this.rejectedNote = rejectedNote;
    }

    public String getBillerNameAr() {
        return billerNameAr;
    }

    public void setBillerNameAr(String billerNameAr) {
        this.billerNameAr = billerNameAr;
    }

}
