package be.bslash.ibps.bills.dto;

import java.util.Locale;

public interface DtoValidation {
    public void validate(Locale locale);
}
