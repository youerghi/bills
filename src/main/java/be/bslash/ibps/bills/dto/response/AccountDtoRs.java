package be.bslash.ibps.bills.dto.response;

import be.bslash.ibps.bills.dto.request.AdditionalContactPersonsRqDto;
import be.bslash.ibps.bills.enums.Status;

import java.math.BigDecimal;
import java.util.List;

public class AccountDtoRs {


    private Long id;

    private Long accountId;

    private String accountName;

    private String accountNumber;

    private String idType;

    private String idNumber;

    private String vatNumber;

    private String attachmentId;

    private Status status;

    private String street;

    private String buildingNumber;

    private String additionalNumber;

    private String postalCode;

    private String account;

    private String type;

    private Long cityId;

    private Long districtId;

    private String primaryContactPersonNumber;

    private String primaryContactPersonEmail;

    private String primaryContactPersonName;

    private String companyName;


    private String contractNumber;

    private String contactPersonName;

    private String contactPersonNumber;

    private String contactPersonEmail;

    private String firstName;

    private String secondName;

    private String thirdName;

    private String lastName;

    private String phoneNumber;

    private String referenceNumber;

    private String email;

    private String gender;

    private Long activityId;

    private String companyIdNumber;

    private String companyIdType;

    private String companyType;

    private String streetName;

    private Long city;

    private String district;

    private String idAttachment;

    private String otherDistrict;

    private BigDecimal accountBalance;

    private String referenceName;

    public String getReferenceName() {
        return referenceName;
    }

    public void setReferenceName(String referenceName) {
        this.referenceName = referenceName;
    }

    private List<AdditionalContactPersonsRqDto> additionalContactPersonsList;

    public List<AdditionalContactPersonsRqDto> getAdditionalContactPersonsList() {
        return additionalContactPersonsList;
    }

    public void setAdditionalContactPersonsList(List<AdditionalContactPersonsRqDto> additionalContactPersonsList) {
        this.additionalContactPersonsList = additionalContactPersonsList;
    }

    public BigDecimal getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(BigDecimal accountBalance) {
        this.accountBalance = accountBalance;
    }

    public Long getCity() {
        return city;
    }

    public void setCity(Long city) {
        this.city = city;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getPrimaryContactPersonName() {
        return primaryContactPersonName;
    }

    public void setPrimaryContactPersonName(String primaryContactPersonName) {
        this.primaryContactPersonName = primaryContactPersonName;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    private List<AccountCustomFieldRsDto> customFieldList;

    public String getCompanyIdNumber() {
        return companyIdNumber;
    }

    public String getCompanyIdType() {
        return companyIdType;
    }

    public void setCompanyIdType(String companyIdType) {
        this.companyIdType = companyIdType;
    }

    public void setCompanyIdNumber(String companyIdNumber) {
        this.companyIdNumber = companyIdNumber;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getContactPersonNumber() {
        return contactPersonNumber;
    }

    public void setContactPersonNumber(String contactPersonNumber) {
        this.contactPersonNumber = contactPersonNumber;
    }

    public String getContactPersonEmail() {
        return contactPersonEmail;
    }

    public void setContactPersonEmail(String contactPersonEmail) {
        this.contactPersonEmail = contactPersonEmail;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getThirdName() {
        return thirdName;
    }

    public void setThirdName(String thirdName) {
        this.thirdName = thirdName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(String attachmentId) {
        this.attachmentId = attachmentId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public String getAdditionalNumber() {
        return additionalNumber;
    }

    public void setAdditionalNumber(String additionalNumber) {
        this.additionalNumber = additionalNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public String getPrimaryContactPersonNumber() {
        return primaryContactPersonNumber;
    }

    public void setPrimaryContactPersonNumber(String primaryContactPersonNumber) {
        this.primaryContactPersonNumber = primaryContactPersonNumber;
    }

    public String getPrimaryContactPersonEmail() {
        return primaryContactPersonEmail;
    }

    public void setPrimaryContactPersonEmail(String primaryContactPersonEmail) {
        this.primaryContactPersonEmail = primaryContactPersonEmail;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    public List<AccountCustomFieldRsDto> getCustomFieldList() {
        return customFieldList;
    }

    public void setCustomFieldList(List<AccountCustomFieldRsDto> customFieldList) {
        this.customFieldList = customFieldList;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getIdAttachment() {
        return idAttachment;
    }

    public void setIdAttachment(String idAttachment) {
        this.idAttachment = idAttachment;
    }

    public String getOtherDistrict() {
        return otherDistrict;
    }

    public void setOtherDistrict(String otherDistrict) {
        this.otherDistrict = otherDistrict;
    }
}
