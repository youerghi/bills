package be.bslash.ibps.bills.dto.request;

import be.bslash.ibps.bills.dto.DtoValidation;
import be.bslash.ibps.bills.exceptions.HttpServiceException;
import be.bslash.ibps.bills.exceptions.ValidationMessage;
import be.bslash.ibps.bills.utils.Utils;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AdCampaignRqDto implements DtoValidation {

    private Integer id;

    private String campaignCode;

    private String name;

    private String startDate;

    private String endDate;

    private String userType;

    private String advCategory;

    private BigDecimal displayDuration;

    private List<AdvReqDto> adList;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getAdvCategory() {
        return advCategory;
    }

    public void setAdvCategory(String advCategory) {
        this.advCategory = advCategory;
    }

    public BigDecimal getDisplayDuration() {
        return displayDuration;
    }

    public void setDisplayDuration(BigDecimal displayDuration) {
        this.displayDuration = displayDuration;
    }

    public List<AdvReqDto> getAdList() {
        return adList;
    }

    public void setAdList(List<AdvReqDto> adList) {
        this.adList = adList;
    }


    @Override
    public void validate(Locale locale) {
        List<ValidationMessage> errorList = new ArrayList<>();

        if (Utils.isNullOrEmpty(this.campaignCode)) {
            ValidationMessage validationMessage = new ValidationMessage("campaignCode", "campaign_code_empty", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.name)) {
            ValidationMessage validationMessage = new ValidationMessage("name", "campaign_name_empty", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.startDate)) {
            ValidationMessage validationMessage = new ValidationMessage("start_date_empty", "startDate_empty", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.endDate)) {
            ValidationMessage validationMessage = new ValidationMessage("end_date_empty", "endDate_empty", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.userType)) {
            ValidationMessage validationMessage = new ValidationMessage("userType", "user_type_empty", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.advCategory)) {
            ValidationMessage validationMessage = new ValidationMessage("addCategory", "adv_catogery_empty", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNull(this.displayDuration)) {
            ValidationMessage validationMessage = new ValidationMessage("displayDuration", "display_duration_empty", locale);
            errorList.add(validationMessage);
        }

        if (this.adList != null && this.adList.size() == 0) {
            ValidationMessage validationMessage = new ValidationMessage("adList", "adv_list_empty", locale);
            errorList.add(validationMessage);
        }


        if (!errorList.isEmpty()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request", errorList, locale);
        }
    }
}
