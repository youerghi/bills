package be.bslash.ibps.bills.dto.request;

import be.bslash.ibps.bills.dto.DtoValidation;
import be.bslash.ibps.bills.exceptions.HttpServiceException;
import be.bslash.ibps.bills.exceptions.ValidationMessage;
import be.bslash.ibps.bills.utils.Utils;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class BillCustomFieldRqDto implements DtoValidation {

    private Long fieldId;
    private String fieldValue;

    public Long getFieldId() {
        return fieldId;
    }

    public void setFieldId(Long fieldId) {
        this.fieldId = fieldId;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    @Override
    public void validate(Locale locale) {
        List<ValidationMessage> errorList = new ArrayList<>();

        if (Utils.isNullOrEmpty(this.fieldId)) {
            ValidationMessage validationMessage = new ValidationMessage("serviceName", "service_name_empty", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.fieldValue)) {
            ValidationMessage validationMessage = new ValidationMessage("billNumber", "bill_number_empty", locale);
            errorList.add(validationMessage);
        }

        if (!errorList.isEmpty()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request", errorList, locale);
        }
    }
}
