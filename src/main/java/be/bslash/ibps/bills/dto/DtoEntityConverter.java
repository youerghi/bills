package be.bslash.ibps.bills.dto;

import java.util.Locale;

public interface DtoEntityConverter {
    public Object toEntity(Locale locale);
}
