package be.bslash.ibps.bills.dto.response;


import be.bslash.ibps.bills.enums.BillStatus;

import java.time.LocalDateTime;

public class BillTimelineDtoRs {

    private Long id;
    private String companyUsername;
    private String companyUserFullName;
    private BillStatus billStatus;
    private String billStatusText;
    private LocalDateTime actionDate;
    private String userReference;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyUsername() {
        return companyUsername;
    }

    public void setCompanyUsername(String companyUsername) {
        this.companyUsername = companyUsername;
    }

    public String getCompanyUserFullName() {
        return companyUserFullName;
    }

    public void setCompanyUserFullName(String companyUserFullName) {
        this.companyUserFullName = companyUserFullName;
    }

    public BillStatus getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(BillStatus billStatus) {
        this.billStatus = billStatus;
    }

    public String getBillStatusText() {
        return billStatusText;
    }

    public void setBillStatusText(String billStatusText) {
        this.billStatusText = billStatusText;
    }

    public LocalDateTime getActionDate() {
        return actionDate;
    }

    public void setActionDate(LocalDateTime actionDate) {
        this.actionDate = actionDate;
    }

    public String getUserReference() {
        return userReference;
    }

    public void setUserReference(String userReference) {
        this.userReference = userReference;
    }
}
