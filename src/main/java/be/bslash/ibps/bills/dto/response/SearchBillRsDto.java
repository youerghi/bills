package be.bslash.ibps.bills.dto.response;

import be.bslash.ibps.bills.enums.BillSource;
import be.bslash.ibps.bills.enums.BillStatus;
import be.bslash.ibps.bills.enums.BillType;
import be.bslash.ibps.bills.enums.BooleanFlag;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class SearchBillRsDto {

    private Long id;
    private String billNumber;
    private String accountNumber;
    private LocalDateTime dueDate;
    private String sadadNumber;
    private String serviceName;
    private String serviceDescription;
    private LocalDateTime issueDate;
    private LocalDateTime expireDate;
    private LocalDateTime createDate;
    private BigDecimal subAmount;
    private BigDecimal vat;
    private BigDecimal discount;
    private String discountType;
    private BigDecimal totalAmount;
    private String customerFullName;
    private String customerMobileNumber;
    private String customerEmailAddress;
    private String customerIdNumber;
    private BillStatus billStatus;
    private String billStatusText;
    private BillType billType;
    private String billTypeText;
    private BillSource billSource;
    private String billSourceText;
    private Long activityId;
    private String activityName;
    private BigDecimal previousBalance;
    private String customerTaxNumber;
    private String customerAddress;
    private List<BillCustomFieldRsDto> customFieldList;
    private Integer campaignId;
    private BooleanFlag vatExemptedFlag;
    private BooleanFlag vatNaFlag;
    private String statusAr;
    private String statusEn;
    private String paymentFlag;

    private String isPartialAllowed;
    private String billCategory;
    private BigDecimal miniPartialAmount;
    private BigDecimal maxPartialAmount;
    private Integer paymentNumber;
    private Integer settlementNumber;

    private BigDecimal remainAmount;
    private BigDecimal currentPaidAmount;

    private String entityCode;
    private String entityName;
    private String activityCode;

    private String paymentRule;

    private String cycle;

    private Long accountId;


    private String referenceNumber;


    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public LocalDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getSadadNumber() {
        return sadadNumber;
    }

    public void setSadadNumber(String sadadNumber) {
        this.sadadNumber = sadadNumber;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public LocalDateTime getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(LocalDateTime issueDate) {
        this.issueDate = issueDate;
    }

    public LocalDateTime getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(LocalDateTime expireDate) {
        this.expireDate = expireDate;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public BigDecimal getSubAmount() {
        return subAmount;
    }

    public void setSubAmount(BigDecimal subAmount) {
        this.subAmount = subAmount;
    }

    public BigDecimal getVat() {
        return vat;
    }

    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getCustomerFullName() {
        return customerFullName;
    }

    public void setCustomerFullName(String customerFullName) {
        this.customerFullName = customerFullName;
    }

    public String getCustomerMobileNumber() {
        return customerMobileNumber;
    }

    public void setCustomerMobileNumber(String customerMobileNumber) {
        this.customerMobileNumber = customerMobileNumber;
    }

    public String getCustomerEmailAddress() {
        return customerEmailAddress;
    }

    public void setCustomerEmailAddress(String customerEmailAddress) {
        this.customerEmailAddress = customerEmailAddress;
    }

    public String getCustomerIdNumber() {
        return customerIdNumber;
    }

    public void setCustomerIdNumber(String customerIdNumber) {
        this.customerIdNumber = customerIdNumber;
    }

    public BillStatus getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(BillStatus billStatus) {
        this.billStatus = billStatus;
    }

    public String getBillStatusText() {
        return billStatusText;
    }

    public void setBillStatusText(String billStatusText) {
        this.billStatusText = billStatusText;
    }

    public BillType getBillType() {
        return billType;
    }

    public void setBillType(BillType billType) {
        this.billType = billType;
    }

    public String getBillTypeText() {
        return billTypeText;
    }

    public void setBillTypeText(String billTypeText) {
        this.billTypeText = billTypeText;
    }

    public BillSource getBillSource() {
        return billSource;
    }

    public void setBillSource(BillSource billSource) {
        this.billSource = billSource;
    }

    public String getBillSourceText() {
        return billSourceText;
    }

    public void setBillSourceText(String billSourceText) {
        this.billSourceText = billSourceText;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public BigDecimal getPreviousBalance() {
        return previousBalance;
    }

    public void setPreviousBalance(BigDecimal previousBalance) {
        this.previousBalance = previousBalance;
    }

    public String getCustomerTaxNumber() {
        return customerTaxNumber;
    }

    public void setCustomerTaxNumber(String customerTaxNumber) {
        this.customerTaxNumber = customerTaxNumber;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public List<BillCustomFieldRsDto> getCustomFieldList() {
        return customFieldList;
    }

    public void setCustomFieldList(List<BillCustomFieldRsDto> customFieldList) {
        this.customFieldList = customFieldList;
    }

    public Integer getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }

    public BooleanFlag getVatExemptedFlag() {
        return vatExemptedFlag;
    }

    public void setVatExemptedFlag(BooleanFlag vatExemptedFlag) {
        this.vatExemptedFlag = vatExemptedFlag;
    }

    public BooleanFlag getVatNaFlag() {
        return vatNaFlag;
    }

    public void setVatNaFlag(BooleanFlag vatNaFlag) {
        this.vatNaFlag = vatNaFlag;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getStatusAr() {
        return statusAr;
    }

    public void setStatusAr(String statusAr) {
        this.statusAr = statusAr;
    }

    public String getStatusEn() {
        return statusEn;
    }

    public void setStatusEn(String statusEn) {
        this.statusEn = statusEn;
    }


    public String getPaymentFlag() {
        return paymentFlag;
    }

    public void setPaymentFlag(String paymentFlag) {
        this.paymentFlag = paymentFlag;
    }

    public String getIsPartialAllowed() {
        return isPartialAllowed;
    }

    public void setIsPartialAllowed(String isPartialAllowed) {
        this.isPartialAllowed = isPartialAllowed;
    }

    public String getBillCategory() {
        return billCategory;
    }

    public void setBillCategory(String billCategory) {
        this.billCategory = billCategory;
    }

    public BigDecimal getMiniPartialAmount() {
        return miniPartialAmount;
    }

    public void setMiniPartialAmount(BigDecimal miniPartialAmount) {
        this.miniPartialAmount = miniPartialAmount;
    }

    public BigDecimal getMaxPartialAmount() {
        return maxPartialAmount;
    }

    public void setMaxPartialAmount(BigDecimal maxPartialAmount) {
        this.maxPartialAmount = maxPartialAmount;
    }

    public Integer getPaymentNumber() {
        return paymentNumber;
    }

    public void setPaymentNumber(Integer paymentNumber) {
        this.paymentNumber = paymentNumber;
    }

    public Integer getSettlementNumber() {
        return settlementNumber;
    }

    public void setSettlementNumber(Integer settlementNumber) {
        this.settlementNumber = settlementNumber;
    }

    public BigDecimal getRemainAmount() {
        return remainAmount;
    }

    public void setRemainAmount(BigDecimal remainAmount) {
        this.remainAmount = remainAmount;
    }

    public BigDecimal getCurrentPaidAmount() {
        return currentPaidAmount;
    }

    public void setCurrentPaidAmount(BigDecimal currentPaidAmount) {
        this.currentPaidAmount = currentPaidAmount;
    }

    public String getEntityCode() {
        return entityCode;
    }

    public void setEntityCode(String entityCode) {
        this.entityCode = entityCode;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public String getPaymentRule() {
        return paymentRule;
    }

    public void setPaymentRule(String paymentRule) {
        this.paymentRule = paymentRule;
    }

    public String getCycle() {
        return cycle;
    }

    public void setCycle(String cycle) {
        this.cycle = cycle;
    }
}
