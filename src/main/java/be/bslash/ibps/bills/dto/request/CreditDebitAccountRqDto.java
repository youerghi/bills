package be.bslash.ibps.bills.dto.request;

import be.bslash.ibps.bills.dto.DtoValidation;
import be.bslash.ibps.bills.exceptions.HttpServiceException;
import be.bslash.ibps.bills.exceptions.ValidationMessage;
import be.bslash.ibps.bills.utils.Utils;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CreditDebitAccountRqDto implements DtoValidation {

    private String accountNumber;
    private BigDecimal amount;
    private String type;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void validate(Locale locale) {
        List<ValidationMessage> errorList = new ArrayList<>();

        if (Utils.isNullOrEmpty(this.accountNumber)) {
            ValidationMessage validationMessage = new ValidationMessage("accountNumber", "account number required", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.type)) {
            ValidationMessage validationMessage = new ValidationMessage("type", "type required", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.amount)) {
            ValidationMessage validationMessage = new ValidationMessage("amount", "amount required", locale);
            errorList.add(validationMessage);
        }


        if (!errorList.isEmpty()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request", errorList, locale);
        }
    }
}
