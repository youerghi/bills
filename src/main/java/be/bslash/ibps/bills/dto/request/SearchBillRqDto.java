package be.bslash.ibps.bills.dto.request;

import java.math.BigDecimal;
import java.util.List;

public class SearchBillRqDto {

    private String entityCode;
    private String activityCode;
    private String billNumber;
    private String sadadNumber;
    private String serviceName;
    private String issueDateFrom;
    private String issueDateTo;
    private String customerFullName;
    private String customerMobileNumber;
    private String customerEmailAddress;
    private String customerIdNumber;
    private List<String> billStatusList;
    private String billType;
    private Integer page;
    private Integer size;
    private BigDecimal totalAmount;

    public String getEntityCode() {
        return entityCode;
    }

    public void setEntityCode(String entityCode) {
        this.entityCode = entityCode;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getSadadNumber() {
        return sadadNumber;
    }

    public void setSadadNumber(String sadadNumber) {
        this.sadadNumber = sadadNumber;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getIssueDateFrom() {
        return issueDateFrom;
    }

    public void setIssueDateFrom(String issueDateFrom) {
        this.issueDateFrom = issueDateFrom;
    }

    public String getIssueDateTo() {
        return issueDateTo;
    }

    public void setIssueDateTo(String issueDateTo) {
        this.issueDateTo = issueDateTo;
    }

    public String getCustomerFullName() {
        return customerFullName;
    }

    public void setCustomerFullName(String customerFullName) {
        this.customerFullName = customerFullName;
    }

    public String getCustomerMobileNumber() {
        return customerMobileNumber;
    }

    public void setCustomerMobileNumber(String customerMobileNumber) {
        this.customerMobileNumber = customerMobileNumber;
    }

    public String getCustomerEmailAddress() {
        return customerEmailAddress;
    }

    public void setCustomerEmailAddress(String customerEmailAddress) {
        this.customerEmailAddress = customerEmailAddress;
    }

    public String getCustomerIdNumber() {
        return customerIdNumber;
    }

    public void setCustomerIdNumber(String customerIdNumber) {
        this.customerIdNumber = customerIdNumber;
    }

    public List<String> getBillStatusList() {
        return billStatusList;
    }

    public void setBillStatusList(List<String> billStatusList) {
        this.billStatusList = billStatusList;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }
}
