package be.bslash.ibps.bills.dto.response;


import be.bslash.ibps.bills.enums.Status;

public class EntityActivityDtoRs {

    private Long id;
    private String activityName;
    private String activityNameAr;
    private String activityNameEn;
    private Status status;

    private String iban;
    private Long bankId;
    private Long cityId;

    private String code;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getActivityNameAr() {
        return activityNameAr;
    }

    public void setActivityNameAr(String activityNameAr) {
        this.activityNameAr = activityNameAr;
    }

    public String getActivityNameEn() {
        return activityNameEn;
    }

    public void setActivityNameEn(String activityNameEn) {
        this.activityNameEn = activityNameEn;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public Long getBankId() {
        return bankId;
    }

    public void setBankId(Long bankId) {
        this.bankId = bankId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
