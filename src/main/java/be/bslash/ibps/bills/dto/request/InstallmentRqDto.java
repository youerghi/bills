package be.bslash.ibps.bills.dto.request;

import java.math.BigDecimal;

public class InstallmentRqDto {

    private String description;
    private String dueDateInstallment;
    private BigDecimal installment;
    private String invoiceNumber;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDueDateInstallment() {
        return dueDateInstallment;
    }

    public void setDueDateInstallment(String dueDateInstallment) {
        this.dueDateInstallment = dueDateInstallment;
    }

    public BigDecimal getInstallment() {
        return installment;
    }

    public void setInstallment(BigDecimal installment) {
        this.installment = installment;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }
}
