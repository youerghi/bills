package be.bslash.ibps.bills.dto.response;

import java.math.BigDecimal;

public class TemplateDtoRs {

    private Long templateId;

    private String templateName;

    private String dueDate;

    private String duration;

    private Integer frequency;

    private Integer installmentNumber;

    private BigDecimal installmentAmount;

    private String notification;

    private Integer notificationDays;

    private String isPartialAllowed;

    private String isAdvancedAllowed;

    private String isOverPaymentAllowed;

    private BigDecimal miniPartialAmount;

    private BigDecimal maxAdvancedPayment;

    private BigDecimal maxOverPayment;


    public Long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Long templateId) {
        this.templateId = templateId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public Integer getInstallmentNumber() {
        return installmentNumber;
    }

    public void setInstallmentNumber(Integer installmentNumber) {
        this.installmentNumber = installmentNumber;
    }

    public BigDecimal getInstallmentAmount() {
        return installmentAmount;
    }

    public void setInstallmentAmount(BigDecimal installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public Integer getNotificationDays() {
        return notificationDays;
    }

    public void setNotificationDays(Integer notificationDays) {
        this.notificationDays = notificationDays;
    }

    public String getIsPartialAllowed() {
        return isPartialAllowed;
    }

    public void setIsPartialAllowed(String isPartialAllowed) {
        this.isPartialAllowed = isPartialAllowed;
    }

    public String getIsAdvancedAllowed() {
        return isAdvancedAllowed;
    }

    public void setIsAdvancedAllowed(String isAdvancedAllowed) {
        this.isAdvancedAllowed = isAdvancedAllowed;
    }

    public String getIsOverPaymentAllowed() {
        return isOverPaymentAllowed;
    }

    public void setIsOverPaymentAllowed(String isOverPaymentAllowed) {
        this.isOverPaymentAllowed = isOverPaymentAllowed;
    }

    public BigDecimal getMiniPartialAmount() {
        return miniPartialAmount;
    }

    public void setMiniPartialAmount(BigDecimal miniPartialAmount) {
        this.miniPartialAmount = miniPartialAmount;
    }

    public BigDecimal getMaxAdvancedPayment() {
        return maxAdvancedPayment;
    }

    public void setMaxAdvancedPayment(BigDecimal maxAdvancedPayment) {
        this.maxAdvancedPayment = maxAdvancedPayment;
    }

    public BigDecimal getMaxOverPayment() {
        return maxOverPayment;
    }

    public void setMaxOverPayment(BigDecimal maxOverPayment) {
        this.maxOverPayment = maxOverPayment;
    }
}
