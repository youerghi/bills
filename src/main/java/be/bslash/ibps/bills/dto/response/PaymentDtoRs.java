package be.bslash.ibps.bills.dto.response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class PaymentDtoRs implements Comparable {

    static Logger LOGGER = LoggerFactory.getLogger(PaymentDtoRs.class);

    private String billNumber;
    private String sadadNumber;
    private String activityCode;
    private String customerName;
    private String sadadPaymentId;
    private String paymentDate;
    private String paymentType;
    private String paymentMethod;
    private BigDecimal billAmount;
    private BigDecimal paymentAmount;
    private BigDecimal remainingAmount;
    private String paymentStatus;
    private String userReference;
    private String paymentImage;
    private String totalAmountAr;
    private String totalAmountEn;
    private String paymentAmountAr;
    private String paymentAmountEn;
    private String voucherNumber;
    private Integer paymentNumber;
    private LocalDateTime voucherDate;
    private String qr;
    private String customerIdNumber;
    private String customerIdType;
    private String customerMobileNumber;
    private String customerEmailAddress;

    public static Logger getLOGGER() {
        return LOGGER;
    }

    public static void setLOGGER(Logger LOGGER) {
        PaymentDtoRs.LOGGER = LOGGER;
    }

    public String getCustomerIdNumber() {
        return customerIdNumber;
    }

    public void setCustomerIdNumber(String customerIdNumber) {
        this.customerIdNumber = customerIdNumber;
    }

    public String getCustomerIdType() {
        return customerIdType;
    }

    public void setCustomerIdType(String customerIdType) {
        this.customerIdType = customerIdType;
    }

    public String getCustomerMobileNumber() {
        return customerMobileNumber;
    }

    public void setCustomerMobileNumber(String customerMobileNumber) {
        this.customerMobileNumber = customerMobileNumber;
    }

    public String getCustomerEmailAddress() {
        return customerEmailAddress;
    }

    public void setCustomerEmailAddress(String customerEmailAddress) {
        this.customerEmailAddress = customerEmailAddress;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getSadadNumber() {
        return sadadNumber;
    }

    public void setSadadNumber(String sadadNumber) {
        this.sadadNumber = sadadNumber;
    }

    public String getSadadPaymentId() {
        return sadadPaymentId;
    }

    public void setSadadPaymentId(String sadadPaymentId) {
        this.sadadPaymentId = sadadPaymentId;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public BigDecimal getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(BigDecimal remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getUserReference() {
        return userReference;
    }

    public void setUserReference(String userReference) {
        this.userReference = userReference;
    }

    public String getPaymentImage() {
        return paymentImage;
    }

    public void setPaymentImage(String paymentImage) {
        this.paymentImage = paymentImage;
    }

    public String getTotalAmountAr() {
        return totalAmountAr;
    }

    public void setTotalAmountAr(String totalAmountAr) {
        this.totalAmountAr = totalAmountAr;
    }

    public String getTotalAmountEn() {
        return totalAmountEn;
    }

    public void setTotalAmountEn(String totalAmountEn) {
        this.totalAmountEn = totalAmountEn;
    }

    public String getPaymentAmountAr() {
        return paymentAmountAr;
    }

    public void setPaymentAmountAr(String paymentAmountAr) {
        this.paymentAmountAr = paymentAmountAr;
    }

    public String getPaymentAmountEn() {
        return paymentAmountEn;
    }

    public void setPaymentAmountEn(String paymentAmountEn) {
        this.paymentAmountEn = paymentAmountEn;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public BigDecimal getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(BigDecimal billAmount) {
        this.billAmount = billAmount;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public String getVoucherNumber() {
        if (voucherNumber.length() < 5) {
            switch (voucherNumber.length()) {
                case 1:
                    voucherNumber = "0000" + voucherNumber;
                    break;
                case 2:
                    voucherNumber = "000" + voucherNumber;
                    break;
                case 3:
                    voucherNumber = "00" + voucherNumber;
                    break;
                case 4:
                    voucherNumber = "0" + voucherNumber;
                    break;
            }
        }
        return voucherNumber;
    }

    public void setVoucherNumber(String voucherNumber) {
        this.voucherNumber = voucherNumber;
    }

    public Integer getPaymentNumber() {
        return paymentNumber;
    }

    public void setPaymentNumber(Integer paymentNumber) {
        this.paymentNumber = paymentNumber;
    }

    public LocalDateTime getVoucherDate() {
        return voucherDate;
    }

    public void setVoucherDate(LocalDateTime voucherDate) {
        this.voucherDate = voucherDate;
    }

    public String getQr() {
        return qr;
    }

    public void setQr(String qr) {
        this.qr = qr;
    }

    @Override
    public int compareTo(Object o) {
        int compareage = ((PaymentDtoRs) o).getPaymentNumber();
        return this.paymentNumber - compareage;
    }
}
