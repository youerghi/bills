package be.bslash.ibps.bills.dto.response;

import be.bslash.ibps.bills.enums.BooleanFlag;
import be.bslash.ibps.bills.enums.FieldType;
import be.bslash.ibps.bills.enums.Status;
import be.bslash.ibps.bills.enums.ValueType;


public class EntityFieldDtoRs {

    private Long id;
    private Integer minLength;
    private Integer maxLength;
    private BooleanFlag isRequired;
    private FieldType fieldType;
    private ValueType valueType;
    private Status status;
    private String fieldNameEn;
    private String fieldNameAr;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMinLength() {
        return minLength;
    }

    public void setMinLength(Integer minLength) {
        this.minLength = minLength;
    }

    public Integer getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(Integer maxLength) {
        this.maxLength = maxLength;
    }

    public BooleanFlag getIsRequired() {
        return isRequired;
    }

    public void setIsRequired(BooleanFlag isRequired) {
        this.isRequired = isRequired;
    }

    public FieldType getFieldType() {
        return fieldType;
    }

    public void setFieldType(FieldType fieldType) {
        this.fieldType = fieldType;
    }

    public ValueType getValueType() {
        return valueType;
    }

    public void setValueType(ValueType valueType) {
        this.valueType = valueType;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getFieldNameEn() {
        return fieldNameEn;
    }

    public void setFieldNameEn(String fieldNameEn) {
        this.fieldNameEn = fieldNameEn;
    }

    public String getFieldNameAr() {
        return fieldNameAr;
    }

    public void setFieldNameAr(String fieldNameAr) {
        this.fieldNameAr = fieldNameAr;
    }
}
