package be.bslash.ibps.bills.dto.request;

import be.bslash.ibps.bills.dto.DtoEntityConverter;
import be.bslash.ibps.bills.dto.DtoValidation;
import be.bslash.ibps.bills.entities.Bill;
import be.bslash.ibps.bills.enums.BooleanFlag;
import be.bslash.ibps.bills.enums.IdType;
import be.bslash.ibps.bills.exceptions.HttpServiceException;
import be.bslash.ibps.bills.exceptions.ValidationMessage;
import be.bslash.ibps.bills.exceptions.ValidationMessageLine;
import be.bslash.ibps.bills.utils.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DetailedBillMakeRqDto implements DtoValidation, DtoEntityConverter {

    private static final Logger LOGGER = LogManager.getLogger("BACKEND_LOGS");

    private String billNumber;
    private String serviceName;
    private String serviceDescription;
    private String issueDate;
    private String expireDate;
    private BigDecimal subAmount;
    private BigDecimal discount;
    private String discountType;
    private String vat;
    private String customerFullName;
    private String customerMobileNumber;
    private String customerEmailAddress;
    private String customerIdNumber;
    private List<BillItemDetailedRqDto> billItemList;
    private Long entityActivityId;
    private String entityActivityCode;
    private String customerTaxNumber;
    private String customerAddress;
    private BigDecimal customerPreviousBalance = BigDecimal.ZERO;
    private List<BillCustomFieldRqDto> customFieldList;

    private String customerIdType;
    private String saveCustomer;
    private Integer campaignId;

    private List<String> customerEmailList;
    private List<String> customerMobileList;

    private String isPartialAllowed;
    private BigDecimal miniPartialAmount;
    private BigDecimal maxPartialAmount;

    private String streetName;
    private String buildingNumber;
    private Long cityId;
    private Long districtId;
    private String otherDistrict;
    private String additionalNumber;
    private String postalCode;


    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    public String getOtherDistrict() {
        return otherDistrict;
    }

    public void setOtherDistrict(String otherDistrict) {
        this.otherDistrict = otherDistrict;
    }

    public String getAdditionalNumber() {
        return additionalNumber;
    }

    public void setAdditionalNumber(String additionalNumber) {
        this.additionalNumber = additionalNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public BigDecimal getSubAmount() {
        return subAmount;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getCustomerFullName() {
        return customerFullName;
    }

    public void setCustomerFullName(String customerFullName) {
        this.customerFullName = customerFullName;
    }

    public String getCustomerMobileNumber() {
        return customerMobileNumber;
    }

    public void setCustomerMobileNumber(String customerMobileNumber) {
        this.customerMobileNumber = customerMobileNumber;
    }

    public String getCustomerEmailAddress() {
        return customerEmailAddress;
    }

    public void setCustomerEmailAddress(String customerEmailAddress) {
        this.customerEmailAddress = customerEmailAddress;
    }

    public String getCustomerIdNumber() {
        return customerIdNumber;
    }

    public void setCustomerIdNumber(String customerIdNumber) {
        this.customerIdNumber = customerIdNumber;
    }

    public List<BillItemDetailedRqDto> getBillItemList() {
        return billItemList;
    }

    public void setBillItemList(List<BillItemDetailedRqDto> billItemList) {
        this.billItemList = billItemList;
    }

    public Long getEntityActivityId() {
        return entityActivityId;
    }

    public void setEntityActivityId(Long entityActivityId) {
        this.entityActivityId = entityActivityId;
    }

    public List<BillCustomFieldRqDto> getCustomFieldList() {
        return customFieldList;
    }

    public void setCustomFieldList(List<BillCustomFieldRqDto> customFieldList) {
        this.customFieldList = customFieldList;
    }

    public void setSubAmount(BigDecimal subAmount) {
        this.subAmount = subAmount;
    }

    public String getCustomerTaxNumber() {
        return customerTaxNumber;
    }

    public void setCustomerTaxNumber(String customerTaxNumber) {
        this.customerTaxNumber = customerTaxNumber;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public BigDecimal getCustomerPreviousBalance() {
        return customerPreviousBalance;
    }

    public void setCustomerPreviousBalance(BigDecimal customerPreviousBalance) {
        this.customerPreviousBalance = customerPreviousBalance;
    }


    public String getCustomerIdType() {
        return customerIdType;
    }

    public void setCustomerIdType(String customerIdType) {
        this.customerIdType = customerIdType;
    }

    public String getSaveCustomer() {
        return saveCustomer;
    }

    public void setSaveCustomer(String saveCustomer) {
        this.saveCustomer = saveCustomer;
    }

    public Integer getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public List<String> getCustomerEmailList() {
        return customerEmailList;
    }

    public void setCustomerEmailList(List<String> customerEmailList) {
        this.customerEmailList = customerEmailList;
    }

    public List<String> getCustomerMobileList() {
        return customerMobileList;
    }

    public void setCustomerMobileList(List<String> customerMobileList) {
        this.customerMobileList = customerMobileList;
    }

    public String getIsPartialAllowed() {
        return isPartialAllowed;
    }

    public void setIsPartialAllowed(String isPartialAllowed) {
        this.isPartialAllowed = isPartialAllowed;
    }

    public BigDecimal getMiniPartialAmount() {
        return miniPartialAmount;
    }

    public void setMiniPartialAmount(BigDecimal miniPartialAmount) {
        this.miniPartialAmount = miniPartialAmount;
    }

    public BigDecimal getMaxPartialAmount() {
        return maxPartialAmount;
    }

    public void setMaxPartialAmount(BigDecimal maxPartialAmount) {
        this.maxPartialAmount = maxPartialAmount;
    }

    public String getEntityActivityCode() {
        return entityActivityCode;
    }

    public void setEntityActivityCode(String entityActivityCode) {
        this.entityActivityCode = entityActivityCode;
    }

    @Override
    public void validate(Locale locale) {
        List<ValidationMessage> errorList = new ArrayList<>();

        if (this.billNumber != null && this.billNumber.length() > 20) {
            ValidationMessage validationMessage = new ValidationMessage("billNumber", "bill_number_length", locale);
            errorList.add(validationMessage);
        }

        if (this.billNumber != null && this.billNumber.length() > 20) {
            ValidationMessage validationMessage = new ValidationMessage("billNumber", "bill_number_length", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.issueDate)) {
            ValidationMessage validationMessage = new ValidationMessage("issueDate", "issue_date_required", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.expireDate)) {
            ValidationMessage validationMessage = new ValidationMessage("expireDate", "expire_date_required", locale);
            errorList.add(validationMessage);
        }

        LocalDateTime issueDateTime = Utils.parseDateFromString(this.issueDate, "yyyy-MM-dd");
        if (!Utils.isNullOrEmpty(this.issueDate) && issueDateTime == null) {
            ValidationMessage validationMessage = new ValidationMessage("issueDate", "issue_date_invalid", locale);
            errorList.add(validationMessage);
        }

        LocalDateTime expireDateTime = Utils.parseDateFromString(this.expireDate, "yyyy-MM-dd");
        if (!Utils.isNullOrEmpty(this.expireDate) && expireDateTime == null) {
            ValidationMessage validationMessage = new ValidationMessage("expireDate", "expire_date_invalid", locale);
            errorList.add(validationMessage);
        }

        if (expireDateTime != null && expireDateTime.isBefore(LocalDate.now().atStartOfDay().plusDays(1))) {
            ValidationMessage validationMessage = new ValidationMessage("expireDate", "Expire date should not be less than the issue date.", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.serviceName)) {
            ValidationMessage validationMessage = new ValidationMessage("serviceName", "service_name_empty", locale);
            errorList.add(validationMessage);
        }


        if (this.vat == null) {
            ValidationMessage validationMessage = new ValidationMessage("vat", "vat_empty", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.customerFullName)) {
            ValidationMessage validationMessage = new ValidationMessage("customerFullName", "customer_full_name_empty", locale);
            errorList.add(validationMessage);
        }

        if (!Utils.isValidSize(this.customerFullName, 1, 255)) {
            ValidationMessage validationMessage = new ValidationMessage("customerFullName", "Customer full name length should not exceed 255", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.customerMobileNumber)) {
            ValidationMessage validationMessage = new ValidationMessage("customerMobileNumber", "customer_mobile_number_empty", locale);
            errorList.add(validationMessage);
        }

        if (!Utils.isNullOrEmpty(this.customerMobileNumber) && !Utils.isValidMobileNumber(this.customerMobileNumber)) {
            ValidationMessage validationMessage = new ValidationMessage("customerMobileNumber", "customer_mobile_number_invalid", locale);
            errorList.add(validationMessage);
        }


        if (!Utils.isNullOrEmpty(this.customerEmailAddress) && !Utils.isValidEmail(this.customerEmailAddress)) {
            ValidationMessage validationMessage = new ValidationMessage("customerEmailAddress", "customer_email_not_valid", locale);
            errorList.add(validationMessage);
        }


        if (Utils.isNullOrEmpty(this.billItemList)) {
            ValidationMessage validationMessage = new ValidationMessage("billItemList", "bill_item_list_empty", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.entityActivityId) && Utils.isNullOrEmpty(this.entityActivityCode)) {
            ValidationMessage validationMessage = new ValidationMessage("entityActivityId", "entity_activity_id_empty", locale);
            errorList.add(validationMessage);
        }

        if (this.discount == null) {
            this.discount = new BigDecimal(0);
        }

        if (this.discount.doubleValue() < 0) {
            ValidationMessage validationMessage = new ValidationMessage("discount", "discount_amount_invalid", locale);
            errorList.add(validationMessage);
        }

        if (this.customerAddress != null && this.customerAddress.trim().length() > 255) {
            ValidationMessage validationMessage = new ValidationMessage("customerAddress", "customer_address_length_255", locale);
            errorList.add(validationMessage);
        }

        if (this.customerEmailAddress != null && this.customerEmailAddress.trim().length() > 255) {
            ValidationMessage validationMessage = new ValidationMessage("customerEmailAddress", "email_length_255", locale);
            errorList.add(validationMessage);
        }

        if (this.serviceDescription != null && this.serviceDescription.trim().length() > 255) {
            ValidationMessage validationMessage = new ValidationMessage("serviceDescription", "description_length_255", locale);
            errorList.add(validationMessage);
        }

        if (this.serviceName != null && this.serviceName.trim().length() > 255) {
            ValidationMessage validationMessage = new ValidationMessage("serviceName", "service_name_length_255", locale);
            errorList.add(validationMessage);
        }

//        if ((this.customerIdType != null) && !(this.customerIdType.isEmpty()) && !(Utils.isEnumValueExist(this.customerIdType,IdType.class))) {
//            ValidationMessage validationMessage = new ValidationMessage("customerIdType", "customer_id_type_not_valid", locale);
//            errorList.add(validationMessage);
//        }

        if (!Utils.isIdNumberValid(this.customerIdNumber, IdType.getValue(this.customerIdType))) {
            ValidationMessage validationMessage = new ValidationMessage("customerIdNumber", "customer_id_number_not_valid", locale);
            errorList.add(validationMessage);
        }

        if (!Utils.isValidTaxNumber(this.customerTaxNumber)) {
            ValidationMessage validationMessage = new ValidationMessage("customerTaxNumber", "customer_tax_number_not_valid", locale);
            errorList.add(validationMessage);
        }


        Integer lineNumber = 0;

        BigDecimal subAmount = new BigDecimal("0");

        this.discount = new BigDecimal("0");

        for (BillItemDetailedRqDto billItemDetailedRqDto : this.billItemList) {
            if (Utils.isNullOrEmpty(billItemDetailedRqDto.getName())) {
                ValidationMessage validationMessage = new ValidationMessageLine("name", "item_name_empty", lineNumber, locale);
                errorList.add(validationMessage);
            }

            if (billItemDetailedRqDto.getUnitPrice() == null || billItemDetailedRqDto.getUnitPrice().doubleValue() < 0) {
                ValidationMessage validationMessage = new ValidationMessageLine("unitPrice", "unit_price_invalid", lineNumber, locale);
                errorList.add(validationMessage);
            }

            if (billItemDetailedRqDto.getQuantity() == null || billItemDetailedRqDto.getQuantity().compareTo(BigDecimal.ZERO) == 0) {
                ValidationMessage validationMessage = new ValidationMessageLine("quantity", "invalid_quantity", lineNumber, locale);
                errorList.add(validationMessage);
            }

            if (billItemDetailedRqDto.getVat() == null || billItemDetailedRqDto.getVat().isEmpty()) {
                billItemDetailedRqDto.setVat("0");
            }

            BigDecimal totalItemPrice = billItemDetailedRqDto.getUnitPrice().multiply(billItemDetailedRqDto.getQuantity());

            LOGGER.info("Item1: " + totalItemPrice);

            String discountTypePerItem = billItemDetailedRqDto.getDiscountType();

            BigDecimal discount = billItemDetailedRqDto.getDiscount();

            if (discount == null) {
                discount = BigDecimal.ZERO;
            }

            if (discountTypePerItem == null || discountTypePerItem.isEmpty()) {
                discountTypePerItem = "PERC";
            }

            BigDecimal discountAmount = null;
            switch (discountTypePerItem) {
                case "PERC":
                    discountAmount = totalItemPrice.multiply(discount.divide(new BigDecimal(100)));
                    break;
                case "FIXED":
                    discountAmount = new BigDecimal(discount.toString());
                    break;
                default:
                    discountTypePerItem = "PERC";
                    discountAmount = totalItemPrice.multiply(discount);
                    ValidationMessage validationMessage = new ValidationMessageLine("discountType", "Discount type is not valid", lineNumber, locale);
                    errorList.add(validationMessage);
                    break;
            }

            LOGGER.info("Item1 discount: " + discountAmount);

            this.discount = this.discount.add(discountAmount);

            totalItemPrice = totalItemPrice.subtract(discountAmount);

            LOGGER.info("Item1 amount after discount: " + totalItemPrice);

            BigDecimal vatPerItem = null;

            switch (billItemDetailedRqDto.getVat()) {
                case "EXE":
                    vatPerItem = new BigDecimal("0");
                    break;
                case "NA":
                    vatPerItem = new BigDecimal("0");
                    break;
                default:
                    vatPerItem = new BigDecimal(billItemDetailedRqDto.getVat());
            }

            LOGGER.info("Item1 vat: " + vatPerItem);

            BigDecimal vatPerItemValue = totalItemPrice.multiply(vatPerItem);

            LOGGER.info("Item1 vat value: " + vatPerItemValue);

            totalItemPrice = totalItemPrice.add(vatPerItemValue);

            LOGGER.info("Item1 amount after vat: " + totalItemPrice);

            subAmount = subAmount.add(totalItemPrice);

            LOGGER.info("subAmount: " + subAmount);

            lineNumber++;
        }

        this.subAmount = subAmount;

        if (!errorList.isEmpty()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request", errorList, locale);
        }
    }

    @Override
    public Object toEntity(Locale locale) {

        Bill bill = new Bill();
        bill.setBillNumber(this.billNumber);
        bill.setServiceName(this.serviceName);
        bill.setCustomerMobileNumber(Utils.formatMobileNumber(customerMobileNumber));
        bill.setCustomerEmailAddress(this.customerEmailAddress);
        bill.setCustomerFullName(this.customerFullName);
        bill.setIssueDate(Utils.parseDateFromString(this.issueDate, "yyyy-MM-dd"));
        bill.setExpireDate(Utils.parseDateFromString(this.expireDate, "yyyy-MM-dd"));
        bill.setServiceDescription(this.serviceDescription);
        bill.setCustomerIdNumber(this.customerIdNumber);
        bill.setSubAmount(this.subAmount);
        bill.setDiscount(this.discount);
        bill.setCustomerAddress(this.customerAddress);
        bill.setCustomerTaxNumber(this.customerTaxNumber);
        bill.setCustomerPreviousBalance(this.customerPreviousBalance);
        bill.setCustomerIdType(IdType.getValue(this.customerIdType));
        bill.setIsPartialAllowed(BooleanFlag.getValue(isPartialAllowed));

        if (this.discountType == null || this.discountType.isEmpty()) {
            this.discountType = "PERC";
        }

        bill.setDiscountType(this.discountType);

        if (this.vat == null) {
            this.vat = "0";
        }

        switch (this.vat) {
            case "EXE":
                bill.setVat(new BigDecimal("0"));
                bill.setVatExemptedFlag(BooleanFlag.YES);
                bill.setVatNaFlag(BooleanFlag.NO);
                break;
            case "NA":
                bill.setVat(new BigDecimal("0"));
                bill.setVatExemptedFlag(BooleanFlag.NO);
                bill.setVatNaFlag(BooleanFlag.YES);
                break;
            default:
                bill.setVat(new BigDecimal(this.vat));
                bill.setVatExemptedFlag(BooleanFlag.NO);
                bill.setVatNaFlag(BooleanFlag.NO);
        }

        return bill;
    }
}
