package be.bslash.ibps.bills.dto.response;


import be.bslash.ibps.bills.entities.LocalPayment;
import be.bslash.ibps.bills.entities.SadadPayment;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class BillResultTimelineDtoRs {

    private String billNumber;
    private String sadadNumber;
    private String serviceName;
    private String serviceDescription;
    private LocalDateTime issueDate;
    private LocalDateTime expireDate;
    private BigDecimal subAmount;
    private BigDecimal vat;
    private BigDecimal discount;
    private BigDecimal totalAmount;
    private String customerFullName;
    private String customerMobileNumber;
    private String customerEmailAddress;
    private String customerIdNumber;
    private List<BillTimelineDtoRs> billTimeline;
    private String statusAr;
    private String statusEn;
    private String paymentFlag;
    private String paymentMethod;
    private LocalDateTime paymentDate;
    private String image;
    private String isPartialAllowed;
    private String billCategory;
    private BigDecimal miniPartialAmount;
    private BigDecimal maxPartialAmount;
    private Integer paymentNumber;
    private Integer settlementNumber;
    private BigDecimal remainAmount;
    private BigDecimal currentPaidAmount;
    private List<SadadPayment> onlinePaymentList;
    private List<LocalPayment> offlinePaymentList;

    private List<PaymentDtoRs> paymentList;
    private BigDecimal totalOfflinePaidAmount;
    private BigDecimal totalOnlinePaidAmount;

    private String invoiceLogo;
    private String invoiceStamp;

    private List<BillNoteDtoRs> billNoteList;

    private String account;
    private LocalDateTime dueDate;
    private String contractNumber;
    private String referenceNumber;
    private String billTypeText;

    public String getBillTypeText() {
        return billTypeText;
    }

    public void setBillTypeText(String billTypeText) {
        this.billTypeText = billTypeText;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public LocalDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getSadadNumber() {
        return sadadNumber;
    }

    public void setSadadNumber(String sadadNumber) {
        this.sadadNumber = sadadNumber;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public LocalDateTime getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(LocalDateTime issueDate) {
        this.issueDate = issueDate;
    }

    public LocalDateTime getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(LocalDateTime expireDate) {
        this.expireDate = expireDate;
    }

    public BigDecimal getSubAmount() {
        return subAmount;
    }

    public void setSubAmount(BigDecimal subAmount) {
        this.subAmount = subAmount;
    }

    public BigDecimal getVat() {
        return vat;
    }

    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getCustomerFullName() {
        return customerFullName;
    }

    public void setCustomerFullName(String customerFullName) {
        this.customerFullName = customerFullName;
    }

    public String getCustomerMobileNumber() {
        return customerMobileNumber;
    }

    public void setCustomerMobileNumber(String customerMobileNumber) {
        this.customerMobileNumber = customerMobileNumber;
    }

    public String getCustomerEmailAddress() {
        return customerEmailAddress;
    }

    public void setCustomerEmailAddress(String customerEmailAddress) {
        this.customerEmailAddress = customerEmailAddress;
    }

    public String getCustomerIdNumber() {
        return customerIdNumber;
    }

    public void setCustomerIdNumber(String customerIdNumber) {
        this.customerIdNumber = customerIdNumber;
    }

    public List<BillTimelineDtoRs> getBillTimeline() {
        return billTimeline;
    }

    public void setBillTimeline(List<BillTimelineDtoRs> billTimeline) {
        this.billTimeline = billTimeline;
    }

    public String getStatusAr() {
        return statusAr;
    }

    public void setStatusAr(String statusAr) {
        this.statusAr = statusAr;
    }

    public String getStatusEn() {
        return statusEn;
    }

    public void setStatusEn(String statusEn) {
        this.statusEn = statusEn;
    }

    public String getPaymentFlag() {
        return paymentFlag;
    }

    public void setPaymentFlag(String paymentFlag) {
        this.paymentFlag = paymentFlag;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public LocalDateTime getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDateTime paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIsPartialAllowed() {
        return isPartialAllowed;
    }

    public void setIsPartialAllowed(String isPartialAllowed) {
        this.isPartialAllowed = isPartialAllowed;
    }

    public String getBillCategory() {
        return billCategory;
    }

    public void setBillCategory(String billCategory) {
        this.billCategory = billCategory;
    }

    public BigDecimal getMiniPartialAmount() {
        return miniPartialAmount;
    }

    public void setMiniPartialAmount(BigDecimal miniPartialAmount) {
        this.miniPartialAmount = miniPartialAmount;
    }

    public BigDecimal getMaxPartialAmount() {
        return maxPartialAmount;
    }

    public void setMaxPartialAmount(BigDecimal maxPartialAmount) {
        this.maxPartialAmount = maxPartialAmount;
    }

    public Integer getPaymentNumber() {
        return paymentNumber;
    }

    public void setPaymentNumber(Integer paymentNumber) {
        this.paymentNumber = paymentNumber;
    }

    public Integer getSettlementNumber() {
        return settlementNumber;
    }

    public void setSettlementNumber(Integer settlementNumber) {
        this.settlementNumber = settlementNumber;
    }

    public BigDecimal getRemainAmount() {
        return remainAmount;
    }

    public void setRemainAmount(BigDecimal remainAmount) {
        this.remainAmount = remainAmount;
    }

    public BigDecimal getCurrentPaidAmount() {
        return currentPaidAmount;
    }

    public void setCurrentPaidAmount(BigDecimal currentPaidAmount) {
        this.currentPaidAmount = currentPaidAmount;
    }

    public List<SadadPayment> getOnlinePaymentList() {
        return onlinePaymentList;
    }

    public void setOnlinePaymentList(List<SadadPayment> onlinePaymentList) {
        this.onlinePaymentList = onlinePaymentList;
    }

    public List<LocalPayment> getOfflinePaymentList() {
        return offlinePaymentList;
    }

    public void setOfflinePaymentList(List<LocalPayment> offlinePaymentList) {
        this.offlinePaymentList = offlinePaymentList;
    }

    public List<PaymentDtoRs> getPaymentList() {
        return paymentList;
    }

    public void setPaymentList(List<PaymentDtoRs> paymentList) {
        this.paymentList = paymentList;
    }

    public BigDecimal getTotalOfflinePaidAmount() {
        return totalOfflinePaidAmount;
    }

    public void setTotalOfflinePaidAmount(BigDecimal totalOfflinePaidAmount) {
        this.totalOfflinePaidAmount = totalOfflinePaidAmount;
    }

    public BigDecimal getTotalOnlinePaidAmount() {
        return totalOnlinePaidAmount;
    }

    public void setTotalOnlinePaidAmount(BigDecimal totalOnlinePaidAmount) {
        this.totalOnlinePaidAmount = totalOnlinePaidAmount;
    }

    public List<BillNoteDtoRs> getBillNoteList() {
        return billNoteList;
    }

    public void setBillNoteList(List<BillNoteDtoRs> billNoteList) {
        this.billNoteList = billNoteList;
    }

    public String getInvoiceLogo() {
        return invoiceLogo;
    }

    public void setInvoiceLogo(String invoiceLogo) {
        this.invoiceLogo = invoiceLogo;
    }

    public String getInvoiceStamp() {
        return invoiceStamp;
    }

    public void setInvoiceStamp(String invoiceStamp) {
        this.invoiceStamp = invoiceStamp;
    }
}
