package be.bslash.ibps.bills.dto.response;

import be.bslash.ibps.bills.enums.Status;

public class BillCustomFieldRsDto {

    private Long id;
    private Long fieldId;
    private String fieldValue;
    private String fieldNameAr;
    private String fieldNameEn;
    private Status status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getFieldId() {
        return fieldId;
    }

    public void setFieldId(Long fieldId) {
        this.fieldId = fieldId;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public String getFieldNameAr() {
        return fieldNameAr;
    }

    public void setFieldNameAr(String fieldNameAr) {
        this.fieldNameAr = fieldNameAr;
    }

    public String getFieldNameEn() {
        return fieldNameEn;
    }

    public void setFieldNameEn(String fieldNameEn) {
        this.fieldNameEn = fieldNameEn;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
