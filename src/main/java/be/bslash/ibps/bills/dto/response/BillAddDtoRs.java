package be.bslash.ibps.bills.dto.response;

public class BillAddDtoRs {

    private Long billId;

    public Long getBillId() {
        return billId;
    }

    public void setBillId(Long billId) {
        this.billId = billId;
    }
}
