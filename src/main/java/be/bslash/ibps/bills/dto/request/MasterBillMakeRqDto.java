package be.bslash.ibps.bills.dto.request;

import be.bslash.ibps.bills.dto.DtoEntityConverter;
import be.bslash.ibps.bills.dto.DtoValidation;
import be.bslash.ibps.bills.entities.Bill;
import be.bslash.ibps.bills.enums.BooleanFlag;
import be.bslash.ibps.bills.enums.IdType;
import be.bslash.ibps.bills.exceptions.HttpServiceException;
import be.bslash.ibps.bills.exceptions.ValidationMessage;
import be.bslash.ibps.bills.utils.Utils;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MasterBillMakeRqDto implements DtoValidation, DtoEntityConverter {

    private String billNumber;
    private String serviceName;
    private String serviceDescription;
    private String issueDate;
    private String expireDate;
    private BigDecimal subAmount;
    private String vat;
    private String customerFullName;
    private String customerMobileNumber;
    private String customerEmailAddress;
    private Long entityActivityId;
    private String entityActivityCode;
    private String customerTaxNumber;
    private String customerAddress;
    private BigDecimal customerPreviousBalance = BigDecimal.ZERO;
    private List<BillCustomFieldRqDto> customFieldList;

    private String customerIdType;
    private String customerIdNumber;
    private String saveCustomer;
    private Integer campaignId;

    private List<String> customerEmailList;
    private List<String> customerMobileList;

    private String isPartialAllowed;
    private BigDecimal miniPartialAmount;
    private BigDecimal maxPartialAmount;

    private BigDecimal discount;
    private String discountType;

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public BigDecimal getSubAmount() {
        return subAmount;
    }

    public void setSubAmount(BigDecimal subAmount) {
        this.subAmount = subAmount;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getCustomerFullName() {
        return customerFullName;
    }

    public void setCustomerFullName(String customerFullName) {
        this.customerFullName = customerFullName;
    }

    public String getCustomerMobileNumber() {
        return customerMobileNumber;
    }

    public void setCustomerMobileNumber(String customerMobileNumber) {
        this.customerMobileNumber = customerMobileNumber;
    }

    public String getCustomerEmailAddress() {
        return customerEmailAddress;
    }

    public void setCustomerEmailAddress(String customerEmailAddress) {
        this.customerEmailAddress = customerEmailAddress;
    }

    public Long getEntityActivityId() {
        return entityActivityId;
    }

    public void setEntityActivityId(Long entityActivityId) {
        this.entityActivityId = entityActivityId;
    }

    public List<BillCustomFieldRqDto> getCustomFieldList() {
        return customFieldList;
    }

    public void setCustomFieldList(List<BillCustomFieldRqDto> customFieldList) {
        this.customFieldList = customFieldList;
    }

    public String getCustomerTaxNumber() {
        return customerTaxNumber;
    }

    public void setCustomerTaxNumber(String customerTaxNumber) {
        this.customerTaxNumber = customerTaxNumber;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public BigDecimal getCustomerPreviousBalance() {
        return customerPreviousBalance;
    }

    public void setCustomerPreviousBalance(BigDecimal customerPreviousBalance) {
        this.customerPreviousBalance = customerPreviousBalance;
    }

    public String getCustomerIdType() {
        return customerIdType;
    }

    public void setCustomerIdType(String customerIdType) {
        this.customerIdType = customerIdType;
    }

    public String getCustomerIdNumber() {
        return customerIdNumber;
    }

    public void setCustomerIdNumber(String customerIdNumber) {
        this.customerIdNumber = customerIdNumber;
    }

    public String getSaveCustomer() {
        return saveCustomer;
    }

    public void setSaveCustomer(String saveCustomer) {
        this.saveCustomer = saveCustomer;
    }

    public Integer getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }

    @Override
    public void validate(Locale locale) {
        List<ValidationMessage> errorList = new ArrayList<>();

        if (this.billNumber != null && this.billNumber.length() > 20) {
            ValidationMessage validationMessage = new ValidationMessage("billNumber", "bill_number_length", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.issueDate)) {
            ValidationMessage validationMessage = new ValidationMessage("issueDate", "issue_date_required", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.expireDate)) {
            ValidationMessage validationMessage = new ValidationMessage("expireDate", "expire_date_required", locale);
            errorList.add(validationMessage);
        }

        LocalDateTime issueDateTime = Utils.parseDateFromString(this.issueDate, "yyyy-MM-dd");
        if (!Utils.isNullOrEmpty(this.issueDate) && issueDateTime == null) {
            ValidationMessage validationMessage = new ValidationMessage("issueDate", "issue_date_invalid", locale);
            errorList.add(validationMessage);
        }

        LocalDateTime expireDateTime = Utils.parseDateFromString(this.expireDate, "yyyy-MM-dd");
        if (!Utils.isNullOrEmpty(this.expireDate) && expireDateTime == null) {
            ValidationMessage validationMessage = new ValidationMessage("expireDate", "expire_date_invalid", locale);
            errorList.add(validationMessage);
        }

        if (expireDateTime != null && expireDateTime.isBefore(LocalDate.now().atStartOfDay().plusDays(1))) {
            ValidationMessage validationMessage = new ValidationMessage("expireDate", "Expire date should not be less than the issue date.", locale);
            errorList.add(validationMessage);
        }


        if (Utils.isNullOrEmpty(this.serviceName)) {
            ValidationMessage validationMessage = new ValidationMessage("serviceName", "service_name_empty", locale);
            errorList.add(validationMessage);
        }


        if (this.subAmount == null) {
            ValidationMessage validationMessage = new ValidationMessage("subAmount", "sub_amount_empty", locale);
            errorList.add(validationMessage);
        }

        if (this.vat == null) {
            ValidationMessage validationMessage = new ValidationMessage("vat", "vat_empty", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.customerFullName)) {
            ValidationMessage validationMessage = new ValidationMessage("customerFullName", "customer_full_name_empty", locale);
            errorList.add(validationMessage);
        }

        if (!Utils.isValidSize(this.customerFullName, 1, 255)) {
            ValidationMessage validationMessage = new ValidationMessage("customerFullName", "Customer full name length should not exceed 255", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.customerMobileNumber)) {
            ValidationMessage validationMessage = new ValidationMessage("customerMobileNumber", "customer_mobile_number_empty", locale);
            errorList.add(validationMessage);
        }

        if (!Utils.isNullOrEmpty(this.customerMobileNumber) && !Utils.isValidMobileNumber(this.customerMobileNumber)) {
            ValidationMessage validationMessage = new ValidationMessage("customerMobileNumber", "customer_mobile_number_invalid", locale);
            errorList.add(validationMessage);
        }


        if (!Utils.isNullOrEmpty(this.customerEmailAddress) && !Utils.isValidEmail(this.customerEmailAddress)) {
            ValidationMessage validationMessage = new ValidationMessage("customerEmailAddress", "customer_email_not_valid", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.entityActivityId) && Utils.isNullOrEmpty(this.entityActivityCode)) {
            ValidationMessage validationMessage = new ValidationMessage("entityActivityId", "entity_activity_id_empty", locale);
            errorList.add(validationMessage);
        }

        if (this.customerAddress != null && this.customerAddress.trim().length() > 255) {
            ValidationMessage validationMessage = new ValidationMessage("customerAddress", "customer_address_length_255", locale);
            errorList.add(validationMessage);
        }

        if (this.customerEmailAddress != null && this.customerEmailAddress.trim().length() > 255) {
            ValidationMessage validationMessage = new ValidationMessage("customerEmailAddress", "email_length_255", locale);
            errorList.add(validationMessage);
        }

        if (this.serviceDescription != null && this.serviceDescription.trim().length() > 255) {
            ValidationMessage validationMessage = new ValidationMessage("serviceDescription", "description_length_255", locale);
            errorList.add(validationMessage);
        }

        if (this.serviceName != null && this.serviceName.trim().length() > 255) {
            ValidationMessage validationMessage = new ValidationMessage("serviceName", "service_name_length_255", locale);
            errorList.add(validationMessage);
        }

//        if ((this.customerIdType != null) && !(this.customerIdType.isEmpty()) && !(Utils.isEnumValueExist(this.customerIdType, IdType.class))) {
//            ValidationMessage validationMessage = new ValidationMessage("customerIdType", "customer_id_type_not_valid", locale);
//            errorList.add(validationMessage);
//        }

        if (!Utils.isIdNumberValid(this.customerIdNumber, IdType.getValue(this.customerIdType))) {
            ValidationMessage validationMessage = new ValidationMessage("customerIdNumber", "customer_id_number_not_valid", locale);
            errorList.add(validationMessage);
        }


        if (!Utils.isValidTaxNumber(this.customerTaxNumber)) {
            ValidationMessage validationMessage = new ValidationMessage("customerTaxNumber", "customer_tax_number_not_valid", locale);
            errorList.add(validationMessage);
        }

        if (!errorList.isEmpty()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request", errorList, locale);
        }
    }

    public List<String> getCustomerEmailList() {
        return customerEmailList;
    }

    public void setCustomerEmailList(List<String> customerEmailList) {
        this.customerEmailList = customerEmailList;
    }

    public List<String> getCustomerMobileList() {
        return customerMobileList;
    }

    public void setCustomerMobileList(List<String> customerMobileList) {
        this.customerMobileList = customerMobileList;
    }

    public String getIsPartialAllowed() {
        return isPartialAllowed;
    }

    public void setIsPartialAllowed(String isPartialAllowed) {
        this.isPartialAllowed = isPartialAllowed;
    }

    public BigDecimal getMiniPartialAmount() {
        return miniPartialAmount;
    }

    public void setMiniPartialAmount(BigDecimal miniPartialAmount) {
        this.miniPartialAmount = miniPartialAmount;
    }

    public BigDecimal getMaxPartialAmount() {
        return maxPartialAmount;
    }

    public void setMaxPartialAmount(BigDecimal maxPartialAmount) {
        this.maxPartialAmount = maxPartialAmount;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getEntityActivityCode() {
        return entityActivityCode;
    }

    public void setEntityActivityCode(String entityActivityCode) {
        this.entityActivityCode = entityActivityCode;
    }

    @Override
    public Object toEntity(Locale locale) {

        Bill bill = new Bill();
        bill.setBillNumber(this.billNumber);
        bill.setServiceName(this.serviceName);
        bill.setCustomerMobileNumber(Utils.formatMobileNumber(customerMobileNumber));
        bill.setCustomerEmailAddress(this.customerEmailAddress);
        bill.setCustomerFullName(this.customerFullName);
        bill.setSubAmount(this.subAmount);
        bill.setIssueDate(Utils.parseDateFromString(this.issueDate, "yyyy-MM-dd"));
        bill.setExpireDate(Utils.parseDateFromString(this.expireDate, "yyyy-MM-dd"));
        bill.setServiceDescription(this.serviceDescription);
        bill.setCustomerAddress(this.customerAddress);
        bill.setCustomerTaxNumber(this.customerTaxNumber);
        bill.setCustomerPreviousBalance(this.customerPreviousBalance);
        bill.setCustomerIdType(IdType.getValue(this.customerIdType));
        bill.setIsPartialAllowed(BooleanFlag.getValue(isPartialAllowed));
        bill.setDiscount(this.discount);
        bill.setDiscountType(this.discountType);

        if (this.vat == null) {
            this.vat = "0";
        }

        switch (this.vat) {
            case "EXE":
                bill.setVat(new BigDecimal("0"));
                bill.setVatExemptedFlag(BooleanFlag.YES);
                bill.setVatNaFlag(BooleanFlag.NO);
                break;
            case "NA":
                bill.setVat(new BigDecimal("0"));
                bill.setVatExemptedFlag(BooleanFlag.NO);
                bill.setVatNaFlag(BooleanFlag.YES);
                break;
            default:
                bill.setVat(new BigDecimal(this.vat));
                bill.setVatExemptedFlag(BooleanFlag.NO);
                bill.setVatNaFlag(BooleanFlag.NO);
        }

        return bill;
    }
}
