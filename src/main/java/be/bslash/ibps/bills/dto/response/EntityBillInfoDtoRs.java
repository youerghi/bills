package be.bslash.ibps.bills.dto.response;

import be.bslash.ibps.bills.dto.request.SettingStyleDtoRq;
import be.bslash.ibps.bills.entities.City;
import be.bslash.ibps.bills.entities.District;

public class EntityBillInfoDtoRs {

    private Long id;
    private String logo;
    private String stamp;
    private String code;
    private String brandNameAr;
    private String brandNameEn;
    private String commercialNameAr;
    private String commercialNameEn;
    private String vatNumber;
    private String vatNameAr;
    private City city;
    private String otherCity;
    private District district;
    private String otherDistrict;
    private String streetName;
    private String entityUnitNumber;
    private String zipCode;
    private String additionalZipCode;
    private String idNumber;

    private String phoneNumber;
    private String phoneNumberCode;
    private String mobileNumber;
    private String mobileNumberCode;
    private String unifiedNumber;
    private String website;
    private String email;

    private SettingStyleDtoRq styling;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getStamp() {
        return stamp;
    }

    public void setStamp(String stamp) {
        this.stamp = stamp;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBrandNameAr() {
        return brandNameAr;
    }

    public void setBrandNameAr(String brandNameAr) {
        this.brandNameAr = brandNameAr;
    }

    public String getBrandNameEn() {
        return brandNameEn;
    }

    public void setBrandNameEn(String brandNameEn) {
        this.brandNameEn = brandNameEn;
    }

    public String getCommercialNameAr() {
        return commercialNameAr;
    }

    public void setCommercialNameAr(String commercialNameAr) {
        this.commercialNameAr = commercialNameAr;
    }

    public String getCommercialNameEn() {
        return commercialNameEn;
    }

    public void setCommercialNameEn(String commercialNameEn) {
        this.commercialNameEn = commercialNameEn;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getVatNameAr() {
        return vatNameAr;
    }

    public void setVatNameAr(String vatNameAr) {
        this.vatNameAr = vatNameAr;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getOtherCity() {
        return otherCity;
    }

    public void setOtherCity(String otherCity) {
        this.otherCity = otherCity;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public String getOtherDistrict() {
        return otherDistrict;
    }

    public void setOtherDistrict(String otherDistrict) {
        this.otherDistrict = otherDistrict;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getEntityUnitNumber() {
        return entityUnitNumber;
    }

    public void setEntityUnitNumber(String entityUnitNumber) {
        this.entityUnitNumber = entityUnitNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getAdditionalZipCode() {
        return additionalZipCode;
    }

    public void setAdditionalZipCode(String additionalZipCode) {
        this.additionalZipCode = additionalZipCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumberCode() {
        return phoneNumberCode;
    }

    public void setPhoneNumberCode(String phoneNumberCode) {
        this.phoneNumberCode = phoneNumberCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getMobileNumberCode() {
        return mobileNumberCode;
    }

    public void setMobileNumberCode(String mobileNumberCode) {
        this.mobileNumberCode = mobileNumberCode;
    }

    public String getUnifiedNumber() {
        return unifiedNumber;
    }

    public void setUnifiedNumber(String unifiedNumber) {
        this.unifiedNumber = unifiedNumber;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public SettingStyleDtoRq getStyling() {
        return styling;
    }

    public void setStyling(SettingStyleDtoRq styling) {
        this.styling = styling;
    }
}
