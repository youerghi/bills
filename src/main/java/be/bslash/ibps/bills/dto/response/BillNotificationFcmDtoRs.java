package be.bslash.ibps.bills.dto.response;

public class BillNotificationFcmDtoRs {

    private Long id;
    private String title;
    private String body;
    private String data;
    private String isReadFlag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getIsReadFlag() {
        return isReadFlag;
    }

    public void setIsReadFlag(String isReadFlag) {
        this.isReadFlag = isReadFlag;
    }
}
