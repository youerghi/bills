package be.bslash.ibps.bills.dto;

import java.util.Locale;

public interface EntityDtoConverter {
    public Object toDto(Locale locale);
}
