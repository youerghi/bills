package be.bslash.ibps.bills.dto.request;

import be.bslash.ibps.bills.dto.DtoValidation;
import be.bslash.ibps.bills.enums.PaymentMethod;
import be.bslash.ibps.bills.exceptions.HttpServiceException;
import be.bslash.ibps.bills.exceptions.ValidationMessage;
import be.bslash.ibps.bills.utils.Utils;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class LocalPaymentRqDto implements DtoValidation {

    private Long billId;
    private String paymentDate;
    private String billNumber;
    private String paymentMethod;
    private BigDecimal paymentAmount;

    public Long getBillId() {
        return billId;
    }

    public void setBillId(Long billId) {
        this.billId = billId;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    @Override
    public void validate(Locale locale) {
        List<ValidationMessage> errorList = new ArrayList<>();

        if (Utils.isNullOrEmpty(this.billId)) {
            ValidationMessage validationMessage = new ValidationMessage("billId", "bill_id_empty", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.paymentDate)) {
            ValidationMessage validationMessage = new ValidationMessage("paymentDate", "Please provide payment date", locale);
            errorList.add(validationMessage);
        }

        LocalDateTime paymentDateTime = Utils.parseDateFromString(this.paymentDate, "yyyy-MM-dd");
        if (!Utils.isNullOrEmpty(this.paymentDate) && paymentDateTime == null) {
            ValidationMessage validationMessage = new ValidationMessage("paymentDate", "Payment date format is not valid", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.paymentMethod)) {
            ValidationMessage validationMessage = new ValidationMessage("paymentMethod", "payment method should not empty", locale);
            errorList.add(validationMessage);
        }

        if (!Utils.isEnumValueExist(this.paymentMethod, PaymentMethod.class)) {
            ValidationMessage validationMessage = new ValidationMessage("paymentMethod", "payment method not valid", locale);
            errorList.add(validationMessage);
        }

        if (Utils.isNullOrEmpty(this.paymentAmount)) {
            ValidationMessage validationMessage = new ValidationMessage("paymentAmount", "payment amount should not empty", locale);
            errorList.add(validationMessage);
        }

        if (!errorList.isEmpty()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request", errorList, locale);
        }
    }
}
