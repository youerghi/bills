package be.bslash.ibps.bills.dto.response;

import be.bslash.ibps.bills.enums.BooleanFlag;

import java.math.BigDecimal;

public class BillItemDetailedRsDto {

    private Integer number;
    private String name;
    private BigDecimal unitPrice;
    private BigDecimal quantity;
    private String vat;
    private BigDecimal totalPrice;
    private BigDecimal discount;
    private String discountType;
    private BooleanFlag vatExemptedFlag;
    private BooleanFlag vatNaFlag;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public BooleanFlag getVatExemptedFlag() {
        return vatExemptedFlag;
    }

    public void setVatExemptedFlag(BooleanFlag vatExemptedFlag) {
        this.vatExemptedFlag = vatExemptedFlag;
    }

    public BooleanFlag getVatNaFlag() {
        return vatNaFlag;
    }

    public void setVatNaFlag(BooleanFlag vatNaFlag) {
        this.vatNaFlag = vatNaFlag;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }
}
