package be.bslash.ibps.bills.dto.response;

import be.bslash.ibps.bills.dto.request.AdCampaignUsersDtoRq;

import java.util.List;

public class AdSettingsDtoRs {

    private Integer id;

    private String adModuleStatus;

    private String masterAdvStatus;

    private Integer masterCampaignId;

    private String urlClickable;

    private String adSwitchEnable;

    private String adSwitchDuration;

    private String masterURL;

    private String masterURLEnable;

    private List<AdCampaignUsersDtoRq> campaignUsers;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAdModuleStatus() {
        return adModuleStatus;
    }

    public void setAdModuleStatus(String adModuleStatus) {
        this.adModuleStatus = adModuleStatus;
    }

    public String getMasterAdvStatus() {
        return masterAdvStatus;
    }

    public void setMasterAdvStatus(String masterAdvStatus) {
        this.masterAdvStatus = masterAdvStatus;
    }

    public String getUrlClickable() {
        return urlClickable;
    }

    public void setUrlClickable(String urlClickable) {
        this.urlClickable = urlClickable;
    }

    public String getAdSwitchEnable() {
        return adSwitchEnable;
    }

    public void setAdSwitchEnable(String adSwitchEnable) {
        this.adSwitchEnable = adSwitchEnable;
    }

    public List<AdCampaignUsersDtoRq> getCampaignUsers() {
        return campaignUsers;
    }

    public void setCampaignUsers(List<AdCampaignUsersDtoRq> campaignUsers) {
        this.campaignUsers = campaignUsers;
    }

    public String getMasterURL() {
        return masterURL;
    }

    public void setMasterURL(String masterURL) {
        this.masterURL = masterURL;
    }

    public String getMasterURLEnable() {
        return masterURLEnable;
    }

    public void setMasterURLEnable(String masterURLEnable) {
        this.masterURLEnable = masterURLEnable;
    }

    public Integer getMasterCampaignId() {
        return masterCampaignId;
    }

    public void setMasterCampaignId(Integer masterCampaignId) {
        this.masterCampaignId = masterCampaignId;
    }

    public String getAdSwitchDuration() {
        return adSwitchDuration;
    }

    public void setAdSwitchDuration(String adSwitchDuration) {
        this.adSwitchDuration = adSwitchDuration;
    }

}
