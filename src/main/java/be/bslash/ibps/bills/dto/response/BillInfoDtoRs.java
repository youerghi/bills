package be.bslash.ibps.bills.dto.response;

import be.bslash.ibps.bills.entities.LocalPayment;
import be.bslash.ibps.bills.entities.SadadPayment;
import be.bslash.ibps.bills.enums.BooleanFlag;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class BillInfoDtoRs {

    private Long id;
    private String billNumber;
    private String sadadNumber;
    private String serviceName;
    private String serviceDescription;
    private LocalDateTime issueDate;
    private LocalDateTime expireDate;
    private BigDecimal subAmount;
    private String vat;
    private BigDecimal discount;
    private String discountType;
    private BigDecimal totalAmount;
    private String customerFullName;
    private String customerMobileNumber;
    private String customerEmailAddress;
    private String customerIdNumber;
    private Long entityActivityId;
    private String statusCode;
    private String statusText;
    private String customerTaxNumber;
    private String customerAddress;
    private BigDecimal customerPreviousBalance;
    private List<BillItemDetailedRsDto> billItemList;
    private List<BillItemDetailedRsDto> runningBillItemList;
    private List<BillCustomFieldRsDto> customFieldList;
    private Integer campaignId;
    private BooleanFlag vatExemptedFlag;
    private BooleanFlag vatNaFlag;
    private String statusAr;
    private String statusEn;
    private String paymentFlag;
    private String paymentMethod;
    private LocalDateTime paymentDate;
    private String image;

    private List<String> customerEmailList;
    private List<String> customerMobileList;

    private String customerIdType;

    private String isPartialAllowed;
    private String billCategory;
    private BigDecimal miniPartialAmount;
    private BigDecimal maxPartialAmount;
    private Integer paymentNumber;
    private Integer settlementNumber;

    private BigDecimal remainAmount;
    private BigDecimal currentPaidAmount;

    private List<PaymentDtoRs> paymentList;

    private List<SadadPayment> onlinePaymentList;
    private List<LocalPayment> offlinePaymentList;

    private List<BillNoteDtoRs> billNoteList;

    private BigDecimal totalOfflinePaidAmount;
    private BigDecimal totalOnlinePaidAmount;

    private String invoiceLogo;
    private String invoiceStamp;

    private AdCampaignDtoRs adCampaign;
    private AdSettingsDtoRs adSettings;

    private String qr;

    private EntityBillInfoDtoRs entity;


    private String streetName;
    private String buildingNumber;
    private Long cityId;
    private Long districtId;
    private String otherDistrict;
    private String additionalNumber;
    private String postalCode;

    private BigDecimal installmentAmount;

    private String account;
    private LocalDateTime dueDate;
    private String contractNumber;
    private String referenceNumber;

    private String billTypeText;

    private Long accountId;

    private AccountDtoRs accountInfo;

    private EntityActivityDtoRs entityActivity;

    public EntityActivityDtoRs getEntityActivity() {
        return entityActivity;
    }

    public void setEntityActivity(EntityActivityDtoRs entityActivity) {
        this.entityActivity = entityActivity;
    }

    public AccountDtoRs getAccountInfo() {
        return accountInfo;
    }

    public void setAccountInfo(AccountDtoRs accountInfo) {
        this.accountInfo = accountInfo;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getBillTypeText() {
        return billTypeText;
    }

    public void setBillTypeText(String billTypeText) {
        this.billTypeText = billTypeText;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public LocalDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public BigDecimal getInstallmentAmount() {
        return installmentAmount;
    }

    public void setInstallmentAmount(BigDecimal installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    public String getOtherDistrict() {
        return otherDistrict;
    }

    public void setOtherDistrict(String otherDistrict) {
        this.otherDistrict = otherDistrict;
    }

    public String getAdditionalNumber() {
        return additionalNumber;
    }

    public void setAdditionalNumber(String additionalNumber) {
        this.additionalNumber = additionalNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getSadadNumber() {
        return sadadNumber;
    }

    public void setSadadNumber(String sadadNumber) {
        this.sadadNumber = sadadNumber;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public LocalDateTime getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(LocalDateTime issueDate) {
        this.issueDate = issueDate;
    }

    public LocalDateTime getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(LocalDateTime expireDate) {
        this.expireDate = expireDate;
    }

    public BigDecimal getSubAmount() {
        return subAmount;
    }

    public void setSubAmount(BigDecimal subAmount) {
        this.subAmount = subAmount;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getCustomerFullName() {
        return customerFullName;
    }

    public void setCustomerFullName(String customerFullName) {
        this.customerFullName = customerFullName;
    }

    public String getCustomerMobileNumber() {
        return customerMobileNumber;
    }

    public void setCustomerMobileNumber(String customerMobileNumber) {
        this.customerMobileNumber = customerMobileNumber;
    }

    public String getCustomerEmailAddress() {
        return customerEmailAddress;
    }

    public void setCustomerEmailAddress(String customerEmailAddress) {
        this.customerEmailAddress = customerEmailAddress;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public String getCustomerIdNumber() {
        return customerIdNumber;
    }

    public void setCustomerIdNumber(String customerIdNumber) {
        this.customerIdNumber = customerIdNumber;
    }

    public List<BillItemDetailedRsDto> getBillItemList() {
        return billItemList;
    }

    public void setBillItemList(List<BillItemDetailedRsDto> billItemList) {
        this.billItemList = billItemList;
    }

    public Long getEntityActivityId() {
        return entityActivityId;
    }

    public void setEntityActivityId(Long entityActivityId) {
        this.entityActivityId = entityActivityId;
    }

    public List<BillCustomFieldRsDto> getCustomFieldList() {
        return customFieldList;
    }

    public void setCustomFieldList(List<BillCustomFieldRsDto> customFieldList) {
        this.customFieldList = customFieldList;
    }

    public String getCustomerTaxNumber() {
        return customerTaxNumber;
    }

    public void setCustomerTaxNumber(String customerTaxNumber) {
        this.customerTaxNumber = customerTaxNumber;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public BigDecimal getCustomerPreviousBalance() {
        return customerPreviousBalance;
    }

    public void setCustomerPreviousBalance(BigDecimal customerPreviousBalance) {
        this.customerPreviousBalance = customerPreviousBalance;
    }

    public String getCustomerIdType() {
        return customerIdType;
    }

    public void setCustomerIdType(String customerIdType) {
        this.customerIdType = customerIdType;
    }

    public Integer getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }

    public BooleanFlag getVatExemptedFlag() {
        return vatExemptedFlag;
    }

    public void setVatExemptedFlag(BooleanFlag vatExemptedFlag) {
        this.vatExemptedFlag = vatExemptedFlag;
    }

    public BooleanFlag getVatNaFlag() {
        return vatNaFlag;
    }

    public void setVatNaFlag(BooleanFlag vatNaFlag) {
        this.vatNaFlag = vatNaFlag;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getStatusAr() {
        return statusAr;
    }

    public void setStatusAr(String statusAr) {
        this.statusAr = statusAr;
    }

    public String getStatusEn() {
        return statusEn;
    }

    public void setStatusEn(String statusEn) {
        this.statusEn = statusEn;
    }

    public String getPaymentFlag() {
        return paymentFlag;
    }

    public void setPaymentFlag(String paymentFlag) {
        this.paymentFlag = paymentFlag;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public LocalDateTime getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDateTime paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<String> getCustomerEmailList() {
        return customerEmailList;
    }

    public void setCustomerEmailList(List<String> customerEmailList) {
        this.customerEmailList = customerEmailList;
    }

    public List<String> getCustomerMobileList() {
        return customerMobileList;
    }

    public void setCustomerMobileList(List<String> customerMobileList) {
        this.customerMobileList = customerMobileList;
    }

    public String getIsPartialAllowed() {
        return isPartialAllowed;
    }

    public void setIsPartialAllowed(String isPartialAllowed) {
        this.isPartialAllowed = isPartialAllowed;
    }

    public String getBillCategory() {
        return billCategory;
    }

    public void setBillCategory(String billCategory) {
        this.billCategory = billCategory;
    }

    public BigDecimal getMiniPartialAmount() {
        return miniPartialAmount;
    }

    public void setMiniPartialAmount(BigDecimal miniPartialAmount) {
        this.miniPartialAmount = miniPartialAmount;
    }

    public BigDecimal getMaxPartialAmount() {
        return maxPartialAmount;
    }

    public void setMaxPartialAmount(BigDecimal maxPartialAmount) {
        this.maxPartialAmount = maxPartialAmount;
    }

    public Integer getPaymentNumber() {
        return paymentNumber;
    }

    public void setPaymentNumber(Integer paymentNumber) {
        this.paymentNumber = paymentNumber;
    }

    public Integer getSettlementNumber() {
        return settlementNumber;
    }

    public void setSettlementNumber(Integer settlementNumber) {
        this.settlementNumber = settlementNumber;
    }

    public BigDecimal getRemainAmount() {
        return remainAmount;
    }

    public void setRemainAmount(BigDecimal remainAmount) {
        this.remainAmount = remainAmount;
    }

    public BigDecimal getCurrentPaidAmount() {
        return currentPaidAmount;
    }

    public void setCurrentPaidAmount(BigDecimal currentPaidAmount) {
        this.currentPaidAmount = currentPaidAmount;
    }

    public List<PaymentDtoRs> getPaymentList() {
        return paymentList;
    }

    public void setPaymentList(List<PaymentDtoRs> paymentList) {
        this.paymentList = paymentList;
    }

    public BigDecimal getTotalOfflinePaidAmount() {
        return totalOfflinePaidAmount;
    }

    public void setTotalOfflinePaidAmount(BigDecimal totalOfflinePaidAmount) {
        this.totalOfflinePaidAmount = totalOfflinePaidAmount;
    }

    public BigDecimal getTotalOnlinePaidAmount() {
        return totalOnlinePaidAmount;
    }

    public void setTotalOnlinePaidAmount(BigDecimal totalOnlinePaidAmount) {
        this.totalOnlinePaidAmount = totalOnlinePaidAmount;
    }

    public List<SadadPayment> getOnlinePaymentList() {
        return onlinePaymentList;
    }

    public void setOnlinePaymentList(List<SadadPayment> onlinePaymentList) {
        this.onlinePaymentList = onlinePaymentList;
    }

    public List<LocalPayment> getOfflinePaymentList() {
        return offlinePaymentList;
    }

    public void setOfflinePaymentList(List<LocalPayment> offlinePaymentList) {
        this.offlinePaymentList = offlinePaymentList;
    }

    public EntityBillInfoDtoRs getEntity() {
        return entity;
    }

    public void setEntity(EntityBillInfoDtoRs entity) {
        this.entity = entity;
    }

    public List<BillNoteDtoRs> getBillNoteList() {
        return billNoteList;
    }

    public void setBillNoteList(List<BillNoteDtoRs> billNoteList) {
        this.billNoteList = billNoteList;
    }

    public AdCampaignDtoRs getAdCampaign() {
        return adCampaign;
    }

    public void setAdCampaign(AdCampaignDtoRs adCampaign) {
        this.adCampaign = adCampaign;
    }

    public AdSettingsDtoRs getAdSettings() {
        return adSettings;
    }

    public void setAdSettings(AdSettingsDtoRs adSettings) {
        this.adSettings = adSettings;
    }

    public String getInvoiceLogo() {
        return invoiceLogo;
    }

    public void setInvoiceLogo(String invoiceLogo) {
        this.invoiceLogo = invoiceLogo;
    }

    public String getInvoiceStamp() {
        return invoiceStamp;
    }

    public void setInvoiceStamp(String invoiceStamp) {
        this.invoiceStamp = invoiceStamp;
    }

    public List<BillItemDetailedRsDto> getRunningBillItemList() {
        return runningBillItemList;
    }

    public void setRunningBillItemList(List<BillItemDetailedRsDto> runningBillItemList) {
        this.runningBillItemList = runningBillItemList;
    }

    public String getQr() {
        return qr;
    }

    public void setQr(String qr) {
        this.qr = qr;
    }
}
