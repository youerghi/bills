package be.bslash.ibps.bills.dto.response;

public class PageDtoRs {

    private Long total;
    private Integer numberOfPages;
    private Object pageList;

    public PageDtoRs() {
    }

    public PageDtoRs(Long total, Integer numberOfPages, Object pageList) {
        this.total = total;
        this.numberOfPages = numberOfPages;
        this.pageList = pageList;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Integer getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(Integer numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public Object getPageList() {
        return pageList;
    }

    public void setPageList(Object pageList) {
        this.pageList = pageList;
    }
}
