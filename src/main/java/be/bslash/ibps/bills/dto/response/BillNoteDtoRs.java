package be.bslash.ibps.bills.dto.response;

import be.bslash.ibps.bills.entities.BillNoteItem;
import be.bslash.ibps.bills.enums.NoteType;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class BillNoteDtoRs implements Comparable {

    private Long id;
    private LocalDateTime date;
    private BigDecimal previousAmount;
    private NoteType noteType;
    private String description;
    private String documentNumber;
    private BigDecimal amount;
    private BigDecimal balanceAmount;
    private List<BillNoteItem> itemList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public BigDecimal getPreviousAmount() {
        return previousAmount;
    }

    public void setPreviousAmount(BigDecimal previousAmount) {
        this.previousAmount = previousAmount;
    }

    public NoteType getNoteType() {
        return noteType;
    }

    public void setNoteType(NoteType noteType) {
        this.noteType = noteType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(BigDecimal balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public List<BillNoteItem> getItemList() {
        return itemList;
    }

    public void setItemList(List<BillNoteItem> itemList) {
        this.itemList = itemList;
    }

    @Override
    public int compareTo(Object o) {
        try {
            LocalDateTime comparable = ((BillNoteDtoRs) o).getDate();
            return this.getDate().compareTo(comparable);
        } catch (Exception ex) {
            return 0;
        }

    }
}
