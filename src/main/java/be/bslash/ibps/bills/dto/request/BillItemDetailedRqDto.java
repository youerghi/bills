package be.bslash.ibps.bills.dto.request;


import be.bslash.ibps.bills.dto.DtoEntityConverter;
import be.bslash.ibps.bills.dto.DtoValidation;
import be.bslash.ibps.bills.entities.BillItem;
import be.bslash.ibps.bills.entities.RunningTaxBillItem;
import be.bslash.ibps.bills.enums.BooleanFlag;
import be.bslash.ibps.bills.exceptions.ValidationMessage;
import be.bslash.ibps.bills.utils.Utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class BillItemDetailedRqDto implements DtoValidation, DtoEntityConverter {

    private String name;
    private BigDecimal unitPrice;
    private BigDecimal quantity;
    private BigDecimal discount;
    private String discountType;
    private String vat;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    @Override
    public void validate(Locale locale) {

        List<ValidationMessage> errorList = new ArrayList<>();

        if (Utils.isNullOrEmpty(this.name)) {
            ValidationMessage validationMessage = new ValidationMessage("name", "item_name_empty", locale);
            errorList.add(validationMessage);
        }

        if (this.unitPrice == null || this.unitPrice.doubleValue() < 0) {
            ValidationMessage validationMessage = new ValidationMessage("unitPrice", "unit_price_invalid", locale);
            errorList.add(validationMessage);
        }

        if (this.quantity == null || this.quantity.compareTo(BigDecimal.ZERO) == 0) {
            ValidationMessage validationMessage = new ValidationMessage("quantity", "invalid_quantity", locale);
            errorList.add(validationMessage);
        }
    }

    @Override
    public Object toEntity(Locale locale) {

        BillItem billItem = new BillItem();
        billItem.setName(this.name);
        billItem.setQuantity(this.quantity);
        billItem.setUnitPrice(this.unitPrice);
        billItem.setTotalPrice(this.unitPrice.multiply(this.quantity));
        billItem.setDiscount(this.discount);
        billItem.setDiscountType(this.discountType);

        switch (this.vat) {
            case "EXE":
                billItem.setVat(new BigDecimal("0"));
                billItem.setVatExemptedFlag(BooleanFlag.YES);
                billItem.setVatNaFlag(BooleanFlag.NO);
                break;
            case "NA":
                billItem.setVat(new BigDecimal("0"));
                billItem.setVatExemptedFlag(BooleanFlag.NO);
                billItem.setVatNaFlag(BooleanFlag.YES);
                break;
            default:
                billItem.setVat(new BigDecimal(this.vat));
                billItem.setVatExemptedFlag(BooleanFlag.NO);
                billItem.setVatNaFlag(BooleanFlag.NO);
        }

        return billItem;
    }

    public Object toRunningTaxBillItemEntity(Locale locale) {

        RunningTaxBillItem billItem = new RunningTaxBillItem();
        billItem.setName(this.name);
        billItem.setQuantity(this.quantity);
        billItem.setUnitPrice(this.unitPrice);
        billItem.setTotalPrice(this.unitPrice.multiply(this.quantity));
        billItem.setDiscount(this.discount);
        billItem.setDiscountType(this.discountType);

        switch (this.vat) {
            case "EXE":
                billItem.setVat(new BigDecimal("0"));
                billItem.setVatExemptedFlag(BooleanFlag.YES);
                billItem.setVatNaFlag(BooleanFlag.NO);
                break;
            case "NA":
                billItem.setVat(new BigDecimal("0"));
                billItem.setVatExemptedFlag(BooleanFlag.NO);
                billItem.setVatNaFlag(BooleanFlag.YES);
                break;
            default:
                billItem.setVat(new BigDecimal(this.vat));
                billItem.setVatExemptedFlag(BooleanFlag.NO);
                billItem.setVatNaFlag(BooleanFlag.NO);
        }

        return billItem;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BillItemDetailedRqDto that = (BillItemDetailedRqDto) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
