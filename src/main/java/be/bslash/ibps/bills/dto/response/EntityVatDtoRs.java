package be.bslash.ibps.bills.dto.response;

public class EntityVatDtoRs {

    private Long id;
    private String vat;
    private String vatDisplay;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getVatDisplay() {
        return vatDisplay;
    }

    public void setVatDisplay(String vatDisplay) {
        this.vatDisplay = vatDisplay;
    }
}
