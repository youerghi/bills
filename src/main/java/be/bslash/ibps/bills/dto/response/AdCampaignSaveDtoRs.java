package be.bslash.ibps.bills.dto.response;

public class AdCampaignSaveDtoRs {

    private Integer adCampaignId;

    public Integer getAdCampaignId() {
        return adCampaignId;
    }

    public void setAdCampaignId(Integer adCampaignId) {
        this.adCampaignId = adCampaignId;
    }

}
