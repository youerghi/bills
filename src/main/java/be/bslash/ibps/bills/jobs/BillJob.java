package be.bslash.ibps.bills.jobs;

import be.bslash.ibps.bills.services.BillNotificationService;
import be.bslash.ibps.bills.services.BillService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class BillJob {

    private static final Logger LOGGER = LogManager.getLogger(BillJob.class);

    @Autowired
    private BillService billService;

    @Autowired
    private BillNotificationService billNotificationService;

    @Scheduled(cron = "0 30 0 * * *")
    public void cancelBillDailyJob() {
        billService.cancelAllExpireBill();
    }

    @Scheduled(cron = "0 20 0 * * *")
    public void deleteBillDailyJob() {
        billService.deleteAllExpireBill();
    }

    //    @Scheduled(fixedDelay = 3000)
    @Scheduled(cron = "0 5 0 * * *")
    public void recurringBillJob() {
        LOGGER.info("start recurring job");
        billService.recurringBillJob();
    }

    @Scheduled(cron = "0 10 0 * * *")
    public void issueNotificationDailyJob() {
        billNotificationService.sendBillNotificationIssueJob();
    }


}
