package be.bslash.ibps.bills.controllers;


import be.bslash.ibps.bills.configrations.MessageEnvelope;
import be.bslash.ibps.bills.exceptions.HttpServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;

@CrossOrigin
@Controller
@RequestMapping("/recurringBill")
public class RecurringBillController {


    @RequestMapping(value = "/make", method = RequestMethod.POST)
    public ResponseEntity<MessageEnvelope> makeBill(HttpServletRequest httpServletRequest) {

        Locale locale = httpServletRequest.getLocale();

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/check", method = RequestMethod.PUT)
    public ResponseEntity<MessageEnvelope> checkBill(HttpServletRequest httpServletRequest,
                                                     @RequestParam(value = "billId", required = false) Long billId) {
        Locale locale = httpServletRequest.getLocale();

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResponseEntity<MessageEnvelope> updateBill(HttpServletRequest httpServletRequest) {
        Locale locale = httpServletRequest.getLocale();

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public ResponseEntity<MessageEnvelope> deleteBill(HttpServletRequest httpServletRequest,
                                                      @RequestParam(value = "customerId", required = true) Long customerId)
            throws HttpServiceException, Exception {

        Locale locale = httpServletRequest.getLocale();

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ResponseEntity<MessageEnvelope> searchBill(HttpServletRequest httpServletRequest, @RequestParam(value = "billNumber", required = false) String billNumber,
                                                      @RequestParam(value = "sadadNumber", required = false) String sadadNumber,
                                                      @RequestParam(value = "serviceName", required = false) String serviceName,
                                                      @RequestParam(value = "issueDateFrom", required = false) String issueDateFrom,
                                                      @RequestParam(value = "issueDateTo", required = false) String issueDateTo,
                                                      @RequestParam(value = "customerFullName", required = false) String customerFullName,
                                                      @RequestParam(value = "customerMobileNumber", required = false) String customerMobileNumber,
                                                      @RequestParam(value = "customerEmailAddress", required = false) String customerEmailAddress,
                                                      @RequestParam(value = "customerIdNumber", required = false) String customerIdNumber,
                                                      @RequestParam(value = "billStatus", required = false) List<String> billStatusList,
                                                      @RequestParam(value = "billType", required = false) String billType,
                                                      @RequestParam(value = "totalAmount", required = false) String totalAmount,
                                                      @RequestParam(value = "page", required = false) Integer page,
                                                      @RequestParam(value = "size", required = false) Integer size
    ) {

        Locale locale = httpServletRequest.getLocale();

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public ResponseEntity<MessageEnvelope> findById(HttpServletRequest httpServletRequest,
                                                    @RequestParam(value = "accountId", required = false) Long accountId
    ) {

        Locale locale = httpServletRequest.getLocale();

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


}
