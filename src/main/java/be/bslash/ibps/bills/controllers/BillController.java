package be.bslash.ibps.bills.controllers;

import be.bslash.ibps.bills.configrations.MessageEnvelope;
import be.bslash.ibps.bills.dto.request.AddTemplateRqDto;
import be.bslash.ibps.bills.dto.request.DetailedBillMakeRqDto;
import be.bslash.ibps.bills.dto.request.DetailedBillUpdateRqDto;
import be.bslash.ibps.bills.dto.request.LocalPaymentRqDto;
import be.bslash.ibps.bills.dto.request.MasterBillMakeRqDto;
import be.bslash.ibps.bills.dto.request.MasterBillUpdateRqDto;
import be.bslash.ibps.bills.dto.request.RecurringBillMakeRqDto;
import be.bslash.ibps.bills.dto.request.SadadPaymentRqDto;
import be.bslash.ibps.bills.dto.request.SearchBillRqDto;
import be.bslash.ibps.bills.dto.response.AdCampaignDtoRs;
import be.bslash.ibps.bills.dto.response.AdSettingsDtoRs;
import be.bslash.ibps.bills.dto.response.BillAddDtoRs;
import be.bslash.ibps.bills.dto.response.BillInfoDtoRs;
import be.bslash.ibps.bills.dto.response.BillResultTimelineDtoRs;
import be.bslash.ibps.bills.dto.response.EntityActivityDtoRs;
import be.bslash.ibps.bills.dto.response.EntityFieldDtoRs;
import be.bslash.ibps.bills.dto.response.EntityItemDtoRs;
import be.bslash.ibps.bills.dto.response.EntityVatDtoRs;
import be.bslash.ibps.bills.dto.response.PageDtoRs;
import be.bslash.ibps.bills.dto.response.TemplateDtoRs;
import be.bslash.ibps.bills.entities.Bill;
import be.bslash.ibps.bills.entities.EntityUser;
import be.bslash.ibps.bills.entities.LocalPayment;
import be.bslash.ibps.bills.enums.BillSource;
import be.bslash.ibps.bills.enums.BillStatus;
import be.bslash.ibps.bills.enums.BooleanFlag;
import be.bslash.ibps.bills.enums.EntityUserType;
import be.bslash.ibps.bills.enums.Status;
import be.bslash.ibps.bills.exceptions.HttpServiceException;
import be.bslash.ibps.bills.security.JwtTokenProvider;
import be.bslash.ibps.bills.services.AdCampaignService;
import be.bslash.ibps.bills.services.AdSettingsService;
import be.bslash.ibps.bills.services.BillNotificationService;
import be.bslash.ibps.bills.services.BillService;
import be.bslash.ibps.bills.services.EntityActivityService;
import be.bslash.ibps.bills.services.EntityFieldService;
import be.bslash.ibps.bills.services.EntityItemService;
import be.bslash.ibps.bills.services.EntityUserService;
import be.bslash.ibps.bills.services.EntityVatService;
import be.bslash.ibps.bills.services.LocalPaymentService;
import be.bslash.ibps.bills.services.OldSystemPaymentService;
import be.bslash.ibps.bills.services.RecurringTemplateService;
import be.bslash.ibps.bills.services.SadadPaymentService;
import be.bslash.ibps.bills.utils.AES256;
import be.bslash.ibps.bills.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

@CrossOrigin
@RestController
@RequestMapping("/bill")
public class BillController {

    static Logger LOGGER = LoggerFactory.getLogger(BillController.class);

    @Autowired
    private BillService billService;

    @Autowired
    private EntityUserService entityUserService;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private BillNotificationService billNotificationService;

    @Autowired
    private LocalPaymentService localPaymentService;

    @Autowired
    private EntityVatService entityVatService;

    @Autowired
    private EntityFieldService entityFieldService;

    @Autowired
    private EntityActivityService entityActivityService;

    @Autowired
    private SadadPaymentService sadadPaymentService;

    @Autowired
    private OldSystemPaymentService oldSystemPaymentService;

    @Autowired
    private EntityItemService entityItemService;

    @Autowired
    AdSettingsService adSettingsService;

    @Autowired
    AdCampaignService adCampaignService;

    @Autowired
    private AES256 aes256;

    @Autowired
    private RecurringTemplateService recurringTemplateService;

    @RequestMapping(value = "/master/make", method = RequestMethod.POST)
    public ResponseEntity<MessageEnvelope> makeMasterBill(HttpServletRequest httpServletRequest,
                                                          @RequestBody MasterBillMakeRqDto masterBillMakeRqDto) {
        Locale locale = httpServletRequest.getLocale();

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        masterBillMakeRqDto.validate(locale);

        Bill bill = billService.saveMasterBill(masterBillMakeRqDto, entityUser, BillSource.USER_INTERFACE, locale);

        BillAddDtoRs billAddDtoRs = new BillAddDtoRs();
        billAddDtoRs.setBillId(bill.getId());

        if (entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN_PLUS)
                || entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN)
                || entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR_PLUS)) {
            new Thread() {
                public void run() {
                    billNotificationService.sendBillNotificationIssue(bill, entityUser, locale);
                }
            }.start();
        }

        new Thread() {
            public void run() {
                billNotificationService.sendFcmNotificationBillIssue(bill, entityUser, locale);
            }
        }.start();

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", billAddDtoRs, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/detailed/make", method = RequestMethod.POST)
    public ResponseEntity<MessageEnvelope> makeDetailedBill(HttpServletRequest httpServletRequest,
                                                            @RequestBody DetailedBillMakeRqDto detailedBillMakeRqDto) {
        Locale locale = httpServletRequest.getLocale();

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        detailedBillMakeRqDto.validate(locale);

        Bill bill = billService.saveDetailedBill(detailedBillMakeRqDto, entityUser, BillSource.USER_INTERFACE, locale);

        BillAddDtoRs billAddDtoRs = new BillAddDtoRs();
        billAddDtoRs.setBillId(bill.getId());

        if (entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN_PLUS)
                || entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN)
                || entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR_PLUS)) {
            new Thread() {
                public void run() {
                    billNotificationService.sendBillNotificationIssue(bill, entityUser, locale);
                }
            }.start();
        }

        new Thread() {
            public void run() {
                billNotificationService.sendFcmNotificationBillIssue(bill, entityUser, locale);
            }
        }.start();

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", billAddDtoRs, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/master/update", method = RequestMethod.POST)
    public ResponseEntity<MessageEnvelope> updateMasterBill(HttpServletRequest httpServletRequest,
                                                            @RequestBody MasterBillUpdateRqDto masterBillUpdateRqDto) {
        Locale locale = httpServletRequest.getLocale();

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        masterBillUpdateRqDto.validate(locale);

        Bill billOld = billService.findBillById(masterBillUpdateRqDto.getBillId(), locale);
        BigDecimal oldTotalAmount = billOld.getTotalAmount();

        Bill bill = billService.updateMasterBill(masterBillUpdateRqDto, entityUser, locale);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

//        if(oldTotalAmount.compareTo(bill.getTotalAmount()) != 0){
//        	if (entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN_PLUS)
//                    || entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN)
//                    || entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR_PLUS)) {
        new Thread() {
            public void run() {
                billNotificationService.sendBillNotificationIssueUpdate(bill, entityUser, locale);
            }
        }.start();
//            }
//        }

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/detailed/update", method = RequestMethod.POST)
    public ResponseEntity<MessageEnvelope> updateDetailedBill(HttpServletRequest httpServletRequest,
                                                              @RequestBody DetailedBillUpdateRqDto detailedBillUpdateRqDto) {
        Locale locale = httpServletRequest.getLocale();

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        detailedBillUpdateRqDto.validate(locale);

        Bill billOld = billService.findBillById(detailedBillUpdateRqDto.getBillId(), locale);
        BigDecimal oldTotalAmount = billOld.getTotalAmount();

        Bill bill = billService.updateDetailedBill(detailedBillUpdateRqDto, entityUser, locale);

//        if(oldTotalAmount.compareTo(bill.getTotalAmount()) != 0){
//        	if (entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN_PLUS)
//                    || entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN)
//                    || entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR_PLUS)) {
        new Thread() {
            public void run() {
                billNotificationService.sendBillNotificationIssueUpdate(bill, entityUser, locale);
            }
        }.start();
//            }
//        }

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/check", method = RequestMethod.PUT)
    public ResponseEntity<MessageEnvelope> checkBill(HttpServletRequest httpServletRequest,
                                                     @RequestParam(value = "billId", required = false) Long billId) {
        Locale locale = httpServletRequest.getLocale();

        Map<String, String[]> searchParameterMap = httpServletRequest.getParameterMap();

        Set<String> keys = searchParameterMap.keySet();

        for (String key : keys) {
            if (!key.equals("billId")) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request_search_parameter", locale);
            }
        }

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        if (Utils.isNullOrEmpty(billId)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "missing_bill_id", locale);
        }

        Bill bill = billService.checkBill(billId, entityUser, locale);

        new Thread() {
            public void run() {
                billNotificationService.sendBillNotificationIssue(bill, entityUser, locale);
            }
        }.start();

        new Thread() {
            public void run() {
                billNotificationService.sendFcmNotificationBillIssue(bill, entityUser, locale);
            }
        }.start();

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/payment", method = RequestMethod.POST)
    public ResponseEntity<MessageEnvelope> paykBill(HttpServletRequest httpServletRequest,
                                                    @RequestBody LocalPaymentRqDto localPaymentRqDto) {
        Locale locale = httpServletRequest.getLocale();

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        localPaymentRqDto.validate(locale);

        LocalPayment localPayment = localPaymentService.saveLocalPayment(localPaymentRqDto, entityUser, locale);

        Bill bill = localPayment.getBill();
        bill.setPaymentId(localPayment.getSadadPaymentId());

        new Thread() {
            public void run() {
                billNotificationService.sendFcmNotificationBillPayment(localPayment, entityUser, locale);
            }
        }.start();

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/cancel", method = RequestMethod.PUT)
    public ResponseEntity<MessageEnvelope> cancelBill(HttpServletRequest httpServletRequest,
                                                      @RequestParam(value = "billId", required = false) Long billId) {
        Locale locale = httpServletRequest.getLocale();

        Map<String, String[]> searchParameterMap = httpServletRequest.getParameterMap();

        Set<String> keys = searchParameterMap.keySet();

        for (String key : keys) {
            if (!key.equals("billId")) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request_search_parameter", locale);
            }
        }

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        if (Utils.isNullOrEmpty(billId)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "missing_bill_id", locale);
        }

        Bill bill = billService.cancelBill(billId, entityUser, locale);

        new Thread() {
            public void run() {
                billNotificationService.sendFcmNotificationBillCancel(bill, entityUser, locale);
            }
        }.start();

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public ResponseEntity<MessageEnvelope> deleteBill(HttpServletRequest httpServletRequest,
                                                      @RequestParam(value = "billId", required = false) Long billId) {
        Locale locale = httpServletRequest.getLocale();

        Map<String, String[]> searchParameterMap = httpServletRequest.getParameterMap();

        Set<String> keys = searchParameterMap.keySet();

        for (String key : keys) {
            if (!key.equals("billId")) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request_search_parameter", locale);
            }
        }

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        if (Utils.isNullOrEmpty(billId)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "missing_bill_id", locale);
        }

        Bill bill = billService.deleteBill(billId, entityUser, locale);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/reminder", method = RequestMethod.PUT)
    public ResponseEntity<MessageEnvelope> sendReminder(HttpServletRequest httpServletRequest,
                                                        @RequestParam(value = "billId", required = false) Long billId) {
        Locale locale = httpServletRequest.getLocale();

        Map<String, String[]> searchParameterMap = httpServletRequest.getParameterMap();

        Set<String> keys = searchParameterMap.keySet();

        for (String key : keys) {
            if (!key.equals("billId")) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request_search_parameter", locale);
            }
        }

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        if (Utils.isNullOrEmpty(billId)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "missing_bill_id", locale);
        }

        Bill bill = billService.findBillById(billId, locale);

        if (bill.getBillStatus().equals(BillStatus.DELETED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
        }

        if (!billService.isBillStatusExist(bill, BillStatus.CHECKED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "cannot_send_reminder_for_non_checked_bill", locale);
        }

        if (billService.isBillStatusExist(bill, BillStatus.CANCELED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "cannot_send_reminder_for_canceled_bill", locale);
        }

        if (billService.isBillStatusExist(bill, BillStatus.PAID_BY_SADAD)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "cannot_send_reminder_for_paid_bill", locale);
        }

        if (billService.isBillStatusExist(bill, BillStatus.PAID_BY_COMPANY)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "cannot_send_reminder_for_paid_bill", locale);
        }

        new Thread() {
            public void run() {
                billNotificationService.sendBillNotificationReminder(bill, entityUser, locale);
            }
        }.start();

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/bulkReminder", method = RequestMethod.PUT)
    public ResponseEntity<MessageEnvelope> sendReminderBulk(HttpServletRequest httpServletRequest,
                                                            @RequestParam(value = "billIdList", required = false) List<Long> billIdList) {
        Locale locale = httpServletRequest.getLocale();

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        if (Utils.isNullOrEmpty(billIdList)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "missing_bill_id", locale);
        }

        for (Long billId : billIdList) {

            Bill bill = billService.findBillById(billId, locale);

            if (bill.getBillStatus().equals(BillStatus.DELETED)) {
                continue;
            }

            if (!billService.isBillStatusExist(bill, BillStatus.CHECKED)) {
                continue;
            }

            if (billService.isBillStatusExist(bill, BillStatus.CANCELED)) {
                continue;
            }

            if (billService.isBillStatusExist(bill, BillStatus.PAID_BY_SADAD)) {
                continue;
            }

            if (billService.isBillStatusExist(bill, BillStatus.PAID_BY_COMPANY)) {
                continue;
            }

            new Thread() {
                public void run() {
                    billNotificationService.sendBillNotificationReminder(bill, entityUser, locale);
                }
            }.start();
        }

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public ResponseEntity<MessageEnvelope> findBillInfo(HttpServletRequest httpServletRequest,
                                                        @RequestParam(value = "billId", required = false) Long billId) {
        Locale locale = httpServletRequest.getLocale();

        Map<String, String[]> searchParameterMap = httpServletRequest.getParameterMap();

        Set<String> keys = searchParameterMap.keySet();

        for (String key : keys) {
            if (!key.equals("billId")) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request_search_parameter", locale);
            }
        }

        if (Utils.isNullOrEmpty(billId)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "missing_bill_id", locale);
        }

        BillInfoDtoRs billInfoDtoRs = billService.findBillDetail(billId, locale);

        try {
            AdSettingsDtoRs adSettingsDtoRs = adSettingsService.getAdCampaignSettings(billInfoDtoRs.getEntity().getId(), locale);

            AdCampaignDtoRs adCampaignDtoRs = null;
            if (billInfoDtoRs.getCampaignId() == null) {
                adCampaignDtoRs = adCampaignService.getAdCampaign(Utils.parseLong(adSettingsDtoRs.getMasterCampaignId() + ""), locale);
            } else {
                adCampaignDtoRs = adCampaignService.getAdCampaign(Utils.parseLong(billInfoDtoRs.getCampaignId() + ""), locale);
            }

            billInfoDtoRs.setAdSettings(adSettingsDtoRs);
            billInfoDtoRs.setAdCampaign(adCampaignDtoRs);
            billInfoDtoRs.setAdCampaign(adCampaignDtoRs);
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
        }

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", billInfoDtoRs, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/timeline", method = RequestMethod.GET)
    public ResponseEntity<MessageEnvelope> findBillTimeline(HttpServletRequest httpServletRequest,
                                                            @RequestParam(value = "billId", required = false) Long billId) {
        Locale locale = httpServletRequest.getLocale();

        Map<String, String[]> searchParameterMap = httpServletRequest.getParameterMap();

        Set<String> keys = searchParameterMap.keySet();

        for (String key : keys) {
            if (!key.equals("billId")) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request_search_parameter", locale);
            }
        }

        if (Utils.isNullOrEmpty(billId)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "missing_bill_id", locale);
        }

        BillResultTimelineDtoRs billResultTimelineDtoRs = billService.findBillTimeline(billId, locale);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", billResultTimelineDtoRs, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/html", method = RequestMethod.GET)
    public ResponseEntity<String> htmlView(HttpServletRequest httpServletRequest,
                                           HttpServletResponse httpServletResponse,
                                           @RequestParam(value = "billNumber", required = false) String billNumber) {
        Locale locale = httpServletRequest.getLocale();

        Map<String, String[]> searchParameterMap = httpServletRequest.getParameterMap();

        Set<String> keys = searchParameterMap.keySet();

        for (String key : keys) {
            if (!key.equals("billNumber")) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request_search_parameter", locale);
            }
        }

        if (Utils.isNullOrEmpty(billNumber)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "missing_bill_number", locale);
        }

        billNumber = billNumber.replaceAll("\\s", "+");

        String billNumberDecoded = aes256.decrypt(billNumber);

        if (billNumberDecoded == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "missing_bill_number", locale);
        }

        Bill bill = billService.findBillBySadadNumber(billNumberDecoded, Locale.ENGLISH);

        if (bill == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
        }

        if (bill.getBillStatus().equals(BillStatus.DELETED) || bill.getBillStatus().equals(BillStatus.CANCELED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
        }

        String htmlContents = billService.findHtmlContentForBill(bill, locale);

        billService.updateStatusToViewed(bill.getId(), locale);

        if (htmlContents != null && !htmlContents.isEmpty()) {
            httpServletResponse.resetBuffer();
            httpServletResponse.setCharacterEncoding("UTF-8");
            httpServletResponse.setStatus(HttpServletResponse.SC_OK);
            httpServletResponse.setHeader("Content-Type", "text/html; charset=UTF-8");
            try {
                httpServletResponse.getWriter().print(htmlContents);
                httpServletResponse.flushBuffer();
            } catch (Exception ex) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
            }

        } else {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
        }

        return null;
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ResponseEntity<MessageEnvelope> searchBill(HttpServletRequest httpServletRequest,
                                                      @RequestParam(value = "billNumber", required = false) String billNumber,
                                                      @RequestParam(value = "sadadNumber", required = false) String sadadNumber,
                                                      @RequestParam(value = "serviceName", required = false) String serviceName,
                                                      @RequestParam(value = "issueDateFrom", required = false) String issueDateFrom,
                                                      @RequestParam(value = "issueDateTo", required = false) String issueDateTo,
                                                      @RequestParam(value = "customerFullName", required = false) String customerFullName,
                                                      @RequestParam(value = "customerMobileNumber", required = false) String customerMobileNumber,
                                                      @RequestParam(value = "customerEmailAddress", required = false) String customerEmailAddress,
                                                      @RequestParam(value = "customerIdNumber", required = false) String customerIdNumber,
                                                      @RequestParam(value = "billStatus", required = false) List<String> billStatusList,
                                                      @RequestParam(value = "billType", required = false) String billType,
                                                      @RequestParam(value = "totalAmount", required = false) String totalAmount,
                                                      @RequestParam(value = "page", required = false) Integer page,
                                                      @RequestParam(value = "size", required = false) Integer size) {

        Locale locale = httpServletRequest.getLocale();

        Map<String, String[]> searchParameterMap = httpServletRequest.getParameterMap();

        Set<String> keys = searchParameterMap.keySet();

        for (String key : keys) {
            if (!key.equals("billNumber")
                    && !key.equals("sadadNumber")
                    && !key.equals("serviceName")
                    && !key.equals("issueDateFrom")
                    && !key.equals("issueDateTo")
                    && !key.equals("customerFullName")
                    && !key.equals("customerMobileNumber")
                    && !key.equals("customerEmailAddress")
                    && !key.equals("customerIdNumber")
                    && !key.equals("billStatus")
                    && !key.equals("billType")
                    && !key.equals("totalAmount")
                    && !key.equals("page")
                    && !key.equals("size")) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request_search_parameter", locale);
            }
        }

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        SearchBillRqDto searchBillRqDto = new SearchBillRqDto();
        searchBillRqDto.setBillNumber(billNumber);
        searchBillRqDto.setSadadNumber(sadadNumber);
        searchBillRqDto.setServiceName(serviceName);
        searchBillRqDto.setIssueDateFrom(issueDateFrom);
        searchBillRqDto.setIssueDateTo(issueDateTo);
        searchBillRqDto.setCustomerFullName(customerFullName);
        searchBillRqDto.setCustomerMobileNumber(customerMobileNumber);
        searchBillRqDto.setCustomerEmailAddress(customerEmailAddress);
        searchBillRqDto.setCustomerIdNumber(customerIdNumber);
        searchBillRqDto.setBillStatusList(billStatusList);
        searchBillRqDto.setBillType(billType);
        searchBillRqDto.setTotalAmount(Utils.convertToBigDecimal(totalAmount));
        searchBillRqDto.setPage(page);
        searchBillRqDto.setSize(size);

        PageDtoRs pageDtoRs = billService.searchBill(searchBillRqDto, entityUser, locale);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", pageDtoRs, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/vat", method = RequestMethod.GET)
    public ResponseEntity<MessageEnvelope> findEntityVat(HttpServletRequest httpServletRequest) {

        Locale locale = httpServletRequest.getLocale();

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        List<EntityVatDtoRs> entityVatDtoRsList = entityVatService.findEntityVatByEntity(entityUser.getEntity(), locale);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", entityVatDtoRsList, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


    @RequestMapping(value = "/entity/activity", method = RequestMethod.GET)
    public ResponseEntity<MessageEnvelope> findEntityActivity(HttpServletRequest httpServletRequest) {

        Locale locale = httpServletRequest.getLocale();

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        List<EntityActivityDtoRs> entityActivityDtoRsList = entityActivityService.findAllByEntity(entityUser.getEntity(), locale);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", entityActivityDtoRsList, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/entity/field", method = RequestMethod.GET)
    public ResponseEntity<MessageEnvelope> findEntityField(HttpServletRequest httpServletRequest) {

        Locale locale = httpServletRequest.getLocale();

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        List<EntityFieldDtoRs> entityFieldDtoRsList = entityFieldService.findAllByEntity(entityUser.getEntity(), locale);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", entityFieldDtoRsList, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


    @RequestMapping(value = "/exportPdf", method = RequestMethod.GET)
    public ResponseEntity<MessageEnvelope> exportPdf(HttpServletRequest httpServletRequest,
                                                     @RequestParam(value = "billNumber", required = false) String billNumber,
                                                     @RequestParam(value = "sadadNumber", required = false) String sadadNumber,
                                                     @RequestParam(value = "serviceName", required = false) String serviceName,
                                                     @RequestParam(value = "issueDateFrom", required = false) String issueDateFrom,
                                                     @RequestParam(value = "issueDateTo", required = false) String issueDateTo,
                                                     @RequestParam(value = "customerFullName", required = false) String customerFullName,
                                                     @RequestParam(value = "customerMobileNumber", required = false) String customerMobileNumber,
                                                     @RequestParam(value = "customerEmailAddress", required = false) String customerEmailAddress,
                                                     @RequestParam(value = "customerIdNumber", required = false) String customerIdNumber,
                                                     @RequestParam(value = "billStatus", required = false) List<String> billStatusList,
                                                     @RequestParam(value = "billType", required = false) String billType,
                                                     @RequestParam(value = "totalAmount", required = false) String totalAmount) {

        Locale locale = httpServletRequest.getLocale();

        Map<String, String[]> searchParameterMap = httpServletRequest.getParameterMap();

        Set<String> keys = searchParameterMap.keySet();

        for (String key : keys) {
            if (!key.equals("billNumber")
                    && !key.equals("sadadNumber")
                    && !key.equals("serviceName")
                    && !key.equals("issueDateFrom")
                    && !key.equals("issueDateTo")
                    && !key.equals("customerFullName")
                    && !key.equals("customerMobileNumber")
                    && !key.equals("customerEmailAddress")
                    && !key.equals("customerIdNumber")
                    && !key.equals("billStatus")
                    && !key.equals("billType")
                    && !key.equals("totalAmount")) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request_search_parameter", locale);
            }
        }

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        SearchBillRqDto searchBillRqDto = new SearchBillRqDto();
        searchBillRqDto.setBillNumber(billNumber);
        searchBillRqDto.setSadadNumber(sadadNumber);
        searchBillRqDto.setServiceName(serviceName);
        searchBillRqDto.setIssueDateFrom(issueDateFrom);
        searchBillRqDto.setIssueDateTo(issueDateTo);
        searchBillRqDto.setCustomerFullName(customerFullName);
        searchBillRqDto.setCustomerMobileNumber(customerMobileNumber);
        searchBillRqDto.setCustomerEmailAddress(customerEmailAddress);
        searchBillRqDto.setCustomerIdNumber(customerIdNumber);
        searchBillRqDto.setBillStatusList(billStatusList);
        searchBillRqDto.setBillType(billType);
        searchBillRqDto.setTotalAmount(Utils.convertToBigDecimal(totalAmount));

        String pdfUrl = billService.exportPdf(searchBillRqDto, entityUser, locale);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", pdfUrl, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


    @RequestMapping(value = "/exportExcel", method = RequestMethod.GET)
    public ResponseEntity<MessageEnvelope> exportExcel(HttpServletRequest httpServletRequest,
                                                       @RequestParam(value = "billNumber", required = false) String billNumber,
                                                       @RequestParam(value = "sadadNumber", required = false) String sadadNumber,
                                                       @RequestParam(value = "serviceName", required = false) String serviceName,
                                                       @RequestParam(value = "issueDateFrom", required = false) String issueDateFrom,
                                                       @RequestParam(value = "issueDateTo", required = false) String issueDateTo,
                                                       @RequestParam(value = "customerFullName", required = false) String customerFullName,
                                                       @RequestParam(value = "customerMobileNumber", required = false) String customerMobileNumber,
                                                       @RequestParam(value = "customerEmailAddress", required = false) String customerEmailAddress,
                                                       @RequestParam(value = "customerIdNumber", required = false) String customerIdNumber,
                                                       @RequestParam(value = "billStatus", required = false) List<String> billStatusList,
                                                       @RequestParam(value = "billType", required = false) String billType,
                                                       @RequestParam(value = "totalAmount", required = false) String totalAmount) {

        Locale locale = httpServletRequest.getLocale();

        Map<String, String[]> searchParameterMap = httpServletRequest.getParameterMap();

        Set<String> keys = searchParameterMap.keySet();

        for (String key : keys) {
            if (!key.equals("billNumber")
                    && !key.equals("sadadNumber")
                    && !key.equals("serviceName")
                    && !key.equals("issueDateFrom")
                    && !key.equals("issueDateTo")
                    && !key.equals("customerFullName")
                    && !key.equals("customerMobileNumber")
                    && !key.equals("customerEmailAddress")
                    && !key.equals("customerIdNumber")
                    && !key.equals("billStatus")
                    && !key.equals("billType")
                    && !key.equals("totalAmount")) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request_search_parameter", locale);
            }
        }

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        SearchBillRqDto searchBillRqDto = new SearchBillRqDto();
        searchBillRqDto.setBillNumber(billNumber);
        searchBillRqDto.setSadadNumber(sadadNumber);
        searchBillRqDto.setServiceName(serviceName);
        searchBillRqDto.setIssueDateFrom(issueDateFrom);
        searchBillRqDto.setIssueDateTo(issueDateTo);
        searchBillRqDto.setCustomerFullName(customerFullName);
        searchBillRqDto.setCustomerMobileNumber(customerMobileNumber);
        searchBillRqDto.setCustomerEmailAddress(customerEmailAddress);
        searchBillRqDto.setCustomerIdNumber(customerIdNumber);
        searchBillRqDto.setBillStatusList(billStatusList);
        searchBillRqDto.setBillType(billType);
        searchBillRqDto.setTotalAmount(Utils.convertToBigDecimal(totalAmount));

        String excelUrl = billService.exportExcel(searchBillRqDto, entityUser, locale);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", excelUrl, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


    @RequestMapping(value = "/sadadPayment", method = RequestMethod.POST)
    public ResponseEntity<MessageEnvelope> sadadPayment(HttpServletRequest httpServletRequest,
                                                        @RequestBody SadadPaymentRqDto sadadPaymentRqDto) {

        Locale locale = httpServletRequest.getLocale();

        Integer sadadPayment = sadadPaymentService.saveSadadPayment(sadadPaymentRqDto, locale);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


    @RequestMapping(value = "/setApproveDate", method = RequestMethod.POST)
    public ResponseEntity<MessageEnvelope> sadadPayment(HttpServletRequest httpServletRequest) {

        Locale locale = httpServletRequest.getLocale();

        billService.setApprovedDateBySadad();

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


    @RequestMapping(value = "/oldSystemPayment", method = RequestMethod.GET)
    public ResponseEntity<MessageEnvelope> searchBill(HttpServletRequest httpServletRequest,
                                                      @RequestParam(value = "page", required = false) Integer page,
                                                      @RequestParam(value = "size", required = false) Integer size) {

        Locale locale = httpServletRequest.getLocale();

        Map<String, String[]> searchParameterMap = httpServletRequest.getParameterMap();

        Set<String> keys = searchParameterMap.keySet();

//        for(String key: keys){
//            if(!key.equals("page")
//                    && !key.equals("sizs")){
//                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request_search_parameter", locale);
//            }
//        }

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        PageDtoRs pageDtoRs = oldSystemPaymentService.findAllByEntity(entityUser.getEntity(), page, size, locale);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", pageDtoRs, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


    @RequestMapping(value = "/findBillsByFileId", method = RequestMethod.GET)
    public ResponseEntity<MessageEnvelope> findBillsByFileId(HttpServletRequest httpServletRequest,
                                                             @RequestParam(value = "fileId", required = false) Long fileId,
                                                             @RequestParam(value = "page", required = false) Integer page,
                                                             @RequestParam(value = "size", required = false) Integer size) {
        Locale locale = httpServletRequest.getLocale();

        Map<String, String[]> searchParameterMap = httpServletRequest.getParameterMap();

        Set<String> keys = searchParameterMap.keySet();

        for (String key : keys) {
            if (!key.equals("fileId")
                    && !key.equals("page")
                    && !key.equals("size")) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request_search_parameter", locale);
            }
        }

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        if (Utils.isNullOrEmpty(fileId)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "missing_bill_id", locale);
        }

        PageDtoRs pageDtoRs = billService.findBillsByFileId(fileId, entityUser, page, size, locale);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", pageDtoRs, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


    @RequestMapping(value = "/generateBillNumber", method = RequestMethod.GET)
    public ResponseEntity<MessageEnvelope> searchBill(HttpServletRequest httpServletRequest) {

        Locale locale = httpServletRequest.getLocale();

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        String generatedBillNumber = "";
        if (entityUser.getEntity().getIsAutoGenerateBillNumber().equals(BooleanFlag.YES)) {
            generatedBillNumber = billService.generateBillNumber(entityUser, locale);
        }

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", generatedBillNumber, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/generateBillNumberForInstallment", method = RequestMethod.GET)
    public ResponseEntity<MessageEnvelope> generateBillNumberForInstallment(HttpServletRequest httpServletRequest,
                                                                            @RequestParam(value = "numberOfInstallment", required = false) Integer numberOfInstallment) {

        Locale locale = httpServletRequest.getLocale();

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        List<String> generatedBillNumberList = billService.generateBillNumber(numberOfInstallment, entityUser, locale);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", generatedBillNumberList, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/items", method = RequestMethod.GET)
    public ResponseEntity<MessageEnvelope> findEntityItems(HttpServletRequest httpServletRequest) {

        Locale locale = httpServletRequest.getLocale();

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        List<EntityItemDtoRs> entityItemDtoRsRsList = entityItemService.findEntityItemByEntity(entityUser.getEntity(), locale);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", entityItemDtoRsRsList, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public ResponseEntity<MessageEnvelope> findBillInfoPublic(HttpServletRequest httpServletRequest,
                                                              @RequestParam(value = "sadadNumber", required = false) String sadadNumber) {
        Locale locale = httpServletRequest.getLocale();

        Map<String, String[]> searchParameterMap = httpServletRequest.getParameterMap();

        Set<String> keys = searchParameterMap.keySet();

        for (String key : keys) {
            if (!key.equals("sadadNumber")) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_request_search_parameter", locale);
            }
        }

        if (Utils.isNullOrEmpty(sadadNumber)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "missing_sadad_number", locale);
        }

        sadadNumber = sadadNumber.replaceAll("\\s", "+");

        String billNumberDecoded = aes256.decrypt(sadadNumber);

        if (billNumberDecoded == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "missing_bill_number", locale);
        }

        BillInfoDtoRs billInfoDtoRs = billService.findBillDetail(billNumberDecoded, locale);

        try {
            AdSettingsDtoRs adSettingsDtoRs = adSettingsService.getAdCampaignSettings(billInfoDtoRs.getEntity().getId(), locale);

            AdCampaignDtoRs adCampaignDtoRs = null;
            if (billInfoDtoRs.getCampaignId() == null) {
                adCampaignDtoRs = adCampaignService.getAdCampaign(Utils.parseLong(adSettingsDtoRs.getMasterCampaignId() + ""), locale);
            } else {
                adCampaignDtoRs = adCampaignService.getAdCampaign(Utils.parseLong(billInfoDtoRs.getCampaignId() + ""), locale);
            }

            billInfoDtoRs.setAdSettings(adSettingsDtoRs);
            billInfoDtoRs.setAdCampaign(adCampaignDtoRs);
        } catch (Exception e) {
            LOGGER.info(e.getMessage());
        }

        billService.updateStatusToViewed(billInfoDtoRs.getId(), locale);


        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", billInfoDtoRs, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


    @RequestMapping(value = "/tax/invoice/add", method = RequestMethod.POST)
    public ResponseEntity<MessageEnvelope> taxInvoiceCreate(HttpServletRequest httpServletRequest,
                                                            @RequestBody DetailedBillMakeRqDto detailedBillMakeRqDto) {
        Locale locale = httpServletRequest.getLocale();

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        detailedBillMakeRqDto.validate(locale);

        Bill bill = billService.saveTaxBill(detailedBillMakeRqDto, entityUser, BillSource.USER_INTERFACE, locale);

        BillAddDtoRs billAddDtoRs = new BillAddDtoRs();
        billAddDtoRs.setBillId(bill.getId());

        if (entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN_PLUS)
                || entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN)
                || entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR_PLUS)) {
            new Thread() {
                public void run() {
                    billNotificationService.sendBillNotificationIssue(bill, entityUser, locale);
                }
            }.start();
        }

        new Thread() {
            public void run() {
                billNotificationService.sendFcmNotificationBillIssue(bill, entityUser, locale);
            }
        }.start();

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", billAddDtoRs, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


    @RequestMapping(value = "/tax/invoice/update", method = RequestMethod.POST)
    public ResponseEntity<MessageEnvelope> taxInvoiceUpdate(HttpServletRequest httpServletRequest,
                                                            @RequestBody DetailedBillUpdateRqDto detailedBillUpdateRqDto) {
        Locale locale = httpServletRequest.getLocale();

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        detailedBillUpdateRqDto.validate(locale);

        Bill billOld = billService.findBillById(detailedBillUpdateRqDto.getBillId(), locale);
        BigDecimal oldTotalAmount = billOld.getTotalAmount();

        Bill bill = billService.updateTaxBill(detailedBillUpdateRqDto, entityUser, locale);

//        if(oldTotalAmount.compareTo(bill.getTotalAmount()) != 0){
//        	if (entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN_PLUS)
//                    || entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN)
//                    || entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR_PLUS)) {
        new Thread() {
            public void run() {
                billNotificationService.sendBillNotificationIssueUpdate(bill, entityUser, locale);
            }
        }.start();
//            }
//        }

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


    @RequestMapping(value = "/recurring/make", method = RequestMethod.POST)
    public ResponseEntity<MessageEnvelope> makeRecurringBill(HttpServletRequest httpServletRequest,
                                                             @RequestBody RecurringBillMakeRqDto recurringBillMakeRqDto) {
        Locale locale = httpServletRequest.getLocale();

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getEntity().getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "entity_inactive", locale);
        }

        if (entityUser.getIsPasswordReset() == 1) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "password_reset_issue", locale);
        }

        recurringBillMakeRqDto.validate(locale);

        Bill bill = billService.saveRecurringBill(recurringBillMakeRqDto, entityUser, BillSource.USER_INTERFACE, locale);

        if (entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN_PLUS)
                || entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN)
                || entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR_PLUS)) {
            new Thread() {
                public void run() {

                    billNotificationService.sendBillNotificationIssue(bill, entityUser, locale);
                }
            }.start();
        }


        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


    @RequestMapping(value = "/recurring/update", method = RequestMethod.POST)
    public ResponseEntity<MessageEnvelope> updateRecurringBill(HttpServletRequest httpServletRequest) {

        Locale locale = httpServletRequest.getLocale();

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/recurring/templates", method = RequestMethod.GET)
    public ResponseEntity<MessageEnvelope> findRecurringTemplates(HttpServletRequest httpServletRequest) {

        Locale locale = httpServletRequest.getLocale();

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        List<TemplateDtoRs> templateDtoRsList = recurringTemplateService.findAllByEntity(entityUser, locale);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", templateDtoRsList, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }

    @RequestMapping(value = "/recurring/accounts", method = RequestMethod.GET)
    public ResponseEntity<MessageEnvelope> findRecurringAccounts(HttpServletRequest httpServletRequest) {

        Locale locale = httpServletRequest.getLocale();

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


    @RequestMapping(value = "/recurring/save/templates", method = RequestMethod.POST)
    public ResponseEntity<MessageEnvelope> saveRecurringTemplates(HttpServletRequest httpServletRequest, @RequestBody AddTemplateRqDto addTemplateRqDto) {

        Locale locale = httpServletRequest.getLocale();

        String token = httpServletRequest.getHeader("Authorization").split(" ")[1];

        String username = jwtTokenProvider.getUsername(token);

        EntityUser entityUser = entityUserService.findEntityUserByUsername(username, locale);

        recurringTemplateService.saveNewTemplates(addTemplateRqDto, entityUser, locale);

        MessageEnvelope messageEnvelope = new MessageEnvelope(HttpStatus.OK, "success", null, locale);

        return new ResponseEntity<>(messageEnvelope, HttpStatus.OK);
    }


}
