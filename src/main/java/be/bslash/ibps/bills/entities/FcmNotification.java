package be.bslash.ibps.bills.entities;


import be.bslash.ibps.bills.enums.BooleanFlag;
import be.bslash.ibps.bills.enums.Language;
import be.bslash.ibps.bills.enums.NotificationStatus;
import be.bslash.ibps.bills.enums.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "fcm_notification")
public class FcmNotification {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fcm_notification_seq")
    @SequenceGenerator(name = "fcm_notification_seq", sequenceName = "fcm_notification_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "title")
    private String title;

    @Lob
    @Column(name = "body")
    private String body;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private NotificationStatus notificationStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "language")
    private Language notificationLanguage;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "user_type")
    private Integer userType;

    @Column(name = "data")
    private String data;

    @Column(name = "create_date")
    private LocalDateTime createDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "is_read_flag")
    private BooleanFlag isReadFlag;

    @Column(name = "category")
    private String category;

    @Enumerated(EnumType.STRING)
    @Column(name = "record_status")
    private Status status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public NotificationStatus getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(NotificationStatus notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public Language getNotificationLanguage() {
        return notificationLanguage;
    }

    public void setNotificationLanguage(Language notificationLanguage) {
        this.notificationLanguage = notificationLanguage;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public BooleanFlag getIsReadFlag() {
        return isReadFlag;
    }

    public void setIsReadFlag(BooleanFlag isReadFlag) {
        this.isReadFlag = isReadFlag;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Status getStatus() {
        if (status == null) {
            return Status.ACTIVE;
        }
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
