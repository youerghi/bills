package be.bslash.ibps.bills.entities;


import be.bslash.ibps.bills.enums.BooleanFlag;
import be.bslash.ibps.bills.enums.EntityType;
import be.bslash.ibps.bills.enums.IdIssueAuthority;
import be.bslash.ibps.bills.enums.IdType;
import be.bslash.ibps.bills.enums.Language;
import be.bslash.ibps.bills.enums.Status;
import be.bslash.ibps.bills.enums.TypeIdDocument;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Locale;

@javax.persistence.Entity
@Table(name = "entity")
public class Entity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "entity_seq")
    @SequenceGenerator(name = "entity_seq", sequenceName = "entity_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Column(name = "register_date")
    private LocalDateTime registerDate;

    @JsonIgnore
    @Column(name = "creation_date")
    private LocalDateTime creationDate;

    @Column(name = "logo")
    private String logo;

    @Column(name = "stamp")
    private String stamp;

    @Column(name = "code")
    private String code;

    @Column(name = "brand_name_ar")
    private String brandNameAr;

    @Column(name = "brand_name_en")
    private String brandNameEn;

    @Column(name = "commercial_name_ar")
    private String commercialNameAr;

    @Column(name = "commercial_name_en")
    private String commercialNameEn;

    @Column(name = "vat_number")
    private String vatNumber;

    @Column(name = "vat_name_ar")
    private String vatNameAr;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_id_document")
    private TypeIdDocument typeIdDocument;

    @Column(name = "other_type_id_document")
    private String otherTypeIdDocument;

    @Column(name = "id_number")
    private String idNumber;

    @Column(name = "id_expiry_date")
    private LocalDateTime idExpiryDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "id_issue_authority")
    private IdIssueAuthority idIssueAuthority;

    @Column(name = "other_issue_authority")
    private String otherIdIssueAuthority;

    @Enumerated(EnumType.STRING)
    @Column(name = "entity_type")
    private EntityType entityType;

    @Column(name = "other_entity_type")
    private String otherEntityType;

    @OneToOne
    @JoinColumn(name = "sector_id")
    private Sector sector;

    @OneToOne
    @JoinColumn(name = "region_id")
    private Region region;

    @OneToOne
    @JoinColumn(name = "city_id")
    private City city;

    @Column(name = "other_city")
    private String otherCity;

    @OneToOne
    @JoinColumn(name = "district_id")
    private District district;

    @Column(name = "other_district")
    private String otherDistrict;

    @Column(name = "street_name")
    private String streetName;

    @Column(name = "entity_unit_number")
    private String entityUnitNumber;

    @Column(name = "building_number")
    private String buildingNumber;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "additional_zip_code")
    private String additionalZipCode;

    @OneToOne
    @JoinColumn(name = "country_id")
    private Country country;

    @Column(name = "other_country")
    private String otherCountry;

    @Enumerated(EnumType.STRING)
    @Column(name = "parent_entity_flag")
    private BooleanFlag parentEntityBooleanFlag;

    @Column(name = "organization_chart_attachment")
    private String ownershipAttachment;

    @Column(name = "organization_structure_attachment")
    private String memberAttachment;

    @Enumerated(EnumType.STRING)
    @Column(name = "subsidiary_flag")
    private BooleanFlag subsidiaryEntityBooleanFlag;

    @Column(name = "subsidiary_attachment")
    private String subsidiaryAttachment;

    @Enumerated(EnumType.STRING)
    @Column(name = "political_person_flag")
    private BooleanFlag politicalPersonBooleanFlag;

    @Column(name = "main_bank_account_number")
    private String mainBankAccountNumber;

    @Column(name = "main_bank_account_iban")
    private String mainBankAccountIban;

    @Column(name = "main_bank_account_type")
    private String mainBankAccountType;

    @Column(name = "main_bank_name")
    private String mainBankName;

    @Column(name = "brief_explanation")
    private String briefExplanation;

    @Enumerated(EnumType.STRING)
    @Column(name = "important_information_flag")
    private BooleanFlag importantInformationBooleanFlag;

    @Column(name = "important_information")
    private String importantInformation;

    @Column(name = "authorizer_name")
    private String applicationFullName;

    @Column(name = "application_date")
    private LocalDateTime applicationDate;

    @Column(name = "job_title")
    private String applicationTitle;

    @Column(name = "bill_sequence")
    private Long billSequence;

    @Enumerated(EnumType.STRING)
    @Column(name = "send_bill_sms_flag")
    private BooleanFlag sendBillSmsBooleanFlag;

    @Enumerated(EnumType.STRING)
    @Column(name = "send_bill_email_flag")
    private BooleanFlag sendBillEmailBooleanFlag;

    @Enumerated(EnumType.STRING)
    @Column(name = "send_bill_lng_flag")
    private Language sendBillLanguage;

    @OneToOne
    @JoinColumn(name = "bank_id")
    private Bank bank;

    @Column(name = "credential_person_full_name")
    private String credentialPersonFullName;

    @Column(name = "credential_person_mobile_number")
    private String credentialPersonMobileNumber;

    @Column(name = "credential_person_mobile_number_code")
    private String credentialPersonMobileNumberCode;

    @Column(name = "credential_person_email")
    private String credentialPersonEmail;

    @Column(name = "credential_person_title")
    private String credentialPersonTitle;

    @OneToOne
    @JoinColumn(name = "credential_person_nationality_id")
    private Nationality credentialPersonNationality;

    @Column(name = "credential_person_id_number")
    private String credentialPersonIdNumber;

    @Column(name = "credential_person_id_expire_date")
    private LocalDateTime credentialPersonIdExpireDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "credential_person_id_type")
    private IdType credentialPersonIdType;

    @Column(name = "dci_commission")
    private BigDecimal dciCommission;

    @Enumerated(EnumType.STRING)
    @Column(name = "maker_flag")
    private BooleanFlag makerFlag;

    @Enumerated(EnumType.STRING)
    @Column(name = "checker_flag")
    private BooleanFlag checkerFlag;

    @Enumerated(EnumType.STRING)
    @Column(name = "ad_module_enable")
    private Status adModuleEnable;

    @Enumerated(EnumType.STRING)
    @Column(name = "vat_exempted_flag", columnDefinition = "nvarchar(255) default 'YES'")
    private BooleanFlag vatExemptedFlag;

    @Enumerated(EnumType.STRING)
    @Column(name = "vat_na_flag", columnDefinition = "nvarchar(255) default 'YES'")
    private BooleanFlag vatNaFlag;

    @Column(name = "payment_notification_url")
    private String paymentNotificationUrl;

    @Column(name = "payment_subscription_fees")
    private BigDecimal paymentSubscriptionFees;

    @Column(name = "integration_fees")
    private BigDecimal integrationFees;

    @Column(name = "api_fees")
    private BigDecimal apiFees;

    @Column(name = "web_portal_fees")
    private BigDecimal webPortalFees;

    @Column(name = "training_fees")
    private BigDecimal trainingFees;

    @Column(name = "insurance_fees")
    private BigDecimal insuranceFees;

    @Column(name = "ad_subscription_fees")
    private BigDecimal adSubscriptionFees;

    @Enumerated(EnumType.STRING)
    @Column(name = "is_updated_flag")
    private BooleanFlag isUpdatedFlag;

    @Enumerated(EnumType.STRING)
    @Column(name = "is_first_activated_flag")
    private BooleanFlag isFirstActivatedFlag;

    @Enumerated(EnumType.STRING)
    @Column(name = "auto_generate_flag", columnDefinition = "nvarchar(255) default 'YES'")
    private BooleanFlag isAutoGenerateBillNumber;

    @Column(name = "sms_point_balance")
    private Long smsPointBalance;

    @Enumerated(EnumType.STRING)
    @Column(name = "is_partial_allowed")
    private BooleanFlag isPartialAllowed;

    @Enumerated(EnumType.STRING)
    @Column(name = "is_recurring_allowed")
    private BooleanFlag isRecurringAllowed;

    @Enumerated(EnumType.STRING)
    @Column(name = "is_notification_allowed")
    private BooleanFlag isEntityNotificationAllowed;

    @Column(name = "discount_default_value")
    private BigDecimal discountDefaultValue;

    @Column(name = "vat_default_value")
    private String vatDefaultValue;

    @Column(name = "mini_partial_default_value")
    private BigDecimal miniPartialDefaultValue;

    @Column(name = "max_partial_default_value")
    private BigDecimal maxPartialDefaultValue;

    @Column(name = "activity_default_value")
    private Long activityDefaultValue;

    @Column(name = "item_service_default_value")
    private String itemServiceDefaultValue;

    @Column(name = "payment_voucher_sequence")
    private Long paymentVoucherSequence;

    @Column(name = "auto_generation_bill_number_sequence")
    private Long autoGenerationBillNumberSequence;

    @Column(name = "other_fees_bill_number_sequence")
    private Long otherFeesBillNumberSequence;

    @Column(name = "account_number_sequence")
    private Long accountNumberSequence;

    @Column(name = "other_balance")
    private BigDecimal otherBalance;

    @Column(name = "color")
    private String color;

    @Column(name = "logo_width")
    private Long logoWidth;

    @Column(name = "stamp_width")
    private Long stampWidth;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public LocalDateTime getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(LocalDateTime registerDate) {
        this.registerDate = registerDate;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBrandNameAr() {
        return brandNameAr;
    }

    public void setBrandNameAr(String brandNameAr) {
        this.brandNameAr = brandNameAr;
    }

    public String getBrandNameEn() {
        return brandNameEn;
    }

    public void setBrandNameEn(String brandNameEn) {
        this.brandNameEn = brandNameEn;
    }

    public String getCommercialNameAr() {
        return commercialNameAr;
    }

    public void setCommercialNameAr(String commercialNameAr) {
        this.commercialNameAr = commercialNameAr;
    }

    public String getCommercialNameEn() {
        return commercialNameEn;
    }

    public void setCommercialNameEn(String commercialNameEn) {
        this.commercialNameEn = commercialNameEn;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public TypeIdDocument getTypeIdDocument() {
        return typeIdDocument;
    }

    public void setTypeIdDocument(TypeIdDocument typeIdDocument) {
        this.typeIdDocument = typeIdDocument;
    }

    public String getOtherTypeIdDocument() {
        return otherTypeIdDocument;
    }

    public void setOtherTypeIdDocument(String otherTypeIdDocument) {
        this.otherTypeIdDocument = otherTypeIdDocument;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public LocalDateTime getIdExpiryDate() {
        return idExpiryDate;
    }

    public void setIdExpiryDate(LocalDateTime idExpiryDate) {
        this.idExpiryDate = idExpiryDate;
    }

    public IdIssueAuthority getIdIssueAuthority() {
        return idIssueAuthority;
    }

    public void setIdIssueAuthority(IdIssueAuthority idIssueAuthority) {
        this.idIssueAuthority = idIssueAuthority;
    }

    public String getOtherIdIssueAuthority() {
        return otherIdIssueAuthority;
    }

    public void setOtherIdIssueAuthority(String otherIdIssueAuthority) {
        this.otherIdIssueAuthority = otherIdIssueAuthority;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public String getOtherEntityType() {
        return otherEntityType;
    }

    public void setOtherEntityType(String otherEntityType) {
        this.otherEntityType = otherEntityType;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getAdditionalZipCode() {
        return additionalZipCode;
    }

    public void setAdditionalZipCode(String additionalZipCode) {
        this.additionalZipCode = additionalZipCode;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public BooleanFlag getParentEntityBooleanFlag() {
        return parentEntityBooleanFlag;
    }

    public void setParentEntityBooleanFlag(BooleanFlag parentEntityBooleanFlag) {
        this.parentEntityBooleanFlag = parentEntityBooleanFlag;
    }

    public String getOwnershipAttachment() {
        return ownershipAttachment;
    }

    public void setOwnershipAttachment(String ownershipAttachment) {
        this.ownershipAttachment = ownershipAttachment;
    }

    public String getMemberAttachment() {
        return memberAttachment;
    }

    public void setMemberAttachment(String memberAttachment) {
        this.memberAttachment = memberAttachment;
    }

    public BooleanFlag getSubsidiaryEntityBooleanFlag() {
        return subsidiaryEntityBooleanFlag;
    }

    public void setSubsidiaryEntityBooleanFlag(BooleanFlag subsidiaryEntityBooleanFlag) {
        this.subsidiaryEntityBooleanFlag = subsidiaryEntityBooleanFlag;
    }

    public String getSubsidiaryAttachment() {
        return subsidiaryAttachment;
    }

    public void setSubsidiaryAttachment(String subsidiaryAttachment) {
        this.subsidiaryAttachment = subsidiaryAttachment;
    }

    public BooleanFlag getPoliticalPersonBooleanFlag() {
        return politicalPersonBooleanFlag;
    }

    public void setPoliticalPersonBooleanFlag(BooleanFlag politicalPersonBooleanFlag) {
        this.politicalPersonBooleanFlag = politicalPersonBooleanFlag;
    }

    public String getMainBankAccountNumber() {
        return mainBankAccountNumber;
    }

    public void setMainBankAccountNumber(String mainBankAccountNumber) {
        this.mainBankAccountNumber = mainBankAccountNumber;
    }

    public String getMainBankAccountIban() {
        return mainBankAccountIban;
    }

    public void setMainBankAccountIban(String mainBankAccountIban) {
        this.mainBankAccountIban = mainBankAccountIban;
    }

    public String getMainBankAccountType() {
        return mainBankAccountType;
    }

    public void setMainBankAccountType(String mainBankAccountType) {
        this.mainBankAccountType = mainBankAccountType;
    }

    public String getMainBankName() {
        return mainBankName;
    }

    public void setMainBankName(String mainBankName) {
        this.mainBankName = mainBankName;
    }

    public String getBriefExplanation() {
        return briefExplanation;
    }

    public void setBriefExplanation(String briefExplanation) {
        this.briefExplanation = briefExplanation;
    }

    public BooleanFlag getImportantInformationBooleanFlag() {
        return importantInformationBooleanFlag;
    }

    public void setImportantInformationBooleanFlag(BooleanFlag importantInformationBooleanFlag) {
        this.importantInformationBooleanFlag = importantInformationBooleanFlag;
    }

    public String getImportantInformation() {
        return importantInformation;
    }

    public void setImportantInformation(String importantInformation) {
        this.importantInformation = importantInformation;
    }

    public String getApplicationFullName() {
        return applicationFullName;
    }

    public void setApplicationFullName(String applicationFullName) {
        this.applicationFullName = applicationFullName;
    }

    public LocalDateTime getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(LocalDateTime applicationDate) {
        this.applicationDate = applicationDate;
    }

    public String getApplicationTitle() {
        return applicationTitle;
    }

    public void setApplicationTitle(String applicationTitle) {
        this.applicationTitle = applicationTitle;
    }

    public String getOtherCity() {
        return otherCity;
    }

    public void setOtherCity(String otherCity) {
        this.otherCity = otherCity;
    }

    public String getOtherDistrict() {
        return otherDistrict;
    }

    public void setOtherDistrict(String otherDistrict) {
        this.otherDistrict = otherDistrict;
    }

    public String getOtherCountry() {
        return otherCountry;
    }

    public void setOtherCountry(String otherCountry) {
        this.otherCountry = otherCountry;
    }

    public Long getBillSequence() {
        return billSequence;
    }

    public void setBillSequence(Long billSequence) {
        this.billSequence = billSequence;
    }

    public BooleanFlag getSendBillSmsBooleanFlag() {
        return sendBillSmsBooleanFlag;
    }

    public void setSendBillSmsBooleanFlag(BooleanFlag sendBillSmsBooleanFlag) {
        this.sendBillSmsBooleanFlag = sendBillSmsBooleanFlag;
    }

    public BooleanFlag getSendBillEmailBooleanFlag() {
        return sendBillEmailBooleanFlag;
    }

    public void setSendBillEmailBooleanFlag(BooleanFlag sendBillEmailBooleanFlag) {
        this.sendBillEmailBooleanFlag = sendBillEmailBooleanFlag;
    }

    public Language getSendBillLanguage() {
        return sendBillLanguage;
    }

    public void setSendBillLanguage(Language sendBillLanguage) {
        this.sendBillLanguage = sendBillLanguage;
    }

    public String getStamp() {
        return stamp;
    }

    public void setStamp(String stamp) {
        this.stamp = stamp;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public String getCredentialPersonFullName() {
        return credentialPersonFullName;
    }

    public void setCredentialPersonFullName(String credentialPersonFullName) {
        this.credentialPersonFullName = credentialPersonFullName;
    }

    public String getCredentialPersonMobileNumber() {
        return credentialPersonMobileNumber;
    }

    public void setCredentialPersonMobileNumber(String credentialPersonMobileNumber) {
        this.credentialPersonMobileNumber = credentialPersonMobileNumber;
    }

    public String getCredentialPersonEmail() {
        return credentialPersonEmail;
    }

    public void setCredentialPersonEmail(String credentialPersonEmail) {
        this.credentialPersonEmail = credentialPersonEmail;
    }

    public String getCredentialPersonTitle() {
        return credentialPersonTitle;
    }

    public void setCredentialPersonTitle(String credentialPersonTitle) {
        this.credentialPersonTitle = credentialPersonTitle;
    }

    public Nationality getCredentialPersonNationality() {
        return credentialPersonNationality;
    }

    public void setCredentialPersonNationality(Nationality credentialPersonNationality) {
        this.credentialPersonNationality = credentialPersonNationality;
    }

    public String getCredentialPersonIdNumber() {
        return credentialPersonIdNumber;
    }

    public void setCredentialPersonIdNumber(String credentialPersonIdNumber) {
        this.credentialPersonIdNumber = credentialPersonIdNumber;
    }

    public LocalDateTime getCredentialPersonIdExpireDate() {
        return credentialPersonIdExpireDate;
    }

    public void setCredentialPersonIdExpireDate(LocalDateTime credentialPersonIdExpireDate) {
        this.credentialPersonIdExpireDate = credentialPersonIdExpireDate;
    }

    public BigDecimal getDciCommission() {
        return dciCommission;
    }

    public void setDciCommission(BigDecimal dciCommission) {
        this.dciCommission = dciCommission;
    }

    public BooleanFlag getMakerFlag() {
        return makerFlag;
    }

    public void setMakerFlag(BooleanFlag makerFlag) {
        this.makerFlag = makerFlag;
    }

    public BooleanFlag getCheckerFlag() {
        return checkerFlag;
    }

    public void setCheckerFlag(BooleanFlag checkerFlag) {
        this.checkerFlag = checkerFlag;
    }

    public Status getAdModuleEnable() {
        return adModuleEnable;
    }

    public void setAdModuleEnable(Status adModuleEnable) {
        this.adModuleEnable = adModuleEnable;
    }

    public BooleanFlag getVatExemptedFlag() {
        return vatExemptedFlag;
    }

    public void setVatExemptedFlag(BooleanFlag vatExemptedFlag) {
        this.vatExemptedFlag = vatExemptedFlag;
    }

    public BooleanFlag getVatNaFlag() {
        return vatNaFlag;
    }

    public void setVatNaFlag(BooleanFlag vatNaFlag) {
        this.vatNaFlag = vatNaFlag;
    }

    public String getPaymentNotificationUrl() {
        return paymentNotificationUrl;
    }

    public void setPaymentNotificationUrl(String paymentNotificationUrl) {
        this.paymentNotificationUrl = paymentNotificationUrl;
    }

    public String getVatNameAr() {
        return vatNameAr;
    }

    public void setVatNameAr(String vatNameAr) {
        this.vatNameAr = vatNameAr;
    }

    public Sector getSector() {
        return sector;
    }

    public void setSector(Sector sector) {
        this.sector = sector;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public String getEntityUnitNumber() {
        return entityUnitNumber;
    }

    public void setEntityUnitNumber(String entityUnitNumber) {
        this.entityUnitNumber = entityUnitNumber;
    }

    public IdType getCredentialPersonIdType() {
        return credentialPersonIdType;
    }

    public void setCredentialPersonIdType(IdType credentialPersonIdType) {
        this.credentialPersonIdType = credentialPersonIdType;
    }

    public BigDecimal getPaymentSubscriptionFees() {
        return paymentSubscriptionFees;
    }

    public void setPaymentSubscriptionFees(BigDecimal paymentSubscriptionFees) {
        this.paymentSubscriptionFees = paymentSubscriptionFees;
    }

    public BigDecimal getApiFees() {
        return apiFees;
    }

    public void setApiFees(BigDecimal apiFees) {
        this.apiFees = apiFees;
    }

    public BigDecimal getWebPortalFees() {
        return webPortalFees;
    }

    public void setWebPortalFees(BigDecimal webPortalFees) {
        this.webPortalFees = webPortalFees;
    }

    public BigDecimal getTrainingFees() {
        return trainingFees;
    }

    public void setTrainingFees(BigDecimal trainingFees) {
        this.trainingFees = trainingFees;
    }

    public BigDecimal getInsuranceFees() {
        return insuranceFees;
    }

    public void setInsuranceFees(BigDecimal insuranceFees) {
        this.insuranceFees = insuranceFees;
    }

    public BigDecimal getAdSubscriptionFees() {
        return adSubscriptionFees;
    }

    public void setAdSubscriptionFees(BigDecimal adSubscriptionFees) {
        this.adSubscriptionFees = adSubscriptionFees;
    }

    public BigDecimal getIntegrationFees() {
        return integrationFees;
    }

    public void setIntegrationFees(BigDecimal integrationFees) {
        this.integrationFees = integrationFees;
    }

    public String getCredentialPersonMobileNumberCode() {
        return credentialPersonMobileNumberCode;
    }

    public void setCredentialPersonMobileNumberCode(String credentialPersonMobileNumberCode) {
        this.credentialPersonMobileNumberCode = credentialPersonMobileNumberCode;
    }

    public BooleanFlag getIsUpdatedFlag() {
        return isUpdatedFlag;
    }

    public void setIsUpdatedFlag(BooleanFlag isUpdatedFlag) {
        this.isUpdatedFlag = isUpdatedFlag;
    }

    public BooleanFlag getIsFirstActivatedFlag() {
        return isFirstActivatedFlag;
    }

    public void setIsFirstActivatedFlag(BooleanFlag isFirstActivatedFlag) {
        this.isFirstActivatedFlag = isFirstActivatedFlag;
    }

    public BooleanFlag getIsAutoGenerateBillNumber() {
        if (isAutoGenerateBillNumber == null) {
            return BooleanFlag.NO;
        }
        return this.isAutoGenerateBillNumber;
    }

    public void setIsAutoGenerateBillNumber(BooleanFlag isAutoGenerateBillNumber) {
        this.isAutoGenerateBillNumber = isAutoGenerateBillNumber;
    }

    public Long getSmsPointBalance() {

        if (smsPointBalance == null) {
            return 0L;
        }
        return smsPointBalance;
    }

    public void setSmsPointBalance(Long smsPointBalance) {
        this.smsPointBalance = smsPointBalance;
    }

    public BooleanFlag getIsPartialAllowed() {
        if (isPartialAllowed == null) {
            return BooleanFlag.NO;
        }
        return isPartialAllowed;
    }

    public void setIsPartialAllowed(BooleanFlag isPartialAllowed) {
        this.isPartialAllowed = isPartialAllowed;
    }

    public BooleanFlag getIsEntityNotificationAllowed() {
        if (isEntityNotificationAllowed == null) {
            return BooleanFlag.NO;
        }

        return isEntityNotificationAllowed;
    }

    public void setIsEntityNotificationAllowed(BooleanFlag isEntityNotificationAllowed) {
        this.isEntityNotificationAllowed = isEntityNotificationAllowed;
    }

    public BigDecimal getDiscountDefaultValue() {
        return discountDefaultValue;
    }

    public void setDiscountDefaultValue(BigDecimal discountDefaultValue) {
        this.discountDefaultValue = discountDefaultValue;
    }

    public String getVatDefaultValue() {
        return vatDefaultValue;
    }

    public void setVatDefaultValue(String vatDefaultValue) {
        this.vatDefaultValue = vatDefaultValue;
    }

    public BigDecimal getMiniPartialDefaultValue() {
        return miniPartialDefaultValue;
    }

    public void setMiniPartialDefaultValue(BigDecimal miniPartialDefaultValue) {
        this.miniPartialDefaultValue = miniPartialDefaultValue;
    }

    public BigDecimal getMaxPartialDefaultValue() {
        return maxPartialDefaultValue;
    }

    public void setMaxPartialDefaultValue(BigDecimal maxPartialDefaultValue) {
        this.maxPartialDefaultValue = maxPartialDefaultValue;
    }

    public Long getActivityDefaultValue() {
        return activityDefaultValue;
    }

    public void setActivityDefaultValue(Long activityDefaultValue) {
        this.activityDefaultValue = activityDefaultValue;
    }

    public String getItemServiceDefaultValue() {
        return itemServiceDefaultValue;
    }

    public void setItemServiceDefaultValue(String itemServiceDefaultValue) {
        this.itemServiceDefaultValue = itemServiceDefaultValue;
    }

    public Long getPaymentVoucherSequence() {
        if (paymentVoucherSequence == null) {
            return 0L;
        }
        return paymentVoucherSequence;
    }

    public void setPaymentVoucherSequence(Long paymentVoucherSequence) {
        this.paymentVoucherSequence = paymentVoucherSequence;
    }

    public Long getAutoGenerationBillNumberSequence() {
        if (autoGenerationBillNumberSequence == null) {
            return 0L;
        }
        return autoGenerationBillNumberSequence;
    }

    public void setAutoGenerationBillNumberSequence(Long autoGenerationBillNumberSequence) {
        this.autoGenerationBillNumberSequence = autoGenerationBillNumberSequence;
    }

    public String getBrandName(Locale locale) {
        if (locale.getISO3Language().equalsIgnoreCase("eng")) {
            return this.brandNameEn;
        } else {
            return this.brandNameAr;
        }
    }

    public Long getOtherFeesBillNumberSequence() {
        if (otherFeesBillNumberSequence == null) {
            return 0L;
        }
        return otherFeesBillNumberSequence;
    }

    public void setOtherFeesBillNumberSequence(Long otherFeesBillNumberSequence) {
        this.otherFeesBillNumberSequence = otherFeesBillNumberSequence;
    }

    public BigDecimal getOtherBalance() {
        if (otherBalance == null) {
            return BigDecimal.ZERO;
        }
        return otherBalance;
    }

    public void setOtherBalance(BigDecimal otherBalance) {
        this.otherBalance = otherBalance;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Long getLogoWidth() {
        return logoWidth;
    }

    public void setLogoWidth(Long logoWidth) {
        this.logoWidth = logoWidth;
    }

    public Long getStampWidth() {
        return stampWidth;
    }

    public void setStampWidth(Long stampWidth) {
        this.stampWidth = stampWidth;
    }

    public Long getAccountNumberSequence() {
        if (accountNumberSequence == null) {
            return 0L;
        }
        return accountNumberSequence;
    }

    public void setAccountNumberSequence(Long accountNumberSequence) {
        this.accountNumberSequence = accountNumberSequence;
    }

    public BooleanFlag getIsRecurringAllowed() {
        if (isRecurringAllowed == null) {
            return BooleanFlag.NO;
        }
        return isRecurringAllowed;
    }

    public void setIsRecurringAllowed(BooleanFlag isRecurringAllowed) {
        this.isRecurringAllowed = isRecurringAllowed;
    }
}
