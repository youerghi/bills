package be.bslash.ibps.bills.entities;


import be.bslash.ibps.bills.enums.BooleanFlag;
import be.bslash.ibps.bills.enums.FieldType;
import be.bslash.ibps.bills.enums.Status;
import be.bslash.ibps.bills.enums.ValueType;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Locale;

@javax.persistence.Entity
@Table(name = "entity_field")
public class EntityField {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "entity_field_seq")
    @SequenceGenerator(name = "entity_field_seq", sequenceName = "entity_field_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "entity_id")
    private Entity entity;

    @Column(name = "field_name_ar")
    private String fieldNameAr;

    @Column(name = "field_name_en")
    private String fieldNameEn;

    @Column(name = "min_length")
    private Integer minLength;

    @Column(name = "max_length")
    private Integer maxLength;

    @Enumerated(EnumType.STRING)
    @Column(name = "is_required")
    private BooleanFlag isRequired;

    @Enumerated(EnumType.STRING)
    @Column(name = "field_type")
    private FieldType fieldType;

    @Enumerated(EnumType.STRING)
    @Column(name = "value_type")
    private ValueType valueType;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public String getFieldNameAr() {
        return fieldNameAr;
    }

    public void setFieldNameAr(String fieldNameAr) {
        this.fieldNameAr = fieldNameAr;
    }

    public String getFieldNameEn() {
        return fieldNameEn;
    }

    public void setFieldNameEn(String fieldNameEn) {
        this.fieldNameEn = fieldNameEn;
    }

    public Integer getMinLength() {
        return minLength;
    }

    public void setMinLength(Integer minLength) {
        this.minLength = minLength;
    }

    public Integer getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(Integer maxLength) {
        this.maxLength = maxLength;
    }

    public BooleanFlag getIsRequired() {
        if (isRequired == null) {
            return BooleanFlag.NO;
        }
        return isRequired;
    }

    public void setIsRequired(BooleanFlag isRequired) {
        this.isRequired = isRequired;
    }

    public FieldType getFieldType() {
        return fieldType;
    }

    public void setFieldType(FieldType fieldType) {
        this.fieldType = fieldType;
    }

    public ValueType getValueType() {
        return valueType;
    }

    public void setValueType(ValueType valueType) {
        this.valueType = valueType;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getName(Locale locale) {
        if (locale.getISO3Language().equalsIgnoreCase("eng")) {
            return this.fieldNameEn;
        } else {
            return this.fieldNameAr;
        }
    }
}
