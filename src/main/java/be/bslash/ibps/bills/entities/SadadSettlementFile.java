package be.bslash.ibps.bills.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "sadad_settlement_file")
public class SadadSettlementFile {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sadad_settlement_file_seq")
    @SequenceGenerator(name = "sadad_settlement_file_seq", sequenceName = "sadad_settlement_file_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "rq_uid")
    private String rqUID;

    @Column(name = "prc_date")
    private LocalDateTime prcDate;

    @Column(name = "collect_pmt_amt")
    private BigDecimal collectPmtAmt;

    @Column(name = "recon_pmt_amt")
    private BigDecimal reconPmtAmt;

    @Column(name = "un_recon_pmt_amt")
    private BigDecimal unReconPmtAmt;

    @Column(name = "sadad_fee_amt")
    private BigDecimal sadadFeeAmt;

    @Column(name = "sadad_vat_amt")
    private BigDecimal sadadVatAmt;

    @Column(name = "sadad_vat_number")
    private String sadadVatNumber;

    @Column(name = "create_date")
    private LocalDateTime createDate;

    @JsonIgnore
    @Lob
    @Column(name = "content_string")
    private String contentString;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRqUID() {
        return rqUID;
    }

    public void setRqUID(String rqUID) {
        this.rqUID = rqUID;
    }

    public LocalDateTime getPrcDate() {
        return prcDate;
    }

    public void setPrcDate(LocalDateTime prcDate) {
        this.prcDate = prcDate;
    }

    public BigDecimal getCollectPmtAmt() {
        return collectPmtAmt;
    }

    public void setCollectPmtAmt(BigDecimal collectPmtAmt) {
        this.collectPmtAmt = collectPmtAmt;
    }

    public BigDecimal getReconPmtAmt() {
        return reconPmtAmt;
    }

    public void setReconPmtAmt(BigDecimal reconPmtAmt) {
        this.reconPmtAmt = reconPmtAmt;
    }

    public BigDecimal getUnReconPmtAmt() {
        return unReconPmtAmt;
    }

    public void setUnReconPmtAmt(BigDecimal unReconPmtAmt) {
        this.unReconPmtAmt = unReconPmtAmt;
    }

    public BigDecimal getSadadFeeAmt() {
        return sadadFeeAmt;
    }

    public void setSadadFeeAmt(BigDecimal sadadFeeAmt) {
        this.sadadFeeAmt = sadadFeeAmt;
    }

    public BigDecimal getSadadVatAmt() {
        return sadadVatAmt;
    }

    public void setSadadVatAmt(BigDecimal sadadVatAmt) {
        this.sadadVatAmt = sadadVatAmt;
    }

    public String getSadadVatNumber() {
        return sadadVatNumber;
    }

    public void setSadadVatNumber(String sadadVatNumber) {
        this.sadadVatNumber = sadadVatNumber;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public String getContentString() {
        return contentString;
    }

    public void setContentString(String contentString) {
        this.contentString = contentString;
    }
}
