package be.bslash.ibps.bills.entities;


import be.bslash.ibps.bills.enums.BulkBillStatus;
import be.bslash.ibps.bills.enums.BulkType;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;

@javax.persistence.Entity
@Table(name = "bulk_bill")
public class BulkBill {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bulk_bill_seq")
    @SequenceGenerator(name = "bulk_bill_seq", sequenceName = "bulk_bill_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "reference")
    private String reference;

    @Column(name = "inner_name")
    private String innerName;

    @Column(name = "outer_name")
    private String outerName;

    @Column(name = "number_of_bills")
    private Long numberOfBills;

    @Column(name = "sadad_request_id")
    private String asyncRqUID;

    @Column(name = "success_count")
    private Integer successCount;

    @Column(name = "reject_count")
    private Integer rejectCount;

    @Column(name = "pending_count")
    private Integer pendingCount;

    @OneToOne
    @JoinColumn(name = "company_id")
    private Entity entity;

    @OneToOne
    @JoinColumn(name = "company_user_id")
    private EntityUser entityUser;

    @Enumerated(EnumType.STRING)
    @Column(name = "bulk_bill_status")
    private BulkBillStatus bulkBillStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "bulk_type")
    private BulkType bulkType;

    @Column(name = "create_date")
    private LocalDateTime createDate;

    @OneToMany(mappedBy = "bulkBill")
    private List<BulkBillTimeline> bulkBillTimelineList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getInnerName() {
        return innerName;
    }

    public void setInnerName(String innerName) {
        this.innerName = innerName;
    }

    public String getOuterName() {
        return outerName;
    }

    public void setOuterName(String outerName) {
        this.outerName = outerName;
    }

    public Long getNumberOfBills() {
        return numberOfBills;
    }

    public void setNumberOfBills(Long numberOfBills) {
        this.numberOfBills = numberOfBills;
    }

    public String getAsyncRqUID() {
        return asyncRqUID;
    }

    public void setAsyncRqUID(String asyncRqUID) {
        this.asyncRqUID = asyncRqUID;
    }

    public Integer getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(Integer successCount) {
        this.successCount = successCount;
    }

    public Integer getRejectCount() {
        return rejectCount;
    }

    public void setRejectCount(Integer rejectCount) {
        this.rejectCount = rejectCount;
    }

    public Integer getPendingCount() {
        return pendingCount;
    }

    public void setPendingCount(Integer pendingCount) {
        this.pendingCount = pendingCount;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public EntityUser getEntityUser() {
        return entityUser;
    }

    public void setEntityUser(EntityUser entityUser) {
        this.entityUser = entityUser;
    }

    public BulkBillStatus getBulkBillStatus() {
        return bulkBillStatus;
    }

    public void setBulkBillStatus(BulkBillStatus bulkBillStatus) {
        this.bulkBillStatus = bulkBillStatus;
    }

    public BulkType getBulkType() {
        return bulkType;
    }

    public void setBulkType(BulkType bulkType) {
        this.bulkType = bulkType;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public List<BulkBillTimeline> getBulkBillTimelineList() {
        return bulkBillTimelineList;
    }

    public void setBulkBillTimelineList(List<BulkBillTimeline> bulkBillTimelineList) {
        this.bulkBillTimelineList = bulkBillTimelineList;
    }
}
