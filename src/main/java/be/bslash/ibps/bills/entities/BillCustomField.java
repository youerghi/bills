package be.bslash.ibps.bills.entities;

import be.bslash.ibps.bills.dto.EntityDtoConverter;
import be.bslash.ibps.bills.dto.response.BillCustomFieldRsDto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Locale;


@Entity
@Table(name = "bill_custom_field")
public class BillCustomField implements EntityDtoConverter {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bill_custom_field_seq")
    @SequenceGenerator(name = "bill_custom_field_seq", sequenceName = "bill_custom_field_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @OneToOne
    @JoinColumn(name = "bill_id")
    private Bill bill;

    @OneToOne
    @JoinColumn(name = "filed_id")
    private EntityField entityField;

    @Column(name = "value")
    private String value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public EntityField getEntityField() {
        return entityField;
    }

    public void setEntityField(EntityField entityField) {
        this.entityField = entityField;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public Object toDto(Locale locale) {

        BillCustomFieldRsDto billCustomFieldRsDto = new BillCustomFieldRsDto();

        billCustomFieldRsDto.setId(this.id);
        billCustomFieldRsDto.setFieldId(this.entityField.getId());
        billCustomFieldRsDto.setFieldValue(this.value);
        billCustomFieldRsDto.setFieldNameAr(this.entityField.getFieldNameAr());
        billCustomFieldRsDto.setFieldNameEn(this.entityField.getFieldNameEn());
        billCustomFieldRsDto.setStatus(this.entityField.getStatus());

        return billCustomFieldRsDto;
    }
}
