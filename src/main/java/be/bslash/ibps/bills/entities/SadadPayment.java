package be.bslash.ibps.bills.entities;


import be.bslash.ibps.bills.enums.AccessChannel;
import be.bslash.ibps.bills.enums.PaymentMethod;
import be.bslash.ibps.bills.enums.PaymentSource;
import be.bslash.ibps.bills.enums.PaymentStatus;
import be.bslash.ibps.bills.utils.NumberToWord;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@javax.persistence.Entity
@Table(name = "sadad_payment")
public class SadadPayment {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sadad_payment_seq")
    @SequenceGenerator(name = "sadad_payment_seq", sequenceName = "sadad_payment_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "bank_payment_id")
    private String bankPaymentId;

    @Column(name = "sadad_payment_id")
    private String sadadPaymentId;

    @Column(name = "payment_amount")
    private BigDecimal paymentAmount;

    @Column(name = "remaining_amount")
    private BigDecimal remainingAmount;

    @Column(name = "payment_date")
    private LocalDateTime paymentDate;

    @Column(name = "bank_id")
    private String bankId;

    @Column(name = "branch_code")
    private String branchCode;

    @Column(name = "district_code")
    private String districtCode;

    @Enumerated(EnumType.STRING)
    @Column(name = "access_channel")
    private AccessChannel accessChannel;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_method")
    private PaymentMethod paymentMethod;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_source")
    private PaymentSource paymentSource;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "bill_id")
    private Bill bill;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "company_id")
    private Entity entity;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_status")
    private PaymentStatus paymentStatus;

    @OneToOne
    @JoinColumn(name = "sadad_settlement_file_id")
    private SadadSettlementFile sadadSettlementFile;

    @Column(name = "voucher_number")
    private Long voucherNumber;

    @Column(name = "payment_number")
    private Integer paymentNumber;

    @OneToOne
    @JoinColumn(name = "account_id")
    private Account account;

    @Transient
    private BigDecimal totalAmount;

    @Transient
    private String paymentImage;

    @Transient
    private String paymentType;

    @Transient
    private String totalAmountAr;

    @Transient
    private String totalAmountEn;

    @Transient
    private String paymentAmountAr;

    @Transient
    private String paymentAmountEn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBankPaymentId() {
        return bankPaymentId;
    }

    public void setBankPaymentId(String bankPaymentId) {
        this.bankPaymentId = bankPaymentId;
    }

    public String getSadadPaymentId() {
        return sadadPaymentId;
    }

    public void setSadadPaymentId(String sadadPaymentId) {
        this.sadadPaymentId = sadadPaymentId;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public LocalDateTime getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDateTime paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public AccessChannel getAccessChannel() {
        return accessChannel;
    }

    public void setAccessChannel(AccessChannel accessChannel) {
        this.accessChannel = accessChannel;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public PaymentSource getPaymentSource() {
        return paymentSource;
    }

    public void setPaymentSource(PaymentSource paymentSource) {
        this.paymentSource = paymentSource;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPaymentImage() {
        return "https://www.sadad.com/_layouts/inc/SADAD.Internet.Portal/img/sadad_logo_ar.png";
    }

    public void setPaymentImage(String paymentImage) {
        this.paymentImage = paymentImage;
    }

    public String getPaymentType() {
        return "ONLINE_PAYMENT";
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public BigDecimal getTotalAmount() {
        return this.bill.getTotalAmount();
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Long getVoucherNumber() {
        if (voucherNumber == null) {
            return 0L;
        }
        return voucherNumber;
    }

    public void setVoucherNumber(Long voucherNumber) {
        this.voucherNumber = voucherNumber;
    }

    public Integer getPaymentNumber() {
        if (paymentNumber == null) {
            return 0;
        }
        return paymentNumber;
    }

    public void setPaymentNumber(Integer paymentNumber) {
        this.paymentNumber = paymentNumber;
    }


    public String getTotalAmountAr() {
        try {
            String amountAr = NumberToWord.convertNumberToArabicWords(this.bill.getTotalAmount().intValue() + "");
            return amountAr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return this.bill.getTotalAmount().toString();
        }
    }

    public void setTotalAmountAr(String totalAmountAr) {
        this.totalAmountAr = totalAmountAr;
    }

    public String getTotalAmountEn() {
        try {
            String amountAr = NumberToWord.convertNumberToEnglishWords(this.bill.getTotalAmount().intValue() + "");
            return amountAr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return this.bill.getTotalAmount().toString();
        }
    }

    public void setTotalAmountEn(String totalAmountEn) {
        this.totalAmountEn = totalAmountEn;
    }

    public String getPaymentAmountAr() {
        try {
            String amountAr = NumberToWord.convertNumberToArabicWords(this.paymentAmount.intValue() + "");
            return amountAr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return this.paymentAmount.toString();
        }
    }

    public void setPaymentAmountAr(String paymentAmountAr) {
        this.paymentAmountAr = paymentAmountAr;
    }

    public String getPaymentAmountEn() {
        try {
            String amountAr = NumberToWord.convertNumberToEnglishWords(this.paymentAmount.intValue() + "");
            return amountAr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return this.paymentAmount.toString();
        }
    }

    public void setPaymentAmountEn(String paymentAmountEn) {
        this.paymentAmountEn = paymentAmountEn;
    }

    public BigDecimal getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(BigDecimal remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public SadadSettlementFile getSadadSettlementFile() {
        return sadadSettlementFile;
    }

    public void setSadadSettlementFile(SadadSettlementFile sadadSettlementFile) {
        this.sadadSettlementFile = sadadSettlementFile;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
