package be.bslash.ibps.bills.entities;

import be.bslash.ibps.bills.enums.AdApprovalType;
import be.bslash.ibps.bills.enums.AdCategoryType;
import be.bslash.ibps.bills.enums.CustomerType;
import be.bslash.ibps.bills.enums.Status;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;


@javax.persistence.Entity
@Table(name = "ad_campaign")
public class AdCampaign {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ad_campaign_seq")
    @SequenceGenerator(name = "ad_campaign_seq", sequenceName = "ad_campaign_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "name")
    private String adName;

    @Column(name = "code")
    private String adCode;

    @Column(name = "start_date")
    private LocalDateTime startDate;

    @Column(name = "end_date")
    private LocalDateTime endDate;

    @Column(name = "display_time")
    private BigDecimal dispalyTime;

    @Enumerated(EnumType.STRING)
    @Column(name = "approval_status")
    AdApprovalType approvalType;

    @Enumerated(EnumType.STRING)
    @Column(name = "category_type")
    AdCategoryType categoryType;

    @Enumerated(EnumType.STRING)
    @Column(name = "user_type")
    CustomerType userType;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @OneToOne
    @JoinColumn(name = "created_user_id")
    private EntityUser entityUser;

    @OneToOne
    @JoinColumn(name = "biller_id")
    private Entity entity;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "adCampaign", cascade = CascadeType.ALL)
    private Set<AdDetails> adDetailsSet;

    @Column(name = "rejected_note")
    private String rejectedNote;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAdName() {
        return adName;
    }

    public void setAdName(String adName) {
        this.adName = adName;
    }

    public String getAdCode() {
        return adCode;
    }

    public void setAdCode(String adCode) {
        this.adCode = adCode;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getDispalyTime() {
        return dispalyTime;
    }

    public void setDispalyTime(BigDecimal dispalyTime) {
        this.dispalyTime = dispalyTime;
    }

    public AdApprovalType getApprovalType() {
        return approvalType;
    }

    public void setApprovalType(AdApprovalType approvalType) {
        this.approvalType = approvalType;
    }

    public AdCategoryType getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(AdCategoryType categoryType) {
        this.categoryType = categoryType;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public EntityUser getEntityUser() {
        return entityUser;
    }

    public void setEntityUser(EntityUser entityUser) {
        this.entityUser = entityUser;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public Set<AdDetails> getAdDetailsSet() {
        return adDetailsSet;
    }

    public void setAdDetailsSet(Set<AdDetails> adDetailsSet) {
        this.adDetailsSet = adDetailsSet;
    }

    public CustomerType getUserType() {
        return userType;
    }

    public void setUserType(CustomerType userType) {
        this.userType = userType;
    }

    public String getRejectedNote() {
        return rejectedNote;
    }

    public void setRejectedNote(String rejectedNote) {
        this.rejectedNote = rejectedNote;
    }

}
