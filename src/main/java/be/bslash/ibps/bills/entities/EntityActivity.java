package be.bslash.ibps.bills.entities;


import be.bslash.ibps.bills.dto.response.EntityActivityDtoRs;
import be.bslash.ibps.bills.enums.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Locale;

@javax.persistence.Entity
@Table(name = "entity_activity")
public class EntityActivity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "entity_activity_seq")
    @SequenceGenerator(name = "entity_activity_seq", sequenceName = "entity_activity_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "entity_id")
    private Entity entity;

    @Column(name = "activity_code")
    private String code;

    @Column(name = "activity_name_ar")
    private String activityNameAr;

    @Column(name = "activity_name_en")
    private String activityNameEn;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Column(name = "iban")
    private String iban;

    @Column(name = "bank_beneficiary_name_en")
    private String bankBeneficiaryNameEn;

    @Column(name = "bank_beneficiary_name_ar")
    private String bankBeneficiaryNameAr;

    @Column(name = "create_date")
    private LocalDateTime createDate;

    @OneToOne
    @JoinColumn(name = "bank_id")
    private Bank bank;

    @OneToOne
    @JoinColumn(name = "region_id")
    private Region region;

    @OneToOne
    @JoinColumn(name = "city_id")
    private City city;

    @OneToOne
    @JoinColumn(name = "sub_sector_id")
    private SubSector subSector;

    @Column(name = "address")
    private String address;

    @Column(name = "estimated_total_value_of_transactionAnnually")
    private BigDecimal estimatedTotalValueOfTransactionAnnually;

    @Column(name = "monthly_average_number_expected_uploaded_bills")
    private BigDecimal monthlyAverageNumberOfExpectedUploadedBills;

    @Column(name = "estimated_number_current_customers")
    private BigDecimal estimatedNumberOfCurrentCustomers;

    @Column(name = "the_highest_amount_per_uploaded_bill")
    private BigDecimal theHighestAmountPerUploadedBill;

    @Column(name = "the_Lowest_amount_per_uploaded_bill")
    private BigDecimal theLowestAmountPerUploadedBill;

    @Column(name = "monthly_average_value_of_expected_uploaded_bills")
    private BigDecimal totalValueOfUploadedBillsPerMonth;

    @Column(name = "monthly_highest_value_of_expected_uploaded_bills")
    private BigDecimal monthlyHighestValueOfExpectedUploadedBills;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getActivityNameAr() {
        return activityNameAr;
    }

    public void setActivityNameAr(String activityNameAr) {
        this.activityNameAr = activityNameAr;
    }

    public String getActivityNameEn() {
        return activityNameEn;
    }

    public void setActivityNameEn(String activityNameEn) {
        this.activityNameEn = activityNameEn;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getBankBeneficiaryNameEn() {
        return bankBeneficiaryNameEn;
    }

    public void setBankBeneficiaryNameEn(String bankBeneficiaryNameEn) {
        this.bankBeneficiaryNameEn = bankBeneficiaryNameEn;
    }

    public String getBankBeneficiaryNameAr() {
        return bankBeneficiaryNameAr;
    }

    public void setBankBeneficiaryNameAr(String bankBeneficiaryNameAr) {
        this.bankBeneficiaryNameAr = bankBeneficiaryNameAr;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public SubSector getSubSector() {
        return subSector;
    }

    public void setSubSector(SubSector subSector) {
        this.subSector = subSector;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigDecimal getEstimatedTotalValueOfTransactionAnnually() {
        return estimatedTotalValueOfTransactionAnnually;
    }

    public void setEstimatedTotalValueOfTransactionAnnually(BigDecimal estimatedTotalValueOfTransactionAnnually) {
        this.estimatedTotalValueOfTransactionAnnually = estimatedTotalValueOfTransactionAnnually;
    }

    public BigDecimal getMonthlyAverageNumberOfExpectedUploadedBills() {
        return monthlyAverageNumberOfExpectedUploadedBills;
    }

    public void setMonthlyAverageNumberOfExpectedUploadedBills(BigDecimal monthlyAverageNumberOfExpectedUploadedBills) {
        this.monthlyAverageNumberOfExpectedUploadedBills = monthlyAverageNumberOfExpectedUploadedBills;
    }

    public BigDecimal getEstimatedNumberOfCurrentCustomers() {
        return estimatedNumberOfCurrentCustomers;
    }

    public void setEstimatedNumberOfCurrentCustomers(BigDecimal estimatedNumberOfCurrentCustomers) {
        this.estimatedNumberOfCurrentCustomers = estimatedNumberOfCurrentCustomers;
    }

    public BigDecimal getTheHighestAmountPerUploadedBill() {
        return theHighestAmountPerUploadedBill;
    }

    public void setTheHighestAmountPerUploadedBill(BigDecimal theHighestAmountPerUploadedBill) {
        this.theHighestAmountPerUploadedBill = theHighestAmountPerUploadedBill;
    }

    public BigDecimal getTheLowestAmountPerUploadedBill() {
        return theLowestAmountPerUploadedBill;
    }

    public void setTheLowestAmountPerUploadedBill(BigDecimal theLowestAmountPerUploadedBill) {
        this.theLowestAmountPerUploadedBill = theLowestAmountPerUploadedBill;
    }

    public BigDecimal getTotalValueOfUploadedBillsPerMonth() {
        return totalValueOfUploadedBillsPerMonth;
    }

    public void setTotalValueOfUploadedBillsPerMonth(BigDecimal totalValueOfUploadedBillsPerMonth) {
        this.totalValueOfUploadedBillsPerMonth = totalValueOfUploadedBillsPerMonth;
    }

    public BigDecimal getMonthlyHighestValueOfExpectedUploadedBills() {
        return monthlyHighestValueOfExpectedUploadedBills;
    }

    public void setMonthlyHighestValueOfExpectedUploadedBills(BigDecimal monthlyHighestValueOfExpectedUploadedBills) {
        this.monthlyHighestValueOfExpectedUploadedBills = monthlyHighestValueOfExpectedUploadedBills;
    }

    public String getName(Locale locale) {
        if (locale.getISO3Language().equalsIgnoreCase("eng")) {
            return this.activityNameEn + ((this.city != null) ? "-" + this.city.getName(locale) : "");
        } else {
            return this.activityNameAr + ((this.city != null) ? "-" + this.city.getName(locale) : "");
        }
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public EntityActivityDtoRs toDto(Locale locale) {
        EntityActivityDtoRs entityActivityDtoRs = new EntityActivityDtoRs();
        entityActivityDtoRs.setActivityName(this.getName(locale));
        entityActivityDtoRs.setId(this.getId());
        entityActivityDtoRs.setStatus(this.getStatus());
        entityActivityDtoRs.setActivityNameAr(this.getActivityNameAr());
        entityActivityDtoRs.setActivityNameEn(this.getActivityNameEn());
        entityActivityDtoRs.setIban(this.getIban());
        entityActivityDtoRs.setCode(this.getCode());

        return entityActivityDtoRs;
    }
}
