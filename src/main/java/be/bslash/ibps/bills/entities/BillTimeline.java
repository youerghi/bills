package be.bslash.ibps.bills.entities;


import be.bslash.ibps.bills.configrations.Utf8ResourceBundle;
import be.bslash.ibps.bills.dto.EntityDtoConverter;
import be.bslash.ibps.bills.dto.response.BillTimelineDtoRs;
import be.bslash.ibps.bills.enums.BillStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Locale;

@Entity
@Table(name = "bill_timeline")
public class BillTimeline implements EntityDtoConverter {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bill_timeline_seq")
    @SequenceGenerator(name = "bill_timeline_seq", sequenceName = "bill_timeline_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @OneToOne
    @JoinColumn(name = "bill_id")
    private Bill bill;

    @OneToOne
    @JoinColumn(name = "company_user_id")
    private EntityUser entityUser;

    @Enumerated(EnumType.STRING)
    @Column(name = "bill_status")
    private BillStatus billStatus;

    @Column(name = "action_date")
    private LocalDateTime actionDate;

    @Column(name = "user_reference")
    private String userReference;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public EntityUser getEntityUser() {
        return entityUser;
    }

    public void setEntityUser(EntityUser entityUser) {
        this.entityUser = entityUser;
    }

    public BillStatus getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(BillStatus billStatus) {
        this.billStatus = billStatus;
    }

    public LocalDateTime getActionDate() {
        return actionDate;
    }

    public void setActionDate(LocalDateTime actionDate) {
        this.actionDate = actionDate;
    }

    public String getUserReference() {
        return userReference;
    }

    public void setUserReference(String userReference) {
        this.userReference = userReference;
    }

    @Override
    public Object toDto(Locale locale) {

        BillTimelineDtoRs billTimelineDtoRs = new BillTimelineDtoRs();
        billTimelineDtoRs.setId(this.id);
        billTimelineDtoRs.setActionDate(this.actionDate);
        billTimelineDtoRs.setBillStatus(this.billStatus);
        billTimelineDtoRs.setBillStatusText(Utf8ResourceBundle.getString(this.billStatus.name(), locale));
        billTimelineDtoRs.setUserReference(this.userReference);
        if (this.entityUser != null) {
            billTimelineDtoRs.setCompanyUsername(this.entityUser.getUsername());
            billTimelineDtoRs.setCompanyUserFullName(this.entityUser.getFirstName() + " " + this.entityUser.getLastName());
        }
        billTimelineDtoRs.setUserReference(this.userReference);

        return billTimelineDtoRs;
    }
}
