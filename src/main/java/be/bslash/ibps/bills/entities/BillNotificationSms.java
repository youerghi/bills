package be.bslash.ibps.bills.entities;


import be.bslash.ibps.bills.enums.BillNotificationType;
import be.bslash.ibps.bills.enums.Language;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDateTime;

@javax.persistence.Entity
@Table(name = "bill_notification_sms")
public class BillNotificationSms {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bill_notification_sms_seq")
    @SequenceGenerator(name = "bill_notification_sms_seq", sequenceName = "bill_notification_sms_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "title")
    private String title;

    @Lob
    @Column(name = "body")
    private String body;

    @Column(name = "mobile_number")
    private String mobileNumber;

    @Enumerated(EnumType.STRING)
    @Column(name = "language")
    private Language notificationLanguage;

    @Column(name = "user_reference")
    private String userReference;

    @Column(name = "point")
    private Long point;

    @Column(name = "create_date")
    private LocalDateTime createDate;

    @Column(name = "response_code")
    private String responseCode;

    @Enumerated(EnumType.STRING)
    @Column(name = "bill_notification_type")
    private BillNotificationType billNotificationType;

    @OneToOne
    @JoinColumn(name = "bill_id")
    private Bill bill;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Language getNotificationLanguage() {
        return notificationLanguage;
    }

    public void setNotificationLanguage(Language notificationLanguage) {
        this.notificationLanguage = notificationLanguage;
    }

    public String getUserReference() {
        return userReference;
    }

    public void setUserReference(String userReference) {
        this.userReference = userReference;
    }

    public Long getPoint() {
        return point;
    }

    public void setPoint(Long point) {
        this.point = point;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public BillNotificationType getBillNotificationType() {
        return billNotificationType;
    }

    public void setBillNotificationType(BillNotificationType billNotificationType) {
        this.billNotificationType = billNotificationType;
    }
}
