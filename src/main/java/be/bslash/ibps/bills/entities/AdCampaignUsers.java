package be.bslash.ibps.bills.entities;

import be.bslash.ibps.bills.enums.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ad_campaign_users")
public class AdCampaignUsers {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ad_campaign_users_seq")
    @SequenceGenerator(name = "ad_campaign_users_seq", sequenceName = "ad_campaign_users_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @OneToOne
    @JoinColumn(name = "user_id")
    private EntityUser entityUser;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ad_settings_id", nullable = false)
    AdSettings adSettings;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EntityUser getEntityUser() {
        return entityUser;
    }

    public void setEntityUser(EntityUser entityUser) {
        this.entityUser = entityUser;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public AdSettings getAdSettings() {
        return adSettings;
    }

    public void setAdSettings(AdSettings adSettings) {
        this.adSettings = adSettings;
    }

}
