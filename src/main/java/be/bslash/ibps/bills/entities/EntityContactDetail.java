package be.bslash.ibps.bills.entities;


import be.bslash.ibps.bills.enums.IdType;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDateTime;

@javax.persistence.Entity
@Table(name = "entity_contact_detail")
public class EntityContactDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "entity_contact_detail_seq")
    @SequenceGenerator(name = "entity_contact_detail_seq", sequenceName = "entity_contact_detail_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "phone_number_code")
    private String phoneNumberCode;

    @Column(name = "mobile_number")
    private String mobileNumber;

    @Column(name = "mobile_number_code")
    private String mobileNumberCode;

    @Column(name = "unified_number")
    private String unifiedNumber;

    @Column(name = "website")
    private String website;

    @Column(name = "email")
    private String email;

    @Column(name = "authorized_full_name")
    private String authorizedFullName;

    @Column(name = "authorized_title")
    private String authorizedTitle;

    @Column(name = "authorized_email")
    private String authorizedEmail;

    @Column(name = "authorized_mobile_number")
    private String authorizedMobileNumber;

    @Column(name = "authorized_mobile_number_code")
    private String authorizedMobileNumberCode;

    @Enumerated(EnumType.STRING)
    @Column(name = "authorized_id_type")
    private IdType authorizedIdType;

    @Column(name = "authorized_id_number")
    private String authorizedIdNumber;

    @Column(name = "authorized_id_expire_date")
    private LocalDateTime authorizedIdExpireDate;

    @OneToOne
    @JoinColumn(name = "authorized_nationality_id")
    private Nationality authorizedNationality;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "entity_id")
    private Entity entity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAuthorizedFullName() {
        return authorizedFullName;
    }

    public void setAuthorizedFullName(String authorizedFullName) {
        this.authorizedFullName = authorizedFullName;
    }

    public String getAuthorizedTitle() {
        return authorizedTitle;
    }

    public void setAuthorizedTitle(String authorizedTitle) {
        this.authorizedTitle = authorizedTitle;
    }

    public String getAuthorizedMobileNumber() {
        return authorizedMobileNumber;
    }

    public void setAuthorizedMobileNumber(String authorizedMobileNumber) {
        this.authorizedMobileNumber = authorizedMobileNumber;
    }

    public String getAuthorizedIdNumber() {
        return authorizedIdNumber;
    }

    public void setAuthorizedIdNumber(String authorizedIdNumber) {
        this.authorizedIdNumber = authorizedIdNumber;
    }

    public LocalDateTime getAuthorizedIdExpireDate() {
        return authorizedIdExpireDate;
    }

    public void setAuthorizedIdExpireDate(LocalDateTime authorizedIdExpireDate) {
        this.authorizedIdExpireDate = authorizedIdExpireDate;
    }

    public Nationality getAuthorizedNationality() {
        return authorizedNationality;
    }

    public void setAuthorizedNationality(Nationality authorizedNationality) {
        this.authorizedNationality = authorizedNationality;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public String getUnifiedNumber() {
        return unifiedNumber;
    }

    public void setUnifiedNumber(String unifiedNumber) {
        this.unifiedNumber = unifiedNumber;
    }

    public String getAuthorizedEmail() {
        return authorizedEmail;
    }

    public void setAuthorizedEmail(String authorizedEmail) {
        this.authorizedEmail = authorizedEmail;
    }

    public IdType getAuthorizedIdType() {
        return authorizedIdType;
    }

    public void setAuthorizedIdType(IdType authorizedIdType) {
        this.authorizedIdType = authorizedIdType;
    }

    public String getPhoneNumberCode() {
        return phoneNumberCode;
    }

    public void setPhoneNumberCode(String phoneNumberCode) {
        this.phoneNumberCode = phoneNumberCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getMobileNumberCode() {
        return mobileNumberCode;
    }

    public void setMobileNumberCode(String mobileNumberCode) {
        this.mobileNumberCode = mobileNumberCode;
    }

    public String getAuthorizedMobileNumberCode() {
        return authorizedMobileNumberCode;
    }

    public void setAuthorizedMobileNumberCode(String authorizedMobileNumberCode) {
        this.authorizedMobileNumberCode = authorizedMobileNumberCode;
    }
}
