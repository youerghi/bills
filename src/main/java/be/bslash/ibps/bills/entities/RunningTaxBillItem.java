package be.bslash.ibps.bills.entities;

import be.bslash.ibps.bills.dto.EntityDtoConverter;
import be.bslash.ibps.bills.dto.response.BillItemDetailedRsDto;
import be.bslash.ibps.bills.enums.BooleanFlag;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Locale;


@Entity
@Table(name = "running_tax_bill_item")
public class RunningTaxBillItem implements EntityDtoConverter {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "running_tax_bill_item_seq")
    @SequenceGenerator(name = "running_tax_bill_item_seq", sequenceName = "running_tax_bill_item_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @OneToOne
    @JoinColumn(name = "bill_id")
    private Bill bill;

    @Column(name = "number")
    private Integer number;

    @Column(name = "name")
    private String name;

    @Column(name = "unit_price")
    private BigDecimal unitPrice;

    @Column(name = "quantity")
    private BigDecimal quantity;

    @Column(name = "item_vat")
    private BigDecimal vat;

    @Column(name = "total_price")
    private BigDecimal totalPrice;

    @Column(name = "discount", columnDefinition = "Decimal(19,5) default '0.00000'")
    private BigDecimal discount;

    @Column(name = "discount_type", columnDefinition = "nvarchar(255) default 'PERC'")
    private String discountType;

    @Enumerated(EnumType.STRING)
    @Column(name = "vat_exempted_flag", columnDefinition = "nvarchar(255) default 'NO'")
    private BooleanFlag vatExemptedFlag;

    @Enumerated(EnumType.STRING)
    @Column(name = "vat_na_flag", columnDefinition = "nvarchar(255) default 'NO'")
    private BooleanFlag vatNaFlag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getVat() {
        return vat;
    }

    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }

    public BooleanFlag getVatExemptedFlag() {
        return vatExemptedFlag;
    }

    public void setVatExemptedFlag(BooleanFlag vatExemptedFlag) {
        this.vatExemptedFlag = vatExemptedFlag;
    }

    public BooleanFlag getVatNaFlag() {
        return vatNaFlag;
    }

    public void setVatNaFlag(BooleanFlag vatNaFlag) {
        this.vatNaFlag = vatNaFlag;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    @Override
    public Object toDto(Locale locale) {

        BillItemDetailedRsDto billItemDetailedRsDto = new BillItemDetailedRsDto();

        billItemDetailedRsDto.setName(this.name);
        billItemDetailedRsDto.setNumber(this.number);
        billItemDetailedRsDto.setQuantity(this.quantity);
        billItemDetailedRsDto.setUnitPrice(this.unitPrice);
        billItemDetailedRsDto.setTotalPrice(this.totalPrice);
        billItemDetailedRsDto.setDiscount(this.discount);
        billItemDetailedRsDto.setDiscountType(this.discountType);

        billItemDetailedRsDto.setVat(this.vat.toString());
        if (this.vatNaFlag.equals(BooleanFlag.YES)) {
            billItemDetailedRsDto.setVat("NA");
        }

        if (this.vatExemptedFlag.equals(BooleanFlag.YES)) {
            billItemDetailedRsDto.setVat("EXE");
        }

        billItemDetailedRsDto.setVatNaFlag(this.vatNaFlag);
        billItemDetailedRsDto.setVatExemptedFlag(this.vatExemptedFlag);

        return billItemDetailedRsDto;
    }
}
