package be.bslash.ibps.bills.entities;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "config_template")
public class ConfigTemplate {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "config_template_seq")
    @SequenceGenerator(name = "config_template_seq", sequenceName = "config_template_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "key_conf")
    private String key;

    @Lob
    @Column(name = "value_conf")
    byte[] content;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }
}
