package be.bslash.ibps.bills.entities;


import be.bslash.ibps.bills.enums.BillNotificationType;
import be.bslash.ibps.bills.enums.Language;
import be.bslash.ibps.bills.enums.NotificationMedia;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name = "bill_notification_template")
public class BillNotificationTemplate {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bill_notification_template_seq")
    @SequenceGenerator(name = "bill_notification_template_seq", sequenceName = "bill_notification_template_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "title")
    private String title;

    @Lob
    @Column(name = "layout")
    private String layout;

    @Enumerated(EnumType.STRING)
    @Column(name = "language")
    private Language notificationLanguage;

    @Enumerated(EnumType.STRING)
    @Column(name = "media")
    private NotificationMedia notificationMedia;

    @Enumerated(EnumType.STRING)
    @Column(name = "bill_notification_type")
    private BillNotificationType billNotificationType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public Language getNotificationLanguage() {
        return notificationLanguage;
    }

    public void setNotificationLanguage(Language notificationLanguage) {
        this.notificationLanguage = notificationLanguage;
    }

    public NotificationMedia getNotificationMedia() {
        return notificationMedia;
    }

    public void setNotificationMedia(NotificationMedia notificationMedia) {
        this.notificationMedia = notificationMedia;
    }

    public BillNotificationType getBillNotificationType() {
        return billNotificationType;
    }

    public void setBillNotificationType(BillNotificationType billNotificationType) {
        this.billNotificationType = billNotificationType;
    }
}
