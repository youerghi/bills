package be.bslash.ibps.bills.entities;

import be.bslash.ibps.bills.enums.NoteType;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;


@Entity
@Table(name = "bill_note")
public class BillNote {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bill_note_seq")
    @SequenceGenerator(name = "bill_note_seq", sequenceName = "bill_note_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "bill_id")
    private Bill bill;

    @Column(name = "date")
    private LocalDateTime date;

    @Column(name = "previous_amount")
    private BigDecimal previousAmount;

    @Enumerated(EnumType.STRING)
    @Column(name = "note_type")
    private NoteType noteType;

    @Column(name = "description")
    private String description;

    @Column(name = "document_number")
    private String documentNumber;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "balance_amount")
    private BigDecimal balanceAmount;

    @OneToMany(mappedBy = "billNote")
    private List<BillNoteItem> itemList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public NoteType getNoteType() {
        return noteType;
    }

    public void setNoteType(NoteType noteType) {
        this.noteType = noteType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public List<BillNoteItem> getItemList() {
        return itemList;
    }

    public void setItemList(List<BillNoteItem> itemList) {
        this.itemList = itemList;
    }

    public BigDecimal getPreviousAmount() {
        return previousAmount;
    }

    public void setPreviousAmount(BigDecimal previousAmount) {
        this.previousAmount = previousAmount;
    }

    public BigDecimal getBalanceAmount() {
        return this.previousAmount.add(amount);
    }

    public void setBalanceAmount(BigDecimal balanceAmount) {
        this.balanceAmount = balanceAmount;
    }
}
