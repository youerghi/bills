package be.bslash.ibps.bills.entities;


import be.bslash.ibps.bills.enums.BillCategory;
import be.bslash.ibps.bills.enums.BillSource;
import be.bslash.ibps.bills.enums.BillStatus;
import be.bslash.ibps.bills.enums.BillType;
import be.bslash.ibps.bills.enums.BooleanFlag;
import be.bslash.ibps.bills.enums.IdType;
import be.bslash.ibps.bills.utils.Utils;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@javax.persistence.Entity
@Table(name = "bill")
public class Bill {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bill_seq")
    @SequenceGenerator(name = "bill_seq", sequenceName = "bill_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @OneToOne
    @JoinColumn(name = "entity_activity_id")
    private EntityActivity entityActivity;

    @Column(name = "bill_number")
    private String billNumber;

    @Column(name = "sadad_number")
    private String sadadNumber;

    @Column(name = "service_name")
    private String serviceName;

    @Column(name = "service_description")
    private String serviceDescription;

    @Column(name = "issue_date")
    private LocalDateTime issueDate;

    @Column(name = "expire_date")
    private LocalDateTime expireDate;

    @Column(name = "create_date")
    private LocalDateTime createDate;

    @Column(name = "sub_amount", columnDefinition = "Decimal(19,3) default '0.000'")
    private BigDecimal subAmount;

    @Column(name = "vat")
    private BigDecimal vat;

    @Column(name = "discount", columnDefinition = "Decimal(19,5) default '0.00000'")
    private BigDecimal discount;

    @Column(name = "discount_type", columnDefinition = "nvarchar(255) default 'PERC'")
    private String discountType;

    @Column(name = "total_amount", columnDefinition = "Decimal(19,3) default '0.000'")
    private BigDecimal totalAmount;

    @Column(name = "remain_amount", columnDefinition = "Decimal(19,3) default '0.000'")
    private BigDecimal remainAmount;

    @Column(name = "current_paid_amount", columnDefinition = "Decimal(19,3) default '0.000'")
    private BigDecimal currentPaidAmount;

    @Column(name = "customer_full_name")
    private String customerFullName;

    @Column(name = "customer_mobile_number")
    private String customerMobileNumber;

    @Column(name = "customer_email_address")
    private String customerEmailAddress;

    @Column(name = "customer_id_number")
    private String customerIdNumber;

    @Column(name = "customer_tax_number")
    private String customerTaxNumber;

    @Column(name = "customer_address")
    private String customerAddress;

    @Column(name = "customer_previous_balance")
    private BigDecimal customerPreviousBalance;

    @OneToOne
    @JoinColumn(name = "company_id")
    private Entity entity;

    @OneToOne
    @JoinColumn(name = "company_user_id")
    private EntityUser entityUser;

    @Enumerated(EnumType.STRING)
    @Column(name = "bill_status")
    private BillStatus billStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "bill_type")
    private BillType billType;

    @Enumerated(EnumType.STRING)
    @Column(name = "bill_source")
    private BillSource billSource;

    @OneToOne
    @JoinColumn(name = "bulk_bill_id")
    private BulkBill bulkBill;

    @OneToMany(mappedBy = "bill")
    private List<BillTimeline> billTimelineList;

    @OneToOne
    @JoinColumn(name = "sadad_settlement_file_id")
    private SadadSettlementFile sadadSettlementFile;

    @Enumerated(EnumType.STRING)
    @Column(name = "customer_id_type")
    private IdType customerIdType;

    @Column(name = "approve_date_by_sadad")
    private LocalDateTime approveDateBySadad;

    @Column(name = "campaign_id")
    private Integer campaignId;

    @Enumerated(EnumType.STRING)
    @Column(name = "vat_exempted_flag", columnDefinition = "nvarchar(255) default 'NO'")
    private BooleanFlag vatExemptedFlag;

    @Enumerated(EnumType.STRING)
    @Column(name = "vat_na_flag", columnDefinition = "nvarchar(255) default 'NO'")
    private BooleanFlag vatNaFlag;

    @Enumerated(EnumType.STRING)
    @Column(name = "sms_issue_flag", columnDefinition = "nvarchar(255) default 'NO'")
    private BooleanFlag smsIssueFlag;

    @Enumerated(EnumType.STRING)
    @Column(name = "is_partial_allowed")
    private BooleanFlag isPartialAllowed;

    @Enumerated(EnumType.STRING)
    @Column(name = "bill_category")
    private BillCategory billCategory;

    @Column(name = "mini_partial_amount")
    private BigDecimal miniPartialAmount;

    @Column(name = "max_partial_amount")
    private BigDecimal maxPartialAmount;

    @Column(name = "payment_number")
    private Integer paymentNumber;

    @Column(name = "settlement_number")
    private Integer settlementNumber;

    @OneToMany(mappedBy = "bill")
    private List<BillItem> billItemList;

    @Column(name = "bill_logo")
    private String logo;

    @Column(name = "bill_stamp")
    private String stamp;

    @Column(name = "total_running_amount")
    private BigDecimal totalRunningAmount;

    @OneToOne
    @JoinColumn(name = "account_id")
    private Account account;

    @Column(name = "cycle_number")
    private Integer cycleNumber;

    @Column(name = "due_date")
    private LocalDateTime dueDate;

    @Transient
    private String paymentId;

    @Transient
    private List<BillCustomField> billCustomFieldList;

    @Column(name = "street")
    private String street;

    @Column(name = "building_number")
    private String buildingNumber;

    @Column(name = "additional_number")
    private String additionalNumber;

    @Column(name = "postal_code")
    private String postalCode;

    @OneToOne
    @JoinColumn(name = "region_id")
    private Region region;

    @OneToOne
    @JoinColumn(name = "city_id")
    private City city;

    @Column(name = "other_city")
    private String otherCity;

    @OneToOne
    @JoinColumn(name = "district_id")
    private District district;

    @Column(name = "other_district")
    private String otherDistrict;

    @Column(name = "reference_number")
    private String referenceNumber;

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public String getAdditionalNumber() {
        return additionalNumber;
    }

    public void setAdditionalNumber(String additionalNumber) {
        this.additionalNumber = additionalNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getOtherCity() {
        return otherCity;
    }

    public void setOtherCity(String otherCity) {
        this.otherCity = otherCity;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public String getOtherDistrict() {
        return otherDistrict;
    }

    public void setOtherDistrict(String otherDistrict) {
        this.otherDistrict = otherDistrict;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EntityActivity getEntityActivity() {
        return entityActivity;
    }

    public void setEntityActivity(EntityActivity entityActivity) {
        this.entityActivity = entityActivity;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public String getSadadNumber() {
        return sadadNumber;
    }

    public void setSadadNumber(String sadadNumber) {
        this.sadadNumber = sadadNumber;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public LocalDateTime getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(LocalDateTime issueDate) {
        this.issueDate = issueDate;
    }

    public LocalDateTime getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(LocalDateTime expireDate) {
        this.expireDate = expireDate;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public BigDecimal getSubAmount() {
        return subAmount;
    }

    public void setSubAmount(BigDecimal subAmount) {
        this.subAmount = subAmount;
    }

    public BigDecimal getVat() {
        if (vat == null) {
            return BigDecimal.ZERO;
        }
        return vat;
    }

    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = Utils.roundDecimalHalfDown(totalAmount);
    }

    public String getCustomerFullName() {
        return customerFullName;
    }

    public void setCustomerFullName(String customerFullName) {
        this.customerFullName = customerFullName;
    }

    public String getCustomerMobileNumber() {
        return customerMobileNumber;
    }

    public void setCustomerMobileNumber(String customerMobileNumber) {
        this.customerMobileNumber = customerMobileNumber;
    }

    public String getCustomerEmailAddress() {
        return customerEmailAddress;
    }

    public void setCustomerEmailAddress(String customerEmailAddress) {
        this.customerEmailAddress = customerEmailAddress;
    }

    public String getCustomerIdNumber() {
        return customerIdNumber;
    }

    public void setCustomerIdNumber(String customerIdNumber) {
        this.customerIdNumber = customerIdNumber;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public EntityUser getEntityUser() {
        return entityUser;
    }

    public void setEntityUser(EntityUser entityUser) {
        this.entityUser = entityUser;
    }

    public BillStatus getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(BillStatus billStatus) {
        this.billStatus = billStatus;
    }

    public BillType getBillType() {
        return billType;
    }

    public void setBillType(BillType billType) {
        this.billType = billType;
    }

    public BillSource getBillSource() {
        return billSource;
    }

    public void setBillSource(BillSource billSource) {
        this.billSource = billSource;
    }

    public BulkBill getBulkBill() {
        return bulkBill;
    }

    public void setBulkBill(BulkBill bulkBill) {
        this.bulkBill = bulkBill;
    }

    public List<BillTimeline> getBillTimelineList() {
        return billTimelineList;
    }

    public void setBillTimelineList(List<BillTimeline> billTimelineList) {
        this.billTimelineList = billTimelineList;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public List<BillCustomField> getBillCustomFieldList() {
        return billCustomFieldList;
    }

    public void setBillCustomFieldList(List<BillCustomField> billCustomFieldList) {
        this.billCustomFieldList = billCustomFieldList;
    }

    public String getCustomerTaxNumber() {
        return customerTaxNumber;
    }

    public void setCustomerTaxNumber(String customerTaxNumber) {
        this.customerTaxNumber = customerTaxNumber;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public BigDecimal getCustomerPreviousBalance() {
        if (customerPreviousBalance == null) {
            return BigDecimal.ZERO;
        }
        return customerPreviousBalance;
    }

    public void setCustomerPreviousBalance(BigDecimal customerPreviousBalance) {
        this.customerPreviousBalance = customerPreviousBalance;
    }

    public SadadSettlementFile getSadadSettlementFile() {
        return sadadSettlementFile;
    }

    public void setSadadSettlementFile(SadadSettlementFile sadadSettlementFile) {
        this.sadadSettlementFile = sadadSettlementFile;
    }

    public IdType getCustomerIdType() {
        return customerIdType;
    }

    public void setCustomerIdType(IdType customerIdType) {
        this.customerIdType = customerIdType;
    }

    public LocalDateTime getApproveDateBySadad() {
        return approveDateBySadad;
    }

    public void setApproveDateBySadad(LocalDateTime approveDateBySadad) {
        this.approveDateBySadad = approveDateBySadad;
    }

    public Integer getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }

    public BooleanFlag getVatExemptedFlag() {
        if (vatExemptedFlag == null) {
            return BooleanFlag.NO;
        }
        return vatExemptedFlag;
    }

    public void setVatExemptedFlag(BooleanFlag vatExemptedFlag) {
        this.vatExemptedFlag = vatExemptedFlag;
    }

    public BooleanFlag getVatNaFlag() {
        if (vatNaFlag == null) {
            return BooleanFlag.NO;
        }
        return vatNaFlag;
    }

    public void setVatNaFlag(BooleanFlag vatNaFlag) {
        this.vatNaFlag = vatNaFlag;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public BooleanFlag getSmsIssueFlag() {
        return smsIssueFlag;
    }

    public void setSmsIssueFlag(BooleanFlag smsIssueFlag) {
        this.smsIssueFlag = smsIssueFlag;
    }

    public BooleanFlag getIsPartialAllowed() {
        if (isPartialAllowed == null) {
            return BooleanFlag.NO;
        }
        return isPartialAllowed;
    }

    public void setIsPartialAllowed(BooleanFlag isPartialAllowed) {
        this.isPartialAllowed = isPartialAllowed;
    }

    public BillCategory getBillCategory() {
        if (billCategory == null) {
            return BillCategory.INSTANTLY;
        }
        return billCategory;
    }

    public void setBillCategory(BillCategory billCategory) {
        this.billCategory = billCategory;
    }

    public BigDecimal getMiniPartialAmount() {
        if (miniPartialAmount == null) {
            return BigDecimal.ZERO;
        }
        return miniPartialAmount;
    }

    public void setMiniPartialAmount(BigDecimal miniPartialAmount) {
        this.miniPartialAmount = miniPartialAmount;
    }

    public BigDecimal getMaxPartialAmount() {
        return maxPartialAmount;
    }

    public void setMaxPartialAmount(BigDecimal maxPartialAmount) {
        this.maxPartialAmount = maxPartialAmount;
    }

    public Integer getPaymentNumber() {
        if (paymentNumber == null) {
            return 0;
        }
        return paymentNumber;
    }

    public void setPaymentNumber(Integer paymentNumber) {
        this.paymentNumber = paymentNumber;
    }

    public Integer getSettlementNumber() {
        if (settlementNumber == null) {
            return 0;
        }
        return settlementNumber;
    }

    public void setSettlementNumber(Integer settlementNumber) {
        this.settlementNumber = settlementNumber;
    }

    public BigDecimal getRemainAmount() {
        if (remainAmount == null) {
            return getTotalRunningAmount();
        }
        return remainAmount;
    }

    public void setRemainAmount(BigDecimal remainAmount) {
        this.remainAmount = Utils.roundDecimalHalfDown(remainAmount);
    }

    public BigDecimal getCurrentPaidAmount() {
        if (currentPaidAmount == null) {
            return BigDecimal.ZERO;
        }
        return currentPaidAmount;
    }

    public void setCurrentPaidAmount(BigDecimal currentPaidAmount) {
        this.currentPaidAmount = Utils.roundDecimalHalfDown(currentPaidAmount);
    }

    public List<BillItem> getBillItemList() {
        return billItemList;
    }

    public void setBillItemList(List<BillItem> billItemList) {
        this.billItemList = billItemList;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getStamp() {
        return stamp;
    }

    public void setStamp(String stamp) {
        this.stamp = stamp;
    }

    public BigDecimal getTotalRunningAmount() {
        if (totalRunningAmount == null) {
            return totalAmount;
        }
        return totalRunningAmount;
    }

    public void setTotalRunningAmount(BigDecimal totalRunningAmount) {
        this.totalRunningAmount = Utils.roundDecimalHalfDown(totalRunningAmount);
    }

    public LocalDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public Integer getCycleNumber() {
        if (cycleNumber == null) {
            return 1;
        }
        return cycleNumber;
    }

    public void setCycleNumber(Integer cycleNumber) {
        this.cycleNumber = cycleNumber;
    }
}
