package be.bslash.ibps.bills.entities;


import be.bslash.ibps.bills.enums.AccessChannel;
import be.bslash.ibps.bills.enums.PaymentMethod;
import be.bslash.ibps.bills.enums.PaymentSource;
import be.bslash.ibps.bills.enums.PaymentStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@javax.persistence.Entity
@Table(name = "old_system_payment")
public class OldSystemPayment {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "old_system_payment_seq")
    @SequenceGenerator(name = "old_system_payment_seq", sequenceName = "old_system_payment_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "bank_payment_id")
    private String bankPaymentId;

    @Column(name = "sadad_payment_id")
    private String sadadPaymentId;

    @Column(name = "payment_amount")
    private BigDecimal paymentAmount;

    @Column(name = "payment_date")
    private LocalDateTime paymentDate;

    @Column(name = "bank_id")
    private String bankId;

    @Column(name = "branch_code")
    private String branchCode;

    @Column(name = "district_code")
    private String districtCode;

    @Enumerated(EnumType.STRING)
    @Column(name = "access_channel")
    private AccessChannel accessChannel;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_method")
    private PaymentMethod paymentMethod;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_source")
    private PaymentSource paymentSource;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "company_id")
    private Entity entity;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_status")
    private PaymentStatus paymentStatus;

    @Column(name = "sadad_bill_number")
    private String sadadBillNumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBankPaymentId() {
        return bankPaymentId;
    }

    public void setBankPaymentId(String bankPaymentId) {
        this.bankPaymentId = bankPaymentId;
    }

    public String getSadadPaymentId() {
        return sadadPaymentId;
    }

    public void setSadadPaymentId(String sadadPaymentId) {
        this.sadadPaymentId = sadadPaymentId;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public LocalDateTime getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDateTime paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public AccessChannel getAccessChannel() {
        return accessChannel;
    }

    public void setAccessChannel(AccessChannel accessChannel) {
        this.accessChannel = accessChannel;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public PaymentSource getPaymentSource() {
        return paymentSource;
    }

    public void setPaymentSource(PaymentSource paymentSource) {
        this.paymentSource = paymentSource;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getSadadBillNumber() {
        return sadadBillNumber;
    }

    public void setSadadBillNumber(String sadadBillNumber) {
        this.sadadBillNumber = sadadBillNumber;
    }
}
