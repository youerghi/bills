package be.bslash.ibps.bills.entities;


import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;

@javax.persistence.Entity
@Table(name = "entity_vat")
public class EntityVat {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "entity_vat_seq")
    @SequenceGenerator(name = "entity_vat_seq", sequenceName = "entity_vat_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "vat")
    private BigDecimal vat;

    @Column(name = "vat_display")
    private String vatDisplay;

    @OneToOne
    @JoinColumn(name = "entity_id")
    private Entity entity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getVat() {
        return vat;
    }

    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public String getVatDisplay() {
        return vatDisplay;
    }

    public void setVatDisplay(String vatDisplay) {
        this.vatDisplay = vatDisplay;
    }
}
