package be.bslash.ibps.bills.entities;


import be.bslash.ibps.bills.enums.BooleanFlag;
import be.bslash.ibps.bills.enums.DurationType;
import be.bslash.ibps.bills.enums.SendNotificationType;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@javax.persistence.Entity
@Table(name = "recurring_template")
public class RecurringTemplate {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "recurring_template_seq")
    @SequenceGenerator(name = "recurring_template_seq", sequenceName = "recurring_template_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "due_date")
    private LocalDateTime dueDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "bank_name_en")
    private DurationType durationType;

    @Column(name = "duration_frequency")
    private Integer durationFrequency;

    @Column(name = "installment_number")
    private Integer installmentNumber;

    @Column(name = "installment_amount")
    private BigDecimal installmentAmount;

    @Enumerated(EnumType.STRING)
    @Column(name = "send_notification")
    private SendNotificationType sendNotificationType;

    @Column(name = "no_of_days")
    private Integer numberOfDays;

    @Enumerated(EnumType.STRING)
    @Column(name = "is_partial_allowed")
    private BooleanFlag isPartialAllowed;

    @Column(name = "mini_partial_amount")
    private BigDecimal miniPartialAmount;

    @OneToOne
    @JoinColumn(name = "entity_id")
    private Entity entity;

    @OneToOne
    @JoinColumn(name = "entity_user_id")
    private EntityUser entityUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public DurationType getDurationType() {
        return durationType;
    }

    public void setDurationType(DurationType durationType) {
        this.durationType = durationType;
    }

    public Integer getDurationFrequency() {
        return durationFrequency;
    }

    public void setDurationFrequency(Integer durationFrequency) {
        this.durationFrequency = durationFrequency;
    }

    public Integer getInstallmentNumber() {
        return installmentNumber;
    }

    public void setInstallmentNumber(Integer installmentNumber) {
        this.installmentNumber = installmentNumber;
    }

    public BigDecimal getInstallmentAmount() {
        return installmentAmount;
    }

    public void setInstallmentAmount(BigDecimal installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public SendNotificationType getSendNotificationType() {
        return sendNotificationType;
    }

    public void setSendNotificationType(SendNotificationType sendNotificationType) {
        this.sendNotificationType = sendNotificationType;
    }

    public Integer getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(Integer numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public EntityUser getEntityUser() {
        return entityUser;
    }

    public void setEntityUser(EntityUser entityUser) {
        this.entityUser = entityUser;
    }

    public BooleanFlag getIsPartialAllowed() {
        return isPartialAllowed;
    }

    public void setIsPartialAllowed(BooleanFlag isPartialAllowed) {
        this.isPartialAllowed = isPartialAllowed;
    }

    public BigDecimal getMiniPartialAmount() {
        return miniPartialAmount;
    }

    public void setMiniPartialAmount(BigDecimal miniPartialAmount) {
        this.miniPartialAmount = miniPartialAmount;
    }
}
