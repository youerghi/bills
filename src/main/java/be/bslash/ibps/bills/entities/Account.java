package be.bslash.ibps.bills.entities;


import be.bslash.ibps.bills.dto.response.AccountDtoRs;
import be.bslash.ibps.bills.enums.AccountCategory;
import be.bslash.ibps.bills.enums.BooleanFlag;
import be.bslash.ibps.bills.enums.IdType;
import be.bslash.ibps.bills.enums.Status;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;

@javax.persistence.Entity
@Table(name = "account")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "account_type", discriminatorType = DiscriminatorType.INTEGER)
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_seq")
    @SequenceGenerator(name = "account_seq", sequenceName = "account_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "account_number")
    private String accountNumber;

    @Enumerated(EnumType.STRING)
    @Column(name = "id_type")
    private IdType idType;

    @Column(name = "id_number")
    private String idNumber;

    @Column(name = "vat_number")
    private String vatNumber;

    @Column(name = "attachment_id")
    private String attachmentId;

    @OneToOne
    @JoinColumn(name = "entity_id")
    private Entity entity;

    @OneToOne
    @JoinColumn(name = "entity_user_id")
    private EntityUser entityUser;

    @OneToOne
    @JoinColumn(name = "activity_id")
    private EntityActivity entityActivity;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Column(name = "street")
    private String street;

    @Column(name = "building_number")
    private String buildingNumber;

    @Column(name = "additional_number")
    private String additionalNumber;

    @Column(name = "postal_code")
    private String postalCode;

    @OneToOne
    @JoinColumn(name = "region_id")
    private Region region;

    @OneToOne
    @JoinColumn(name = "city_id")
    private City city;

    @Column(name = "other_city")
    private String otherCity;

    @OneToOne
    @JoinColumn(name = "district_id")
    private District district;

    @Column(name = "other_district")
    private String otherDistrict;

    @Enumerated(EnumType.STRING)
    @Column(name = "account_category")
    private AccountCategory accountCategory;

    @Enumerated(EnumType.STRING)
    @Column(name = "is_draft")
    private BooleanFlag isDraft;

    @Column(name = "balance")
    private BigDecimal balance;

    @Column(name = "mini_partial_amount")
    private BigDecimal miniPartialAmount;

    @Column(name = "cycle_number")
    private Integer cycleNumber;

    @Column(name = "reference_name")
    private String referenceName;

    public String getReferenceName() {
        return referenceName;
    }

    public void setReferenceName(String referenceName) {
        this.referenceName = referenceName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public IdType getIdType() {
        return idType;
    }

    public void setIdType(IdType idType) {
        this.idType = idType;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(String attachmentId) {
        this.attachmentId = attachmentId;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public EntityActivity getEntityActivity() {
        return entityActivity;
    }

    public void setEntityActivity(EntityActivity entityActivity) {
        this.entityActivity = entityActivity;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public String getAdditionalNumber() {
        return additionalNumber;
    }

    public void setAdditionalNumber(String additionalNumber) {
        this.additionalNumber = additionalNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getOtherCity() {
        return otherCity;
    }

    public void setOtherCity(String otherCity) {
        this.otherCity = otherCity;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public String getOtherDistrict() {
        return otherDistrict;
    }

    public void setOtherDistrict(String otherDistrict) {
        this.otherDistrict = otherDistrict;
    }

    public AccountCategory getAccountCategory() {
        return accountCategory;
    }

    public void setAccountCategory(AccountCategory accountCategory) {
        this.accountCategory = accountCategory;
    }

    public BooleanFlag getIsDraft() {
        return isDraft;
    }

    public void setIsDraft(BooleanFlag isDraft) {
        this.isDraft = isDraft;
    }

    public EntityUser getEntityUser() {
        return entityUser;
    }

    public void setEntityUser(EntityUser entityUser) {
        this.entityUser = entityUser;
    }

    public BigDecimal getBalance() {
        if (balance == null) {
            return BigDecimal.ZERO;
        }
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getMiniPartialAmount() {
        return miniPartialAmount;
    }

    public void setMiniPartialAmount(BigDecimal miniPartialAmount) {
        this.miniPartialAmount = miniPartialAmount;
    }

    public Integer getCycleNumber() {
        if (cycleNumber == null) {
            return 1;
        }
        return cycleNumber;
    }

    public void setCycleNumber(Integer cycleNumber) {
        this.cycleNumber = cycleNumber;
    }

    public AccountDtoRs toAccountDtoRs() {
        AccountDtoRs accountDtoRs = new AccountDtoRs();
        accountDtoRs.setId(this.getId());

        if (this instanceof CorporateAccount) {

            CorporateAccount corporateAccount = (CorporateAccount) this;
            accountDtoRs.setCityId(1L);
            accountDtoRs.setAccountNumber(corporateAccount.getAccountNumber());
            accountDtoRs.setType(corporateAccount.getAccountCategory() != null ? corporateAccount.getAccountCategory().name() : null);
            accountDtoRs.setAccount("BUSINESS");
            accountDtoRs.setCompanyName(corporateAccount.getCompanyName());
            accountDtoRs.setPrimaryContactPersonEmail(corporateAccount.getContactPersonEmail());
            accountDtoRs.setContactPersonNumber(corporateAccount.getContactPersonNumber());
            accountDtoRs.setIdNumber(corporateAccount.getIdNumber());
            accountDtoRs.setIdType(corporateAccount.getTypeIdDocument() != null ? corporateAccount.getTypeIdDocument().name() : null);
            accountDtoRs.setAccountName(corporateAccount.getCompanyName());
            accountDtoRs.setActivityId(corporateAccount.getEntityActivity() != null ? corporateAccount.getEntityActivity().getId() : null);
            accountDtoRs.setCityId(corporateAccount.getCity() != null ? corporateAccount.getCity().getId() : null);
            accountDtoRs.setStatus(corporateAccount.getStatus());
            accountDtoRs.setAttachmentId(corporateAccount.getAttachmentId());
            accountDtoRs.setContractNumber(corporateAccount.getContractNumber());
            accountDtoRs.setVatNumber(corporateAccount.getVatNumber());
            accountDtoRs.setAdditionalNumber(corporateAccount.getAdditionalNumber());
            accountDtoRs.setBuildingNumber(corporateAccount.getBuildingNumber());
            accountDtoRs.setDistrictId(corporateAccount.getDistrict() != null ? corporateAccount.getDistrict().getId() : 0L);
            accountDtoRs.setPostalCode(corporateAccount.getPostalCode());
            accountDtoRs.setStreet(corporateAccount.getStreet());
            accountDtoRs.setPrimaryContactPersonNumber(corporateAccount.getContactPersonNumber());
            accountDtoRs.setPrimaryContactPersonEmail(corporateAccount.getContactPersonEmail());
            accountDtoRs.setCompanyIdNumber(corporateAccount.getIdNumber());
            accountDtoRs.setCompanyIdType(corporateAccount.getTypeIdDocument() != null ? corporateAccount.getTypeIdDocument().name() : null);
            accountDtoRs.setCompanyType(corporateAccount.getEntityType() != null ? corporateAccount.getEntityType().name() : null);
            accountDtoRs.setPrimaryContactPersonName(corporateAccount.getContactPersonName());
            accountDtoRs.setStreetName(this.getStreet());
            accountDtoRs.setCity(corporateAccount.getCity() != null ? corporateAccount.getCity().getId() : null);
            accountDtoRs.setDistrict(corporateAccount.getDistrict() != null ? corporateAccount.getDistrict().getId() + "" : "0");
            accountDtoRs.setIdAttachment(corporateAccount.getAttachmentId());
            accountDtoRs.setOtherDistrict(corporateAccount.getOtherDistrict());
            accountDtoRs.setAccountBalance(corporateAccount.getBalance());
            accountDtoRs.setReferenceName(corporateAccount.getReferenceName());

        } else {
            IndividualAccount individualAccount = (IndividualAccount) this;
            accountDtoRs.setCityId(1L);
            accountDtoRs.setAccountNumber(individualAccount.getAccountNumber());
            accountDtoRs.setType(individualAccount.getAccountCategory() != null ? individualAccount.getAccountCategory().name() : null);
            accountDtoRs.setAccount("INDIVIDUAL");
            accountDtoRs.setFirstName(individualAccount.getFirstName());
            accountDtoRs.setSecondName(individualAccount.getSecondName());
            accountDtoRs.setThirdName(individualAccount.getThirdName());
            accountDtoRs.setLastName(individualAccount.getLastName());
            accountDtoRs.setEmail(individualAccount.getEmail());
            accountDtoRs.setPhoneNumber(individualAccount.getPhoneNumber());
            accountDtoRs.setIdNumber(individualAccount.getIdNumber());
            accountDtoRs.setIdType(individualAccount.getIdType() != null ? individualAccount.getIdType().name() : null);
            accountDtoRs.setAccountName(individualAccount.getFirstName() + " " + individualAccount.getLastName());
            accountDtoRs.setActivityId(individualAccount.getEntityActivity() != null ? individualAccount.getEntityActivity().getId() : null);
            accountDtoRs.setCityId(individualAccount.getCity() != null ? individualAccount.getCity().getId() : null);
            accountDtoRs.setStatus(individualAccount.getStatus());
            accountDtoRs.setAttachmentId(individualAccount.getAttachmentId());
            accountDtoRs.setGender(individualAccount.getGender());
            accountDtoRs.setReferenceNumber(individualAccount.getReferenceNumber());
            accountDtoRs.setVatNumber(individualAccount.getVatNumber());
            accountDtoRs.setAdditionalNumber(individualAccount.getAdditionalNumber());
            accountDtoRs.setBuildingNumber(individualAccount.getBuildingNumber());
            accountDtoRs.setDistrictId(individualAccount.getDistrict() != null ? individualAccount.getDistrict().getId() : 0L);
            accountDtoRs.setPostalCode(individualAccount.getPostalCode());
            accountDtoRs.setStreet(individualAccount.getStreet());
            accountDtoRs.setStreetName(this.getStreet());
            accountDtoRs.setCity(individualAccount.getCity() != null ? individualAccount.getCity().getId() : null);
            accountDtoRs.setDistrict(individualAccount.getDistrict() != null ? individualAccount.getDistrict().getId() + "" : "0");
            accountDtoRs.setIdAttachment(individualAccount.getAttachmentId());
            accountDtoRs.setOtherDistrict(individualAccount.getOtherDistrict());
            accountDtoRs.setAccountBalance(individualAccount.getBalance());
            accountDtoRs.setReferenceName(individualAccount.getReferenceName());
        }

        return accountDtoRs;
    }
}
