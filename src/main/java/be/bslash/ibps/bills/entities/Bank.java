package be.bslash.ibps.bills.entities;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Locale;

@Entity
@Table(name = "bank")
public class Bank {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bank_seq")
    @SequenceGenerator(name = "bank_seq", sequenceName = "bank_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "bank_code")
    private String code;

    @Column(name = "bank_name_ar")
    private String bankNameAr;

    @Column(name = "bank_name_en")
    private String bankNameEn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBankNameAr() {
        return bankNameAr;
    }

    public void setBankNameAr(String bankNameAr) {
        this.bankNameAr = bankNameAr;
    }

    public String getBankNameEn() {
        return bankNameEn;
    }

    public void setBankNameEn(String bankNameEn) {
        this.bankNameEn = bankNameEn;
    }

    public String getName(Locale locale) {
        if (locale.getISO3Language().equalsIgnoreCase("eng")) {
            return this.bankNameEn;
        } else {
            return this.bankNameAr;
        }
    }
}
