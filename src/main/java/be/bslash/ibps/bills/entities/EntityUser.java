package be.bslash.ibps.bills.entities;


import be.bslash.ibps.bills.enums.EntityUserType;
import be.bslash.ibps.bills.enums.IdType;
import be.bslash.ibps.bills.enums.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.LocalDateTime;

@javax.persistence.Entity
@Table(name = "entity_user")
public class EntityUser {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "entity_user_seq")
    @SequenceGenerator(name = "entity_user_seq", sequenceName = "entity_user_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

//    @Column(name = "second_name")
//    private String secondName;
//
//    @Column(name = "third_name")
//    private String thirdName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "username")
    private String username;

    @JsonIgnore
    @Column(name = "password")
    private String password;

    @OneToOne
    @JoinColumn(name = "nationality_id")
    private Nationality nationality;

    @Column(name = "id_number")
    private String idNumber;

    @Column(name = "date_of_birth")
    private LocalDateTime dateOfBirth;

    @Enumerated(EnumType.STRING)
    @Column(name = "id_type")
    private IdType idType;

    @Column(name = "id_expire_date")
    private LocalDateTime idExpireDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "user_type")
    private EntityUserType entityUserType;

    @Column(name = "mobile_number")
    private String mobileNumber;

    @Column(name = "email")
    private String email;

    @OneToOne
    @JoinColumn(name = "entity_id")
    private Entity entity;

    @Column(name = "create_date")
    private LocalDateTime createDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Column(name = "failed_login_counter", columnDefinition = "int default '0'")
    private Integer failedLoginCounter;

    @Column(name = "fcm_token")
    private String fcmToken;

    @Column(name = "is_password_reset", columnDefinition = "int default '0'")
    private Integer isPasswordReset;

    @Transient
    private String idExpiryReminderFlag;

    @Transient
    private Long idExpiryLeftDays;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

//    public String getSecondName() {
//        return secondName;
//    }
//
//    public void setSecondName(String secondName) {
//        this.secondName = secondName;
//    }
//
//    public String getThirdName() {
//        return thirdName;
//    }
//
//    public void setThirdName(String thirdName) {
//        this.thirdName = thirdName;
//    }

    public Nationality getNationality() {
        return nationality;
    }

    public void setNationality(Nationality nationality) {
        this.nationality = nationality;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public LocalDateTime getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDateTime dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public IdType getIdType() {
        return idType;
    }

    public void setIdType(IdType idType) {
        this.idType = idType;
    }

    public LocalDateTime getIdExpireDate() {
        return idExpireDate;
    }

    public void setIdExpireDate(LocalDateTime idExpireDate) {
        this.idExpireDate = idExpireDate;
    }

    public EntityUserType getEntityUserType() {
        return entityUserType;
    }

    public void setEntityUserType(EntityUserType entityUserType) {
        this.entityUserType = entityUserType;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getIdExpiryReminderFlag() {
        return idExpiryReminderFlag;
    }

    public void setIdExpiryReminderFlag(String idExpiryReminderFlag) {
        this.idExpiryReminderFlag = idExpiryReminderFlag;
    }

    public Long getIdExpiryLeftDays() {
        return idExpiryLeftDays;
    }

    public void setIdExpiryLeftDays(Long idExpiryLeftDays) {
        this.idExpiryLeftDays = idExpiryLeftDays;
    }

    public Integer getFailedLoginCounter() {
        return failedLoginCounter;
    }

    public void setFailedLoginCounter(Integer failedLoginCounter) {
        this.failedLoginCounter = failedLoginCounter;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public Integer getIsPasswordReset() {
        if (isPasswordReset == null) {
            return 0;
        }
        return isPasswordReset;
    }

    public void setIsPasswordReset(Integer isPasswordReset) {
        this.isPasswordReset = isPasswordReset;
    }
}
