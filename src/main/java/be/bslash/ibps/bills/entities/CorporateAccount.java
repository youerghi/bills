package be.bslash.ibps.bills.entities;


import be.bslash.ibps.bills.enums.EntityType;
import be.bslash.ibps.bills.enums.TypeIdDocument;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
@DiscriminatorValue("2")
public class CorporateAccount extends Account {

    @Column(name = "company_name")
    private String companyName;

    @Enumerated(EnumType.STRING)
    @Column(name = "entity_type")
    private EntityType entityType;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_id_document")
    private TypeIdDocument typeIdDocument;

    @Column(name = "contract_number")
    private String contractNumber;

    @Column(name = "primary_contact_person_name")
    private String contactPersonName;

    @Column(name = "primary_contact_person_number")
    private String contactPersonNumber;

    @Column(name = "primary_contact_person_email")
    private String contactPersonEmail;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getContactPersonNumber() {
        return contactPersonNumber;
    }

    public void setContactPersonNumber(String contactPersonNumber) {
        this.contactPersonNumber = contactPersonNumber;
    }

    public String getContactPersonEmail() {
        return contactPersonEmail;
    }

    public void setContactPersonEmail(String contactPersonEmail) {
        this.contactPersonEmail = contactPersonEmail;
    }

    public TypeIdDocument getTypeIdDocument() {
        return typeIdDocument;
    }

    public void setTypeIdDocument(TypeIdDocument typeIdDocument) {
        this.typeIdDocument = typeIdDocument;
    }
}
