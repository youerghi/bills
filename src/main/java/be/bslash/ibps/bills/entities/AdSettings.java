package be.bslash.ibps.bills.entities;

import be.bslash.ibps.bills.enums.Status;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Set;

@javax.persistence.Entity
@Table(name = "ad_settings")
public class AdSettings {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ad_settings_seq")
    @SequenceGenerator(name = "ad_settings_seq", sequenceName = "ad_settings_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @OneToOne
    @JoinColumn(name = "biller_id")
    private Entity entity;

    @Enumerated(EnumType.STRING)
    @Column(name = "ad_module_status")
    private Status campaignActive;

    @Enumerated(EnumType.STRING)
    @Column(name = "master_adv_status")
    private Status masterAdvEnable;

    @Column(name = "campaign_id")
    private Integer masterCampaignId;

    @Enumerated(EnumType.STRING)
    @Column(name = "url_click_status")
    private Status urlClickable;

    @Enumerated(EnumType.STRING)
    @Column(name = "ad_switch_enable")
    private Status adSwitchEnable;

    @Column(name = "switch_duration")
    private Integer adSwitchDuration;

    @Column(name = "master_url")
    private String masterURL;

    @Enumerated(EnumType.STRING)
    @Column(name = "master_url_enable")
    private Status masterURLEnable;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "adSettings", cascade = CascadeType.ALL)
    private Set<AdCampaignUsers> adCampaignUserSet;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }

    public Status getCampaignActive() {
        return campaignActive;
    }

    public void setCampaignActive(Status campaignActive) {
        this.campaignActive = campaignActive;
    }

    public Status getMasterAdvEnable() {
        return masterAdvEnable;
    }

    public void setMasterAdvEnable(Status masterAdvEnable) {
        this.masterAdvEnable = masterAdvEnable;
    }

    public Status getUrlClickable() {
        return urlClickable;
    }

    public void setUrlClickable(Status urlClickable) {
        this.urlClickable = urlClickable;
    }

    public Status getAdSwitchEnable() {
        return adSwitchEnable;
    }

    public void setAdSwitchEnable(Status adSwitchEnable) {
        this.adSwitchEnable = adSwitchEnable;
    }

    public Set<AdCampaignUsers> getAdCampaignUserSet() {
        return adCampaignUserSet;
    }

    public void setAdCampaignUserSet(Set<AdCampaignUsers> adCampaignUserSet) {
        this.adCampaignUserSet = adCampaignUserSet;
    }

    public String getMasterURL() {
        return masterURL;
    }

    public void setMasterURL(String masterURL) {
        this.masterURL = masterURL;
    }

    public Status getMasterURLEnable() {
        return masterURLEnable;
    }

    public void setMasterURLEnable(Status masterURLEnable) {
        this.masterURLEnable = masterURLEnable;
    }

    public Integer getMasterCampaignId() {
        return masterCampaignId;
    }

    public void setMasterCampaignId(Integer masterCampaignId) {
        this.masterCampaignId = masterCampaignId;
    }

    public Integer getAdSwitchDuration() {
        return adSwitchDuration;
    }

    public void setAdSwitchDuration(Integer adSwitchDuration) {
        this.adSwitchDuration = adSwitchDuration;
    }

}
