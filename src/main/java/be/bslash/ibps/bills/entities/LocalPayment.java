package be.bslash.ibps.bills.entities;


import be.bslash.ibps.bills.enums.PaymentMethod;
import be.bslash.ibps.bills.enums.PaymentSource;
import be.bslash.ibps.bills.enums.PaymentStatus;
import be.bslash.ibps.bills.utils.NumberToWord;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "local_payment")
public class LocalPayment {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "local_payment_seq")
    @SequenceGenerator(name = "local_payment_seq", sequenceName = "local_payment_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "local_payment_id")
    private String localPaymentId;

    @Column(name = "global_payment_id")
    private String globalPaymentId;

    @Column(name = "sadad_payment_id")
    private String sadadPaymentId;

    @Column(name = "payment_amount")
    private BigDecimal paymentAmount;

    @Column(name = "remaining_amount")
    private BigDecimal remainingAmount;

    @Column(name = "payment_date")
    private LocalDateTime paymentDate;

    @Column(name = "create_date")
    private LocalDateTime createDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_source")
    private PaymentSource paymentSource;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "bill_id")
    private Bill bill;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "company_user_id")
    private EntityUser entityUser;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_method")
    private PaymentMethod paymentMethod;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_status")
    private PaymentStatus paymentStatus;

    @Column(name = "voucher_number")
    private Long voucherNumber;

    @Column(name = "payment_number")
    private Integer paymentNumber;

    @Transient
    private BigDecimal totalAmount;

    @Transient
    private String paymentImage;

    @Transient
    private String paymentType;

    @Transient
    private String totalAmountAr;

    @Transient
    private String totalAmountEn;

    @Transient
    private String paymentAmountAr;

    @Transient
    private String paymentAmountEn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocalPaymentId() {
        return localPaymentId;
    }

    public void setLocalPaymentId(String localPaymentId) {
        this.localPaymentId = localPaymentId;
    }

    public String getGlobalPaymentId() {
        return globalPaymentId;
    }

    public void setGlobalPaymentId(String globalPaymentId) {
        this.globalPaymentId = globalPaymentId;
    }

    public String getSadadPaymentId() {
        return sadadPaymentId;
    }

    public void setSadadPaymentId(String sadadPaymentId) {
        this.sadadPaymentId = sadadPaymentId;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public LocalDateTime getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDateTime paymentDate) {
        this.paymentDate = paymentDate;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public PaymentSource getPaymentSource() {
        return paymentSource;
    }

    public void setPaymentSource(PaymentSource paymentSource) {
        this.paymentSource = paymentSource;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public EntityUser getEntityUser() {
        return entityUser;
    }

    public void setEntityUser(EntityUser entityUser) {
        this.entityUser = entityUser;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentImage() {
        if (paymentMethod == null) {
            return null;
        }
        switch (this.paymentMethod.name()) {
            case "MADA":
                return "https://www.alahli.com/en-us/about-us/csr/PublishingImages/mada-logo-474-Px.png";
            case "VISA":
                return "https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/Visa.svg/1200px-Visa.svg.png";
            case "MASTER":
                return "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Mastercard-logo.svg/200px-Mastercard-logo.svg.png";
            case "AMERICAN_EXP":
                return "https://i.dlpng.com/static/png/6699838_preview.png";
            case "APPLEPAY":
                return "https://seeklogo.com/images/A/apple-pay-logo-F68C9AC252-seeklogo.com.png";
            case "EWALLET":
                return "https://cdn6.f-cdn.com/contestentries/92694/11771521/53da7de995a1a_thumb900.jpg";
            default:
                return null;
        }
    }

    public void setPaymentImage(String paymentImage) {
        this.paymentImage = paymentImage;
    }

    public String getPaymentType() {
        return "OFFLINE_PAYMENT";
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public BigDecimal getTotalAmount() {
        return this.bill.getTotalAmount();
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Long getVoucherNumber() {
        if (voucherNumber == null) {
            return 0L;
        }
        return voucherNumber;
    }

    public void setVoucherNumber(Long voucherNumber) {
        this.voucherNumber = voucherNumber;
    }

    public Integer getPaymentNumber() {
        if (paymentNumber == null) {
            return 0;
        }
        return paymentNumber;
    }

    public void setPaymentNumber(Integer paymentNumber) {
        this.paymentNumber = paymentNumber;
    }

    public String getTotalAmountAr() {
        try {
            String amountAr = NumberToWord.convertNumberToArabicWords(this.bill.getTotalAmount().intValue() + "");
            return amountAr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return this.bill.getTotalAmount().toString();
        }
    }

    public void setTotalAmountAr(String totalAmountAr) {
        this.totalAmountAr = totalAmountAr;
    }

    public String getTotalAmountEn() {
        try {
            String amountAr = NumberToWord.convertNumberToEnglishWords(this.bill.getTotalAmount().intValue() + "");
            return amountAr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return this.bill.getTotalAmount().toString();
        }
    }

    public void setTotalAmountEn(String totalAmountEn) {
        this.totalAmountEn = totalAmountEn;
    }

    public String getPaymentAmountAr() {
        try {
            String amountAr = NumberToWord.convertNumberToArabicWords(this.paymentAmount.intValue() + "");
            return amountAr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return this.paymentAmount.toString();
        }
    }

    public void setPaymentAmountAr(String paymentAmountAr) {
        this.paymentAmountAr = paymentAmountAr;
    }

    public String getPaymentAmountEn() {
        try {
            String amountAr = NumberToWord.convertNumberToEnglishWords(this.paymentAmount.intValue() + "");
            return amountAr;
        } catch (Exception ex) {
            ex.printStackTrace();
            return this.paymentAmount.toString();
        }
    }

    public void setPaymentAmountEn(String paymentAmountEn) {
        this.paymentAmountEn = paymentAmountEn;
    }

    public BigDecimal getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(BigDecimal remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }
}
