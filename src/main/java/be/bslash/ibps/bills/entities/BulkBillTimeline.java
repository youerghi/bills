package be.bslash.ibps.bills.entities;


import be.bslash.ibps.bills.enums.BulkBillStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "bulk_bill_timeline")
public class BulkBillTimeline {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bulk_bill_timeline_seq")
    @SequenceGenerator(name = "bulk_bill_timeline_seq", sequenceName = "bulk_bill_timeline_seq", initialValue = 1, allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @OneToOne
    @JoinColumn(name = "builk_bill_id")
    private BulkBill bulkBill;

    @OneToOne
    @JoinColumn(name = "company_user_id")
    private EntityUser entityUser;

    @Enumerated(EnumType.STRING)
    @Column(name = "bulk_bill_status")
    private BulkBillStatus bulkBillStatus;

    @Column(name = "action_date")
    private LocalDateTime actionDate;

    @Column(name = "user_reference")
    private String userReference;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BulkBill getBulkBill() {
        return bulkBill;
    }

    public void setBulkBill(BulkBill bulkBill) {
        this.bulkBill = bulkBill;
    }

    public EntityUser getEntityUser() {
        return entityUser;
    }

    public void setEntityUser(EntityUser entityUser) {
        this.entityUser = entityUser;
    }

    public BulkBillStatus getBulkBillStatus() {
        return bulkBillStatus;
    }

    public void setBulkBillStatus(BulkBillStatus bulkBillStatus) {
        this.bulkBillStatus = bulkBillStatus;
    }

    public LocalDateTime getActionDate() {
        return actionDate;
    }

    public void setActionDate(LocalDateTime actionDate) {
        this.actionDate = actionDate;
    }

    public String getUserReference() {
        return userReference;
    }

    public void setUserReference(String userReference) {
        this.userReference = userReference;
    }


}
