package be.bslash.ibps.bills.ws;

public class WebServiceResponse {

    private Integer status;
    private String body;

    public WebServiceResponse() {
    }

    public WebServiceResponse(Integer status, String body) {
        this.status = status;
        this.body = body;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        if (status == 201 || status == 202)
            this.status = 200;
        else
            this.status = status;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
