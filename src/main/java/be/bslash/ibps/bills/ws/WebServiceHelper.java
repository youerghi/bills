package be.bslash.ibps.bills.ws;

import be.bslash.ibps.bills.entities.Bill;
import be.bslash.ibps.bills.entities.BulkBill;
import be.bslash.ibps.bills.entities.LocalPayment;
import be.bslash.ibps.bills.utils.Utils;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import java.io.IOException;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Component
public class WebServiceHelper {

    private static final Logger LOGGER = LogManager.getLogger("BACKEND_LOGS");

    @Autowired
    private Environment environment;

    public WebServiceResponse sendSmsNotification(String title,
                                                  String body,
                                                  String mobileNumber) {
        TrustAllManager trustAllManager = new TrustAllManager();

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(180, TimeUnit.SECONDS);
        client.setSslSocketFactory(createTrustAllSSLFactory(trustAllManager));
        client.setHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("title", title);
        jsonObject.put("body", body);
        jsonObject.put("mobileNumber", mobileNumber);

        MediaType mediaType = MediaType.parse("application/json");

        RequestBody reqBody = RequestBody.create(mediaType, jsonObject.toString());
        Request request = new Request.Builder()
                .url(environment.getProperty("notification_url") + "/send/sms")
                .post(reqBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();

        WebServiceResponse webServiceResponse = new WebServiceResponse();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            webServiceResponse.setStatus(response.code());
            webServiceResponse.setBody(response.body().string());
        } catch (Exception ex) {
            webServiceResponse.setStatus(500);
            webServiceResponse.setBody(ex.getMessage());
        } finally {
            if (response != null) {
                try {
                    response.body().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return webServiceResponse;
    }

    public WebServiceResponse calculateSmsPoint(String title,
                                                String body,
                                                String mobileNumber) {
        TrustAllManager trustAllManager = new TrustAllManager();

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(180, TimeUnit.SECONDS);
        client.setSslSocketFactory(createTrustAllSSLFactory(trustAllManager));
        client.setHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("title", title);
        jsonObject.put("body", body);
        jsonObject.put("mobileNumber", mobileNumber);

        MediaType mediaType = MediaType.parse("application/json");

        RequestBody reqBody = RequestBody.create(mediaType, jsonObject.toString());
        Request request = new Request.Builder()
                .url(environment.getProperty("notification_url") + "/calculate/sms/cost")
                .post(reqBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();

        WebServiceResponse webServiceResponse = new WebServiceResponse();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            webServiceResponse.setStatus(response.code());
            webServiceResponse.setBody(response.body().string());
        } catch (Exception ex) {
            webServiceResponse.setStatus(500);
            webServiceResponse.setBody(ex.getMessage());
        } finally {
            if (response != null) {
                try {
                    response.body().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return webServiceResponse;
    }


    public WebServiceResponse sendEmailNotification(String fromEmail,
                                                    String subject,
                                                    String toEmail,
                                                    String type,
                                                    String body) {
        TrustAllManager trustAllManager = new TrustAllManager();

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(180, TimeUnit.SECONDS);
        client.setSslSocketFactory(createTrustAllSSLFactory(trustAllManager));
        client.setHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("fromEmail", fromEmail);
        jsonObject.put("subject", subject);
        jsonObject.put("toEmail", toEmail);
        jsonObject.put("type", type);
        jsonObject.put("body", body);

        LOGGER.info(jsonObject);

        MediaType mediaType = MediaType.parse("application/json");

        RequestBody reqBody = RequestBody.create(mediaType, jsonObject.toString());
        Request request = new Request.Builder()
                .url(environment.getProperty("notification_url") + "/send/email")
                .post(reqBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();

        WebServiceResponse webServiceResponse = new WebServiceResponse();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            webServiceResponse.setStatus(response.code());
            webServiceResponse.setBody(response.body().string());
        } catch (Exception ex) {
            webServiceResponse.setStatus(500);
            webServiceResponse.setBody(ex.getMessage());
        } finally {
            if (response != null) {
                try {
                    response.body().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return webServiceResponse;
    }

    public WebServiceResponse sendFcmNotification(String title,
                                                  String body,
                                                  String data,
                                                  List<String> tokenList) {
        TrustAllManager trustAllManager = new TrustAllManager();

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(180, TimeUnit.SECONDS);
        client.setSslSocketFactory(createTrustAllSSLFactory(trustAllManager));
        client.setHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("title", title);
        jsonObject.put("body", body);
        jsonObject.put("data", data);
        jsonObject.put("tokenList", tokenList);

        LOGGER.info("FCM: " + jsonObject.toString());

        MediaType mediaType = MediaType.parse("application/json");

        RequestBody reqBody = RequestBody.create(mediaType, jsonObject.toString());
        Request request = new Request.Builder()
                .url(environment.getProperty("notification_url") + "/send/fcm")
                .post(reqBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();

        WebServiceResponse webServiceResponse = new WebServiceResponse();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            webServiceResponse.setStatus(response.code());
            webServiceResponse.setBody(response.body().string());
        } catch (Exception ex) {
            webServiceResponse.setStatus(500);
            webServiceResponse.setBody(ex.getMessage());
        } finally {
            if (response != null) {
                try {
                    response.body().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        LOGGER.info("FCM Status: " + webServiceResponse.getStatus());
        LOGGER.info("FCM Body: " + webServiceResponse.getBody());
        return webServiceResponse;
    }

    public WebServiceResponse uploadBill(Bill bill, String billStatusCode) {
        LOGGER.info("Start upload bill");

        TrustAllManager trustAllManager = new TrustAllManager();

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(180, TimeUnit.SECONDS);
        client.setSslSocketFactory(createTrustAllSSLFactory(trustAllManager));
        client.setHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });


        JSONObject jsonObject = new JSONObject();
        jsonObject.put("rqUID", UUID.randomUUID());
        jsonObject.put("date", Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd'T'HH:mm:ss"));
        jsonObject.put("generationDate", Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd'T'HH:mm:ss"));
        jsonObject.put("billStatusCode", billStatusCode);
        jsonObject.put("billCategory", bill.getBillCategory() != null ? bill.getBillCategory().getValue() : "instantly");
        jsonObject.put("serviceType", "UTIL");
        jsonObject.put("billCycle", "One-off");
        jsonObject.put("billNumber", bill.getSadadNumber());
        jsonObject.put("billerId", "902");
        jsonObject.put("amountDue", bill.getTotalRunningAmount());
        jsonObject.put("dueDt", Utils.parseDateTime(bill.getIssueDate().withHour(8).withMinute(0), "yyyy-MM-dd'T'hh:mm:00"));
        jsonObject.put("expDt", Utils.parseDateTime(bill.getExpireDate().withHour(8).withMinute(0), "yyyy-MM-dd'T'hh:mm:00"));
        jsonObject.put("chkDigit", "01");
        jsonObject.put("billRefInfo", bill.getSadadNumber());

        if (bill.getMiniPartialAmount() != null) {
            jsonObject.put("miniPartialAmount", bill.getMiniPartialAmount());
        } else {
            jsonObject.put("miniPartialAmount", bill.getEntityActivity().getTheLowestAmountPerUploadedBill());
        }

        LOGGER.info("Upload Rq: " + jsonObject.toString());

        MediaType mediaType = MediaType.parse("application/json");

        RequestBody reqBody = RequestBody.create(mediaType, jsonObject.toString());
        Request request = new Request.Builder()
                .url(environment.getProperty("sadad_url") + "/bill/uploadBill")
                .post(reqBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();

        WebServiceResponse webServiceResponse = new WebServiceResponse();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            webServiceResponse.setStatus(response.code());
            webServiceResponse.setBody(response.body().string());
        } catch (Exception ex) {
            LOGGER.error("Error while calling integration");
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            webServiceResponse.setStatus(500);
            webServiceResponse.setBody(ex.getMessage());
        } finally {
            if (response != null) {
                try {
                    response.body().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return webServiceResponse;

    }

    public WebServiceResponse uploadRecurringBill(Bill bill, String billStatusCode) {
        LOGGER.info("Start upload bill");

        TrustAllManager trustAllManager = new TrustAllManager();

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(180, TimeUnit.SECONDS);
        client.setSslSocketFactory(createTrustAllSSLFactory(trustAllManager));
        client.setHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });


        JSONObject jsonObject = new JSONObject();
        jsonObject.put("rqUID", UUID.randomUUID());
        jsonObject.put("date", Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd'T'HH:mm:ss"));
        jsonObject.put("generationDate", Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd'T'HH:mm:ss"));
        jsonObject.put("billStatusCode", billStatusCode);
        jsonObject.put("billCategory", bill.getBillCategory().getValue());
        jsonObject.put("serviceType", "UTIL");
        jsonObject.put("billCycle", bill.getCycleNumber());
        jsonObject.put("billNumber", bill.getSadadNumber());
        jsonObject.put("billerId", "902");
        jsonObject.put("amountDue", bill.getTotalRunningAmount());
        jsonObject.put("dueDt", Utils.parseDateTime(bill.getDueDate().withHour(8).withMinute(0), "yyyy-MM-dd'T'hh:mm:00"));
        jsonObject.put("chkDigit", "01");
        jsonObject.put("billRefInfo", bill.getSadadNumber());

        if (bill.getMiniPartialAmount() != null) {
            jsonObject.put("miniPartialAmount", bill.getMiniPartialAmount());
        } else {
            jsonObject.put("miniPartialAmount", bill.getEntityActivity().getTheLowestAmountPerUploadedBill());
        }

        LOGGER.info("Upload Rq: " + jsonObject.toString());

        MediaType mediaType = MediaType.parse("application/json");

        RequestBody reqBody = RequestBody.create(mediaType, jsonObject.toString());
        Request request = new Request.Builder()
                .url(environment.getProperty("sadad_url") + "/bill/uploadRecurringBill")
                .post(reqBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();

        WebServiceResponse webServiceResponse = new WebServiceResponse();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            webServiceResponse.setStatus(response.code());
            webServiceResponse.setBody(response.body().string());
        } catch (Exception ex) {
            LOGGER.error("Error while calling integration");
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            webServiceResponse.setStatus(500);
            webServiceResponse.setBody(ex.getMessage());
        } finally {
            if (response != null) {
                try {
                    response.body().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return webServiceResponse;

    }


    public WebServiceResponse uploadPayment(LocalPayment localPayment) {

        LOGGER.info("Start uploadPayment bill");

        TrustAllManager trustAllManager = new TrustAllManager();

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(180, TimeUnit.SECONDS);
        client.setSslSocketFactory(createTrustAllSSLFactory(trustAllManager));
        client.setHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });


        JSONObject jsonObject = new JSONObject();
        jsonObject.put("rqUID", UUID.randomUUID());
        jsonObject.put("date", Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd'T'HH:mm:ss"));
        jsonObject.put("billerId", "902");
        jsonObject.put("curAmt", Utils.roundDecimal(localPayment.getPaymentAmount()));
        jsonObject.put("prcDt", Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd'T'HH:mm:ss"));
        jsonObject.put("dueDt", Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd'T'HH:mm:ss"));
        jsonObject.put("billerPmtId", localPayment.getGlobalPaymentId());
        jsonObject.put("billingAcct", localPayment.getBill().getSadadNumber());
        jsonObject.put("billNumber", localPayment.getBill().getSadadNumber());
        jsonObject.put("timeStamp", Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd'T'HH:mm:ss"));

        MediaType mediaType = MediaType.parse("application/json");

        RequestBody reqBody = RequestBody.create(mediaType, jsonObject.toString());
        Request request = new Request.Builder()
                .url(environment.getProperty("sadad_url") + "/bill/uploadPayment")
                .post(reqBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();

        WebServiceResponse webServiceResponse = new WebServiceResponse();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            webServiceResponse.setStatus(response.code());
            webServiceResponse.setBody(response.body().string());
        } catch (Exception ex) {
            LOGGER.error("Error while calling integration");
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            webServiceResponse.setStatus(500);
            webServiceResponse.setBody(ex.getMessage());
        } finally {
            if (response != null) {
                try {
                    response.body().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return webServiceResponse;
    }

    public WebServiceResponse uploadBulk(BulkBill bulkBill, List<Bill> billList) {

        LOGGER.info("Start uploadBulk bill");


        TrustAllManager trustAllManager = new TrustAllManager();

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(180, TimeUnit.SECONDS);
        client.setSslSocketFactory(createTrustAllSSLFactory(trustAllManager));
        client.setHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("rqUID", bulkBill.getAsyncRqUID());
        jsonObject.put("clientDt", Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd'T'HH:mm:ss"));
        jsonObject.put("billerId", "902");
        jsonObject.put("serviceType", "UTIL");
        jsonObject.put("timestamp", Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd'T'HH:mm:ss"));
        jsonObject.put("billCategory", "instantly");

        JSONArray billsJsonArray = new JSONArray();
        for (Bill bill : billList) {

            JSONObject billJsonObject = new JSONObject();
            billJsonObject.put("billStatusCode", "BillNew");
            billJsonObject.put("billCategory", "instantly");
            billJsonObject.put("serviceType", "UTIL");
            billJsonObject.put("billCycle", "One-off");
            billJsonObject.put("billNumber", bill.getSadadNumber());
            billJsonObject.put("billingAcct", bill.getSadadNumber());
            billJsonObject.put("billerId", "902");
            billJsonObject.put("amountDue", bill.getTotalAmount());
            billJsonObject.put("dueDt", Utils.parseDateTime(bill.getIssueDate().withHour(8).withMinute(0), "yyyy-MM-dd'T'HH:mm:ss"));
            billJsonObject.put("expDt", Utils.parseDateTime(bill.getExpireDate().withHour(8).withMinute(0), "yyyy-MM-dd'T'HH:mm:ss"));
            billJsonObject.put("chkDigit", "01");
            billJsonObject.put("billRefInfo", bill.getSadadNumber());
            billJsonObject.put("timestamp", Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd'T'HH:mm:ss"));

            billsJsonArray.put(billJsonObject);
        }

        jsonObject.put("billRecords", billsJsonArray);

        MediaType mediaType = MediaType.parse("application/json");

        RequestBody reqBody = RequestBody.create(mediaType, jsonObject.toString());
        Request request = new Request.Builder()
                .url(environment.getProperty("sadad_url") + "/bill/uploadBulk")
                .post(reqBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();

        WebServiceResponse webServiceResponse = new WebServiceResponse();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            webServiceResponse.setStatus(response.code());
            webServiceResponse.setBody(response.body().string());
        } catch (Exception ex) {
            LOGGER.error("Error while calling integration");
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            webServiceResponse.setStatus(500);
            webServiceResponse.setBody(ex.getMessage());
        } finally {
            if (response != null) {
                try {
                    response.body().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return webServiceResponse;
    }


    public WebServiceResponse verifyRecaptcha(String recaptcha) {
        LOGGER.info("Start verify recaptcha");

        TrustAllManager trustAllManager = new TrustAllManager();

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(180, TimeUnit.SECONDS);
        client.setSslSocketFactory(createTrustAllSSLFactory(trustAllManager));
        client.setHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });


        JSONObject jsonObject = new JSONObject();
        jsonObject.put("response", recaptcha);

        LOGGER.info("Verify Rq: " + jsonObject.toString());

        MediaType mediaType = MediaType.parse("application/json");

        RequestBody reqBody = RequestBody.create(mediaType, jsonObject.toString());
        Request request = new Request.Builder()
                .url(environment.getProperty("notification_url") + "/verify/recaptcha")
                .post(reqBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();

        WebServiceResponse webServiceResponse = new WebServiceResponse();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            webServiceResponse.setStatus(response.code());
            webServiceResponse.setBody(response.body().string());
        } catch (Exception ex) {
            LOGGER.error("Error while calling integration");
            LOGGER.error(ExceptionUtils.getStackTrace(ex));
            webServiceResponse.setStatus(500);
            webServiceResponse.setBody(ex.getMessage());
        } finally {
            if (response != null) {
                try {
                    response.body().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return webServiceResponse;
    }

    private SSLSocketFactory createTrustAllSSLFactory(TrustAllManager trustAllManager) {
        SSLSocketFactory ssfFactory = null;
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, new TrustManager[]{trustAllManager}, new SecureRandom());
            ssfFactory = sc.getSocketFactory();
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }

        return ssfFactory;
    }


}
