package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.CustomerCustomField;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CustomerCustomFieldRepo extends CrudRepository<CustomerCustomField, Long> {

    List<CustomerCustomField> findAll();

}
