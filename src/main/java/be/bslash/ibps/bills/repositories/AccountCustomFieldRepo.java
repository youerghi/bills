package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.AccountCustomField;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AccountCustomFieldRepo extends CrudRepository<AccountCustomField, Long> {

    List<AccountCustomField> findAll();

}
