package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.Bill;
import be.bslash.ibps.bills.entities.BillMobileNumber;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BillMobileNumberRepo extends CrudRepository<BillMobileNumber, Long> {

    List<BillMobileNumber> findAll();


    @Query("select item from BillMobileNumber item " +
            "where item.bill=:bill " +
            "order by item.id asc ")
    List<BillMobileNumber> findAllBillMobileNumber(@Param("bill") Bill bill);

    void deleteAllByBill(Bill bill);
}
