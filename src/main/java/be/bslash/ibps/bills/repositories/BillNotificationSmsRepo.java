package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.Bill;
import be.bslash.ibps.bills.entities.BillNotificationSms;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BillNotificationSmsRepo extends CrudRepository<BillNotificationSms, Long>, JpaSpecificationExecutor {

    List<BillNotificationSms> findAll();

    @Query("select sms from BillNotificationSms sms " +
            "where (:bill is null or sms.bill = :bill) " +
            "and (:mobile is null or sms.mobileNumber = :mobile) " +
            "and sms.billNotificationType='ISSUE' ")
    List<BillNotificationSms> findByMobile(
            @Param("bill") Bill bill,
            @Param("mobile") String mobile
    );


}
