package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.BulkBill;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BulkBillRepo extends CrudRepository<BulkBill, Long>, JpaSpecificationExecutor<BulkBill> {

    List<BulkBill> findAll();

    BulkBill findBulkBillById(Long id);

}
