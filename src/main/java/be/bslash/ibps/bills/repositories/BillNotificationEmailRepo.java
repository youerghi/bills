package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.Bill;
import be.bslash.ibps.bills.entities.BillNotificationEmail;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BillNotificationEmailRepo extends CrudRepository<BillNotificationEmail, Long> {

    List<BillNotificationEmail> findAll();

    @Query("select sms from BillNotificationEmail sms " +
            "where (:bill is null or sms.bill = :bill) " +
            "and (:mobile is null or sms.email = :mobile) " +
            "and sms.billNotificationType='ISSUE' ")
    List<BillNotificationEmail> findByEmail(
            @Param("bill") Bill bill,
            @Param("mobile") String mobile
    );

}
