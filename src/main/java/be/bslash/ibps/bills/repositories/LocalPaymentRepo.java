package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.Bill;
import be.bslash.ibps.bills.entities.Entity;
import be.bslash.ibps.bills.entities.LocalPayment;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LocalPaymentRepo extends CrudRepository<LocalPayment, Long> {

    List<LocalPayment> findAll();

    List<LocalPayment> findLocalPaymentByBillOrderByIdAsc(Bill bill);

}
