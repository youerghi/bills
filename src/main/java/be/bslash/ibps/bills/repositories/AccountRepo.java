package be.bslash.ibps.bills.repositories;


import be.bslash.ibps.bills.entities.Account;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface AccountRepo extends CrudRepository<Account, Long> {


    List<Account> findAll();

    Account findAccountById(Long id);

    Account findAccountByAccountNumber(String accountNumber);

    @Modifying
    @Query("update Account acc set acc.cycleNumber=:cycleNumber " +
            "where acc.id=:accountId")
    void increaseCycleNumberCounter(
            @Param("cycleNumber") Integer cycleNumber,
            @Param("accountId") Long accountId);

    @Modifying
    @Query("update Account acc set acc.balance=:balance " +
            "where acc.id=:accountId")
    void updateBalance(
            @Param("balance") BigDecimal balance,
            @Param("accountId") Long accountId);
}
