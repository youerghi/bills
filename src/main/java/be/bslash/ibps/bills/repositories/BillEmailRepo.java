package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.Bill;
import be.bslash.ibps.bills.entities.BillEmail;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BillEmailRepo extends CrudRepository<BillEmail, Long> {

    List<BillEmail> findAll();

    @Query("select item from BillEmail item " +
            "where item.bill=:bill " +
            "order by item.id asc ")
    List<BillEmail> findAllBillEmail(@Param("bill") Bill bill);

    void deleteAllByBill(Bill bill);
}
