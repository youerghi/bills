package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.Config;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ConfigRepo extends CrudRepository<Config, Long> {
    List<Config> findAll();

    Config findConfigByKey(String key);

}
