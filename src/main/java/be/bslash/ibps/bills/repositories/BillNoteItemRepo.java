package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.BillNote;
import be.bslash.ibps.bills.entities.BillNoteItem;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BillNoteItemRepo extends CrudRepository<BillNoteItem, Long> {

    List<BillNoteItem> findAll();


    @Query("select item from BillNoteItem item " +
            "where item.billNote=:billNote " +
            "order by item.id asc ")
    List<BillNoteItem> findAllBillNoteItem(@Param("billNote") BillNote billNote);

}
