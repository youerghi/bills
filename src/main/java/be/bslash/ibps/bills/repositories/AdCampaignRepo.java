package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.AdCampaign;
import be.bslash.ibps.bills.enums.AdApprovalType;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface AdCampaignRepo extends CrudRepository<AdCampaign, Long>, JpaSpecificationExecutor<AdCampaign> {
    AdCampaign findByIdAndApprovalType(Long id, AdApprovalType adApprovalType);
}
