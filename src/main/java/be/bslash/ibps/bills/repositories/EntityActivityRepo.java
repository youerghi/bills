package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.Entity;
import be.bslash.ibps.bills.entities.EntityActivity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EntityActivityRepo extends CrudRepository<EntityActivity, Long> {
    List<EntityActivity> findAll();

    EntityActivity findEntityActivityById(Long id);

    @Query("select entAct from EntityActivity entAct " +
            "where entAct.entity=:entity " +
            "and entAct.code=:code " +
            "and entAct.status ='ACTIVE' ")
    EntityActivity findEntityActivity(
            @Param("code") String code,
            @Param("entity") Entity entity);

    @Query("select entAct from EntityActivity entAct " +
            "where entAct.entity=:entity " +
            "and entAct.code=:code " +
            "and entAct.status ='ACTIVE' ")
    List<EntityActivity> findEntityActivityList(
            @Param("code") String code,
            @Param("entity") Entity entity);


    @Query("select entAct from EntityActivity entAct " +
            "where entAct.entity=:entity " +
            "and entAct.status <>'DELETED' ")
    List<EntityActivity> findAllByEntity(@Param("entity") Entity entity);

    @Query("select entAct from EntityActivity entAct " +
            "where entAct.entity=:entity " +
            "and entAct.status ='ACTIVE' ")
    List<EntityActivity> findAllActiveActivityByEntity(@Param("entity") Entity entity);


}
