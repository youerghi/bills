package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.City;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CityRepo extends CrudRepository<City, Long> {
    List<City> findAll();

    City findCityById(Long id);
}
