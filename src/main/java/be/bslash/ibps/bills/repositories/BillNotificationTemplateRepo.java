package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.BillNotificationTemplate;
import be.bslash.ibps.bills.enums.BillNotificationType;
import be.bslash.ibps.bills.enums.Language;
import be.bslash.ibps.bills.enums.NotificationMedia;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BillNotificationTemplateRepo extends CrudRepository<BillNotificationTemplate, Long> {

    List<BillNotificationTemplate> findAll();

    @Query("select bnt from BillNotificationTemplate bnt " +
            "where bnt.notificationMedia=:notificationMedia " +
            "and bnt.notificationLanguage=:notificationLanguage " +
            "and bnt.billNotificationType=:billNotificationType")
    BillNotificationTemplate find(@Param("notificationMedia") NotificationMedia notificationMedia,
                                  @Param("notificationLanguage") Language notificationLanguage,
                                  @Param("billNotificationType") BillNotificationType billNotificationType);
}
