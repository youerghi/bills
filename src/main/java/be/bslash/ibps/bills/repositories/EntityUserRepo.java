package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.Entity;
import be.bslash.ibps.bills.entities.EntityUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EntityUserRepo extends CrudRepository<EntityUser, Long> {
    List<EntityUser> findAll();

    @Query("select comUsr from EntityUser comUsr " +
            "where comUsr.username=:username " +
            "and comUsr.status='DELETED'")
    List<EntityUser> findDeletedEntityUserByUsername(@Param("username") String username);

    @Query("select comUsr from EntityUser comUsr " +
            "where comUsr.username=:username " +
            "and comUsr.status <>'DELETED'")
    EntityUser findEntityUserByUsername(@Param("username") String username);

    @Query("select comUsr from EntityUser comUsr " +
            "where comUsr.entity=:entity " +
            "and comUsr.status <>'DELETED' " +
            "order by comUsr.id asc ")
    List<EntityUser> findEntityUserByEntity(@Param("entity") Entity entity);
}
