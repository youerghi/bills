package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.Entity;
import be.bslash.ibps.bills.entities.OldSystemPayment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

public interface OldSystemPaymentRepo extends CrudRepository<OldSystemPayment, Long> {

    Page<OldSystemPayment> findAllByEntity(Entity entity, Pageable pageable);

}
