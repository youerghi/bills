package be.bslash.ibps.bills.repositories;


import be.bslash.ibps.bills.entities.Bill;
import be.bslash.ibps.bills.entities.BillTimeline;
import be.bslash.ibps.bills.enums.BillStatus;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BillTimelineRepo extends CrudRepository<BillTimeline, Long> {
    List<BillTimeline> findAll();

    List<BillTimeline> findBillTimelineByBill(Bill bill);

    BillTimeline findFirstBillTimelineByBillAndBillStatus(Bill bill, BillStatus billStatus);

    @Query("select t from BillTimeline t " +
            "where t.bill=:bill " +
            "and t.billStatus=:billStatus ")
    List<BillTimeline> findBillTimeline(@Param("bill") Bill bill,
                                        @Param("billStatus") BillStatus billStatus);
}
