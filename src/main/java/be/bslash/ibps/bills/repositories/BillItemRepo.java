package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.Bill;
import be.bslash.ibps.bills.entities.BillItem;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BillItemRepo extends CrudRepository<BillItem, Long> {

    List<BillItem> findAll();

    List<BillItem> findAllByBill(Bill bill);

    void deleteAllByBill(Bill bill);
}
