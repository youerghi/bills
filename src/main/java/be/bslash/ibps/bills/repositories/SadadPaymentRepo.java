package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.Account;
import be.bslash.ibps.bills.entities.Bill;
import be.bslash.ibps.bills.entities.SadadPayment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface SadadPaymentRepo extends CrudRepository<SadadPayment, Long> {
    List<SadadPayment> findAll();

    List<SadadPayment> findSadadPaymentByBillOrderByIdAsc(Bill bill);

    @Query("select sum(pmt.paymentAmount) from SadadPayment pmt " +
            "where (pmt.bill.account = :account) ")
    BigDecimal sumOfRemaningPayment(@Param("account") Account account);
}
