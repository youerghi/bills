package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.AdSettings;
import be.bslash.ibps.bills.entities.Entity;
import org.springframework.data.repository.CrudRepository;

public interface AddSettingsRepo extends CrudRepository<AdSettings, Long> {

    AdSettings findByEntity(Entity entity);

}

