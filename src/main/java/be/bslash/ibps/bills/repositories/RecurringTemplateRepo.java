package be.bslash.ibps.bills.repositories;


import be.bslash.ibps.bills.entities.Entity;
import be.bslash.ibps.bills.entities.RecurringTemplate;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RecurringTemplateRepo extends CrudRepository<RecurringTemplate, Long> {

    List<RecurringTemplate> findAllByEntity(Entity entity);

    List<RecurringTemplate> findRecurringTemplateByNameAndEntity(String name, Entity entity);

}
