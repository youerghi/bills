package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.Entity;
import be.bslash.ibps.bills.entities.EntityContactDetail;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EntityContactDetailRepo extends CrudRepository<EntityContactDetail, Long> {
    List<EntityContactDetail> findAll();

    EntityContactDetail findAllByEntity(Entity entity);
}
