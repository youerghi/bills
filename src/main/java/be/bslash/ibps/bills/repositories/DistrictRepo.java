package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.District;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DistrictRepo extends CrudRepository<District, Long> {
    List<District> findAll();

    District findDistrictByCode(String code);

    District findDistrictById(Long id);
}
