package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.Bill;
import be.bslash.ibps.bills.entities.BillCustomField;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BillCustomFieldRepo extends CrudRepository<BillCustomField, Long> {

    List<BillCustomField> findAll();

    List<BillCustomField> findAllByBill(Bill bill);

    void deleteAllByBill(Bill bill);
}
