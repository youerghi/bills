package be.bslash.ibps.bills.repositories;


import be.bslash.ibps.bills.entities.Customer;
import be.bslash.ibps.bills.entities.Entity;
import be.bslash.ibps.bills.enums.IdType;
import be.bslash.ibps.bills.enums.Status;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface CustomerRepo extends CrudRepository<Customer, Long>, JpaSpecificationExecutor<Customer> {

    List<Customer> findAll();

    @Query("select c from Customer c where "
            + "c.entityUser.entity = :entity and "
            + "c.idNo = :idNo and "
            + "c.contactIdType = :idType and "
            + "c.status = :status")
    List<Customer> findCustomerByContactIdTypeAndIdNoAndStatusAndEntity(@Param("entity") Entity entity, @Param("idNo") String idNo, @Param("idType") IdType idType, @Param("status") Status staus);

}
