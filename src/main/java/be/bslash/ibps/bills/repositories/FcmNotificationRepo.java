package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.FcmNotification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FcmNotificationRepo extends CrudRepository<FcmNotification, Long> {

    List<FcmNotification> findAll();

}
