package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.Bill;
import be.bslash.ibps.bills.entities.BillNote;
import be.bslash.ibps.bills.entities.Entity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BillNoteRepo extends CrudRepository<BillNote, Long> {

    List<BillNote> findAll();


    @Query("select item from BillNote item " +
            "where item.documentNumber=:documentNumber " +
            "and item.bill.entity=:entity " +
            "order by item.id asc ")
    List<BillNote> findAllByBill(@Param("documentNumber") String documentNumber,
                                 @Param("entity") Entity entity);

    @Query("select item from BillNote item " +
            "where item.bill=:bill " +
            "order by item.id asc ")
    List<BillNote> findAllBillNote(@Param("bill") Bill bill);

}
