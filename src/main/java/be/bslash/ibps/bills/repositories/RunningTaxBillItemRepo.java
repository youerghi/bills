package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.Bill;
import be.bslash.ibps.bills.entities.RunningTaxBillItem;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RunningTaxBillItemRepo extends CrudRepository<RunningTaxBillItem, Long> {

    List<RunningTaxBillItem> findAll();

    List<RunningTaxBillItem> findAllByBill(Bill bill);

    void deleteAllByBill(Bill bill);
}
