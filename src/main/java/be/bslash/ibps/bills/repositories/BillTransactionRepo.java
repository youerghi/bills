package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.BillTransaction;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BillTransactionRepo extends CrudRepository<BillTransaction, Long>, JpaSpecificationExecutor<BillTransaction> {

    List<BillTransaction> findAll();

}
