package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.Entity;
import be.bslash.ibps.bills.entities.EntityVat;
import org.springframework.data.repository.CrudRepository;

import java.math.BigDecimal;
import java.util.List;

public interface EntityVatRepo extends CrudRepository<EntityVat, Long> {
    List<EntityVat> findAll();

    List<EntityVat> findEntityVatByEntity(Entity entity);

    List<EntityVat> findEntityVatByEntityAndVat(Entity entity, BigDecimal vat);
}
