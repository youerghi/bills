package be.bslash.ibps.bills.repositories;

import be.bslash.ibps.bills.entities.ConfigTemplate;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ConfigTemplateRepo extends CrudRepository<ConfigTemplate, Long> {
    List<ConfigTemplate> findAll();

    ConfigTemplate findConfigTemplateByKey(String key);
}
