package be.bslash.ibps.bills.repositories;


import be.bslash.ibps.bills.entities.Account;
import be.bslash.ibps.bills.entities.Bill;
import be.bslash.ibps.bills.entities.BulkBill;
import be.bslash.ibps.bills.entities.Entity;
import be.bslash.ibps.bills.entities.EntityActivity;
import be.bslash.ibps.bills.enums.BillStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public interface BillRepo extends CrudRepository<Bill, Long>, JpaSpecificationExecutor<Bill> {
    List<Bill> findAll();

    @Query("select bill from Bill bill " +
            "where bill.billNumber=:billNumber " +
            "and bill.entity=:entity " +
            "and bill.billStatus <> 'DELETED' " +
            "and bill.billStatus <> 'CANCELED'")
    Bill findBill(@Param("billNumber") String billNumber,
                  @Param("entity") Entity entity);


    Bill findBillBySadadNumber(String sadadNumber);

    List<Bill> findBySadadNumber(String sadadNumber);

    Bill findBySadadNumberAndCycleNumber(String sadadNumber, Integer cycleNumber);

    Bill findBillById(Long id);

    List<Bill> findAllByBulkBill(BulkBill bulkBill);

    Page<Bill> findAllByBulkBill(BulkBill bulkBill, Pageable pageable);

    @Query("select count(distinct b.id) from Bill b " +
            "join b.billTimelineList t " +
            "where (:billStatus is null or t.billStatus = :billStatus) " +
            "and (:fromDate is null or b.createDate >= :fromDate) " +
            "and (:toDate is null or b.createDate <= :toDate) " +
            "and (:entity is null or b.entity = :entity) " +
            "and b.billStatus <>'DELETED'" +
            "group by b.entity")
    Long countOfBill(@Param("billStatus") BillStatus billStatus,
                     @Param("fromDate") LocalDateTime fromDate,
                     @Param("toDate") LocalDateTime toDate,
                     @Param("entity") Entity entity);

    @Query("select sum(b.totalAmount) from Bill b " +
            "where b.id in " +
            "(select distinct b2.id from Bill b2 join b2.billTimelineList t " +
            "where (:billStatus is null or t.billStatus = :billStatus) " +
            "and (:fromDate is null or b2.createDate >= :fromDate) " +
            "and (:toDate is null or b2.createDate <= :toDate) " +
            "and (:entity is null or b2.entity = :entity) " +
            "and b2.billStatus<>'DELETED')")
    BigDecimal volumeOfBill(@Param("billStatus") BillStatus billStatus,
                            @Param("fromDate") LocalDateTime fromDate,
                            @Param("toDate") LocalDateTime toDate,
                            @Param("entity") Entity entity);


    @Query("select count(distinct b.id)  from Bill b " +
            "where (:entity is null or b.entity = :entity) " +
            "and (:fromDate is null or b.createDate >= :fromDate) " +
            "and (:toDate is null or b.createDate <= :toDate) " +
            "and b.id in " +
            "(select distinct locPay.bill.id from LocalPayment locPay)")
    Long countOfPaidByEntity(@Param("fromDate") LocalDateTime fromDate,
                             @Param("toDate") LocalDateTime toDate,
                             @Param("entity") Entity entity);

    @Query("select sum(b.totalAmount) from Bill b " +
            "where (:entity is null or b.entity = :entity) " +
            "and (:fromDate is null or b.createDate >= :fromDate) " +
            "and (:toDate is null or b.createDate <= :toDate) " +
            "and b.id in " +
            "(select distinct locPay.bill.id from LocalPayment locPay)")
    BigDecimal volumeOfPaidByEntity(@Param("fromDate") LocalDateTime fromDate,
                                    @Param("toDate") LocalDateTime toDate,
                                    @Param("entity") Entity entity);


    @Query("select count(distinct b.id)  from Bill b " +
            "where (:entity is null or b.entity = :entity) " +
            "and (:fromDate is null or b.createDate >= :fromDate) " +
            "and (:toDate is null or b.createDate <= :toDate) " +
            "and b.id in " +
            "(select distinct locPay.bill.id from SadadPayment locPay)")
    Long countOfPaidBySadad(@Param("fromDate") LocalDateTime fromDate,
                            @Param("toDate") LocalDateTime toDate,
                            @Param("entity") Entity entity);

    @Query("select sum(b.totalAmount) from Bill b " +
            "where (:entity is null or b.entity = :entity) " +
            "and (:fromDate is null or b.createDate >= :fromDate) " +
            "and (:toDate is null or b.createDate <= :toDate) " +
            "and b.id in " +
            "(select distinct locPay.bill.id from SadadPayment locPay)")
    BigDecimal volumeOfPaidBySadad(@Param("fromDate") LocalDateTime fromDate,
                                   @Param("toDate") LocalDateTime toDate,
                                   @Param("entity") Entity entity);


    @Query("select count(distinct b.id) from Bill b " +
            "where (:billStatus is null or b.billStatus = :billStatus) " +
            "and (:fromDate is null or b.createDate >= :fromDate) " +
            "and (:toDate is null or b.createDate <= :toDate) " +
            "and (:entity is null or b.entity = :entity) " +
            "and b.billStatus <>'DELETED'" +
            "group by b.entity")
    Long countByCurrentBillStatus(@Param("billStatus") BillStatus billStatus,
                                  @Param("fromDate") LocalDateTime fromDate,
                                  @Param("toDate") LocalDateTime toDate,
                                  @Param("entity") Entity entity);

    @Query("select sum(b.totalAmount) from Bill b " +
            "where (:billStatus is null or b.billStatus = :billStatus) " +
            "and (:fromDate is null or b.createDate >= :fromDate) " +
            "and (:toDate is null or b.createDate <= :toDate) " +
            "and (:entity is null or b.entity = :entity) " +
            "and b.billStatus <>'DELETED'" +
            "group by b.entity")
    BigDecimal volumeByCurrentBillStatus(@Param("billStatus") BillStatus billStatus,
                                         @Param("fromDate") LocalDateTime fromDate,
                                         @Param("toDate") LocalDateTime toDate,
                                         @Param("entity") Entity entity);


    @Query("select count(distinct b.id) from Bill b " +
            "where (:billStatus is null or b.billStatus = :billStatus) " +
            "and (:fromDate is null or b.createDate >= :fromDate) " +
            "and (:toDate is null or b.createDate <= :toDate) " +
            "and (:entityActivity is null or b.entityActivity = :entityActivity) " +
            "and (:entity is null or b.entity = :entity) " +
            "and b.billStatus <>'DELETED'" +
            "group by b.entity")
    Long countByCurrentBillStatus(@Param("billStatus") BillStatus billStatus,
                                  @Param("fromDate") LocalDateTime fromDate,
                                  @Param("toDate") LocalDateTime toDate,
                                  @Param("entityActivity") EntityActivity entityActivity,
                                  @Param("entity") Entity entity);


    @Query("select b from Bill b " +
            "where (b.billStatus='APPROVED_BY_SADAD' or b.billStatus='VIEWED_BY_CUSTOMER')" +
            "and b.expireDate < :date ")
    List<Bill> findAllExpiredBill(@Param("date") LocalDateTime date
    );


    @Query("select sum(b.totalAmount) from Bill b " +
            "where (:fromDate is null or b.approveDateBySadad >= :fromDate) " +
            "and (:toDate is null or b.approveDateBySadad <= :toDate) " +
            "and (:entity is null or b.entity = :entity) " +
            "and (:entityActivity is null or b.entityActivity = :entityActivity) " +
            "and b.billStatus <>'CANCELED'" +
            "group by b.entity")
    BigDecimal volumeByApprovedDate(@Param("fromDate") LocalDateTime fromDate,
                                    @Param("toDate") LocalDateTime toDate,
                                    @Param("entityActivity") EntityActivity entityActivity,
                                    @Param("entity") Entity entity);

    Bill findBillByBillNumberAndEntity(String billNumber, Entity entity);

    @Modifying
    @Query("update Bill bill set bill.smsIssueFlag='YES' " +
            "where bill.id=:id")
    void updateSmsFlag(
            @Param("id") Long id);


    @Query("select count(distinct b.id) from Bill b " +
            "where (:billStatus is null or b.billStatus = :billStatus) " +
            "and (:fromDate is null or b.issueDate >= :fromDate) " +
            "and (:toDate is null or b.issueDate <= :toDate) " +
            "and (:entity is null or b.entity = :entity) " +
            "and b.billStatus <>'DELETED'" +
            "group by b.entity")
    Long countByCurrentBillStatusIssueDate(@Param("billStatus") BillStatus billStatus,
                                           @Param("fromDate") LocalDateTime fromDate,
                                           @Param("toDate") LocalDateTime toDate,
                                           @Param("entity") Entity entity);

    @Query("select sum(b.totalAmount) from Bill b " +
            "where (:billStatus is null or b.billStatus = :billStatus) " +
            "and (:fromDate is null or b.issueDate >= :fromDate) " +
            "and (:toDate is null or b.issueDate <= :toDate) " +
            "and (:entity is null or b.entity = :entity) " +
            "and b.billStatus <>'DELETED'" +
            "group by b.entity")
    BigDecimal volumeByCurrentBillStatusIssueDate(@Param("billStatus") BillStatus billStatus,
                                                  @Param("fromDate") LocalDateTime fromDate,
                                                  @Param("toDate") LocalDateTime toDate,
                                                  @Param("entity") Entity entity);


    @Query("select sum(b.remainAmount) from Bill b " +
            "where (b.billStatus = 'APPROVED_BY_SADAD' or b.billStatus ='VIEWED_BY_CUSTOMER' or b.billStatus ='PARTIALLY_PAID_BY_SADAD' or b.billStatus ='PARTIALLY_PAID_BY_COMPANY') " +
            "and (:dueDate is null or b.dueDate <= :dueDate) " +
            "and (:account is null or b.account = :account) " +
            "and b.billStatus <>'DELETED'")
    BigDecimal sumOfRemaningDueBillsTest(
            @Param("dueDate") LocalDateTime dueDate,
            @Param("account") Account account);


    @Query("select b from Bill b " +
            "where (b.billStatus = 'APPROVED_BY_SADAD' or b.billStatus ='VIEWED_BY_CUSTOMER') " +
            "and (:dueDate is null or b.dueDate <= :dueDate) " +
            "and (:account is null or b.account = :account) " +
            "and b.billStatus <>'DELETED' " +
            "order by b.id asc ")
    List<Bill> findAllDueBillByAccount(@Param("dueDate") LocalDateTime dueDate,
                                       @Param("account") Account account);

}

