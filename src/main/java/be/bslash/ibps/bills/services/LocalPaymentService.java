package be.bslash.ibps.bills.services;


import be.bslash.ibps.bills.dto.request.LocalPaymentRqDto;
import be.bslash.ibps.bills.entities.Account;
import be.bslash.ibps.bills.entities.Bill;
import be.bslash.ibps.bills.entities.BillTimeline;
import be.bslash.ibps.bills.entities.BillTransaction;
import be.bslash.ibps.bills.entities.Config;
import be.bslash.ibps.bills.entities.EntityUser;
import be.bslash.ibps.bills.entities.LocalPayment;
import be.bslash.ibps.bills.enums.BillCategory;
import be.bslash.ibps.bills.enums.BillStatus;
import be.bslash.ibps.bills.enums.BillType;
import be.bslash.ibps.bills.enums.EntityUserType;
import be.bslash.ibps.bills.enums.PaymentMethod;
import be.bslash.ibps.bills.enums.PaymentSource;
import be.bslash.ibps.bills.enums.PaymentStatus;
import be.bslash.ibps.bills.enums.Status;
import be.bslash.ibps.bills.exceptions.HttpServiceException;
import be.bslash.ibps.bills.repositories.AccountRepo;
import be.bslash.ibps.bills.repositories.BillRepo;
import be.bslash.ibps.bills.repositories.BillTimelineRepo;
import be.bslash.ibps.bills.repositories.BillTransactionRepo;
import be.bslash.ibps.bills.repositories.ConfigRepo;
import be.bslash.ibps.bills.repositories.EntityRepo;
import be.bslash.ibps.bills.repositories.LocalPaymentRepo;
import be.bslash.ibps.bills.repositories.SadadPaymentRepo;
import be.bslash.ibps.bills.utils.Utils;
import be.bslash.ibps.bills.ws.WebServiceHelper;
import be.bslash.ibps.bills.ws.WebServiceResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Locale;

@Service
@Transactional
public class LocalPaymentService {

    private static final Logger LOGGER = LogManager.getLogger("BACKEND_LOGS");

    @Autowired
    private BillRepo billRepo;

    @Autowired
    private BillTimelineRepo billTimelineRepo;

    @Autowired
    private ConfigRepo configRepo;

    @Autowired
    private LocalPaymentRepo localPaymentRepo;

    @Autowired
    private WebServiceHelper webServiceHelper;

    @Autowired
    private BillService billService;

    @Autowired
    private EntityRepo entityRepo;

    @Autowired
    private AccountRepo accountRepo;

    @Autowired
    private SadadPaymentRepo sadadPaymentRepo;

    @Autowired
    private BillTransactionRepo billTransactionRepo;

    public LocalPayment saveLocalPayment(LocalPaymentRqDto localPaymentRqDto, EntityUser entityUser, Locale locale) {

        if (entityUser.getEntityUserType().equals(EntityUserType.OFFICER)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "payment_is_not_allowed_for_officer", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        Bill bill = null;
        if (localPaymentRqDto.getBillId() != null) {
            bill = billRepo.findBillById(localPaymentRqDto.getBillId());
        } else {
            if (localPaymentRqDto.getBillNumber() != null) {
                bill = billRepo.findBillByBillNumberAndEntity(localPaymentRqDto.getBillNumber(), entityUser.getEntity());
            }
        }

        if (bill == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
        }

        if (bill.getBillStatus().equals(BillStatus.DELETED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
        }

        if (!bill.getEntity().getId().equals(entityUser.getEntity().getId())) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_for_company", locale);
        }

        if (!billService.isBillStatusExist(bill, BillStatus.CHECKED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "cannot_paid_non_checked_bill", locale);
        }

        if (billService.isBillStatusExist(bill, BillStatus.CANCELED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_already_canceled", locale);
        }

        if (billService.isBillStatusExist(bill, BillStatus.PAID_BY_COMPANY) || billService.isBillStatusExist(bill, BillStatus.PAID_BY_SADAD)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_already_paid", locale);
        }

        int isMoreThanRemaining = localPaymentRqDto.getPaymentAmount().compareTo(bill.getRemainAmount());
        if (isMoreThanRemaining == 1) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "Over payment is not allowed", locale);
        }

        int isLessThanConfig = localPaymentRqDto.getPaymentAmount().compareTo(bill.getMiniPartialAmount());
        if (isLessThanConfig == -1) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "amount less than minimum config amount", locale);
        }

        LocalDateTime billExpiryDate = bill.getExpireDate();
        if (billExpiryDate != null && billExpiryDate.isBefore(LocalDate.now().atStartOfDay())) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_already_canceled", locale);
        }


        LocalDateTime localDateTime = Utils.parseDateFromString(localPaymentRqDto.getPaymentDate(), "yyyy-MM-dd");

        LocalPayment localPayment = new LocalPayment();
        localPayment.setBill(bill);
        localPayment.setEntityUser(entityUser);
        localPayment.setLocalPaymentId(Utils.generatePaymentNumber(entityUser.getEntity()));
        localPayment.setGlobalPaymentId(Utils.generatePaymentNumber(entityUser.getEntity()));
        localPayment.setPaymentAmount(localPaymentRqDto.getPaymentAmount());
        localPayment.setPaymentDate(localDateTime);
        localPayment.setPaymentSource(PaymentSource.ENTITY_PAYMENT);
        localPayment.setPaymentStatus(PaymentStatus.APPROVED);
        localPayment.setPaymentMethod(PaymentMethod.getValue(localPaymentRqDto.getPaymentMethod()));
        localPayment.setCreateDate(LocalDateTime.now());
        localPayment = localPaymentRepo.save(localPayment);

        Long voucherNumber = entityUser.getEntity().getPaymentVoucherSequence();
        voucherNumber += 1;

        BigDecimal paymentAmount = localPayment.getPaymentAmount();

        BigDecimal billAmount = bill.getRemainAmount();

        BigDecimal remainingAmount = billAmount.subtract(paymentAmount);

        BigDecimal currentPaidAmount = bill.getCurrentPaidAmount().add(paymentAmount);

        Integer paymentNumber = bill.getPaymentNumber() + 1;

        bill.setRemainAmount(remainingAmount);
        bill.setCurrentPaidAmount(currentPaidAmount);
        bill.setPaymentNumber(paymentNumber);

        BillStatus billStatus = BillStatus.PAID_BY_COMPANY;
        if (remainingAmount.doubleValue() > 0.0) {
            billStatus = (BillStatus.PARTIALLY_PAID_BY_COMPANY);
        }

        BillTimeline paymentBillTimeline = new BillTimeline();
        paymentBillTimeline.setBillStatus(billStatus);
        paymentBillTimeline.setBill(bill);
        paymentBillTimeline.setActionDate(LocalDateTime.now());
        paymentBillTimeline.setEntityUser(entityUser);
        paymentBillTimeline.setUserReference(entityUser.getUsername());

        paymentBillTimeline = billTimelineRepo.save(paymentBillTimeline);

        bill.setBillStatus(billStatus);

        localPayment.setRemainingAmount(remainingAmount);
        localPayment.setPaymentNumber(paymentNumber);
        localPayment.setVoucherNumber(voucherNumber);
        localPaymentRepo.save(localPayment);

        billRepo.save(bill);

        entityRepo.increasePaymentVoucherCounter(voucherNumber, entityUser.getEntity().getId());

        if (bill.getBillType().equals(BillType.RECURRING_BILL)) {

            BillTransaction billTransaction = new BillTransaction();
            billTransaction.setBill(bill);
            billTransaction.setPreviousAmount(bill.getAccount().getBalance());
            billTransaction.setAccount(bill.getAccount());
            billTransaction.setAmount(paymentAmount.multiply(BigDecimal.valueOf(-1)));
            billTransaction.setDate(localPayment.getPaymentDate());
            billTransaction.setBalanceAmount(paymentAmount.subtract(bill.getAccount().getBalance()).abs());
            billTransaction.setName("Payment (offline)");
            billTransaction.setPaymentId(localPayment.getId());
            billTransaction.setDocumentNumber(voucherNumber + "");

            billTransaction = billTransactionRepo.save(billTransaction);

            recurringBillJob(bill.getAccount());
        } else {
            Config config = configRepo.findConfigByKey("INTG_FLAG");

            if (config != null && config.getValue().equalsIgnoreCase("true")) {

                WebServiceResponse webServiceResponse = webServiceHelper.uploadPayment(localPayment);

                if (!webServiceResponse.getStatus().equals(200)) {
                    Document documentXmlResponse = Utils.convertStringToXmlDocument(new JSONObject(webServiceResponse.getBody()).getString("data"));
                    String message = Utils.getElementValueByTagName(documentXmlResponse, "faultstring");
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, message, webServiceResponse.getBody(), locale);
                }
                Document documentXmlResponse = Utils.convertStringToXmlDocument(new JSONObject(webServiceResponse.getBody()).getString("data"));
                String sadadPaymentId = Utils.getElementValueByTagName(documentXmlResponse, "ns1:SadadPmtId");
                localPayment.setSadadPaymentId(sadadPaymentId);
            } else {
                localPayment.setSadadPaymentId(Utils.generateOtp(8));
            }
        }

        localPaymentRepo.save(localPayment);

        return localPayment;
    }

    public void recurringBillJob(Account account) {

        LocalDateTime dueDate = LocalDateTime.now();

        BigDecimal totalRemainingBalance = billRepo.sumOfRemaningDueBillsTest(dueDate, account);

        LOGGER.info("totalRemainingBalance:: " + totalRemainingBalance);

        if (totalRemainingBalance != null) {

            BigDecimal totalPaymentBalance = sadadPaymentRepo.sumOfRemaningPayment(account);

            if (totalPaymentBalance == null) {
                totalPaymentBalance = BigDecimal.ZERO;
            }

            Integer cycleNumber = account.getCycleNumber();
            cycleNumber = cycleNumber + 1;
            account.setCycleNumber(cycleNumber);
            accountRepo.increaseCycleNumberCounter(cycleNumber, account.getId());

            account.setBalance(totalRemainingBalance);
            accountRepo.updateBalance(totalRemainingBalance, account.getId());

            LOGGER.info("totalRemainingBalance: " + totalRemainingBalance + " for account: " + account.getAccountNumber());

            Bill bill = new Bill();
            bill.setBillCategory(BillCategory.RECURRING_ACCOUNT_PAR);
            bill.setSadadNumber(account.getAccountNumber());
            bill.setCycleNumber(cycleNumber);
            bill.setTotalRunningAmount(totalRemainingBalance.add(totalPaymentBalance));
            bill.setDueDate(LocalDateTime.now());
            bill.setMiniPartialAmount(account.getMiniPartialAmount() != null ? account.getMiniPartialAmount() : totalRemainingBalance.add(totalPaymentBalance));

            WebServiceResponse webServiceResponse = webServiceHelper.uploadRecurringBill(bill, "BillCreate");

            LOGGER.info("Recurring webServiceResponse status: " + webServiceResponse.getStatus());
            LOGGER.info("Recurring webServiceResponse body: " + webServiceResponse.getBody());

        } else {

            totalRemainingBalance = BigDecimal.ZERO;

            Integer cycleNumber = account.getCycleNumber();
            cycleNumber = cycleNumber + 1;
            account.setCycleNumber(cycleNumber);
            accountRepo.increaseCycleNumberCounter(cycleNumber, account.getId());

            account.setBalance(totalRemainingBalance);
            accountRepo.updateBalance(totalRemainingBalance, account.getId());

            LOGGER.info("totalRemainingBalance: " + totalRemainingBalance + " for account: " + account.getAccountNumber());

            Bill bill = new Bill();
            bill.setBillCategory(BillCategory.RECURRING_ACCOUNT_PAR);
            bill.setSadadNumber(account.getAccountNumber());
            bill.setCycleNumber(cycleNumber);
            bill.setTotalRunningAmount(totalRemainingBalance);
            bill.setDueDate(LocalDateTime.now());
            bill.setMiniPartialAmount(account.getMiniPartialAmount() != null ? account.getMiniPartialAmount() : totalRemainingBalance);

            WebServiceResponse webServiceResponse = webServiceHelper.uploadRecurringBill(bill, "BillCreate");

            LOGGER.info("Recurring webServiceResponse status: " + webServiceResponse.getStatus());
            LOGGER.info("Recurring webServiceResponse body: " + webServiceResponse.getBody());
        }

    }
}
