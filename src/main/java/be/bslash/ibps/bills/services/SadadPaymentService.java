package be.bslash.ibps.bills.services;


import be.bslash.ibps.bills.dto.request.SadadPaymentRqDto;
import be.bslash.ibps.bills.entities.Account;
import be.bslash.ibps.bills.entities.Bill;
import be.bslash.ibps.bills.entities.BillTimeline;
import be.bslash.ibps.bills.entities.BillTransaction;
import be.bslash.ibps.bills.entities.Entity;
import be.bslash.ibps.bills.entities.SadadPayment;
import be.bslash.ibps.bills.enums.AccessChannel;
import be.bslash.ibps.bills.enums.BillCategory;
import be.bslash.ibps.bills.enums.BillStatus;
import be.bslash.ibps.bills.enums.PaymentMethod;
import be.bslash.ibps.bills.enums.PaymentSource;
import be.bslash.ibps.bills.enums.PaymentStatus;
import be.bslash.ibps.bills.repositories.AccountRepo;
import be.bslash.ibps.bills.repositories.BillRepo;
import be.bslash.ibps.bills.repositories.BillTimelineRepo;
import be.bslash.ibps.bills.repositories.BillTransactionRepo;
import be.bslash.ibps.bills.repositories.EntityRepo;
import be.bslash.ibps.bills.repositories.SadadPaymentRepo;
import be.bslash.ibps.bills.utils.Utils;
import be.bslash.ibps.bills.ws.WebServiceHelper;
import be.bslash.ibps.bills.ws.WebServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Locale;

@Service
@Transactional
public class SadadPaymentService {

    @Autowired
    private BillRepo billRepo;

    @Autowired
    private BillTimelineRepo billTimelineRepo;

    @Autowired
    private SadadPaymentRepo sadadPaymentRepo;

    @Autowired
    private AccountRepo accountRepo;

    @Autowired
    private EntityRepo entityRepo;

    @Autowired
    private BillTransactionRepo billTransactionRepo;

    @Autowired
    private WebServiceHelper webServiceHelper;

    public Integer saveSadadPayment(SadadPaymentRqDto sadadPaymentRqDto, Locale locale) {

        String sadadNumber = sadadPaymentRqDto.getSadadNumber();

        if (sadadNumber == null) {
            return 3000;
        }

        Account account = accountRepo.findAccountByAccountNumber(sadadNumber);

        if (account != null) {


            SadadPayment sadadPayment = new SadadPayment();
            sadadPayment.setPaymentAmount(sadadPaymentRqDto.getPaymentAmount());
            sadadPayment.setBankPaymentId(Utils.generateOtp(8));
            sadadPayment.setBill(null);
            sadadPayment.setEntity(account.getEntity());
            sadadPayment.setPaymentDate(LocalDateTime.now());
            sadadPayment.setSadadPaymentId(Utils.generateOtp(8));
            sadadPayment.setPaymentMethod(PaymentMethod.ACTDEB);
            sadadPayment.setPaymentStatus(PaymentStatus.APPROVED);
            sadadPayment.setBankId(sadadPaymentRqDto.getBankId());
            sadadPayment.setBranchCode("001");
            sadadPayment.setDistrictCode("002");
            sadadPayment.setAccessChannel(AccessChannel.ATM);
            sadadPayment.setPaymentSource(PaymentSource.SADAD_NOTIFICATION);

            BigDecimal paymentAmount = sadadPayment.getPaymentAmount();


            Long voucherNumber = account.getEntity().getPaymentVoucherSequence();
            voucherNumber += 1;
            account.getEntity().setPaymentVoucherSequence(voucherNumber);

            sadadPayment.setRemainingAmount(BigDecimal.ZERO);
            sadadPayment.setPaymentNumber(null);
            sadadPayment.setVoucherNumber(voucherNumber);

            entityRepo.increasePaymentVoucherCounter(voucherNumber, account.getEntity().getId());

            sadadPayment = sadadPaymentRepo.save(sadadPayment);

            account.setBalance(account.getBalance().subtract(paymentAmount));
            accountRepo.updateBalance(account.getBalance(), account.getId());

            BillTransaction billTransaction = new BillTransaction();
            billTransaction.setBill(null);
            billTransaction.setPreviousAmount(account.getBalance());
            billTransaction.setAccount(account);
            billTransaction.setAmount(paymentAmount.multiply(BigDecimal.valueOf(-1)));
            billTransaction.setDate(LocalDateTime.now());
            billTransaction.setBalanceAmount(paymentAmount.subtract(account.getBalance()).abs());
            billTransaction.setName("Payment (online)");
            billTransaction.setPaymentId(sadadPayment.getId());
            billTransaction.setDocumentNumber(voucherNumber + "");

            billTransaction = billTransactionRepo.save(billTransaction);

            recurringBillJob(account);

            return 0;

        } else {
            Bill bill = billRepo.findBillBySadadNumber(sadadNumber);


            if (bill == null) {

                String entityCode = sadadNumber.substring(0, 5);

                Entity entity = entityRepo.findEntityByCode(entityCode);

                return 111;
            }


            if (billTimelineRepo.findFirstBillTimelineByBillAndBillStatus(bill, BillStatus.APPROVED_BY_SADAD) == null) {
                return 32002;
            }

            if (billTimelineRepo.findFirstBillTimelineByBillAndBillStatus(bill, BillStatus.PAID_BY_COMPANY) != null) {
                return 111;
            }

            if (billTimelineRepo.findFirstBillTimelineByBillAndBillStatus(bill, BillStatus.PAID_BY_SADAD) != null) {
                return 111;
            }

            if (billTimelineRepo.findFirstBillTimelineByBillAndBillStatus(bill, BillStatus.CANCELED) != null) {

                SadadPayment sadadPayment = new SadadPayment();
                sadadPayment.setPaymentAmount(sadadPaymentRqDto.getPaymentAmount());
                sadadPayment.setBankPaymentId(Utils.generateOtp(8));
                sadadPayment.setBill(bill);
                sadadPayment.setEntity(bill.getEntity());
                sadadPayment.setPaymentDate(LocalDateTime.now());
                sadadPayment.setSadadPaymentId(Utils.generateOtp(8));
                sadadPayment.setPaymentMethod(PaymentMethod.ACTDEB);
                sadadPayment.setPaymentStatus(PaymentStatus.APPROVED);
                sadadPayment.setBankId(sadadPaymentRqDto.getBankId());
                sadadPayment.setBranchCode("001");
                sadadPayment.setDistrictCode("002");
                sadadPayment.setAccessChannel(AccessChannel.ATM);
                sadadPayment.setPaymentSource(PaymentSource.SADAD_NOTIFICATION);

                BigDecimal paymentAmount = sadadPayment.getPaymentAmount();

                BigDecimal billAmount = bill.getRemainAmount();

                billAmount = billAmount.setScale(2, BigDecimal.ROUND_HALF_UP);

                BigDecimal remainingAmount = billAmount.subtract(paymentAmount);

                BigDecimal currentPaidAmount = bill.getCurrentPaidAmount().add(paymentAmount);

                Integer paymentNumber = bill.getPaymentNumber() + 1;

                sadadPayment.setRemainingAmount(remainingAmount);

                bill.setRemainAmount(remainingAmount);
                bill.setCurrentPaidAmount(currentPaidAmount);
                bill.setPaymentNumber(paymentNumber);

                BillStatus billStatus = BillStatus.PAID_BY_SADAD;
                if (remainingAmount.doubleValue() > 0.0) {
                    billStatus = (BillStatus.PARTIALLY_PAID_BY_SADAD);
                }

                BillTimeline sadadPaymentBillTimeline = new BillTimeline();
                sadadPaymentBillTimeline.setBillStatus(billStatus);
                sadadPaymentBillTimeline.setBill(bill);
                sadadPaymentBillTimeline.setActionDate(LocalDateTime.now());
                sadadPaymentBillTimeline.setEntityUser(null);
                sadadPaymentBillTimeline.setUserReference("SADAD");

                sadadPaymentBillTimeline = billTimelineRepo.save(sadadPaymentBillTimeline);

                bill.setBillStatus(billStatus);

                Long voucherNumber = bill.getEntity().getPaymentVoucherSequence();
                voucherNumber += 1;

                sadadPayment.setRemainingAmount(remainingAmount);
                sadadPayment.setPaymentNumber(paymentNumber);
                sadadPayment.setVoucherNumber(voucherNumber);

                entityRepo.increasePaymentVoucherCounter(voucherNumber, bill.getEntity().getId());

                sadadPayment = sadadPaymentRepo.save(sadadPayment);
                billRepo.save(bill);

                return 0;
            }


            SadadPayment sadadPayment = new SadadPayment();
            sadadPayment.setPaymentAmount(sadadPaymentRqDto.getPaymentAmount());
            sadadPayment.setBankPaymentId(Utils.generateOtp(8));
            sadadPayment.setBill(bill);
            sadadPayment.setEntity(bill.getEntity());
            sadadPayment.setPaymentDate(LocalDateTime.now());
            sadadPayment.setSadadPaymentId(Utils.generateOtp(8));
            sadadPayment.setPaymentMethod(PaymentMethod.ACTDEB);
            sadadPayment.setPaymentStatus(PaymentStatus.APPROVED);
            sadadPayment.setBankId(sadadPaymentRqDto.getBankId());
            sadadPayment.setBranchCode("001");
            sadadPayment.setDistrictCode("002");
            sadadPayment.setAccessChannel(AccessChannel.ATM);
            sadadPayment.setPaymentSource(PaymentSource.SADAD_NOTIFICATION);

            BigDecimal paymentAmount = sadadPayment.getPaymentAmount();

            BigDecimal billAmount = bill.getRemainAmount();

            billAmount = billAmount.setScale(2, BigDecimal.ROUND_HALF_UP);

            BigDecimal remainingAmount = billAmount.subtract(paymentAmount);

            BigDecimal currentPaidAmount = bill.getCurrentPaidAmount().add(paymentAmount);

            Integer paymentNumber = bill.getPaymentNumber() + 1;

            sadadPayment.setRemainingAmount(remainingAmount);

            bill.setRemainAmount(remainingAmount);
            bill.setCurrentPaidAmount(currentPaidAmount);
            bill.setPaymentNumber(paymentNumber);

            BillStatus billStatus = BillStatus.PAID_BY_SADAD;
            if (remainingAmount.doubleValue() > 0.0) {
                billStatus = (BillStatus.PARTIALLY_PAID_BY_SADAD);
            }

            BillTimeline sadadPaymentBillTimeline = new BillTimeline();
            sadadPaymentBillTimeline.setBillStatus(billStatus);
            sadadPaymentBillTimeline.setBill(bill);
            sadadPaymentBillTimeline.setActionDate(LocalDateTime.now());
            sadadPaymentBillTimeline.setEntityUser(null);
            sadadPaymentBillTimeline.setUserReference("SADAD");

            sadadPaymentBillTimeline = billTimelineRepo.save(sadadPaymentBillTimeline);

            bill.setBillStatus(billStatus);

            Long voucherNumber = bill.getEntity().getPaymentVoucherSequence();
            voucherNumber += 1;

            sadadPayment.setRemainingAmount(remainingAmount);
            sadadPayment.setPaymentNumber(paymentNumber);
            sadadPayment.setVoucherNumber(voucherNumber);

            entityRepo.increasePaymentVoucherCounter(voucherNumber, bill.getEntity().getId());

            sadadPayment = sadadPaymentRepo.save(sadadPayment);
            billRepo.save(bill);

            return 0;
        }
    }

    public void recurringBillJob(Account account) {

        LocalDateTime dueDate = LocalDateTime.now();

        BigDecimal totalRemainingBalance = billRepo.sumOfRemaningDueBillsTest(dueDate, account);

        if (totalRemainingBalance != null) {

            BigDecimal totalPaymentBalance = sadadPaymentRepo.sumOfRemaningPayment(account);

            if (totalPaymentBalance == null) {
                totalPaymentBalance = BigDecimal.ZERO;
            }

            Integer cycleNumber = account.getCycleNumber();
            cycleNumber = cycleNumber + 1;
            account.setCycleNumber(cycleNumber);
            accountRepo.increaseCycleNumberCounter(cycleNumber, account.getId());

            account.setBalance(totalRemainingBalance);
            accountRepo.updateBalance(totalRemainingBalance, account.getId());

            Bill bill = new Bill();
            bill.setBillCategory(BillCategory.RECURRING_ACCOUNT_PAR);
            bill.setSadadNumber(account.getAccountNumber());
            bill.setCycleNumber(cycleNumber);
            bill.setTotalRunningAmount(totalRemainingBalance.add(totalPaymentBalance));
            bill.setDueDate(LocalDateTime.now());
            bill.setMiniPartialAmount(account.getMiniPartialAmount() != null ? account.getMiniPartialAmount() : totalRemainingBalance.add(totalPaymentBalance));

            WebServiceResponse webServiceResponse = webServiceHelper.uploadRecurringBill(bill, "BillCreate");

        } else {

            totalRemainingBalance = BigDecimal.ZERO;

            Integer cycleNumber = account.getCycleNumber();
            cycleNumber = cycleNumber + 1;
            account.setCycleNumber(cycleNumber);
            accountRepo.increaseCycleNumberCounter(cycleNumber, account.getId());

            account.setBalance(totalRemainingBalance);
            accountRepo.updateBalance(totalRemainingBalance, account.getId());

            Bill bill = new Bill();
            bill.setBillCategory(BillCategory.RECURRING_ACCOUNT_PAR);
            bill.setSadadNumber(account.getAccountNumber());
            bill.setCycleNumber(cycleNumber);
            bill.setTotalRunningAmount(totalRemainingBalance);
            bill.setDueDate(LocalDateTime.now());
            bill.setMiniPartialAmount(account.getMiniPartialAmount() != null ? account.getMiniPartialAmount() : totalRemainingBalance);

            WebServiceResponse webServiceResponse = webServiceHelper.uploadRecurringBill(bill, "BillCreate");

        }

    }
}
