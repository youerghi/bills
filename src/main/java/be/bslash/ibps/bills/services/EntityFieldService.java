package be.bslash.ibps.bills.services;

import be.bslash.ibps.bills.dto.response.EntityFieldDtoRs;
import be.bslash.ibps.bills.entities.Entity;
import be.bslash.ibps.bills.entities.EntityField;
import be.bslash.ibps.bills.repositories.EntityFieldRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
@Transactional
public class EntityFieldService {

    @Autowired
    private EntityFieldRepo entityFieldRepo;


    public List<EntityFieldDtoRs> findAllByEntity(Entity entity, Locale locale) {

        List<EntityField> entityFieldList = entityFieldRepo.findAllActiveEntityFieldByEntity(entity);

        List<EntityFieldDtoRs> entityFieldDtoRsList = new ArrayList<>();
        entityFieldList.forEach(entityField -> {

            EntityFieldDtoRs entityFieldDtoRs = new EntityFieldDtoRs();

            entityFieldDtoRs.setId(entityField.getId());
            entityFieldDtoRs.setFieldType(entityField.getFieldType());
            entityFieldDtoRs.setIsRequired(entityField.getIsRequired());
            entityFieldDtoRs.setValueType(entityField.getValueType());
            entityFieldDtoRs.setMaxLength(entityField.getMaxLength());
            entityFieldDtoRs.setMinLength(entityField.getMinLength());
            entityFieldDtoRs.setStatus(entityField.getStatus());
            entityFieldDtoRs.setFieldNameAr(entityField.getFieldNameAr());
            entityFieldDtoRs.setFieldNameEn(entityField.getFieldNameEn());

            entityFieldDtoRsList.add(entityFieldDtoRs);
        });

        return entityFieldDtoRsList;
    }
}
