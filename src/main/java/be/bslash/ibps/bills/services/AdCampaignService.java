
package be.bslash.ibps.bills.services;

import be.bslash.ibps.bills.dto.request.AdvReqDto;
import be.bslash.ibps.bills.dto.response.AdCampaignDtoRs;
import be.bslash.ibps.bills.entities.AdCampaign;
import be.bslash.ibps.bills.entities.AdDetails;
import be.bslash.ibps.bills.enums.Status;
import be.bslash.ibps.bills.exceptions.HttpServiceException;
import be.bslash.ibps.bills.repositories.AdCampaignRepo;
import be.bslash.ibps.bills.repositories.AddSettingsRepo;
import be.bslash.ibps.bills.utils.UserManager;
import be.bslash.ibps.bills.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
@Transactional
public class AdCampaignService {

    @Autowired
    UserManager userManager;

    @Autowired
    AdCampaignRepo adCampaignRepo;

    @Autowired
    AddSettingsRepo addSettingsRepo;

    public AdCampaignDtoRs getAdCampaign(Long adCampiagnId, Locale locale) {
        AdCampaignDtoRs adCampaignDtoRs = new AdCampaignDtoRs();

        if (Utils.isNullOrEmpty(adCampiagnId)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "missing_adcampaign_id", locale);
        }

        Optional<AdCampaign> adCampaignCheck = adCampaignRepo.findById(adCampiagnId);

        if (!adCampaignCheck.isPresent() || adCampaignCheck.get().getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.NOT_FOUND, "ad_campaign_not_found", locale);
        }

        populateAdCampaignResponse(adCampaignDtoRs, adCampaignCheck.get());

        return adCampaignDtoRs;
    }


    public void populateAdCampaignResponse(AdCampaignDtoRs adCampaignDtoRs, AdCampaign adCampaign) {

        List<AdvReqDto> adDetailsList = new ArrayList<>();

        adCampaignDtoRs.setId(Integer.valueOf(adCampaign.getId().toString()));
        adCampaignDtoRs.setCampaignCode(adCampaign.getAdCode());
        adCampaignDtoRs.setName(adCampaign.getAdName());
        adCampaignDtoRs.setDisplayDuration(adCampaign.getDispalyTime());
        adCampaignDtoRs.setStartDate(adCampaign.getStartDate().toString());
        adCampaignDtoRs.setEndDate(adCampaign.getEndDate().toString());
        adCampaignDtoRs.setUserType(adCampaign.getStatus().name());
        adCampaignDtoRs.setApprovalType(adCampaign.getApprovalType().name());
        adCampaignDtoRs.setAdvCategory(adCampaign.getCategoryType().name());
        adCampaignDtoRs.setBillerId(Integer.valueOf(adCampaign.getEntity().getId().toString()));
        adCampaignDtoRs.setBillerNameEn(adCampaign.getEntity().getBrandNameEn());
        adCampaignDtoRs.setBillerNameAr(adCampaign.getEntity().getBrandNameAr());
        adCampaignDtoRs.setRejectedNote(adCampaign.getRejectedNote() != null ? adCampaign.getRejectedNote() : "");

        for (AdDetails adDetails : adCampaign.getAdDetailsSet()) {

            AdvReqDto advReqDto = new AdvReqDto();
            advReqDto.setId(Integer.valueOf(adDetails.getId().toString()));
            advReqDto.setAdName(adDetails.getAdName());
            advReqDto.setAdURL(adDetails.getUrl());
            advReqDto.setImageURL(adDetails.getImageURL());
            advReqDto.setHitCount(adDetails.getHitCount());
            advReqDto.setOrderNo(adDetails.getOrderNo());
            advReqDto.setStatus(adDetails.getStatus().name());

            if (!adDetails.getStatus().equals(Status.DELETED)) {
                adDetailsList.add(advReqDto);
            }
        }

        adCampaignDtoRs.setAdList(adDetailsList);
    }

}
