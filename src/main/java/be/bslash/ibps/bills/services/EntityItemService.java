package be.bslash.ibps.bills.services;


import be.bslash.ibps.bills.dto.response.EntityItemDtoRs;
import be.bslash.ibps.bills.entities.Entity;
import be.bslash.ibps.bills.entities.EntityItem;
import be.bslash.ibps.bills.repositories.EntityItemRepo;
import be.bslash.ibps.bills.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
@Transactional
public class EntityItemService {

    @Autowired
    private EntityItemRepo entityItemRepo;

    public List<EntityItemDtoRs> findEntityItemByEntity(Entity entity, Locale locale) {

        List<EntityItemDtoRs> EntityItemDtoRsList = new ArrayList<>();

        List<EntityItem> EntityItemList = entityItemRepo.findEntityItemByEntity(entity);

        EntityItemList.forEach(entityItem -> {
            EntityItemDtoRs EntityItemDtoRs = new EntityItemDtoRs();
            EntityItemDtoRs.setId(entityItem.getId());
            EntityItemDtoRs.setName(entityItem.getName());
            EntityItemDtoRs.setIsDefault(Utils.parseBoolean(entityItem.getIsDefault()));
            EntityItemDtoRsList.add(EntityItemDtoRs);
        });


        return EntityItemDtoRsList;
    }
}
