
package be.bslash.ibps.bills.services;

import be.bslash.ibps.bills.dto.request.AdCampaignUsersDtoRq;
import be.bslash.ibps.bills.dto.response.AdSettingsDtoRs;
import be.bslash.ibps.bills.entities.AdCampaignUsers;
import be.bslash.ibps.bills.entities.AdSettings;
import be.bslash.ibps.bills.entities.Entity;
import be.bslash.ibps.bills.enums.Status;
import be.bslash.ibps.bills.exceptions.HttpServiceException;
import be.bslash.ibps.bills.repositories.AddSettingsRepo;
import be.bslash.ibps.bills.repositories.EntityRepo;
import be.bslash.ibps.bills.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
@Transactional
public class AdSettingsService {


    @Autowired
    private AddSettingsRepo addSettingsRepo;

    @Autowired
    private EntityRepo entityRepo;

    public AdSettingsDtoRs getAdCampaignSettings(Long billerId, Locale locale) {
        AdSettings adSettings = null;

        Entity entity = entityRepo.findEntityById(billerId);

        if (Utils.isNullOrEmpty(billerId)) {
            adSettings = addSettingsRepo.findByEntity(entity);
        } else {
            adSettings = addSettingsRepo.findByEntity(entity);
        }

        AdSettingsDtoRs adSettingsDtoRs = new AdSettingsDtoRs();
        List<AdCampaignUsersDtoRq> adCampaignUsersDtoRsList = new ArrayList<AdCampaignUsersDtoRq>();

        if (adSettings != null) {
            adSettingsDtoRs.setAdModuleStatus(adSettings.getCampaignActive().name());
            adSettingsDtoRs.setAdSwitchEnable(adSettings.getAdSwitchEnable().name());
            adSettingsDtoRs.setAdSwitchDuration(adSettings.getAdSwitchDuration() != null ? adSettings.getAdSwitchDuration().toString() : null);
            adSettingsDtoRs.setId(Integer.valueOf(adSettings.getId().toString()));
            adSettingsDtoRs.setMasterAdvStatus(adSettings.getMasterAdvEnable().name());
            adSettingsDtoRs.setMasterCampaignId(adSettings.getMasterCampaignId() != null ? adSettings.getMasterCampaignId() : null);
            adSettingsDtoRs.setUrlClickable(adSettings.getUrlClickable().name());
            adSettingsDtoRs.setMasterURLEnable(adSettings.getMasterURLEnable() != null ? adSettings.getMasterURLEnable().name() : Status.ACTIVE.name());
            adSettingsDtoRs.setMasterURL(adSettings.getMasterURL() != null ? adSettings.getMasterURL() : "");

            for (AdCampaignUsers user : adSettings.getAdCampaignUserSet()) {

                AdCampaignUsersDtoRq AdCampaignUsersDtoRs = new AdCampaignUsersDtoRq();
                AdCampaignUsersDtoRs.setId(Integer.valueOf(user.getId().toString()));
                AdCampaignUsersDtoRs.setUserId(Integer.valueOf(user.getEntityUser().getId().toString()));
                AdCampaignUsersDtoRs.setStatus(user.getStatus().name());
                AdCampaignUsersDtoRs.setName(user.getEntityUser().getUsername());
                adCampaignUsersDtoRsList.add(AdCampaignUsersDtoRs);
            }
            if (adCampaignUsersDtoRsList.size() > 0) {
                adSettingsDtoRs.setCampaignUsers(adCampaignUsersDtoRsList);
            }

        } else {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "no_settings_found", locale);
        }
        return adSettingsDtoRs;
    }


}
