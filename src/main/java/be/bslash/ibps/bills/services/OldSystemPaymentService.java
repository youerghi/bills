package be.bslash.ibps.bills.services;

import be.bslash.ibps.bills.dto.response.PageDtoRs;
import be.bslash.ibps.bills.entities.Entity;
import be.bslash.ibps.bills.entities.OldSystemPayment;
import be.bslash.ibps.bills.repositories.OldSystemPaymentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Locale;

@Service
@Transactional
public class OldSystemPaymentService {

    @Autowired
    private OldSystemPaymentRepo oldSystemPaymentRepo;


    public PageDtoRs findAllByEntity(Entity entity, Integer page, Integer size, Locale locale) {

        if (page == null)
            page = (0);
        if (size == null)
            size = (10);

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "id");

        Page<OldSystemPayment> oldSystemPaymentPage = oldSystemPaymentRepo.findAllByEntity(entity, pageable);

        return new PageDtoRs(oldSystemPaymentPage.getTotalElements(), oldSystemPaymentPage.getTotalPages(), oldSystemPaymentPage);
    }
}
