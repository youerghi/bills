package be.bslash.ibps.bills.services;

import be.bslash.ibps.bills.entities.EntityUser;
import be.bslash.ibps.bills.repositories.EntityUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Locale;

@Service
@Transactional
public class EntityUserService {

    @Autowired
    private EntityUserRepo entityUserRepo;

    public EntityUser findEntityUserByUsername(String username, Locale locale) {

        EntityUser entityUser = entityUserRepo.findEntityUserByUsername(username);
        if (entityUser == null) {
            List<EntityUser> entityUserList = entityUserRepo.findDeletedEntityUserByUsername(username);
            if (entityUserList != null && !entityUserList.isEmpty()) {
                entityUser = entityUserList.get(0);
            }
        }
        return entityUser;
    }

}
