package be.bslash.ibps.bills.services;

import be.bslash.ibps.bills.dto.response.EntityActivityDtoRs;
import be.bslash.ibps.bills.entities.Entity;
import be.bslash.ibps.bills.entities.EntityActivity;
import be.bslash.ibps.bills.repositories.EntityActivityRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
@Transactional
public class EntityActivityService {

    @Autowired
    private EntityActivityRepo entityFieldRepo;

    public List<EntityActivityDtoRs> findAllByEntity(Entity entity, Locale locale) {

        List<EntityActivity> entityActivityList = entityFieldRepo.findAllActiveActivityByEntity(entity);

        List<EntityActivityDtoRs> entityActivityDtoRsList = new ArrayList<>();

        entityActivityList.forEach(entityActivity -> {
            EntityActivityDtoRs entityActivityDtoRs = new EntityActivityDtoRs();
            entityActivityDtoRs.setActivityName(entityActivity.getName(locale));
            entityActivityDtoRs.setId(entityActivity.getId());
            entityActivityDtoRs.setStatus(entityActivity.getStatus());
            entityActivityDtoRs.setActivityNameAr(entityActivity.getActivityNameAr());
            entityActivityDtoRs.setActivityNameEn(entityActivity.getActivityNameEn());
            entityActivityDtoRs.setIban(entityActivity.getIban());
            entityActivityDtoRs.setCode(entityActivity.getCode());

            entityActivityDtoRsList.add(entityActivityDtoRs);
        });

        return entityActivityDtoRsList;
    }

}
