
package be.bslash.ibps.bills.services;

import be.bslash.ibps.bills.dto.request.AddTemplateRqDto;
import be.bslash.ibps.bills.dto.response.TemplateDtoRs;
import be.bslash.ibps.bills.entities.EntityUser;
import be.bslash.ibps.bills.entities.RecurringTemplate;
import be.bslash.ibps.bills.enums.BooleanFlag;
import be.bslash.ibps.bills.enums.DurationType;
import be.bslash.ibps.bills.enums.SendNotificationType;
import be.bslash.ibps.bills.exceptions.HttpServiceException;
import be.bslash.ibps.bills.repositories.RecurringTemplateRepo;
import be.bslash.ibps.bills.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
@Transactional
public class RecurringTemplateService {


    @Autowired
    private RecurringTemplateRepo recurringTemplateRepo;

    public List<TemplateDtoRs> findAllByEntity(EntityUser entityUser, Locale locale) {

        List<RecurringTemplate> recurringTemplateList = recurringTemplateRepo.findAllByEntity(entityUser.getEntity());

        List<TemplateDtoRs> templateDtoRsList = new ArrayList<>();

        if (recurringTemplateList != null) {
            recurringTemplateList.forEach(recurringTemplate -> {

                TemplateDtoRs templateDtoRs = new TemplateDtoRs();

                templateDtoRs.setTemplateId(recurringTemplate.getId());
                templateDtoRs.setTemplateName(recurringTemplate.getName());
                templateDtoRs.setFrequency(recurringTemplate.getDurationFrequency());
                templateDtoRs.setDueDate(recurringTemplate.getDueDate().toString());
                templateDtoRs.setDuration(recurringTemplate.getDurationType().name());
                templateDtoRs.setInstallmentNumber(recurringTemplate.getInstallmentNumber());
                templateDtoRs.setInstallmentAmount(recurringTemplate.getInstallmentAmount());
                templateDtoRs.setNotification(recurringTemplate.getSendNotificationType().name());
                templateDtoRs.setNotificationDays(recurringTemplate.getNumberOfDays());
                templateDtoRs.setIsPartialAllowed(recurringTemplate.getIsPartialAllowed() != null ? recurringTemplate.getIsPartialAllowed().name() : null);
                templateDtoRs.setMiniPartialAmount(recurringTemplate.getMiniPartialAmount());

                templateDtoRsList.add(templateDtoRs);
            });
        }

        return templateDtoRsList;
    }

    public void saveNewTemplates(AddTemplateRqDto addTemplateRqDto, EntityUser entityUser, Locale locale) {

        String dueDate = addTemplateRqDto.getDueDate();

        if (dueDate != null) {

            if (dueDate.contains("T")) {
                dueDate = dueDate.split("T")[0];
            }
        }

        List<RecurringTemplate> recurringTemplates = recurringTemplateRepo.findRecurringTemplateByNameAndEntity(addTemplateRqDto.getTemplateName(), entityUser.getEntity());

        if (recurringTemplates != null && recurringTemplates.size() > 0) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "Template name already exist", locale);
        }

        RecurringTemplate recurringTemplate = new RecurringTemplate();
        recurringTemplate.setEntity(entityUser.getEntity());
        recurringTemplate.setEntityUser(entityUser);
        recurringTemplate.setName(addTemplateRqDto.getTemplateName());
        recurringTemplate.setDueDate(Utils.parseDateFromString(dueDate, "yyyy-MM-dd"));
        recurringTemplate.setDurationFrequency(addTemplateRqDto.getFrequency());
        recurringTemplate.setInstallmentNumber(addTemplateRqDto.getInstallmentNumber());
        recurringTemplate.setInstallmentAmount(addTemplateRqDto.getInstallmentAmount());
        recurringTemplate.setDurationType(DurationType.getValue(addTemplateRqDto.getDuration()));
        recurringTemplate.setSendNotificationType(SendNotificationType.getValue(addTemplateRqDto.getNotification()));
        recurringTemplate.setNumberOfDays(addTemplateRqDto.getNotificationDays());
        recurringTemplate.setIsPartialAllowed(BooleanFlag.getValue(addTemplateRqDto.getIsPartialAllowed()));
        recurringTemplate.setMiniPartialAmount(addTemplateRqDto.getMiniPartialAmount());

        recurringTemplateRepo.save(recurringTemplate);

    }
}
