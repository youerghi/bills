package be.bslash.ibps.bills.services;

import be.bslash.ibps.bills.entities.Bill;
import be.bslash.ibps.bills.entities.BillEmail;
import be.bslash.ibps.bills.entities.BillMobileNumber;
import be.bslash.ibps.bills.entities.BillNotificationEmail;
import be.bslash.ibps.bills.entities.BillNotificationSms;
import be.bslash.ibps.bills.entities.BillNotificationTemplate;
import be.bslash.ibps.bills.entities.Entity;
import be.bslash.ibps.bills.entities.EntityUser;
import be.bslash.ibps.bills.entities.FcmNotification;
import be.bslash.ibps.bills.entities.LocalPayment;
import be.bslash.ibps.bills.enums.BillNotificationType;
import be.bslash.ibps.bills.enums.BillStatus;
import be.bslash.ibps.bills.enums.BooleanFlag;
import be.bslash.ibps.bills.enums.Language;
import be.bslash.ibps.bills.enums.NotificationMedia;
import be.bslash.ibps.bills.enums.NotificationStatus;
import be.bslash.ibps.bills.enums.Status;
import be.bslash.ibps.bills.repositories.BillEmailRepo;
import be.bslash.ibps.bills.repositories.BillMobileNumberRepo;
import be.bslash.ibps.bills.repositories.BillNotificationEmailRepo;
import be.bslash.ibps.bills.repositories.BillNotificationSmsRepo;
import be.bslash.ibps.bills.repositories.BillNotificationTemplateRepo;
import be.bslash.ibps.bills.repositories.BillRepo;
import be.bslash.ibps.bills.repositories.EntityRepo;
import be.bslash.ibps.bills.repositories.EntityUserRepo;
import be.bslash.ibps.bills.repositories.FcmNotificationRepo;
import be.bslash.ibps.bills.utils.AES256;
import be.bslash.ibps.bills.utils.Utils;
import be.bslash.ibps.bills.ws.WebServiceHelper;
import be.bslash.ibps.bills.ws.WebServiceResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
@Transactional
public class BillNotificationService {

    private static final Logger LOGGER = LogManager.getLogger(BillNotificationService.class);

    @Autowired
    private BillNotificationEmailRepo billNotificationEmailRepo;

    @Autowired
    private BillNotificationSmsRepo billNotificationSmsRepo;

    @Autowired
    private BillNotificationTemplateRepo billNotificationTemplateRepo;

    @Autowired
    private BillRepo billRepo;

    @Autowired
    private WebServiceHelper webServiceHelper;

    @Autowired
    private Environment environment;

    @Autowired
    private AES256 aes256;

    @Autowired
    private BillMobileNumberRepo billMobileNumberRepo;

    @Autowired
    private BillEmailRepo billEmailRepo;

    @Autowired
    private EntityRepo entityRepo;

    @Autowired
    private EntityUserRepo entityUserRepo;

    @Autowired
    private FcmNotificationRepo fcmNotificationRepo;

    public void sendBillNotificationIssueJob() {

        List<Bill> billList = billRepo.findAll(new Specification<Bill>() {


            public Predicate toPredicate(Root<Bill> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                List<Predicate> predicates = new ArrayList<>();

                predicates.add(cb.equal(root.get("billStatus"), BillStatus.APPROVED_BY_SADAD));
                predicates.add(cb.greaterThanOrEqualTo(root.get("issueDate"), LocalDate.now().atStartOfDay()));
                predicates.add(cb.lessThanOrEqualTo(root.get("issueDate"), LocalDate.now().atStartOfDay().plusHours(23).plusMinutes(59).plusSeconds(59)));

                return cb.and(predicates.toArray(new Predicate[0]));
            }
        });

        LOGGER.info("billList: " + billList.size());

        List<BillNotificationEmail> billNotificationEmailList = new ArrayList<>();

        List<BillNotificationSms> billNotificationSmsList = new ArrayList<>();

        BillNotificationTemplate billNotificationTemplate = billNotificationTemplateRepo.find(NotificationMedia.SMS, Language.ARABIC, BillNotificationType.ISSUE);

        for (Bill bill : billList) {

            Entity entity = bill.getEntity();

            Language notificationLanguage = entity.getSendBillLanguage();

            //Check if entity allow SMS send
            if (entity.getSendBillSmsBooleanFlag().equals(BooleanFlag.YES)) {

                BooleanFlag smsIssueFlag = bill.getSmsIssueFlag();

                String entityNameAr = entity.getBrandNameAr();
                String entityNameEn = entity.getBrandNameEn();
                String totalAmount = Utils.roundDecimal(bill.getTotalAmount());
                String sadadNumber = bill.getSadadNumber();
                String billNumberEncoded = aes256.encrypt(sadadNumber + "_" + bill.getCycleNumber());
                String dateTime = Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd hh:mm:ss");

                String body = Utils.getSmsBody(billNotificationTemplate.getLayout(),
                        entityNameAr,
                        entityNameEn,
                        totalAmount,
                        sadadNumber,
                        billNumberEncoded,
                        dateTime);

                WebServiceResponse costSmsResponse = webServiceHelper.calculateSmsPoint(billNotificationTemplate.getTitle(), body, bill.getCustomerMobileNumber());
                LOGGER.info("costSmsResponse: " + costSmsResponse.getBody());

                Long smsPoint = Utils.getCostSms(costSmsResponse.getBody());
                LOGGER.info("smsPoint: " + smsPoint);

                WebServiceResponse sensSmsWebServiceResponse = webServiceHelper.sendSmsNotification(billNotificationTemplate.getTitle(), body, bill.getCustomerMobileNumber());
                LOGGER.info("sensSmsWebServiceResponse: " + sensSmsWebServiceResponse.getBody());

                String responseCode = Utils.getSmsResponseCode(sensSmsWebServiceResponse.getBody());
                LOGGER.info("smsResponseCode: " + responseCode);

                BillNotificationSms billNotificationSms = new BillNotificationSms();
                billNotificationSms.setBill(bill);
                billNotificationSms.setTitle(billNotificationTemplate.getTitle());
                billNotificationSms.setNotificationLanguage(notificationLanguage);
                billNotificationSms.setMobileNumber(bill.getCustomerMobileNumber());
                billNotificationSms.setUserReference("SYSTEM");
                billNotificationSms.setCreateDate(LocalDateTime.now());
                billNotificationSms.setBody(body);
                billNotificationSms.setResponseCode(responseCode);
                billNotificationSms.setBillNotificationType(BillNotificationType.REMINDER);

                if (responseCode.equals("1") || responseCode.equals("M0000")) {
                    billNotificationSms.setPoint(smsPoint);

                    Long point = entity.getSmsPointBalance() - smsPoint;
                    entityRepo.updateSmsPointBalance(point, entity.getId());
                } else {
                    billNotificationSms.setPoint(0L);
                }

                billNotificationSmsList.add(billNotificationSms);
            }

            //Check if entity allow Email send
            if (entity.getSendBillEmailBooleanFlag().equals(BooleanFlag.YES)) {


                if (bill.getCustomerEmailAddress() != null && !bill.getCustomerEmailAddress().isEmpty()) {

                    String fromEmail = "efaa.info@dci.sa";
                    String subject = "New Invoice";
                    String toEmail = bill.getCustomerEmailAddress();
                    String type = "text/html";

                    String layout = "تذكير اصدار فاتورة جديدة من ENTITY_NAME_AR بمبلغ BILL_AMOUNT ريال وبرقم سداد SADAD_NUMBER, يمكنك دفع الفاتورة من رقم المفوتر 902(مدفوعات ايفاء).للإطلاع على الفاتورة يرجى الضغط على الرابط ادناه:";

                    String entityNameAr = entity.getBrandNameAr();
                    String entityNameEn = entity.getBrandNameEn();
                    String totalAmount = Utils.roundDecimal(bill.getTotalAmount());
                    String sadadNumber = bill.getSadadNumber();
                    String billNumberEncoded = aes256.encrypt(sadadNumber + "_" + bill.getCycleNumber());
                    String dateTime = Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd hh:mm:ss");

                    String body = Utils.getSmsBody(layout,
                            entityNameAr,
                            entityNameEn,
                            totalAmount,
                            sadadNumber,
                            billNumberEncoded,
                            dateTime);

                    String smsViewUrl = environment.getProperty("sms_view_url") + billNumberEncoded;

                    String html = "<!DOCTYPE html>\n" +
                            "\n" +
                            "<title></title>\n" +
                            "\n" +
                            "<body style=\"direction: rtl;\">\n" +
                            "\n" +
                            "    <div style=\"width: 100%; text-align: center;\">\n" +
                            "        <img alt=\"logo\" src=\"" + bill.getLogo() + "\">\n" +
                            "    </div>\n" +
                            "\n" +
                            "    <h1>عزيزي عميل شركة " + entityNameAr + "</h1>\n" +
                            "\n" +
                            "    <p>\n" +
                            "        نأمل أن تحوز خدماتنا المقدمة اليكم \"الفاتورة الالكترونية - الدفع الالكتروني\" على رضاكم واستحسانكم.. \n" +
                            "    </p>\n" +
                            "\n" +
                            "    <p>\n" +
                            " نود إشعاركم بصدور فاتورة جديده بمبلغ " + totalAmount + " ريال - ورقم سداد " + sadadNumber + "،\n" +
                            "        يمكنك دفع الفاتورة الخاصة بكم من خلال رقم المفوتر 902 \"مدفوعات ايفاء\"..\n" +
                            "    </p>\n" +
                            "\n" +
                            "    <p>\n" +
                            "        وللاطلاع على تفاصيل الفاتورة يرجى الضغط على الرابط ادناه:\n" +
                            "    </p>\n" +
                            "    <p>\n" +
                            "        <a href=\"" + smsViewUrl + "\">اضغط هنا</a>\n" +
                            "    </p>\n" +
                            "\n" +
                            "    <p>\n" +
                            "        وشكرا لكم..\n" +
                            "    </p>\n" +
                            "\n" +
                            "</body>\n" +
                            "\n" +
                            "</html>";

                    WebServiceResponse sensEmailWebServiceResponse = webServiceHelper.sendEmailNotification(fromEmail, subject, toEmail, type, html);
                    LOGGER.info("sensEmailWebServiceResponse: " + sensEmailWebServiceResponse.getBody());

                    LOGGER.info("smsResponseCode: " + sensEmailWebServiceResponse.getStatus());

                    BillNotificationEmail issueEmailBillNotification = new BillNotificationEmail();
                    issueEmailBillNotification.setBill(bill);
                    issueEmailBillNotification.setTitle(subject);
                    issueEmailBillNotification.setNotificationLanguage(Language.ALL);
                    issueEmailBillNotification.setEmail(toEmail);
                    issueEmailBillNotification.setUserReference("SYSTEM");
                    issueEmailBillNotification.setBody(html);
                    issueEmailBillNotification.setResponseCode(sensEmailWebServiceResponse.getStatus().toString());
                    issueEmailBillNotification.setCreateDate(LocalDateTime.now());
                    issueEmailBillNotification.setBillNotificationType(BillNotificationType.REMINDER);

                    billNotificationEmailList.add(issueEmailBillNotification);
                }
            }
        }

        billNotificationSmsRepo.saveAll(billNotificationSmsList);
        billNotificationEmailRepo.saveAll(billNotificationEmailList);
    }

    public void sendBillNotificationIssue(Bill bill, EntityUser entityUser, Locale locale) {

        LocalDateTime issueDateInterval = LocalDate.now().atStartOfDay().plusHours(23).plusMinutes(59).plusSeconds(59);
        LOGGER.info("issueDateInterval: " + issueDateInterval + " bill: " + bill.getIssueDate());

        if (bill.getIssueDate().isAfter(issueDateInterval)) {
            return;
        }

        Entity entity = bill.getEntity();


        //Check if entity allow SMS send
        if (entity.getSendBillSmsBooleanFlag().equals(BooleanFlag.YES)) {

            Long totalSmsPoint = 0L;

            List<BillNotificationSms> billNotificationSmsList = new ArrayList<>();

            BooleanFlag smsIssueFlag = bill.getSmsIssueFlag();

            if (smsIssueFlag != null && smsIssueFlag.equals(BooleanFlag.YES)) {
                return;
            }

            Language notificationLanguage = entity.getSendBillLanguage();

            BillNotificationTemplate billNotificationTemplate = billNotificationTemplateRepo.find(NotificationMedia.SMS, notificationLanguage, BillNotificationType.ISSUE);

            String entityNameAr = entity.getBrandNameAr();
            String entityNameEn = entity.getBrandNameEn();
            String totalAmount = Utils.roundDecimal(bill.getTotalAmount());
            String sadadNumber = bill.getSadadNumber();
            String billNumberEncoded = aes256.encrypt(sadadNumber + "_" + bill.getCycleNumber());
            String dateTime = Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd hh:mm:ss");

            String body = Utils.getSmsBody(billNotificationTemplate.getLayout(),
                    entityNameAr,
                    entityNameEn,
                    totalAmount,
                    sadadNumber,
                    billNumberEncoded,
                    dateTime);

            WebServiceResponse costSmsResponse = webServiceHelper.calculateSmsPoint(billNotificationTemplate.getTitle(), body, bill.getCustomerMobileNumber());
            LOGGER.info("costSmsResponse: " + costSmsResponse.getBody());

            Long smsPoint = Utils.getCostSms(costSmsResponse.getBody());
            LOGGER.info("smsPoint: " + smsPoint);

            WebServiceResponse sensSmsWebServiceResponse = webServiceHelper.sendSmsNotification(billNotificationTemplate.getTitle(), body, bill.getCustomerMobileNumber());
            LOGGER.info("sensSmsWebServiceResponse: " + sensSmsWebServiceResponse.getBody());

            String responseCode = Utils.getSmsResponseCode(sensSmsWebServiceResponse.getBody());
            LOGGER.info("smsResponseCode: " + responseCode);

            BillNotificationSms billNotificationSms = new BillNotificationSms();
            billNotificationSms.setBill(bill);
            billNotificationSms.setTitle(billNotificationTemplate.getTitle());
            billNotificationSms.setNotificationLanguage(notificationLanguage);
            billNotificationSms.setMobileNumber(bill.getCustomerMobileNumber());
            billNotificationSms.setUserReference(entityUser.getUsername());
            billNotificationSms.setCreateDate(LocalDateTime.now());
            billNotificationSms.setBody(body);
            billNotificationSms.setResponseCode(responseCode);
            billNotificationSms.setBillNotificationType(BillNotificationType.ISSUE);

            billRepo.updateSmsFlag(bill.getId());

            if (responseCode.equals("1") || responseCode.equals("M0000")) {
                totalSmsPoint += smsPoint;
                billNotificationSms.setPoint(smsPoint);
            } else {
                billNotificationSms.setPoint(0L);
            }

            billNotificationSmsList.add(billNotificationSms);

            List<BillMobileNumber> billMobileNumberList = billMobileNumberRepo.findAllBillMobileNumber(bill);
            if (billMobileNumberList != null && !billMobileNumberList.isEmpty()) {

                for (BillMobileNumber billMobileNumber : billMobileNumberList) {

                    WebServiceResponse costSmsResponseOther = webServiceHelper.calculateSmsPoint(billNotificationTemplate.getTitle(), body, billMobileNumber.getMobileNumber());
                    LOGGER.info("costSmsResponseOther: " + costSmsResponseOther.getBody());

                    Long smsPointOther = Utils.getCostSms(costSmsResponseOther.getBody());
                    LOGGER.info("smsPointOther: " + smsPointOther);

                    WebServiceResponse sensSmsWebServiceResponseOther = webServiceHelper.sendSmsNotification(billNotificationTemplate.getTitle(), body, billMobileNumber.getMobileNumber());
                    LOGGER.info("sensSmsWebServiceResponseOther: " + sensSmsWebServiceResponseOther.getBody());

                    String responseCodeOther = Utils.getSmsResponseCode(sensSmsWebServiceResponseOther.getBody());
                    LOGGER.info("smsResponseCode: " + responseCodeOther);

                    BillNotificationSms billNotificationSmsOther = new BillNotificationSms();
                    billNotificationSmsOther.setBill(bill);
                    billNotificationSmsOther.setTitle(billNotificationTemplate.getTitle());
                    billNotificationSmsOther.setNotificationLanguage(notificationLanguage);
                    billNotificationSmsOther.setMobileNumber(billMobileNumber.getMobileNumber());
                    billNotificationSmsOther.setUserReference(entityUser.getUsername());
                    billNotificationSmsOther.setCreateDate(LocalDateTime.now());
                    billNotificationSmsOther.setBody(body);
                    billNotificationSmsOther.setPoint(smsPointOther);
                    billNotificationSmsOther.setResponseCode(responseCodeOther);
                    billNotificationSmsOther.setBillNotificationType(BillNotificationType.ISSUE);

                    if (responseCodeOther.equals("1") || responseCodeOther.equals("M0000")) {
                        totalSmsPoint += smsPointOther;
                        billNotificationSmsOther.setPoint(smsPointOther);
                    } else {
                        billNotificationSmsOther.setPoint(0L);
                    }

                    billNotificationSmsList.add(billNotificationSmsOther);
                }
            }
            LOGGER.info("SMS LIST: " + billNotificationSmsList.size());

            billNotificationSmsRepo.saveAll(billNotificationSmsList);

            Entity updateEntity = entityRepo.findEntityForUpdate(entity.getId());
            Long point = updateEntity.getSmsPointBalance() - totalSmsPoint;
            entityRepo.updateSmsPointBalance(point, updateEntity.getId());
        }

        //Check if entity allow Email send
        if (entity.getSendBillEmailBooleanFlag().equals(BooleanFlag.YES)) {

            List<BillNotificationEmail> billNotificationEmailList = new ArrayList<>();


            if (bill.getCustomerEmailAddress() != null && !bill.getCustomerEmailAddress().isEmpty()) {
                Language notificationLanguage = entity.getSendBillLanguage();

                String fromEmail = "efaa.info@dci.sa";
                String subject = "New Invoice";
                String toEmail = bill.getCustomerEmailAddress();
                String type = "text/html";

                String layout = "لقد تم اصدار فاتورة جديدة من ENTITY_NAME_AR بمبلغ BILL_AMOUNT ريال وبرقم سداد SADAD_NUMBER, يمكنك دفع الفاتورة من رقم المفوتر 902(مدفوعات ايفاء).للإطلاع على الفاتورة يرجى الضغط على الرابط ادناه:";

                String entityNameAr = entity.getBrandNameAr();
                String entityNameEn = entity.getBrandNameEn();
                String totalAmount = Utils.roundDecimal(bill.getTotalAmount());
                String sadadNumber = bill.getSadadNumber();
                String billNumberEncoded = aes256.encrypt(sadadNumber + "_" + bill.getCycleNumber());
                String dateTime = Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd hh:mm:ss");

                String body = Utils.getSmsBody(layout,
                        entityNameAr,
                        entityNameEn,
                        totalAmount,
                        sadadNumber,
                        billNumberEncoded,
                        dateTime);

                String smsViewUrl = environment.getProperty("sms_view_url") + billNumberEncoded;

                String html = "<!DOCTYPE html>\n" +
                        "\n" +
                        "<title></title>\n" +
                        "\n" +
                        "<body style=\"direction: rtl;\">\n" +
                        "\n" +
                        "    <div style=\"width: 100%; text-align: center;\">\n" +
                        "        <img alt=\"logo\" src=\"" + bill.getLogo() + "\">\n" +
                        "    </div>\n" +
                        "\n" +
                        "    <h1>عزيزي عميل شركة " + entityNameAr + "</h1>\n" +
                        "\n" +
                        "    <p>\n" +
                        "        نأمل أن تحوز خدماتنا المقدمة اليكم \"الفاتورة الالكترونية - الدفع الالكتروني\" على رضاكم واستحسانكم.. \n" +
                        "    </p>\n" +
                        "\n" +
                        "    <p>\n" +
                        " نود إشعاركم بصدور فاتورة جديده بمبلغ " + totalAmount + " ريال - ورقم سداد " + sadadNumber + "،\n" +
                        "        يمكنك دفع الفاتورة الخاصة بكم من خلال رقم المفوتر 902 \"مدفوعات ايفاء\"..\n" +
                        "    </p>\n" +
                        "\n" +
                        "    <p>\n" +
                        "        وللاطلاع على تفاصيل الفاتورة يرجى الضغط على الرابط ادناه:\n" +
                        "    </p>\n" +
                        "    <p>\n" +
                        "        <a href=\"" + smsViewUrl + "\">اضغط هنا</a>\n" +
                        "    </p>\n" +
                        "\n" +
                        "    <p>\n" +
                        "        وشكرا لكم..\n" +
                        "    </p>\n" +
                        "\n" +
                        "</body>\n" +
                        "\n" +
                        "</html>";

                WebServiceResponse sensEmailWebServiceResponse = webServiceHelper.sendEmailNotification(fromEmail, subject, toEmail, type, html);
                LOGGER.info("sensEmailWebServiceResponse: " + sensEmailWebServiceResponse.getBody());

                LOGGER.info("smsResponseCode: " + sensEmailWebServiceResponse.getStatus());

                BillNotificationEmail issueEmailBillNotification = new BillNotificationEmail();
                issueEmailBillNotification.setBill(bill);
                issueEmailBillNotification.setTitle(subject);
                issueEmailBillNotification.setNotificationLanguage(Language.ALL);
                issueEmailBillNotification.setEmail(toEmail);
                issueEmailBillNotification.setUserReference(entityUser.getUsername());
                issueEmailBillNotification.setBody(html);
                issueEmailBillNotification.setResponseCode(sensEmailWebServiceResponse.getStatus().toString());
                issueEmailBillNotification.setCreateDate(LocalDateTime.now());
                issueEmailBillNotification.setBillNotificationType(BillNotificationType.ISSUE);

                billNotificationEmailList.add(issueEmailBillNotification);
            }


            List<BillEmail> billEmailList = billEmailRepo.findAllBillEmail(bill);
            if (billEmailList != null && !billEmailList.isEmpty()) {

                for (BillEmail billEmail : billEmailList) {

                    Language notificationLanguage = entity.getSendBillLanguage();

                    String fromEmail = "efaa.info@dci.sa";
                    String subject = "New Invoice";
                    String toEmail = billEmail.getEmail();
                    String type = "text/html";

                    String layout = "لقد تم اصدار فاتورة جديدة من ENTITY_NAME_AR بمبلغ BILL_AMOUNT ريال وبرقم سداد SADAD_NUMBER, يمكنك دفع الفاتورة من رقم المفوتر 902(مدفوعات ايفاء).للإطلاع على الفاتورة يرجى الضغط على الرابط ادناه:";

                    String entityNameAr = entity.getBrandNameAr();
                    String entityNameEn = entity.getBrandNameEn();
                    String totalAmount = Utils.roundDecimal(bill.getTotalAmount());
                    String sadadNumber = bill.getSadadNumber();
                    String billNumberEncoded = aes256.encrypt(sadadNumber + "_" + bill.getCycleNumber());
                    String dateTime = Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd hh:mm:ss");

                    String body = Utils.getSmsBody(layout,
                            entityNameAr,
                            entityNameEn,
                            totalAmount,
                            sadadNumber,
                            billNumberEncoded,
                            dateTime);

                    String smsViewUrl = environment.getProperty("sms_view_url") + billNumberEncoded;

                    String html = "<!DOCTYPE html>\n" +
                            "\n" +
                            "<title></title>\n" +
                            "\n" +
                            "<body style=\"direction: rtl;\">\n" +
                            "\n" +
                            "    <div style=\"width: 100%; text-align: center;\">\n" +
                            "        <img alt=\"logo\" src=\"" + bill.getLogo() + "\">\n" +
                            "    </div>\n" +
                            "\n" +
                            "    <h1>عزيزي عميل شركة " + entityNameAr + "</h1>\n" +
                            "\n" +
                            "    <p>\n" +
                            "        نأمل أن تحوز خدماتنا المقدمة اليكم \"الفاتورة الالكترونية - الدفع الالكتروني\" على رضاكم واستحسانكم.. \n" +
                            "    </p>\n" +
                            "\n" +
                            "    <p>\n" +
                            " نود إشعاركم بصدور فاتورة جديده بمبلغ " + totalAmount + " ريال - ورقم سداد " + sadadNumber + "،\n" +
                            "        يمكنك دفع الفاتورة الخاصة بكم من خلال رقم المفوتر 902 \"مدفوعات ايفاء\"..\n" +
                            "    </p>\n" +
                            "\n" +
                            "    <p>\n" +
                            "        وللاطلاع على تفاصيل الفاتورة يرجى الضغط على الرابط ادناه:\n" +
                            "    </p>\n" +
                            "    <p>\n" +
                            "        <a href=\"" + smsViewUrl + "\">اضغط هنا</a>\n" +
                            "    </p>\n" +
                            "\n" +
                            "    <p>\n" +
                            "        وشكرا لكم..\n" +
                            "    </p>\n" +
                            "\n" +
                            "</body>\n" +
                            "\n" +
                            "</html>";

                    WebServiceResponse sensEmailWebServiceResponse = webServiceHelper.sendEmailNotification(fromEmail, subject, toEmail, type, html);
                    LOGGER.info("sensEmailWebServiceResponse: " + sensEmailWebServiceResponse.getBody());

                    LOGGER.info("smsResponseCode: " + sensEmailWebServiceResponse.getStatus());

                    BillNotificationEmail issueEmailBillNotification = new BillNotificationEmail();
                    issueEmailBillNotification.setBill(bill);
                    issueEmailBillNotification.setTitle(subject);
                    issueEmailBillNotification.setNotificationLanguage(Language.ALL);
                    issueEmailBillNotification.setEmail(billEmail.getEmail());
                    issueEmailBillNotification.setUserReference(entityUser.getUsername());
                    issueEmailBillNotification.setBody(html);
                    issueEmailBillNotification.setResponseCode(sensEmailWebServiceResponse.getStatus().toString());
                    issueEmailBillNotification.setCreateDate(LocalDateTime.now());
                    issueEmailBillNotification.setBillNotificationType(BillNotificationType.ISSUE);

                    billNotificationEmailList.add(issueEmailBillNotification);
                }
            }
            LOGGER.info("EMAIL LIST: " + billNotificationEmailList.size());

            billNotificationEmailRepo.saveAll(billNotificationEmailList);
        }
    }

    public void sendBillNotificationIssueUpdate(Bill bill, EntityUser entityUser, Locale locale) {
        LocalDateTime issueDateInterval = LocalDate.now().atStartOfDay().plusHours(23).plusMinutes(59).plusSeconds(59);
        LOGGER.info("issueDateInterval: " + issueDateInterval + " bill: " + bill.getIssueDate());

        if (bill.getIssueDate().isAfter(issueDateInterval)) {
            return;
        }

        Entity entity = bill.getEntity();


        //Check if entity allow SMS send
        if (entity.getSendBillSmsBooleanFlag().equals(BooleanFlag.YES)) {

            Long totalSmsPoint = 0L;

            List<BillNotificationSms> billNotificationSmsList = new ArrayList<>();

            Language notificationLanguage = entity.getSendBillLanguage();

            BillNotificationTemplate billNotificationTemplate = billNotificationTemplateRepo.find(NotificationMedia.SMS, notificationLanguage, BillNotificationType.ISSUE);

            String entityNameAr = entity.getBrandNameAr();
            String entityNameEn = entity.getBrandNameEn();
            String totalAmount = Utils.roundDecimal(bill.getTotalAmount());
            String sadadNumber = bill.getSadadNumber();
            String billNumberEncoded = aes256.encrypt(sadadNumber + "_" + bill.getCycleNumber());
            String dateTime = Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd hh:mm:ss");

            String body = Utils.getSmsBody(billNotificationTemplate.getLayout(),
                    entityNameAr,
                    entityNameEn,
                    totalAmount,
                    sadadNumber,
                    billNumberEncoded,
                    dateTime);


            List<BillMobileNumber> billMobileNumberList = billMobileNumberRepo.findAllBillMobileNumber(bill);

            if (billMobileNumberList != null && !billMobileNumberList.isEmpty()) {

                for (BillMobileNumber billMobileNumber : billMobileNumberList) {

                    List<BillNotificationSms> currentBillNotificationSmsList = billNotificationSmsRepo.findByMobile(bill, billMobileNumber.getMobileNumber());

                    if (currentBillNotificationSmsList != null && !currentBillNotificationSmsList.isEmpty()) {
                        continue;
                    }

                    WebServiceResponse costSmsResponseOther = webServiceHelper.calculateSmsPoint(billNotificationTemplate.getTitle(), body, billMobileNumber.getMobileNumber());
                    LOGGER.info("costSmsResponseOther: " + costSmsResponseOther.getBody());

                    Long smsPointOther = Utils.getCostSms(costSmsResponseOther.getBody());
                    LOGGER.info("smsPointOther: " + smsPointOther);

                    WebServiceResponse sensSmsWebServiceResponseOther = webServiceHelper.sendSmsNotification(billNotificationTemplate.getTitle(), body, billMobileNumber.getMobileNumber());
                    LOGGER.info("sensSmsWebServiceResponseOther: " + sensSmsWebServiceResponseOther.getBody());

                    String responseCodeOther = Utils.getSmsResponseCode(sensSmsWebServiceResponseOther.getBody());
                    LOGGER.info("smsResponseCode: " + responseCodeOther);

                    BillNotificationSms billNotificationSmsOther = new BillNotificationSms();
                    billNotificationSmsOther.setBill(bill);
                    billNotificationSmsOther.setTitle(billNotificationTemplate.getTitle());
                    billNotificationSmsOther.setNotificationLanguage(notificationLanguage);
                    billNotificationSmsOther.setMobileNumber(billMobileNumber.getMobileNumber());
                    billNotificationSmsOther.setUserReference(entityUser.getUsername());
                    billNotificationSmsOther.setCreateDate(LocalDateTime.now());
                    billNotificationSmsOther.setBody(body);
                    billNotificationSmsOther.setPoint(smsPointOther);
                    billNotificationSmsOther.setResponseCode(responseCodeOther);
                    billNotificationSmsOther.setBillNotificationType(BillNotificationType.ISSUE);

                    if (responseCodeOther.equals("1") || responseCodeOther.equals("M0000")) {
                        totalSmsPoint += smsPointOther;
                        billNotificationSmsOther.setPoint(smsPointOther);
                    } else {
                        billNotificationSmsOther.setPoint(0L);
                    }

                    billNotificationSmsList.add(billNotificationSmsOther);
                }
            }
            LOGGER.info("SMS LIST: " + billNotificationSmsList.size());

            billNotificationSmsRepo.saveAll(billNotificationSmsList);

            Entity updateEntity = entityRepo.findEntityForUpdate(entity.getId());
            Long point = updateEntity.getSmsPointBalance() - totalSmsPoint;
            entityRepo.updateSmsPointBalance(point, updateEntity.getId());
        }

        //Check if entity allow Email send
        if (entity.getSendBillEmailBooleanFlag().equals(BooleanFlag.YES)) {

            List<BillNotificationEmail> billNotificationEmailList = new ArrayList<>();

            List<BillEmail> billEmailList = billEmailRepo.findAllBillEmail(bill);

            if (billEmailList != null && !billEmailList.isEmpty()) {

                for (BillEmail billEmail : billEmailList) {

                    List<BillNotificationEmail> currentBillNotificationEmailList = billNotificationEmailRepo.findByEmail(bill, billEmail.getEmail());

                    if (currentBillNotificationEmailList != null && !currentBillNotificationEmailList.isEmpty()) {
                        continue;
                    }

                    Language notificationLanguage = entity.getSendBillLanguage();

                    String fromEmail = "efaa.info@dci.sa";
                    String subject = "New Invoice";
                    String toEmail = billEmail.getEmail();
                    String type = "text/html";

                    String layout = "لقد تم اصدار فاتورة جديدة من ENTITY_NAME_AR بمبلغ BILL_AMOUNT ريال وبرقم سداد SADAD_NUMBER, يمكنك دفع الفاتورة من رقم المفوتر 902(مدفوعات ايفاء).للإطلاع على الفاتورة يرجى الضغط على الرابط ادناه:";

                    String entityNameAr = entity.getBrandNameAr();
                    String entityNameEn = entity.getBrandNameEn();
                    String totalAmount = Utils.roundDecimal(bill.getTotalAmount());
                    String sadadNumber = bill.getSadadNumber();
                    String billNumberEncoded = aes256.encrypt(sadadNumber + "_" + bill.getCycleNumber());
                    String dateTime = Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd hh:mm:ss");

                    String body = Utils.getSmsBody(layout,
                            entityNameAr,
                            entityNameEn,
                            totalAmount,
                            sadadNumber,
                            billNumberEncoded,
                            dateTime);

                    String smsViewUrl = environment.getProperty("sms_view_url") + billNumberEncoded;

                    String html = "<!DOCTYPE html>\n" +
                            "\n" +
                            "<title></title>\n" +
                            "\n" +
                            "<body style=\"direction: rtl;\">\n" +
                            "\n" +
                            "    <div style=\"width: 100%; text-align: center;\">\n" +
                            "        <img alt=\"logo\" src=\"" + bill.getLogo() + "\">\n" +
                            "    </div>\n" +
                            "\n" +
                            "    <h1>عزيزي عميل شركة " + entityNameAr + "</h1>\n" +
                            "\n" +
                            "    <p>\n" +
                            "        نأمل أن تحوز خدماتنا المقدمة اليكم \"الفاتورة الالكترونية - الدفع الالكتروني\" على رضاكم واستحسانكم.. \n" +
                            "    </p>\n" +
                            "\n" +
                            "    <p>\n" +
                            " نود إشعاركم بصدور فاتورة جديده بمبلغ " + totalAmount + " ريال - ورقم سداد " + sadadNumber + "،\n" +
                            "        يمكنك دفع الفاتورة الخاصة بكم من خلال رقم المفوتر 902 \"مدفوعات ايفاء\"..\n" +
                            "    </p>\n" +
                            "\n" +
                            "    <p>\n" +
                            "        وللاطلاع على تفاصيل الفاتورة يرجى الضغط على الرابط ادناه:\n" +
                            "    </p>\n" +
                            "    <p>\n" +
                            "        <a href=\"" + smsViewUrl + "\">اضغط هنا</a>\n" +
                            "    </p>\n" +
                            "\n" +
                            "    <p>\n" +
                            "        وشكرا لكم..\n" +
                            "    </p>\n" +
                            "\n" +
                            "</body>\n" +
                            "\n" +
                            "</html>";

                    WebServiceResponse sensEmailWebServiceResponse = webServiceHelper.sendEmailNotification(fromEmail, subject, toEmail, type, html);
                    LOGGER.info("sensEmailWebServiceResponse: " + sensEmailWebServiceResponse.getBody());

                    LOGGER.info("smsResponseCode: " + sensEmailWebServiceResponse.getStatus());

                    BillNotificationEmail issueEmailBillNotification = new BillNotificationEmail();
                    issueEmailBillNotification.setBill(bill);
                    issueEmailBillNotification.setTitle(subject);
                    issueEmailBillNotification.setNotificationLanguage(Language.ALL);
                    issueEmailBillNotification.setEmail(billEmail.getEmail());
                    issueEmailBillNotification.setUserReference(entityUser.getUsername());
                    issueEmailBillNotification.setBody(html);
                    issueEmailBillNotification.setResponseCode(sensEmailWebServiceResponse.getStatus().toString());
                    issueEmailBillNotification.setCreateDate(LocalDateTime.now());
                    issueEmailBillNotification.setBillNotificationType(BillNotificationType.ISSUE);

                    billNotificationEmailList.add(issueEmailBillNotification);
                }
            }
            LOGGER.info("EMAIL LIST: " + billNotificationEmailList.size());

            billNotificationEmailRepo.saveAll(billNotificationEmailList);
        }
    }

    public void sendBillNotificationReminder(Bill bill, EntityUser entityUser, Locale locale) {
        LocalDateTime issueDateInterval = LocalDate.now().atStartOfDay().plusHours(23).plusMinutes(59).plusSeconds(59);
        LOGGER.info("issueDateInterval: " + issueDateInterval + " bill: " + bill.getIssueDate());

        if (bill.getIssueDate().isAfter(issueDateInterval)) {
            return;
        }

        Entity entity = bill.getEntity();


        //Check if entity allow SMS send
        if (entity.getSendBillSmsBooleanFlag().equals(BooleanFlag.YES)) {

            Long totalSmsPoint = 0L;

            List<BillNotificationSms> billNotificationSmsList = new ArrayList<>();

            BooleanFlag smsIssueFlag = bill.getSmsIssueFlag();

            Language notificationLanguage = entity.getSendBillLanguage();

            BillNotificationTemplate billNotificationTemplate = billNotificationTemplateRepo.find(NotificationMedia.SMS, notificationLanguage, BillNotificationType.REMINDER);

            String entityNameAr = entity.getBrandNameAr();
            String entityNameEn = entity.getBrandNameEn();
            String totalAmount = Utils.roundDecimal(bill.getTotalAmount());
            String sadadNumber = bill.getSadadNumber();
            String billNumberEncoded = aes256.encrypt(sadadNumber + "_" + bill.getCycleNumber());
            String dateTime = Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd hh:mm:ss");

            String body = Utils.getSmsBody(billNotificationTemplate.getLayout(),
                    entityNameAr,
                    entityNameEn,
                    totalAmount,
                    sadadNumber,
                    billNumberEncoded,
                    dateTime);

            WebServiceResponse costSmsResponse = webServiceHelper.calculateSmsPoint(billNotificationTemplate.getTitle(), body, bill.getCustomerMobileNumber());
            LOGGER.info("costSmsResponse: " + costSmsResponse.getBody());

            Long smsPoint = Utils.getCostSms(costSmsResponse.getBody());
            LOGGER.info("smsPoint: " + smsPoint);

            WebServiceResponse sensSmsWebServiceResponse = webServiceHelper.sendSmsNotification(billNotificationTemplate.getTitle(), body, bill.getCustomerMobileNumber());
            LOGGER.info("sensSmsWebServiceResponse: " + sensSmsWebServiceResponse.getBody());

            String responseCode = Utils.getSmsResponseCode(sensSmsWebServiceResponse.getBody());
            LOGGER.info("smsResponseCode: " + responseCode);

            BillNotificationSms billNotificationSms = new BillNotificationSms();
            billNotificationSms.setBill(bill);
            billNotificationSms.setTitle(billNotificationTemplate.getTitle());
            billNotificationSms.setNotificationLanguage(notificationLanguage);
            billNotificationSms.setMobileNumber(bill.getCustomerMobileNumber());
            billNotificationSms.setUserReference(entityUser.getUsername());
            billNotificationSms.setCreateDate(LocalDateTime.now());
            billNotificationSms.setBody(body);
            billNotificationSms.setResponseCode(responseCode);
            billNotificationSms.setBillNotificationType(BillNotificationType.REMINDER);


            if (responseCode.equals("1") || responseCode.equals("M0000")) {
                totalSmsPoint += smsPoint;
                billNotificationSms.setPoint(smsPoint);
            } else {
                billNotificationSms.setPoint(0L);
            }

            billNotificationSmsList.add(billNotificationSms);

            List<BillMobileNumber> billMobileNumberList = billMobileNumberRepo.findAllBillMobileNumber(bill);
            if (billMobileNumberList != null && !billMobileNumberList.isEmpty()) {

                for (BillMobileNumber billMobileNumber : billMobileNumberList) {

                    WebServiceResponse costSmsResponseOther = webServiceHelper.calculateSmsPoint(billNotificationTemplate.getTitle(), body, billMobileNumber.getMobileNumber());
                    LOGGER.info("costSmsResponseOther: " + costSmsResponseOther.getBody());

                    Long smsPointOther = Utils.getCostSms(costSmsResponseOther.getBody());
                    LOGGER.info("smsPointOther: " + smsPointOther);

                    WebServiceResponse sensSmsWebServiceResponseOther = webServiceHelper.sendSmsNotification(billNotificationTemplate.getTitle(), body, billMobileNumber.getMobileNumber());
                    LOGGER.info("sensSmsWebServiceResponseOther: " + sensSmsWebServiceResponseOther.getBody());

                    String responseCodeOther = Utils.getSmsResponseCode(sensSmsWebServiceResponseOther.getBody());
                    LOGGER.info("smsResponseCode: " + responseCodeOther);

                    BillNotificationSms billNotificationSmsOther = new BillNotificationSms();
                    billNotificationSmsOther.setBill(bill);
                    billNotificationSmsOther.setTitle(billNotificationTemplate.getTitle());
                    billNotificationSmsOther.setNotificationLanguage(notificationLanguage);
                    billNotificationSmsOther.setMobileNumber(billMobileNumber.getMobileNumber());
                    billNotificationSmsOther.setUserReference(entityUser.getUsername());
                    billNotificationSmsOther.setCreateDate(LocalDateTime.now());
                    billNotificationSmsOther.setBody(body);
                    billNotificationSmsOther.setPoint(smsPointOther);
                    billNotificationSmsOther.setResponseCode(responseCodeOther);
                    billNotificationSmsOther.setBillNotificationType(BillNotificationType.REMINDER);

                    if (responseCodeOther.equals("1") || responseCodeOther.equals("M0000")) {
                        totalSmsPoint += smsPointOther;
                        billNotificationSmsOther.setPoint(smsPointOther);
                    } else {
                        billNotificationSmsOther.setPoint(0L);
                    }

                    billNotificationSmsList.add(billNotificationSmsOther);
                }
            }
            LOGGER.info("SMS LIST: " + billNotificationSmsList.size());

            billNotificationSmsRepo.saveAll(billNotificationSmsList);

            Entity updateEntity = entityRepo.findEntityForUpdate(entity.getId());
            Long point = updateEntity.getSmsPointBalance() - totalSmsPoint;
            entityRepo.updateSmsPointBalance(point, updateEntity.getId());
        }

        //Check if entity allow Email send
        if (entity.getSendBillEmailBooleanFlag().equals(BooleanFlag.YES)) {

            List<BillNotificationEmail> billNotificationEmailList = new ArrayList<>();


            if (bill.getCustomerEmailAddress() != null && !bill.getCustomerEmailAddress().isEmpty()) {
                Language notificationLanguage = entity.getSendBillLanguage();

                String fromEmail = "efaa.info@dci.sa";
                String subject = "New Invoice";
                String toEmail = bill.getCustomerEmailAddress();
                String type = "text/html";

                String layout = "تذكير اصدار فاتورة جديدة من ENTITY_NAME_AR بمبلغ BILL_AMOUNT ريال وبرقم سداد SADAD_NUMBER, يمكنك دفع الفاتورة من رقم المفوتر 902(مدفوعات ايفاء).للإطلاع على الفاتورة يرجى الضغط على الرابط ادناه:";

                String entityNameAr = entity.getBrandNameAr();
                String entityNameEn = entity.getBrandNameEn();
                String totalAmount = Utils.roundDecimal(bill.getTotalAmount());
                String sadadNumber = bill.getSadadNumber();
                String billNumberEncoded = aes256.encrypt(sadadNumber + "_" + bill.getCycleNumber());
                String dateTime = Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd hh:mm:ss");

                String body = Utils.getSmsBody(layout,
                        entityNameAr,
                        entityNameEn,
                        totalAmount,
                        sadadNumber,
                        billNumberEncoded,
                        dateTime);

                String smsViewUrl = environment.getProperty("sms_view_url") + billNumberEncoded;

                String html = "<!DOCTYPE html>\n" +
                        "\n" +
                        "<title></title>\n" +
                        "\n" +
                        "<body style=\"direction: rtl;\">\n" +
                        "\n" +
                        "    <div style=\"width: 100%; text-align: center;\">\n" +
                        "        <img alt=\"logo\" src=\"" + bill.getLogo() + "\">\n" +
                        "    </div>\n" +
                        "\n" +
                        "    <h1>عزيزي عميل شركة " + entityNameAr + "</h1>\n" +
                        "\n" +
                        "    <p>\n" +
                        "        نأمل أن تحوز خدماتنا المقدمة اليكم \"الفاتورة الالكترونية - الدفع الالكتروني\" على رضاكم واستحسانكم.. \n" +
                        "    </p>\n" +
                        "\n" +
                        "    <p>\n" +
                        " نود تذكيركم بصدور فاتورة جديده بمبلغ " + totalAmount + " ريال - ورقم سداد " + sadadNumber + "،\n" +
                        "        يمكنك دفع الفاتورة الخاصة بكم من خلال رقم المفوتر 902 \"مدفوعات ايفاء\"..\n" +
                        "    </p>\n" +
                        "\n" +
                        "    <p>\n" +
                        "        وللاطلاع على تفاصيل الفاتورة يرجى الضغط على الرابط ادناه:\n" +
                        "    </p>\n" +
                        "    <p>\n" +
                        "        <a href=\"" + smsViewUrl + "\">اضغط هنا</a>\n" +
                        "    </p>\n" +
                        "\n" +
                        "    <p>\n" +
                        "        وشكرا لكم..\n" +
                        "    </p>\n" +
                        "\n" +
                        "</body>\n" +
                        "\n" +
                        "</html>";

                WebServiceResponse sensEmailWebServiceResponse = webServiceHelper.sendEmailNotification(fromEmail, subject, toEmail, type, html);
                LOGGER.info("sensEmailWebServiceResponse: " + sensEmailWebServiceResponse.getBody());

                LOGGER.info("smsResponseCode: " + sensEmailWebServiceResponse.getStatus());

                BillNotificationEmail issueEmailBillNotification = new BillNotificationEmail();
                issueEmailBillNotification.setBill(bill);
                issueEmailBillNotification.setTitle(subject);
                issueEmailBillNotification.setNotificationLanguage(Language.ALL);
                issueEmailBillNotification.setEmail(toEmail);
                issueEmailBillNotification.setUserReference(entityUser.getUsername());
                issueEmailBillNotification.setBody(html);
                issueEmailBillNotification.setResponseCode(sensEmailWebServiceResponse.getStatus().toString());
                issueEmailBillNotification.setCreateDate(LocalDateTime.now());
                issueEmailBillNotification.setBillNotificationType(BillNotificationType.REMINDER);

                billNotificationEmailList.add(issueEmailBillNotification);
            }


            List<BillEmail> billEmailList = billEmailRepo.findAllBillEmail(bill);
            if (billEmailList != null && !billEmailList.isEmpty()) {

                for (BillEmail billEmail : billEmailList) {

                    Language notificationLanguage = entity.getSendBillLanguage();

                    String fromEmail = "efaa.info@dci.sa";
                    String subject = "New Invoice";
                    String toEmail = billEmail.getEmail();
                    String type = "text/html";

                    String layout = "تذكير اصدار فاتورة جديدة من ENTITY_NAME_AR بمبلغ BILL_AMOUNT ريال وبرقم سداد SADAD_NUMBER, يمكنك دفع الفاتورة من رقم المفوتر 902(مدفوعات ايفاء).للإطلاع على الفاتورة يرجى الضغط على الرابط ادناه:";

                    String entityNameAr = entity.getBrandNameAr();
                    String entityNameEn = entity.getBrandNameEn();
                    String totalAmount = Utils.roundDecimal(bill.getTotalAmount());
                    String sadadNumber = bill.getSadadNumber();
                    String billNumberEncoded = aes256.encrypt(sadadNumber + "_" + bill.getCycleNumber());
                    String dateTime = Utils.parseDateTime(LocalDateTime.now(), "yyyy-MM-dd hh:mm:ss");

                    String body = Utils.getSmsBody(layout,
                            entityNameAr,
                            entityNameEn,
                            totalAmount,
                            sadadNumber,
                            billNumberEncoded,
                            dateTime);

                    String smsViewUrl = environment.getProperty("sms_view_url") + billNumberEncoded;

                    String html = "<!DOCTYPE html>\n" +
                            "\n" +
                            "<title></title>\n" +
                            "\n" +
                            "<body style=\"direction: rtl;\">\n" +
                            "\n" +
                            "    <div style=\"width: 100%; text-align: center;\">\n" +
                            "        <img alt=\"logo\" src=\"" + bill.getLogo() + "\">\n" +
                            "    </div>\n" +
                            "\n" +
                            "    <h1>عزيزي عميل شركة " + entityNameAr + "</h1>\n" +
                            "\n" +
                            "    <p>\n" +
                            "        نأمل أن تحوز خدماتنا المقدمة اليكم \"الفاتورة الالكترونية - الدفع الالكتروني\" على رضاكم واستحسانكم.. \n" +
                            "    </p>\n" +
                            "\n" +
                            "    <p>\n" +
                            " نود تذكيركم بصدور فاتورة جديده بمبلغ " + totalAmount + " ريال - ورقم سداد " + sadadNumber + "،\n" +
                            "        يمكنك دفع الفاتورة الخاصة بكم من خلال رقم المفوتر 902 \"مدفوعات ايفاء\"..\n" +
                            "    </p>\n" +
                            "\n" +
                            "    <p>\n" +
                            "        وللاطلاع على تفاصيل الفاتورة يرجى الضغط على الرابط ادناه:\n" +
                            "    </p>\n" +
                            "    <p>\n" +
                            "        <a href=\"" + smsViewUrl + "\">اضغط هنا</a>\n" +
                            "    </p>\n" +
                            "\n" +
                            "    <p>\n" +
                            "        وشكرا لكم..\n" +
                            "    </p>\n" +
                            "\n" +
                            "</body>\n" +
                            "\n" +
                            "</html>";


                    WebServiceResponse sensEmailWebServiceResponse = webServiceHelper.sendEmailNotification(fromEmail, subject, toEmail, type, html);
                    LOGGER.info("sensEmailWebServiceResponse: " + sensEmailWebServiceResponse.getBody());

                    LOGGER.info("smsResponseCode: " + sensEmailWebServiceResponse.getStatus());

                    BillNotificationEmail issueEmailBillNotification = new BillNotificationEmail();
                    issueEmailBillNotification.setBill(bill);
                    issueEmailBillNotification.setTitle(subject);
                    issueEmailBillNotification.setNotificationLanguage(Language.ALL);
                    issueEmailBillNotification.setEmail(billEmail.getEmail());
                    issueEmailBillNotification.setUserReference(entityUser.getUsername());
                    issueEmailBillNotification.setBody(html);
                    issueEmailBillNotification.setResponseCode(sensEmailWebServiceResponse.getStatus().toString());
                    issueEmailBillNotification.setCreateDate(LocalDateTime.now());
                    issueEmailBillNotification.setBillNotificationType(BillNotificationType.REMINDER);

                    billNotificationEmailList.add(issueEmailBillNotification);
                }
            }

            billNotificationEmailRepo.saveAll(billNotificationEmailList);
        }
    }

    public void sendFcmNotificationBillIssue(Bill bill, EntityUser issueEntityUser, Locale locale) {

        if (bill.getEntity().getIsEntityNotificationAllowed() == null || bill.getEntity().getIsEntityNotificationAllowed().equals(BooleanFlag.NO)) {
            return;
        }

        List<EntityUser> entityUserList = entityUserRepo.findEntityUserByEntity(bill.getEntity());

        List<FcmNotification> fcmNotificationList = new ArrayList<>();

        for (EntityUser entityUser : entityUserList) {

            if (!Utils.isSendNotificationForRole(issueEntityUser, entityUser)) {
                continue;
            }

            String title = "Issue Invoice";
            String body = "Dear " + entityUser.getUsername() + "\n" +
                    "A new invoice has been issued by " + issueEntityUser.getUsername() + " for the amount of " + Utils.roundDecimal(bill.getTotalAmount()) + " SR, please confirm the invoice info in order to upload it to SADAD System.";


            List<String> tokenList = new ArrayList<>();
            tokenList.add(entityUser.getFcmToken());

            JSONObject dataJsonObject = new JSONObject();
            dataJsonObject.put("type", "BILL_ISSUE");
            dataJsonObject.put("referenceId", bill.getId());

            WebServiceResponse fcmWebServiceResponse = webServiceHelper.sendFcmNotification(title, body, dataJsonObject.toString(), tokenList);

            LOGGER.info("fcmWebServiceStatus: " + fcmWebServiceResponse.getStatus());
            LOGGER.info("fcmWebServiceResponse: " + fcmWebServiceResponse.getBody());

            if (entityUser.getEntity().getIsEntityNotificationAllowed().equals(BooleanFlag.NO)) {
                continue;
            }

            FcmNotification fcmNotification = new FcmNotification();
            fcmNotification.setTitle(title);
            fcmNotification.setBody(body);
            fcmNotification.setCreateDate(LocalDateTime.now());
            fcmNotification.setIsReadFlag(BooleanFlag.NO);
            fcmNotification.setNotificationLanguage(Language.ALL);
            fcmNotification.setUserId(entityUser.getId());
            fcmNotification.setUserType(1);
            fcmNotification.setData(dataJsonObject.toString());
            fcmNotification.setStatus(Status.ACTIVE);
            fcmNotification.setCategory("ISSUE");


            if (fcmWebServiceResponse.getStatus().equals(200)) {
                fcmNotification.setNotificationStatus(NotificationStatus.SUCCESS);
            } else {
                fcmNotification.setNotificationStatus(NotificationStatus.FAILED);
            }

            fcmNotificationList.add(fcmNotification);
        }

        fcmNotificationRepo.saveAll(fcmNotificationList);
    }

    public void sendFcmNotificationBillPayment(LocalPayment localPayment, EntityUser issueEntityUser, Locale locale) {

        Bill bill = localPayment.getBill();

        if (bill.getEntity().getIsEntityNotificationAllowed() == null || bill.getEntity().getIsEntityNotificationAllowed().equals(BooleanFlag.NO)) {
            return;
        }


        List<EntityUser> entityUserList = entityUserRepo.findEntityUserByEntity(bill.getEntity());

        List<FcmNotification> fcmNotificationList = new ArrayList<>();

        for (EntityUser entityUser : entityUserList) {

            if (!Utils.isSendNotificationForRole(issueEntityUser, entityUser)) {
                continue;
            }

            String title = "Pay Invoice";
            String body = "Dear " + entityUser.getUsername() + "\n" +
                    "A new offline payment has been received for the amount of " + Utils.roundDecimal(localPayment.getPaymentAmount()) + " SR by " + localPayment.getEntityUser().getUsername() + ", and SADAD number " + bill.getSadadNumber() + ".";


            List<String> tokenList = new ArrayList<>();
            tokenList.add(entityUser.getFcmToken());

            JSONObject dataJsonObject = new JSONObject();
            dataJsonObject.put("type", "BILL_PAY");
            dataJsonObject.put("referenceId", bill.getId());

            WebServiceResponse fcmWebServiceResponse = webServiceHelper.sendFcmNotification(title, body, dataJsonObject.toString(), tokenList);

            LOGGER.info("fcmWebServiceStatus: " + fcmWebServiceResponse.getStatus());
            LOGGER.info("fcmWebServiceResponse: " + fcmWebServiceResponse.getBody());

            if (entityUser.getEntity().getIsEntityNotificationAllowed().equals(BooleanFlag.NO)) {
                continue;
            }

            FcmNotification fcmNotification = new FcmNotification();
            fcmNotification.setTitle(title);
            fcmNotification.setBody(body);
            fcmNotification.setCreateDate(LocalDateTime.now());
            fcmNotification.setIsReadFlag(BooleanFlag.NO);
            fcmNotification.setNotificationLanguage(Language.ALL);
            fcmNotification.setUserId(entityUser.getId());
            fcmNotification.setUserType(1);
            fcmNotification.setData(dataJsonObject.toString());
            fcmNotification.setStatus(Status.ACTIVE);
            fcmNotification.setCategory("PAYMENT");


            if (fcmWebServiceResponse.getStatus().equals(200)) {
                fcmNotification.setNotificationStatus(NotificationStatus.SUCCESS);
            } else {
                fcmNotification.setNotificationStatus(NotificationStatus.FAILED);
            }

            fcmNotificationList.add(fcmNotification);
        }

        fcmNotificationRepo.saveAll(fcmNotificationList);
    }

    public void sendFcmNotificationBillCancel(Bill bill, EntityUser issueEntityUser, Locale locale) {


        if (bill.getEntity().getIsEntityNotificationAllowed() == null || bill.getEntity().getIsEntityNotificationAllowed().equals(BooleanFlag.NO)) {
            return;
        }


        List<EntityUser> entityUserList = entityUserRepo.findEntityUserByEntity(bill.getEntity());

        List<FcmNotification> fcmNotificationList = new ArrayList<>();

        for (EntityUser entityUser : entityUserList) {

            if (!Utils.isSendNotificationForRole(issueEntityUser, entityUser)) {
                continue;
            }


            String title = "Cancel Invoice";
            String body = "Dear " + entityUser.getEntity().getBrandNameEn() + "\n" +
                    "The invoice has been Cancelled by the " + issueEntityUser.getUsername() + ", for the amount of " + bill.getTotalAmount() + " SR, and SADAD number " + bill.getSadadNumber() + ".";


            List<String> tokenList = new ArrayList<>();
            tokenList.add(entityUser.getFcmToken());

            JSONObject dataJsonObject = new JSONObject();
            dataJsonObject.put("type", "BILL_CANCEL");
            dataJsonObject.put("referenceId", bill.getId());

            WebServiceResponse fcmWebServiceResponse = webServiceHelper.sendFcmNotification(title, body, dataJsonObject.toString(), tokenList);

            LOGGER.info("fcmWebServiceStatus: " + fcmWebServiceResponse.getStatus());
            LOGGER.info("fcmWebServiceResponse: " + fcmWebServiceResponse.getBody());

            if (entityUser.getEntity().getIsEntityNotificationAllowed().equals(BooleanFlag.NO)) {
                continue;
            }

            FcmNotification fcmNotification = new FcmNotification();
            fcmNotification.setTitle(title);
            fcmNotification.setBody(body);
            fcmNotification.setCreateDate(LocalDateTime.now());
            fcmNotification.setIsReadFlag(BooleanFlag.NO);
            fcmNotification.setNotificationLanguage(Language.ALL);
            fcmNotification.setUserId(entityUser.getId());
            fcmNotification.setUserType(1);
            fcmNotification.setData(dataJsonObject.toString());
            fcmNotification.setStatus(Status.ACTIVE);
            fcmNotification.setCategory("ISSUE");

            if (fcmWebServiceResponse.getStatus().equals(200)) {
                fcmNotification.setNotificationStatus(NotificationStatus.SUCCESS);
            } else {
                fcmNotification.setNotificationStatus(NotificationStatus.FAILED);
            }

            fcmNotificationList.add(fcmNotification);
        }

        fcmNotificationRepo.saveAll(fcmNotificationList);
    }

}
