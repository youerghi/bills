package be.bslash.ibps.bills.services;


import be.bslash.ibps.bills.configrations.Utf8ResourceBundle;
import be.bslash.ibps.bills.dto.response.EntityVatDtoRs;
import be.bslash.ibps.bills.entities.Entity;
import be.bslash.ibps.bills.entities.EntityVat;
import be.bslash.ibps.bills.enums.BooleanFlag;
import be.bslash.ibps.bills.repositories.EntityVatRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
@Transactional
public class EntityVatService {

    @Autowired
    private EntityVatRepo entityVatRepo;

    public List<EntityVatDtoRs> findEntityVatByEntity(Entity entity, Locale locale) {

        List<EntityVatDtoRs> entityVatDtoRsList = new ArrayList<>();

        List<EntityVat> entityVatList = entityVatRepo.findEntityVatByEntity(entity);

        entityVatList.forEach(entityVat -> {
            EntityVatDtoRs entityVatDtoRs = new EntityVatDtoRs();
            entityVatDtoRs.setId(entityVat.getId());
            entityVatDtoRs.setVat(entityVat.getVat().toString());
            entityVatDtoRs.setVatDisplay(entityVat.getVatDisplay());

            entityVatDtoRsList.add(entityVatDtoRs);
        });

        if (entity.getVatExemptedFlag().equals(BooleanFlag.YES)) {
            EntityVatDtoRs entityVatDtoRs = new EntityVatDtoRs();
            entityVatDtoRs.setVatDisplay(Utf8ResourceBundle.getString("VAT_EXE", locale));
            entityVatDtoRs.setVat("EXE");

            entityVatDtoRsList.add(entityVatDtoRs);
        }

        if (entity.getVatNaFlag().equals(BooleanFlag.YES)) {
            EntityVatDtoRs entityVatDtoRs = new EntityVatDtoRs();
            entityVatDtoRs.setVatDisplay(Utf8ResourceBundle.getString("VAT_NA", locale));
            entityVatDtoRs.setVat("NA");

            entityVatDtoRsList.add(entityVatDtoRs);
        }


        return entityVatDtoRsList;
    }
}
