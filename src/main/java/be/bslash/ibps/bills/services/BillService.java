package be.bslash.ibps.bills.services;


import be.bslash.ibps.bills.configrations.Utf8ResourceBundle;
import be.bslash.ibps.bills.dto.request.BillCustomFieldRqDto;
import be.bslash.ibps.bills.dto.request.BillItemDetailedRqDto;
import be.bslash.ibps.bills.dto.request.DetailedBillMakeRqDto;
import be.bslash.ibps.bills.dto.request.DetailedBillUpdateRqDto;
import be.bslash.ibps.bills.dto.request.MasterBillMakeRqDto;
import be.bslash.ibps.bills.dto.request.MasterBillUpdateRqDto;
import be.bslash.ibps.bills.dto.request.RecurringBillMakeRqDto;
import be.bslash.ibps.bills.dto.request.SearchBillRqDto;
import be.bslash.ibps.bills.dto.request.SettingStyleDtoRq;
import be.bslash.ibps.bills.dto.response.BillCustomFieldRsDto;
import be.bslash.ibps.bills.dto.response.BillInfoDtoRs;
import be.bslash.ibps.bills.dto.response.BillItemDetailedRsDto;
import be.bslash.ibps.bills.dto.response.BillNoteDtoRs;
import be.bslash.ibps.bills.dto.response.BillReportEntityDtoRs;
import be.bslash.ibps.bills.dto.response.BillResultTimelineDtoRs;
import be.bslash.ibps.bills.dto.response.BillTimelineDtoRs;
import be.bslash.ibps.bills.dto.response.EntityBillInfoDtoRs;
import be.bslash.ibps.bills.dto.response.PageDtoRs;
import be.bslash.ibps.bills.dto.response.PaymentDtoRs;
import be.bslash.ibps.bills.dto.response.SearchBillRsDto;
import be.bslash.ibps.bills.entities.Account;
import be.bslash.ibps.bills.entities.AccountCustomField;
import be.bslash.ibps.bills.entities.Bill;
import be.bslash.ibps.bills.entities.BillCustomField;
import be.bslash.ibps.bills.entities.BillEmail;
import be.bslash.ibps.bills.entities.BillItem;
import be.bslash.ibps.bills.entities.BillMobileNumber;
import be.bslash.ibps.bills.entities.BillNote;
import be.bslash.ibps.bills.entities.BillNoteItem;
import be.bslash.ibps.bills.entities.BillTimeline;
import be.bslash.ibps.bills.entities.BillTransaction;
import be.bslash.ibps.bills.entities.BulkBill;
import be.bslash.ibps.bills.entities.Config;
import be.bslash.ibps.bills.entities.ConfigTemplate;
import be.bslash.ibps.bills.entities.CorporateAccount;
import be.bslash.ibps.bills.entities.Customer;
import be.bslash.ibps.bills.entities.CustomerCustomField;
import be.bslash.ibps.bills.entities.Entity;
import be.bslash.ibps.bills.entities.EntityActivity;
import be.bslash.ibps.bills.entities.EntityContactDetail;
import be.bslash.ibps.bills.entities.EntityField;
import be.bslash.ibps.bills.entities.EntityUser;
import be.bslash.ibps.bills.entities.EntityVat;
import be.bslash.ibps.bills.entities.IndividualAccount;
import be.bslash.ibps.bills.entities.LocalPayment;
import be.bslash.ibps.bills.entities.RunningTaxBillItem;
import be.bslash.ibps.bills.entities.SadadPayment;
import be.bslash.ibps.bills.enums.AccountCategory;
import be.bslash.ibps.bills.enums.BillCategory;
import be.bslash.ibps.bills.enums.BillSource;
import be.bslash.ibps.bills.enums.BillStatus;
import be.bslash.ibps.bills.enums.BillType;
import be.bslash.ibps.bills.enums.BooleanFlag;
import be.bslash.ibps.bills.enums.EntityUserType;
import be.bslash.ibps.bills.enums.IdType;
import be.bslash.ibps.bills.enums.NoteType;
import be.bslash.ibps.bills.enums.PaymentRule;
import be.bslash.ibps.bills.enums.Status;
import be.bslash.ibps.bills.exceptions.HttpServiceException;
import be.bslash.ibps.bills.ftp.FtpHandler;
import be.bslash.ibps.bills.repositories.AccountCustomFieldRepo;
import be.bslash.ibps.bills.repositories.AccountRepo;
import be.bslash.ibps.bills.repositories.BillCustomFieldRepo;
import be.bslash.ibps.bills.repositories.BillEmailRepo;
import be.bslash.ibps.bills.repositories.BillItemRepo;
import be.bslash.ibps.bills.repositories.BillMobileNumberRepo;
import be.bslash.ibps.bills.repositories.BillNoteItemRepo;
import be.bslash.ibps.bills.repositories.BillNoteRepo;
import be.bslash.ibps.bills.repositories.BillRepo;
import be.bslash.ibps.bills.repositories.BillTimelineRepo;
import be.bslash.ibps.bills.repositories.BillTransactionRepo;
import be.bslash.ibps.bills.repositories.BulkBillRepo;
import be.bslash.ibps.bills.repositories.CityRepo;
import be.bslash.ibps.bills.repositories.ConfigRepo;
import be.bslash.ibps.bills.repositories.ConfigTemplateRepo;
import be.bslash.ibps.bills.repositories.CustomerCustomFieldRepo;
import be.bslash.ibps.bills.repositories.CustomerRepo;
import be.bslash.ibps.bills.repositories.DistrictRepo;
import be.bslash.ibps.bills.repositories.EntityActivityRepo;
import be.bslash.ibps.bills.repositories.EntityContactDetailRepo;
import be.bslash.ibps.bills.repositories.EntityFieldRepo;
import be.bslash.ibps.bills.repositories.EntityRepo;
import be.bslash.ibps.bills.repositories.EntityVatRepo;
import be.bslash.ibps.bills.repositories.LocalPaymentRepo;
import be.bslash.ibps.bills.repositories.RunningTaxBillItemRepo;
import be.bslash.ibps.bills.repositories.SadadPaymentRepo;
import be.bslash.ibps.bills.utils.AES256;
import be.bslash.ibps.bills.utils.AdverstisementUtil;
import be.bslash.ibps.bills.utils.NumberToWord;
import be.bslash.ibps.bills.utils.Utils;
import be.bslash.ibps.bills.ws.WebServiceHelper;
import be.bslash.ibps.bills.ws.WebServiceResponse;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

@Service
@Transactional
public class BillService {

    private static final Logger LOGGER = LogManager.getLogger("BACKEND_LOGS");

    @Autowired
    private BillRepo billRepo;

    @Autowired
    private AccountRepo accountRepo;

    @Autowired
    private ConfigRepo configRepo;

    @Autowired
    private BillTimelineRepo billTimelineRepo;

    @Autowired
    private BillItemRepo billItemRepo;

    @Autowired
    private WebServiceHelper webServiceHelper;

    @Autowired
    private EntityActivityRepo entityActivityRepo;

    @Autowired
    private EntityFieldRepo entityFieldRepo;

    @Autowired
    private BillCustomFieldRepo billCustomFieldRepo;

    @Autowired
    private FtpHandler ftpHandler;

    @Autowired
    private Environment environment;

    @Autowired
    private CustomerRepo customerRepo;

    @Autowired
    private EntityRepo entityRepo;

    @Autowired
    private BulkBillRepo bulkBillRepo;

    @Autowired
    private AdverstisementUtil adverstisementUtil;

    @Autowired
    private ConfigTemplateRepo configTemplateRepo;

    @Autowired
    private SadadPaymentRepo sadadPaymentRepo;

    @Autowired
    private LocalPaymentRepo localPaymentRepo;

    @Autowired
    private BillEmailRepo billEmailRepo;

    @Autowired
    private BillMobileNumberRepo billMobileNumberRepo;

    @Autowired
    private BillNoteRepo billNoteRepo;

    @Autowired
    private CustomerCustomFieldRepo customerCustomFieldRepo;

    @Autowired
    private BillNoteItemRepo billNoteItemRepo;

    @Autowired
    private RunningTaxBillItemRepo runningTaxBillItemRepo;

    @Autowired
    private AES256 aes256;

    @Autowired
    private EntityContactDetailRepo entityContactDetailRepo;

    @Autowired
    private CityRepo cityRepo;

    @Autowired
    private DistrictRepo districtRepo;

    @Autowired
    private EntityVatRepo entityVatRepo;

    @Autowired
    private AccountCustomFieldRepo accountCustomFieldRepo;

    @Autowired
    private BillTransactionRepo billTransactionRepo;

    public List<Bill> findAll(Locale locale) {
        return billRepo.findAll();
    }

    public Bill findBillBySadadNumber(String sadadNumber, Locale locale) {
        try {
            return billRepo.findBillBySadadNumber(sadadNumber);
        } catch (Exception ex) {
            List<Bill> billList = billRepo.findBySadadNumber(sadadNumber);
            return billList.get(0);
        }
    }

    public Bill findBillById(Long id, Locale locale) {
        return billRepo.findBillById(id);
    }

    public Bill saveMasterBill(MasterBillMakeRqDto masterBillMakeRqDto, EntityUser entityUser, BillSource billSource, Locale locale) {

        if (!entityUser.getEntityUserType().equals(EntityUserType.OFFICER) && entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "access_officer_user", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        if (masterBillMakeRqDto.getBillNumber() != null && !masterBillMakeRqDto.getBillNumber().isEmpty()) {
            Bill currentBill = billRepo.findBill(masterBillMakeRqDto.getBillNumber(), entityUser.getEntity());

            if (currentBill != null) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_already_exist", locale);
            }
        }

        String discountType = masterBillMakeRqDto.getDiscountType();
        if (discountType == null) {
            discountType = "PERC";
        }

        BigDecimal discount = masterBillMakeRqDto.getDiscount();
        if (discount == null) {
            discount = BigDecimal.ZERO;
        }

        BigDecimal subAmount = masterBillMakeRqDto.getSubAmount();

        BigDecimal discountAmount = BigDecimal.ZERO;
        switch (discountType) {
            case "PERC":
                discountAmount = masterBillMakeRqDto.getSubAmount().multiply(discount.divide(new BigDecimal(100)));
                break;
            case "FIXED":
                discountAmount = new BigDecimal(discount.toString());
                break;
            default:
                discountAmount = masterBillMakeRqDto.getSubAmount().multiply(discount);
                break;
        }

        BigDecimal totalAmount = Utils.calculateTotalAmountFromVatAndPreviousBalance(subAmount.subtract(discountAmount), masterBillMakeRqDto.getVat(), masterBillMakeRqDto.getCustomerPreviousBalance());

        BigDecimal miniPartialAmount = masterBillMakeRqDto.getMiniPartialAmount();
        if (miniPartialAmount == null) {
            miniPartialAmount = BigDecimal.ZERO;
        }

        if (totalAmount.doubleValue() < miniPartialAmount.doubleValue()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "Total amount should not less than minimum partial amount", locale);
        }

        EntityActivity entityActivity = entityActivityRepo.findEntityActivityById((masterBillMakeRqDto.getEntityActivityId()));

        if (entityActivity == null) {
            entityActivity = entityActivityRepo.findEntityActivity(masterBillMakeRqDto.getEntityActivityCode(), entityUser.getEntity());
        }

        if (masterBillMakeRqDto.getVat() != null) {
            List<EntityVat> entityVatList = entityVatRepo.findEntityVatByEntityAndVat(entityUser.getEntity(), Utils.convertToBigDecimal(masterBillMakeRqDto.getVat()));

            if (entityVatList == null || entityVatList.isEmpty()) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "VAT value is not match allowed VAT", locale);
            }
        }

        if (entityActivity != null && entityActivity.getStatus().equals(Status.ACTIVE)) {

            LOGGER.info("************************************");
            LOGGER.info("totalAmount: " + totalAmount);

            if (entityActivity.getTheLowestAmountPerUploadedBill() != null) {
                if (totalAmount.compareTo(entityActivity.getTheLowestAmountPerUploadedBill()) == -1) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_amount_min", locale);
                }
            }

            if (entityActivity.getTheHighestAmountPerUploadedBill() != null) {
                //checking bill limit per bill
                if (totalAmount.compareTo(entityActivity.getTheHighestAmountPerUploadedBill()) == 1) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_amount_limit_per_bill", locale);
                }
            }

            if (entityActivity.getMonthlyHighestValueOfExpectedUploadedBills() != null) {
                //checking bill limit per month
                LocalDateTime startDayOfMonth = Utils.getStartDayOfCurrentMonth();
                LocalDateTime currentDay = Utils.getCurrentDateTime();

                LOGGER.info("startDayOfMonth: " + startDayOfMonth);
                LOGGER.info("currentDay: " + currentDay);

                BigDecimal totalAmountForCurrentMonth = billRepo.volumeByApprovedDate(startDayOfMonth, currentDay, entityActivity, entityUser.getEntity());
                if (totalAmountForCurrentMonth == null) {
                    totalAmountForCurrentMonth = new BigDecimal("0");
                }

                LOGGER.info("************************************");
                LOGGER.info("totalAmountForCurrentMonth: " + totalAmountForCurrentMonth);
                LOGGER.info("BillMaxAmountPerBill: " + entityActivity.getMonthlyHighestValueOfExpectedUploadedBills());

                if ((totalAmount.add(totalAmountForCurrentMonth)).compareTo(entityActivity.getMonthlyHighestValueOfExpectedUploadedBills()) == 1) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_amount_limit_per_month", locale);
                }
            }
        } else {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_entity_activity_id", locale);
        }


        Bill masterBill = (Bill) masterBillMakeRqDto.toEntity(locale);

        masterBill.setTotalAmount(totalAmount);
        masterBill.setBillType(BillType.MASTER_BILL);
        masterBill.setBillSource(billSource);
        masterBill.setBillStatus(BillStatus.MAKED);
        masterBill.setEntity(entityUser.getEntity());
        masterBill.setEntityUser(entityUser);
        masterBill.setCreateDate(LocalDateTime.now());
        masterBill.setDiscount(discount);
        masterBill.setDiscountType(discountType);
        masterBill.setEntityActivity(entityActivity);
        masterBill.setCustomerIdNumber(masterBillMakeRqDto.getCustomerIdNumber());
        masterBill.setCustomerIdType(IdType.getValue(masterBillMakeRqDto.getCustomerIdType()));
        masterBill.setCampaignId(masterBillMakeRqDto.getCampaignId());

        masterBill.setLogo(entityUser.getEntity().getLogo());
        masterBill.setStamp(entityUser.getEntity().getStamp());

        masterBill.setIsPartialAllowed(BooleanFlag.getValue(masterBillMakeRqDto.getIsPartialAllowed()));
        if (masterBill.getIsPartialAllowed() != null && masterBill.getIsPartialAllowed().equals(BooleanFlag.YES)) {
            masterBill.setBillCategory(BillCategory.ONE_OFF_PARTIAL);
        } else {
            masterBill.setBillCategory(BillCategory.INSTANTLY);
            masterBill.setIsPartialAllowed(BooleanFlag.NO);
        }
        masterBill.setMiniPartialAmount(masterBillMakeRqDto.getMiniPartialAmount());
        masterBill.setMaxPartialAmount(masterBillMakeRqDto.getMaxPartialAmount());
        masterBill.setPaymentNumber(0);
        masterBill.setSettlementNumber(0);

        if (masterBill.getBillNumber() == null || masterBill.getBillNumber().trim().isEmpty()) {
            if (entityUser.getEntity().getIsAutoGenerateBillNumber().equals(BooleanFlag.YES)) {
                String prefixBillNumber = Utils.generateBillNumber(entityUser.getEntity());
                Long sequence = (entityUser.getEntity().getAutoGenerationBillNumberSequence() + 1);
                entityRepo.increaseBillNumberCounter(sequence, entityUser.getEntity().getId());
                masterBill.setBillNumber(prefixBillNumber + sequence);
            } else {
                masterBill.setBillNumber(null);
            }
        } else {
            if (entityUser.getEntity().getIsAutoGenerateBillNumber().equals(BooleanFlag.YES)) {
                Long sequence = (entityUser.getEntity().getAutoGenerationBillNumberSequence() + 1);
                entityRepo.increaseBillNumberCounter(sequence, entityUser.getEntity().getId());
            }
        }

        masterBill = billRepo.save(masterBill);

        Integer expectedRequiredCounter = entityFieldRepo.countRequiredEntityFieldByEntity(entityUser.getEntity());

        Integer currentRequiredCounter = 0;

        if (masterBillMakeRqDto.getCustomFieldList() != null && !masterBillMakeRqDto.getCustomFieldList().isEmpty()) {

            for (BillCustomFieldRqDto billCustomFieldRqDto : masterBillMakeRqDto.getCustomFieldList()) {

                EntityField entityField = entityFieldRepo.findEntityFieldById(billCustomFieldRqDto.getFieldId());

                if (entityField == null) {
                    continue;
                }

                if (entityField.getStatus().equals(Status.DELETED)) {
                    continue;
                }

                if (entityField.getStatus().equals(Status.INACTIVE)) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "custom field inactive", locale);
                }

                if (!entityField.getEntity().getId().equals(entityUser.getEntity().getId())) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "custom field not related for current biller", locale);
                }

                if (entityField.getIsRequired().equals(BooleanFlag.YES)) {
                    currentRequiredCounter++;
                }

                if (entityField.getIsRequired().equals(BooleanFlag.YES) && Utils.isNullOrEmpty(billCustomFieldRqDto.getFieldValue())) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "custom field required value is missing", locale);
                }


                BillCustomField billCustomField = new BillCustomField();
                billCustomField.setBill(masterBill);
                billCustomField.setEntityField(entityField);
                billCustomField.setValue(billCustomFieldRqDto.getFieldValue());

                billCustomFieldRepo.save(billCustomField);
            }
        }

        LOGGER.info("expectedRequiredCounter " + expectedRequiredCounter);
        LOGGER.info("currentRequiredCounter " + currentRequiredCounter);

        if (expectedRequiredCounter != null) {
            if (!currentRequiredCounter.equals(expectedRequiredCounter)) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "Missing required custom field", locale);
            }
        }

        BillTimeline makedBillTimeline = new BillTimeline();
        makedBillTimeline.setBillStatus(BillStatus.MAKED);
        makedBillTimeline.setBill(masterBill);
        makedBillTimeline.setActionDate(LocalDateTime.now());
        makedBillTimeline.setEntityUser(entityUser);
        makedBillTimeline.setUserReference(entityUser.getUsername());

        makedBillTimeline = billTimelineRepo.save(makedBillTimeline);

        if (masterBillMakeRqDto.getCustomerEmailList() != null && !masterBillMakeRqDto.getCustomerEmailList().isEmpty()) {

            if (masterBillMakeRqDto.getCustomerEmailList().size() > 5) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "email list should not more than 5 times", locale);
            }

            Set<String> set = new HashSet<>(masterBillMakeRqDto.getCustomerEmailList());
            masterBillMakeRqDto.getCustomerEmailList().clear();
            masterBillMakeRqDto.getCustomerEmailList().addAll(set);

            List<BillEmail> billEmailList = new ArrayList<>();

            for (String email : masterBillMakeRqDto.getCustomerEmailList()) {

                if (email == null) {
                    continue;
                }

                if (email.equals(masterBill.getCustomerEmailAddress())) {
                    continue;
                }

                BillEmail billEmail = new BillEmail();
                billEmail.setBill(masterBill);
                billEmail.setEmail(email);
                billEmailList.add(billEmail);
            }

            billEmailRepo.saveAll(billEmailList);
        }

        if (masterBillMakeRqDto.getCustomerMobileList() != null && !masterBillMakeRqDto.getCustomerMobileList().isEmpty()) {

            if (masterBillMakeRqDto.getCustomerMobileList().size() > 5) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "mobile list should not more than 5 times", locale);
            }

            Set<String> set = new HashSet<>(masterBillMakeRqDto.getCustomerMobileList());
            masterBillMakeRqDto.getCustomerMobileList().clear();
            masterBillMakeRqDto.getCustomerMobileList().addAll(set);

            List<BillMobileNumber> billMobileNumberList = new ArrayList<>();

            for (String mobile : masterBillMakeRqDto.getCustomerMobileList()) {

                if (mobile == null) {
                    continue;
                }

                String formattedMobile = Utils.formatMobileNumber(mobile);

                if (formattedMobile.equals(masterBill.getCustomerMobileNumber())) {
                    continue;
                }

                BillMobileNumber billMobileNumber = new BillMobileNumber();
                billMobileNumber.setBill(masterBill);
                billMobileNumber.setMobileNumber(formattedMobile);
                billMobileNumberList.add(billMobileNumber);
            }

            billMobileNumberRepo.saveAll(billMobileNumberList);
        }

        //------------------------- save contact for customer --------------------------------
        if ((masterBillMakeRqDto.getSaveCustomer() != null) && (masterBillMakeRqDto.getSaveCustomer().equals("YES"))) {

            IndividualAccount customer = new IndividualAccount();
            customer.setFirstName(masterBill.getCustomerFullName());
            customer.setSecondName("-");
            customer.setThirdName("-");
            customer.setLastName("-");
            customer.setEmail(masterBill.getCustomerEmailAddress());
            customer.setPhoneNumber(masterBill.getCustomerMobileNumber());
            customer.setIdType(masterBill.getCustomerIdType());
            customer.setVatNumber(masterBill.getCustomerTaxNumber() != null ? masterBill.getCustomerTaxNumber() : "");
            customer.setEntityUser(entityUser);
            customer.setStatus(Status.ACTIVE);
            customer.setIsDraft(BooleanFlag.YES);
            customer.setAccountCategory(AccountCategory.NORMAL);
            customer.setEntity(entityUser.getEntity());
            customer = accountRepo.save(customer);

            if (masterBillMakeRqDto.getCustomFieldList() != null && !masterBillMakeRqDto.getCustomFieldList().isEmpty()) {
                for (BillCustomFieldRqDto billCustomFieldRqDto : masterBillMakeRqDto.getCustomFieldList()) {

                    EntityField entityField = entityFieldRepo.findEntityFieldById(billCustomFieldRqDto.getFieldId());

                    AccountCustomField customerCustomField = new AccountCustomField();
                    customerCustomField.setEntityAccount(customer);
                    customerCustomField.setEntityField(entityField);
                    customerCustomField.setValue(billCustomFieldRqDto.getFieldValue());

                    accountCustomFieldRepo.save(customerCustomField);
                }
            }
        }
        //-------------------------

        if (!Utils.isMinimumPartialAmountAllowed(masterBill)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "minimum partial amount should not less than " + masterBill.getEntityActivity().getTheLowestAmountPerUploadedBill().doubleValue(), locale);
        }

        if (entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN_PLUS)
                || entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN)
                || entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR_PLUS)) {
            this.checkBill(masterBill.getId(), entityUser, locale);
        }

        return masterBill;
    }

    public Bill updateMasterBill(MasterBillUpdateRqDto masterBillUpdateRqDto, EntityUser entityUser, Locale locale) {

        if (entityUser.getEntityUserType().equals(EntityUserType.OFFICER)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "update_is_not_allowed_for_officer", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }


        Bill currentBill = billRepo.findBillById(masterBillUpdateRqDto.getBillId());

        if (currentBill == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
        }

        if (currentBill.getBillStatus().equals(BillStatus.DELETED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_already_deleted", locale);
        }

        if (isBillStatusExist(currentBill, BillStatus.CANCELED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "cannot_update_bill_already_canceled", locale);
        }

        if (isBillStatusExist(currentBill, BillStatus.PAID_BY_SADAD) || isBillStatusExist(currentBill, BillStatus.PAID_BY_COMPANY)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "cannot_update_bill_already_paid", locale);
        }

//        if (isBillStatusExist(currentBill, BillStatus.PARTIALLY_PAID_BY_COMPANY) || isBillStatusExist(currentBill, BillStatus.PARTIALLY_PAID_BY_SADAD)) {
//            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "cannot_update_bill_already_partially_paid", locale);
//        }
//

        String discountType = masterBillUpdateRqDto.getDiscountType();
        if (discountType == null) {
            discountType = "PERC";
        }

        BigDecimal discount = masterBillUpdateRqDto.getDiscount();
        if (discount == null) {
            discount = BigDecimal.ZERO;
        }

        BigDecimal subAmount = masterBillUpdateRqDto.getSubAmount();

        BigDecimal discountAmount = BigDecimal.ZERO;
        switch (discountType) {
            case "PERC":
                discountAmount = masterBillUpdateRqDto.getSubAmount().multiply(discount.divide(new BigDecimal(100)));
                break;
            case "FIXED":
                discountAmount = new BigDecimal(discount.toString());
                break;
            default:
                discountAmount = masterBillUpdateRqDto.getSubAmount().multiply(discount);
                break;
        }

        BigDecimal totalAmount = Utils.calculateTotalAmountFromVatAndPreviousBalance(subAmount.subtract(discountAmount), masterBillUpdateRqDto.getVat(), masterBillUpdateRqDto.getCustomerPreviousBalance());

        BigDecimal miniPartialAmount = masterBillUpdateRqDto.getMiniPartialAmount();

        BigDecimal difference = totalAmount.subtract(currentBill.getTotalAmount());

        if (miniPartialAmount == null) {
            miniPartialAmount = BigDecimal.ZERO;
        }

        if (totalAmount.doubleValue() < miniPartialAmount.doubleValue()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "Total amount should not less than minimum partial amount", locale);
        }

        if (masterBillUpdateRqDto.getVat() != null) {
            List<EntityVat> entityVatList = entityVatRepo.findEntityVatByEntityAndVat(entityUser.getEntity(), Utils.convertToBigDecimal(masterBillUpdateRqDto.getVat()));

            if (entityVatList == null || entityVatList.isEmpty()) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "VAT value is not match allowed VAT", locale);
            }
        }

        EntityActivity entityActivity = entityActivityRepo.findEntityActivityById((masterBillUpdateRqDto.getEntityActivityId()));

        if (entityActivity == null) {
            entityActivity = entityActivityRepo.findEntityActivity(masterBillUpdateRqDto.getEntityActivityCode(), entityUser.getEntity());
        }

        if (entityActivity != null && entityActivity.getStatus().equals(Status.ACTIVE)) {

            LOGGER.info("************************************");
            LOGGER.info("totalAmount: " + totalAmount);

            if (entityActivity.getTheLowestAmountPerUploadedBill() != null) {
                if (totalAmount.compareTo(entityActivity.getTheLowestAmountPerUploadedBill()) == -1) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_amount_min", locale);
                }
            }

            if (entityActivity.getTheHighestAmountPerUploadedBill() != null) {
                //checking bill limit per bill
                if (totalAmount.compareTo(entityActivity.getTheHighestAmountPerUploadedBill()) == 1) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_amount_limit_per_bill", locale);
                }
            }

            if (entityActivity.getMonthlyHighestValueOfExpectedUploadedBills() != null) {
                //checking bill limit per month
                LocalDateTime startDayOfMonth = Utils.getStartDayOfCurrentMonth();
                LocalDateTime currentDay = Utils.getCurrentDateTime();

                LOGGER.info("startDayOfMonth: " + startDayOfMonth);
                LOGGER.info("currentDay: " + currentDay);

                BigDecimal totalAmountForCurrentMonth = billRepo.volumeByApprovedDate(startDayOfMonth, currentDay, entityActivity, entityUser.getEntity());
                if (totalAmountForCurrentMonth == null) {
                    totalAmountForCurrentMonth = new BigDecimal("0");
                }

                LOGGER.info("************************************");
                LOGGER.info("totalAmountForCurrentMonth: " + totalAmountForCurrentMonth);
                LOGGER.info("BillMaxAmountPerBill: " + entityActivity.getMonthlyHighestValueOfExpectedUploadedBills());

                if ((totalAmount.add(totalAmountForCurrentMonth)).compareTo(entityActivity.getMonthlyHighestValueOfExpectedUploadedBills()) == 1) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_amount_limit_per_month", locale);
                }
            }
        } else {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_entity_activity_id", locale);
        }

        Bill updatedBill = (Bill) masterBillUpdateRqDto.toEntity(locale);

        currentBill.setTotalAmount(totalAmount);
        currentBill.setEntity(entityUser.getEntity());
        currentBill.setEntityUser(entityUser);
        currentBill.setCreateDate(LocalDateTime.now());
        currentBill.setDiscount(discount);
        currentBill.setDiscountType(discountType);

        if (updatedBill.getBillNumber() != null && !updatedBill.getBillNumber().isEmpty()) {

            Bill existBill = billRepo.findBill(updatedBill.getBillNumber(), entityUser.getEntity());
            if (existBill != null && !existBill.getId().equals(currentBill.getId())) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_already_exist", locale);
            }
            currentBill.setBillNumber(updatedBill.getBillNumber());
        } else {
            currentBill.setBillNumber(null);
        }

        currentBill.setSubAmount(updatedBill.getSubAmount());
        currentBill.setServiceName(updatedBill.getServiceName());
        currentBill.setServiceDescription(updatedBill.getServiceDescription());
        currentBill.setIssueDate(updatedBill.getIssueDate());
        currentBill.setExpireDate(updatedBill.getExpireDate());
        currentBill.setCustomerFullName(updatedBill.getCustomerFullName());
        currentBill.setCustomerEmailAddress(updatedBill.getCustomerEmailAddress());
        currentBill.setCustomerMobileNumber(updatedBill.getCustomerMobileNumber());

        switch (masterBillUpdateRqDto.getVat()) {
            case "EXE":
                currentBill.setVat(new BigDecimal("0"));
                currentBill.setVatExemptedFlag(BooleanFlag.YES);
                currentBill.setVatNaFlag(BooleanFlag.NO);
                break;
            case "NA":
                currentBill.setVat(new BigDecimal("0"));
                currentBill.setVatExemptedFlag(BooleanFlag.NO);
                currentBill.setVatNaFlag(BooleanFlag.YES);
                break;
            default:
                currentBill.setVat(new BigDecimal(masterBillUpdateRqDto.getVat()));
                currentBill.setVatExemptedFlag(BooleanFlag.NO);
                currentBill.setVatNaFlag(BooleanFlag.NO);
        }

        currentBill.setEntityActivity(entityActivityRepo.findEntityActivityById(masterBillUpdateRqDto.getEntityActivityId()));

        currentBill.setCustomerAddress(updatedBill.getCustomerAddress());
        currentBill.setCustomerTaxNumber(updatedBill.getCustomerTaxNumber());
        currentBill.setCustomerPreviousBalance(updatedBill.getCustomerPreviousBalance());
        currentBill.setCustomerIdType(updatedBill.getCustomerIdType());
        currentBill.setCampaignId(masterBillUpdateRqDto.getCampaignId());

        currentBill.setIsPartialAllowed(BooleanFlag.getValue(masterBillUpdateRqDto.getIsPartialAllowed()));
        if (currentBill.getIsPartialAllowed() != null && currentBill.getIsPartialAllowed().equals(BooleanFlag.YES)) {
            currentBill.setBillCategory(BillCategory.ONE_OFF_PARTIAL);
        } else {
            currentBill.setBillCategory(BillCategory.INSTANTLY);
            currentBill.setIsPartialAllowed(BooleanFlag.NO);
        }
        currentBill.setMiniPartialAmount(masterBillUpdateRqDto.getMiniPartialAmount());
        currentBill.setMaxPartialAmount(masterBillUpdateRqDto.getMaxPartialAmount());

        if (totalAmount.compareTo(currentBill.getCurrentPaidAmount()) == -1) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "cannot update this invoice with amount less than paid amount", locale);
        }

        currentBill.setRemainAmount(totalAmount.subtract(currentBill.getCurrentPaidAmount()));


        if (isBillStatusExist(currentBill, BillStatus.CHECKED)) {

            currentBill.setBillStatus(BillStatus.UPDATED_BY_COMPANY);

            BillTimeline updateBillTimeline = new BillTimeline();
            updateBillTimeline.setBillStatus(BillStatus.UPDATED_BY_COMPANY);
            updateBillTimeline.setBill(currentBill);
            updateBillTimeline.setActionDate(LocalDateTime.now());
            updateBillTimeline.setEntityUser(entityUser);
            updateBillTimeline.setUserReference(entityUser.getUsername());

            updateBillTimeline = billTimelineRepo.save(updateBillTimeline);

            currentBill.setBillStatus(BillStatus.APPROVED_BY_SADAD);

            boolean isSADADAprovedBill = isBillStatusExist(currentBill, BillStatus.APPROVED_BY_SADAD);

            if (!isSADADAprovedBill) {
                BillTimeline acceptedBillTimeline = new BillTimeline();
                acceptedBillTimeline.setBillStatus(BillStatus.APPROVED_BY_SADAD);
                acceptedBillTimeline.setBill(currentBill);
                acceptedBillTimeline.setActionDate(LocalDateTime.now());
                acceptedBillTimeline.setEntityUser(entityUser);
                acceptedBillTimeline.setUserReference(entityUser.getUsername());

                acceptedBillTimeline = billTimelineRepo.save(acceptedBillTimeline);
                currentBill.setApproveDateBySadad(acceptedBillTimeline.getActionDate());

                if (currentBill.getDueDate() != null && currentBill.getDueDate().isBefore(LocalDateTime.now())) {
                    BillTransaction billTransaction = new BillTransaction();
                    billTransaction.setBill(currentBill);
                    billTransaction.setPreviousAmount(currentBill.getAccount() != null ? currentBill.getAccount().getBalance() : BigDecimal.ZERO);
                    billTransaction.setAccount(currentBill.getAccount());
                    billTransaction.setAmount(difference);
                    billTransaction.setDate(currentBill.getDueDate());
                    billTransaction.setBalanceAmount(currentBill.getAccount() != null ? currentBill.getAccount().getBalance().add(difference) : BigDecimal.ZERO);
                    billTransaction.setName("Updated Invoice");
                    billTransaction.setDocumentNumber(currentBill.getBillNumber());

                    billTransaction = billTransactionRepo.save(billTransaction);
                }
            }
        }

        billCustomFieldRepo.deleteAllByBill(currentBill);

        if (masterBillUpdateRqDto.getCustomFieldList() != null && !masterBillUpdateRqDto.getCustomFieldList().isEmpty()) {
            for (BillCustomFieldRqDto billCustomFieldRqDto : masterBillUpdateRqDto.getCustomFieldList()) {

                EntityField entityField = entityFieldRepo.findEntityFieldById(billCustomFieldRqDto.getFieldId());

                if (entityField == null) {
                    continue;
                }

                if (entityField.getStatus().equals(Status.DELETED)) {
                    continue;
                }

                if (entityField.getStatus().equals(Status.INACTIVE)) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "custom field inactive", locale);
                }

//                if (!entityField.getEntity().getId().equals(entityUser.getEntity().getId())) {
//                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "custom field not related for current biller", locale);
//                }
//
//                if (!Utils.isValidCustomField(entityField, billCustomFieldRqDto.getFieldValue())) {
//                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "custom field value is not valid", locale);
//                }

                BillCustomField billCustomField = new BillCustomField();
                billCustomField.setBill(currentBill);
                billCustomField.setEntityField(entityField);
                billCustomField.setValue(billCustomFieldRqDto.getFieldValue());

                billCustomFieldRepo.save(billCustomField);
            }
        }

        if (masterBillUpdateRqDto.getCustomerEmailList() != null && !masterBillUpdateRqDto.getCustomerEmailList().isEmpty()) {
            List<BillEmail> billEmailList = new ArrayList<>();

            billEmailRepo.deleteAllByBill(currentBill);

            if (masterBillUpdateRqDto.getCustomerEmailList().size() > 5) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "email list should not more than 5 times", locale);
            }

            Set<String> set = new HashSet<>(masterBillUpdateRqDto.getCustomerEmailList());
            masterBillUpdateRqDto.getCustomerEmailList().clear();
            masterBillUpdateRqDto.getCustomerEmailList().addAll(set);

            for (String email : masterBillUpdateRqDto.getCustomerEmailList()) {

                if (email == null) {
                    continue;
                }

                BillEmail billEmail = new BillEmail();
                billEmail.setBill(currentBill);
                billEmail.setEmail(email);
                billEmailList.add(billEmail);
            }

            billEmailRepo.saveAll(billEmailList);
        }

        if (masterBillUpdateRqDto.getCustomerMobileList() != null && !masterBillUpdateRqDto.getCustomerMobileList().isEmpty()) {
            List<BillMobileNumber> billMobileNumberList = new ArrayList<>();

            billMobileNumberRepo.deleteAllByBill(currentBill);

            if (masterBillUpdateRqDto.getCustomerMobileList().size() > 5) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "mobile list should not more than 5 times", locale);
            }

            Set<String> set = new HashSet<>(masterBillUpdateRqDto.getCustomerMobileList());
            masterBillUpdateRqDto.getCustomerMobileList().clear();
            masterBillUpdateRqDto.getCustomerMobileList().addAll(set);

            for (String mobile : masterBillUpdateRqDto.getCustomerMobileList()) {

                if (mobile == null) {
                    continue;
                }
                BillMobileNumber billMobileNumber = new BillMobileNumber();
                billMobileNumber.setBill(currentBill);
                billMobileNumber.setMobileNumber(Utils.formatMobileNumber(mobile));
                billMobileNumberList.add(billMobileNumber);
            }

            billMobileNumberRepo.saveAll(billMobileNumberList);
        }

        //update bill to sadad if the bill was checked before:
        if (isBillStatusExist(currentBill, BillStatus.CHECKED)) {
            Config config = configRepo.findConfigByKey("INTG_FLAG");

            if (config != null && config.getValue().equalsIgnoreCase("true")) {

                WebServiceResponse webServiceResponse = webServiceHelper.uploadBill(currentBill, "BillUpdated");

                if (!webServiceResponse.getStatus().equals(200)) {
                    Document documentXmlResponse = Utils.convertStringToXmlDocument(new JSONObject(webServiceResponse.getBody()).getString("data"));
                    String message = Utils.getElementValueByTagName(documentXmlResponse, "faultstring");
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, message, webServiceResponse.getBody(), locale);
                }
            }
        }

        if (!Utils.isMinimumPartialAmountAllowed(currentBill)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "minimum partial amount should not less than activity minimum amount", locale);
        }

        billRepo.save(currentBill);

        return currentBill;

    }

    public Bill saveDetailedBill(DetailedBillMakeRqDto detailedBillMakeRqDto, EntityUser entityUser, BillSource billSource, Locale locale) {
        if (!entityUser.getEntityUserType().equals(EntityUserType.OFFICER) && entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "access_officer_user", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }


        if (detailedBillMakeRqDto.getBillNumber() != null && !detailedBillMakeRqDto.getBillNumber().isEmpty()) {
            Bill currentBill = billRepo.findBill(detailedBillMakeRqDto.getBillNumber(), entityUser.getEntity());
            if (currentBill != null) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_already_exist", locale);
            }
        }

        BigDecimal subAmountAfterDiscount = Utils.calculateSubAmountDiscount(detailedBillMakeRqDto.getSubAmount(), detailedBillMakeRqDto.getDiscount(), detailedBillMakeRqDto.getDiscountType());

        LOGGER.info("subAmountAfterDiscount: " + subAmountAfterDiscount);

        BigDecimal totalAmount = Utils.calculateTotalAmountFromVatAndPreviousBalance(subAmountAfterDiscount, detailedBillMakeRqDto.getVat(), detailedBillMakeRqDto.getCustomerPreviousBalance());

        LOGGER.info("totalAmount: " + totalAmount);

        BigDecimal miniPartialAmount = detailedBillMakeRqDto.getMiniPartialAmount();
        if (miniPartialAmount == null) {
            miniPartialAmount = BigDecimal.ZERO;
        }

        if (totalAmount.doubleValue() < miniPartialAmount.doubleValue()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "Total amount should not less than minimum partial amount", locale);
        }

        EntityActivity entityActivity = entityActivityRepo.findEntityActivityById(detailedBillMakeRqDto.getEntityActivityId());

        if (entityActivity == null) {
            entityActivity = entityActivityRepo.findEntityActivity(detailedBillMakeRqDto.getEntityActivityCode(), entityUser.getEntity());
        }

        if (entityActivity != null && entityActivity.getStatus().equals(Status.ACTIVE)) {

            LOGGER.info("************************************");
            LOGGER.info("totalAmount: " + totalAmount);

            if (entityActivity.getTheLowestAmountPerUploadedBill() != null) {
                if (totalAmount.compareTo(entityActivity.getTheLowestAmountPerUploadedBill()) == -1) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_amount_min", locale);
                }
            }

            if (entityActivity.getTheHighestAmountPerUploadedBill() != null) {
                //checking bill limit per bill
                if (totalAmount.compareTo(entityActivity.getTheHighestAmountPerUploadedBill()) == 1) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_amount_limit_per_bill", locale);
                }
            }

            if (entityActivity.getMonthlyHighestValueOfExpectedUploadedBills() != null) {
                //checking bill limit per month
                LocalDateTime startDayOfMonth = Utils.getStartDayOfCurrentMonth();
                LocalDateTime currentDay = Utils.getCurrentDateTime();

                LOGGER.info("startDayOfMonth: " + startDayOfMonth);
                LOGGER.info("currentDay: " + currentDay);

                BigDecimal totalAmountForCurrentMonth = billRepo.volumeByApprovedDate(startDayOfMonth, currentDay, entityActivity, entityUser.getEntity());
                if (totalAmountForCurrentMonth == null) {
                    totalAmountForCurrentMonth = new BigDecimal("0");
                }

                LOGGER.info("************************************");
                LOGGER.info("totalAmountForCurrentMonth: " + totalAmountForCurrentMonth);
                LOGGER.info("BillMaxAmountPerBill: " + entityActivity.getMonthlyHighestValueOfExpectedUploadedBills());

                if ((totalAmount.add(totalAmountForCurrentMonth)).compareTo(entityActivity.getMonthlyHighestValueOfExpectedUploadedBills()) == 1) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_amount_limit_per_month", locale);
                }
            }
        } else {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_entity_activity_id", locale);
        }

        Bill detailedBill = (Bill) detailedBillMakeRqDto.toEntity(locale);

        detailedBill.setTotalAmount(totalAmount);
        detailedBill.setBillType(BillType.DETAIL_BILL);
        detailedBill.setBillSource(billSource);
        detailedBill.setBillStatus(BillStatus.MAKED);
        detailedBill.setEntity(entityUser.getEntity());
        detailedBill.setEntityUser(entityUser);
        detailedBill.setCreateDate(LocalDateTime.now());
        detailedBill.setEntityActivity(entityActivity);
        detailedBill.setCampaignId(detailedBillMakeRqDto.getCampaignId());


        detailedBill.setIsPartialAllowed(BooleanFlag.getValue(detailedBillMakeRqDto.getIsPartialAllowed()));
        if (detailedBill.getIsPartialAllowed() != null && detailedBill.getIsPartialAllowed().equals(BooleanFlag.YES)) {
            detailedBill.setBillCategory(BillCategory.ONE_OFF_PARTIAL);
        } else {
            detailedBill.setBillCategory(BillCategory.INSTANTLY);
            detailedBill.setIsPartialAllowed(BooleanFlag.NO);
        }

        detailedBill.setMiniPartialAmount(detailedBillMakeRqDto.getMiniPartialAmount());
        detailedBill.setMaxPartialAmount(detailedBillMakeRqDto.getMaxPartialAmount());
        detailedBill.setPaymentNumber(0);
        detailedBill.setSettlementNumber(0);

        detailedBill.setAdditionalNumber(detailedBillMakeRqDto.getAdditionalNumber());
        detailedBill.setBuildingNumber(detailedBillMakeRqDto.getBuildingNumber());
        detailedBill.setCity(cityRepo.findCityById(detailedBillMakeRqDto.getCityId()));
        detailedBill.setDistrict(districtRepo.findDistrictById(detailedBillMakeRqDto.getDistrictId()));
        detailedBill.setPostalCode(detailedBillMakeRqDto.getPostalCode());
        detailedBill.setStreet(detailedBillMakeRqDto.getStreetName());
        detailedBill.setOtherDistrict(detailedBillMakeRqDto.getOtherDistrict());


        detailedBill.setLogo(entityUser.getEntity().getLogo());
        detailedBill.setStamp(entityUser.getEntity().getStamp());

        if (detailedBill.getBillNumber() == null || detailedBill.getBillNumber().trim().isEmpty()) {
            if (entityUser.getEntity().getIsAutoGenerateBillNumber().equals(BooleanFlag.YES)) {
                String prefixBillNumber = Utils.generateBillNumber(entityUser.getEntity());
                Long sequence = (entityUser.getEntity().getAutoGenerationBillNumberSequence() + 1);
                entityRepo.increaseBillNumberCounter(sequence, entityUser.getEntity().getId());
                detailedBill.setBillNumber(prefixBillNumber + sequence);
            } else {
                detailedBill.setBillNumber(null);
            }
        } else {
            if (entityUser.getEntity().getIsAutoGenerateBillNumber().equals(BooleanFlag.YES)) {
                Long sequence = (entityUser.getEntity().getAutoGenerationBillNumberSequence() + 1);
                entityRepo.increaseBillNumberCounter(sequence, entityUser.getEntity().getId());
            }
        }

        detailedBill = billRepo.save(detailedBill);

        Set billItemDetailedRqDtoSet = new HashSet(detailedBillMakeRqDto.getBillItemList());


        if (billItemDetailedRqDtoSet.size() < detailedBillMakeRqDto.getBillItemList().size()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "Duplicate item name is not allowed", locale);
        }

        Integer line = 1;
        for (BillItemDetailedRqDto billItemDetailedRqDto : detailedBillMakeRqDto.getBillItemList()) {


            if (billItemDetailedRqDto.getVat() != null) {
                List<EntityVat> entityVatList = entityVatRepo.findEntityVatByEntityAndVat(entityUser.getEntity(), Utils.convertToBigDecimal(billItemDetailedRqDto.getVat()));

                if (entityVatList == null || entityVatList.isEmpty()) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "VAT value is not match allowed VAT", locale);
                }
            }

            BillItem billItem = (BillItem) billItemDetailedRqDto.toEntity(locale);
            billItem.setBill(detailedBill);
            billItem.setNumber(line++);
            billItem = billItemRepo.save(billItem);
        }

        Integer expectedRequiredCounter = entityFieldRepo.countRequiredEntityFieldByEntity(entityUser.getEntity());

        Integer currentRequiredCounter = 0;

        if (detailedBillMakeRqDto.getCustomFieldList() != null && !detailedBillMakeRqDto.getCustomFieldList().isEmpty()) {
            for (BillCustomFieldRqDto billCustomFieldRqDto : detailedBillMakeRqDto.getCustomFieldList()) {

                EntityField entityField = entityFieldRepo.findEntityFieldById(billCustomFieldRqDto.getFieldId());

                if (entityField == null) {
                    continue;
                }

                if (entityField.getStatus().equals(Status.DELETED)) {
                    continue;
                }

                if (entityField.getStatus().equals(Status.INACTIVE)) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "custom field inactive", locale);
                }

                if (!entityField.getEntity().getId().equals(entityUser.getEntity().getId())) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "custom field not related for current biller", locale);
                }

                if (entityField.getIsRequired().equals(BooleanFlag.YES) && Utils.isNullOrEmpty(billCustomFieldRqDto.getFieldValue())) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "custom field required value is missing", locale);
                }

                if (entityField.getIsRequired().equals(BooleanFlag.YES)) {
                    currentRequiredCounter++;
                }

                BillCustomField billCustomField = new BillCustomField();
                billCustomField.setBill(detailedBill);
                billCustomField.setEntityField(entityField);
                billCustomField.setValue(billCustomFieldRqDto.getFieldValue());

                billCustomFieldRepo.save(billCustomField);
            }
        }

        LOGGER.info("expectedRequiredCounter " + expectedRequiredCounter);
        LOGGER.info("currentRequiredCounter " + currentRequiredCounter);

        if (expectedRequiredCounter != null) {
            if (!currentRequiredCounter.equals(expectedRequiredCounter)) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "Missing required custom field", locale);
            }
        }

        BillTimeline makedBillTimeline = new BillTimeline();
        makedBillTimeline.setBillStatus(BillStatus.MAKED);
        makedBillTimeline.setBill(detailedBill);
        makedBillTimeline.setActionDate(LocalDateTime.now());
        makedBillTimeline.setEntityUser(entityUser);
        makedBillTimeline.setUserReference(entityUser.getUsername());

        makedBillTimeline = billTimelineRepo.save(makedBillTimeline);

        if (detailedBillMakeRqDto.getCustomerEmailList() != null && !detailedBillMakeRqDto.getCustomerEmailList().isEmpty()) {

            if (detailedBillMakeRqDto.getCustomerEmailList().size() > 5) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "email list should not more than 5 times", locale);
            }

            Set<String> set = new HashSet<>(detailedBillMakeRqDto.getCustomerEmailList());
            detailedBillMakeRqDto.getCustomerEmailList().clear();
            detailedBillMakeRqDto.getCustomerEmailList().addAll(set);

            List<BillEmail> billEmailList = new ArrayList<>();

            for (String email : detailedBillMakeRqDto.getCustomerEmailList()) {

                if (email == null) {
                    continue;
                }

                if (email.equals(detailedBill.getCustomerEmailAddress())) {
                    continue;
                }

                BillEmail billEmail = new BillEmail();
                billEmail.setBill(detailedBill);
                billEmail.setEmail(email);
                billEmailList.add(billEmail);
            }

            billEmailRepo.saveAll(billEmailList);
        }

        if (detailedBillMakeRqDto.getCustomerMobileList() != null && !detailedBillMakeRqDto.getCustomerMobileList().isEmpty()) {

            if (detailedBillMakeRqDto.getCustomerMobileList().size() > 5) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "mobile list should not more than 5 times", locale);
            }

            Set<String> set = new HashSet<>(detailedBillMakeRqDto.getCustomerMobileList());
            detailedBillMakeRqDto.getCustomerMobileList().clear();
            detailedBillMakeRqDto.getCustomerMobileList().addAll(set);

            List<BillMobileNumber> billMobileNumberList = new ArrayList<>();

            for (String mobile : detailedBillMakeRqDto.getCustomerMobileList()) {

                if (mobile == null) {
                    continue;
                }

                String formattedMobile = Utils.formatMobileNumber(mobile);

                if (formattedMobile.equals(detailedBill.getCustomerMobileNumber())) {
                    continue;
                }

                BillMobileNumber billMobileNumber = new BillMobileNumber();
                billMobileNumber.setBill(detailedBill);
                billMobileNumber.setMobileNumber(Utils.formatMobileNumber(mobile));
                billMobileNumberList.add(billMobileNumber);
            }

            billMobileNumberRepo.saveAll(billMobileNumberList);
        }

        //------------------------- save contact for customer --------------------------------
        if ((detailedBillMakeRqDto.getSaveCustomer() != null) && (detailedBillMakeRqDto.getSaveCustomer().equals("YES"))) {

            IndividualAccount customer = new IndividualAccount();
            customer.setFirstName(detailedBill.getCustomerFullName());
            customer.setSecondName("-");
            customer.setThirdName("-");
            customer.setLastName("-");
            customer.setEmail(detailedBill.getCustomerEmailAddress());
            customer.setPhoneNumber(detailedBill.getCustomerMobileNumber());
            customer.setIdType(detailedBill.getCustomerIdType());
            customer.setVatNumber(detailedBill.getCustomerTaxNumber() != null ? detailedBill.getCustomerTaxNumber() : "");
            customer.setEntityUser(entityUser);
            customer.setStatus(Status.ACTIVE);
            customer.setIsDraft(BooleanFlag.YES);
            customer.setAccountCategory(AccountCategory.NORMAL);
            customer.setEntity(entityUser.getEntity());
            customer.setEntity(entityUser.getEntity());
            customer = accountRepo.save(customer);

            if (detailedBillMakeRqDto.getCustomFieldList() != null && !detailedBillMakeRqDto.getCustomFieldList().isEmpty()) {
                for (BillCustomFieldRqDto billCustomFieldRqDto : detailedBillMakeRqDto.getCustomFieldList()) {

                    EntityField entityField = entityFieldRepo.findEntityFieldById(billCustomFieldRqDto.getFieldId());

                    AccountCustomField customerCustomField = new AccountCustomField();
                    customerCustomField.setEntityAccount(customer);
                    customerCustomField.setEntityField(entityField);
                    customerCustomField.setValue(billCustomFieldRqDto.getFieldValue());

                    accountCustomFieldRepo.save(customerCustomField);
                }
            }

        }
        //-------------------------

        if (!Utils.isMinimumPartialAmountAllowed(detailedBill)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "minimum partial amount should not less than " + detailedBill.getEntityActivity().getTheLowestAmountPerUploadedBill().doubleValue(), locale);
        }


        if (entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN_PLUS)
                || entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN)
                || entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR_PLUS)) {
            this.checkBill(detailedBill.getId(), entityUser, locale);
        }


        return detailedBill;
    }

    public Bill saveRecurringBill(RecurringBillMakeRqDto recurringBillMakeRqDto, EntityUser entityUser, BillSource billSource, Locale locale) {
        if (!entityUser.getEntityUserType().equals(EntityUserType.OFFICER) && entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "access_officer_user", locale);
        }

        Account account = accountRepo.findAccountById(recurringBillMakeRqDto.getAccountId());

        if (account == null || account.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "account not exist", locale);
        }

        if (account.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "account inactive", locale);
        }

        if (account.getAccountCategory().equals(AccountCategory.NORMAL)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "normal account cannot create recurring bill", locale);
        }

        if (recurringBillMakeRqDto.getBillNumber() != null && !recurringBillMakeRqDto.getBillNumber().isEmpty()) {
            Bill currentBill = billRepo.findBill(recurringBillMakeRqDto.getBillNumber(), entityUser.getEntity());

            if (currentBill != null) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_already_exist", locale);
            }
        }

        BigDecimal subAmountAfterDiscount = Utils.calculateSubAmountDiscount(recurringBillMakeRqDto.getSubAmount(), recurringBillMakeRqDto.getDiscount(), recurringBillMakeRqDto.getDiscountType());

        LOGGER.info("subAmountAfterDiscount: " + subAmountAfterDiscount);

        BigDecimal totalAmount = Utils.calculateTotalAmountFromVatAndPreviousBalance(subAmountAfterDiscount, recurringBillMakeRqDto.getVat(), recurringBillMakeRqDto.getCustomerPreviousBalance());

        LOGGER.info("totalAmount: " + totalAmount);

        BigDecimal miniPartialAmount = recurringBillMakeRqDto.getMiniPartialAmount();
        if (miniPartialAmount == null) {
            miniPartialAmount = BigDecimal.ZERO;
        }

        if (totalAmount.doubleValue() < miniPartialAmount.doubleValue()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "Total amount should not less than minimum partial amount", locale);
        }

        EntityActivity entityActivity = entityActivityRepo.findEntityActivityById(recurringBillMakeRqDto.getEntityActivityId());

        if (entityActivity == null) {
            entityActivity = entityActivityRepo.findEntityActivity(recurringBillMakeRqDto.getEntityActivityCode(), entityUser.getEntity());
        }

        String customerMobileNumber = null;
        String customerEmail = null;
        String customerFullName = null;
        String customerIdNumber = null;
        String customerTaxNumber = null;
        String customerIdType = null;

        if (account instanceof IndividualAccount) {

            IndividualAccount individualAccount = (IndividualAccount) account;

            customerMobileNumber = recurringBillMakeRqDto.getCustomerMobileNumber() != null ? recurringBillMakeRqDto.getCustomerMobileNumber() : individualAccount.getPhoneNumber();
            customerEmail = recurringBillMakeRqDto.getCustomerEmailAddress() != null ? recurringBillMakeRqDto.getCustomerEmailAddress() : individualAccount.getEmail();
            customerFullName = recurringBillMakeRqDto.getCustomerFullName() != null ? recurringBillMakeRqDto.getCustomerFullName() : individualAccount.getFirstName() + " " + individualAccount.getLastName();
            customerIdNumber = recurringBillMakeRqDto.getCustomerIdNumber() != null ? recurringBillMakeRqDto.getCustomerIdNumber() : individualAccount.getIdNumber();
            customerTaxNumber = recurringBillMakeRqDto.getCustomerTaxNumber() != null ? recurringBillMakeRqDto.getCustomerTaxNumber() : individualAccount.getVatNumber();
            customerIdType = recurringBillMakeRqDto.getCustomerIdType() != null ? recurringBillMakeRqDto.getCustomerIdType() : individualAccount.getIdType() != null ? individualAccount.getIdType().name() : null;

        } else {

            CorporateAccount corporateAccount = (CorporateAccount) account;

            customerMobileNumber = recurringBillMakeRqDto.getCustomerMobileNumber() != null ? recurringBillMakeRqDto.getCustomerMobileNumber() : corporateAccount.getContactPersonNumber();
            customerEmail = recurringBillMakeRqDto.getCustomerEmailAddress() != null ? recurringBillMakeRqDto.getCustomerEmailAddress() : corporateAccount.getContactPersonEmail();
            customerFullName = recurringBillMakeRqDto.getCustomerFullName() != null ? recurringBillMakeRqDto.getCustomerFullName() : corporateAccount.getCompanyName();
            customerIdNumber = recurringBillMakeRqDto.getCustomerIdNumber() != null ? recurringBillMakeRqDto.getCustomerIdNumber() : corporateAccount.getIdNumber();
            customerTaxNumber = recurringBillMakeRqDto.getCustomerTaxNumber() != null ? recurringBillMakeRqDto.getCustomerTaxNumber() : corporateAccount.getVatNumber();
            customerIdType = recurringBillMakeRqDto.getCustomerIdType() != null ? recurringBillMakeRqDto.getCustomerIdType() : corporateAccount.getTypeIdDocument() != null ? corporateAccount.getTypeIdDocument().name() : null;
        }

        Bill recurringBill = new Bill();
        recurringBill.setBillNumber(recurringBillMakeRqDto.getBillNumber());
        recurringBill.setCustomerMobileNumber(Utils.formatMobileNumber(customerMobileNumber));
        recurringBill.setCustomerEmailAddress(customerEmail);
        recurringBill.setCustomerFullName(customerFullName);
        recurringBill.setDueDate(Utils.parseDateFromString(recurringBillMakeRqDto.getDueDate(), "yyyy-MM-dd"));
        recurringBill.setIssueDate(Utils.parseDateFromString(recurringBillMakeRqDto.getDueDate(), "yyyy-MM-dd"));
        recurringBill.setCustomerIdNumber(customerIdNumber);
        recurringBill.setSubAmount(recurringBillMakeRqDto.getSubAmount());
        recurringBill.setDiscount(null);
        recurringBill.setCustomerTaxNumber(customerTaxNumber);
        recurringBill.setCustomerPreviousBalance(BigDecimal.ZERO);
        recurringBill.setCustomerIdType(IdType.getValue(customerIdType));
        recurringBill.setIsPartialAllowed(BooleanFlag.getValue(recurringBillMakeRqDto.getIsPartialAllowed()));
        recurringBill.setMiniPartialAmount(recurringBillMakeRqDto.getMiniPartialAmount());

        recurringBill.setTotalAmount(totalAmount);
        recurringBill.setBillType(BillType.RECURRING_BILL);
        recurringBill.setAccount(account);
        recurringBill.setBillSource(billSource);
        recurringBill.setBillStatus(BillStatus.MAKED);
        recurringBill.setEntity(entityUser.getEntity());
        recurringBill.setEntityUser(entityUser);
        recurringBill.setCreateDate(LocalDateTime.now());
        recurringBill.setEntityActivity(entityActivity);
        recurringBill.setCampaignId(recurringBillMakeRqDto.getCampaignId());


        recurringBill.setBillCategory(BillCategory.RECURRING_ACCOUNT_PAR);


        recurringBill.setMiniPartialAmount(recurringBillMakeRqDto.getMiniPartialAmount());
        recurringBill.setMaxPartialAmount(recurringBillMakeRqDto.getMaxPartialAmount());
        recurringBill.setPaymentNumber(0);
        recurringBill.setSettlementNumber(0);

        recurringBill.setAdditionalNumber(recurringBillMakeRqDto.getAdditionalNumber());
        recurringBill.setBuildingNumber(recurringBillMakeRqDto.getBuildingNumber());
        recurringBill.setCity(cityRepo.findCityById(recurringBillMakeRqDto.getCityId()));
        recurringBill.setDistrict(districtRepo.findDistrictById(recurringBillMakeRqDto.getDistrictId()));
        recurringBill.setPostalCode(recurringBillMakeRqDto.getPostalCode());
        recurringBill.setStreet(recurringBillMakeRqDto.getStreetName());

        if (account instanceof IndividualAccount) {
            recurringBill.setReferenceNumber(recurringBillMakeRqDto.getReferenceNumber());
        } else {
            recurringBill.setReferenceNumber(recurringBillMakeRqDto.getReferenceNumber());
        }

        recurringBill.setLogo(entityUser.getEntity().getLogo());
        recurringBill.setStamp(entityUser.getEntity().getStamp());

        Integer cycleNumber = account.getCycleNumber();
        cycleNumber = cycleNumber + 1;
        account.setCycleNumber(cycleNumber);
        accountRepo.increaseCycleNumberCounter(cycleNumber, account.getId());
        recurringBill.setCycleNumber(cycleNumber);

        recurringBill.setSadadNumber(account.getAccountNumber());

        recurringBill.setRemainAmount(totalAmount);

        recurringBill.setTotalRunningAmount(totalAmount);

        String prefixBillNumber = Utils.generateBillNumber(entityUser.getEntity());
        Long sequence = (entityUser.getEntity().getAutoGenerationBillNumberSequence() + 1);
        entityUser.getEntity().setAutoGenerationBillNumberSequence(sequence);
        entityRepo.increaseBillNumberCounter(sequence, entityUser.getEntity().getId());
        recurringBill.setBillNumber(prefixBillNumber + sequence);

        recurringBill.setCustomerAddress(recurringBillMakeRqDto.getCustomerAddress());

        recurringBill = billRepo.save(recurringBill);

        BillTimeline makedBillTimeline = new BillTimeline();
        makedBillTimeline.setBillStatus(BillStatus.MAKED);
        makedBillTimeline.setBill(recurringBill);
        makedBillTimeline.setActionDate(LocalDateTime.now());
        makedBillTimeline.setEntityUser(entityUser);
        makedBillTimeline.setUserReference(entityUser.getUsername());

        makedBillTimeline = billTimelineRepo.save(makedBillTimeline);


        Set billItemDetailedRqDtoSet = new HashSet(recurringBillMakeRqDto.getBillItemList());

        if (billItemDetailedRqDtoSet.size() < recurringBillMakeRqDto.getBillItemList().size()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "Duplicate item name is not allowed", locale);
        }

        Integer line = 1;
        for (BillItemDetailedRqDto billItemDetailedRqDto : recurringBillMakeRqDto.getBillItemList()) {


            if (billItemDetailedRqDto.getVat() != null) {
                List<EntityVat> entityVatList = entityVatRepo.findEntityVatByEntityAndVat(entityUser.getEntity(), Utils.convertToBigDecimal(billItemDetailedRqDto.getVat()));

                if (entityVatList == null || entityVatList.isEmpty()) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "VAT value is not match allowed VAT", locale);
                }
            }

            BillItem billItem = (BillItem) billItemDetailedRqDto.toEntity(locale);
            billItem.setBill(recurringBill);
            billItem.setNumber(line++);

            billItem.setDiscount(billItem.getDiscount());
            billItem.setVat(billItem.getVat());

            billItem.setUnitPrice(billItemDetailedRqDto.getUnitPrice());
            billItem.setQuantity(billItemDetailedRqDto.getQuantity());

            billItem.setTotalPrice(billItem.getUnitPrice().multiply(billItem.getQuantity()));

            billItem = billItemRepo.save(billItem);
        }

        Integer expectedRequiredCounter = entityFieldRepo.countRequiredEntityFieldByEntity(entityUser.getEntity());

        Integer currentRequiredCounter = 0;

        if (recurringBillMakeRqDto.getCustomFieldList() != null && !recurringBillMakeRqDto.getCustomFieldList().isEmpty()) {
            for (BillCustomFieldRqDto billCustomFieldRqDto : recurringBillMakeRqDto.getCustomFieldList()) {

                EntityField entityField = entityFieldRepo.findEntityFieldById(billCustomFieldRqDto.getFieldId());

                if (entityField == null) {
                    continue;
                }

                if (entityField.getStatus().equals(Status.DELETED)) {
                    continue;
                }

                if (entityField.getStatus().equals(Status.INACTIVE)) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "custom field inactive", locale);
                }

                if (!entityField.getEntity().getId().equals(entityUser.getEntity().getId())) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "custom field not related for current biller", locale);
                }

                if (!Utils.isValidCustomField(entityField, billCustomFieldRqDto.getFieldValue())) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "custom field value is not valid", locale);
                }

                if (entityField.getIsRequired().equals(BooleanFlag.YES)) {
                    currentRequiredCounter++;
                }

                if (entityField.getIsRequired().equals(BooleanFlag.YES) && Utils.isNullOrEmpty(billCustomFieldRqDto.getFieldValue())) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "custom field required value is missing", locale);
                }

                BillCustomField billCustomField = new BillCustomField();
                billCustomField.setBill(recurringBill);
                billCustomField.setEntityField(entityField);
                billCustomField.setValue(billCustomFieldRqDto.getFieldValue());

                billCustomFieldRepo.save(billCustomField);
            }
        }

        LOGGER.info("expectedRequiredCounter " + expectedRequiredCounter);
        LOGGER.info("currentRequiredCounter " + currentRequiredCounter);

        if (expectedRequiredCounter != null) {
            if (!currentRequiredCounter.equals(expectedRequiredCounter)) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "Missing required custom field", locale);
            }
        }

        if (recurringBillMakeRqDto.getCustomerEmailList() != null && !recurringBillMakeRqDto.getCustomerEmailList().isEmpty()) {

            if (recurringBillMakeRqDto.getCustomerEmailList().size() > 5) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "email list should not more than 5 times", locale);
            }

            Set<String> set = new HashSet<>(recurringBillMakeRqDto.getCustomerEmailList());
            recurringBillMakeRqDto.getCustomerEmailList().clear();
            recurringBillMakeRqDto.getCustomerEmailList().addAll(set);

            List<BillEmail> billEmailList = new ArrayList<>();

            for (String email : recurringBillMakeRqDto.getCustomerEmailList()) {

                if (email == null) {
                    continue;
                }

                if (email.equals(recurringBill.getCustomerEmailAddress())) {
                    continue;
                }

                BillEmail billEmail = new BillEmail();
                billEmail.setBill(recurringBill);
                billEmail.setEmail(email);
                billEmailList.add(billEmail);
            }

            billEmailRepo.saveAll(billEmailList);
        }

        if (recurringBillMakeRqDto.getCustomerMobileList() != null && !recurringBillMakeRqDto.getCustomerMobileList().isEmpty()) {
            if (recurringBillMakeRqDto.getCustomerMobileList().size() > 5) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "mobile list should not more than 5 times", locale);
            }

            Set<String> set = new HashSet<>(recurringBillMakeRqDto.getCustomerMobileList());
            recurringBillMakeRqDto.getCustomerMobileList().clear();
            recurringBillMakeRqDto.getCustomerMobileList().addAll(set);

            List<BillMobileNumber> billMobileNumberList = new ArrayList<>();

            for (String mobile : recurringBillMakeRqDto.getCustomerMobileList()) {

                if (mobile == null) {
                    continue;
                }

                String formattedMobile = Utils.formatMobileNumber(mobile);

                if (formattedMobile.equals(recurringBill.getCustomerMobileNumber())) {
                    continue;
                }

                BillMobileNumber billMobileNumber = new BillMobileNumber();
                billMobileNumber.setBill(recurringBill);
                billMobileNumber.setMobileNumber(Utils.formatMobileNumber(mobile));
                billMobileNumberList.add(billMobileNumber);
            }

            billMobileNumberRepo.saveAll(billMobileNumberList);
        }

        account.setMiniPartialAmount(recurringBill.getMiniPartialAmount());


        if (entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN_PLUS)
                || entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN)
                || entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR_PLUS)) {
            this.checkRecurringBill(recurringBill, entityUser, account, locale);
        }

        return recurringBill;
    }

    public Bill updateDetailedBill(DetailedBillUpdateRqDto detailedBillUpdateRqDto, EntityUser entityUser, Locale locale) {

        if (entityUser.getEntityUserType().equals(EntityUserType.OFFICER)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "update_is_not_allowed_for_officer", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }


        Bill currentBill = billRepo.findBillById(detailedBillUpdateRqDto.getBillId());

        if (currentBill == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
        }

        if (currentBill.getBillStatus().equals(BillStatus.DELETED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_already_deleted", locale);
        }

        if (isBillStatusExist(currentBill, BillStatus.CANCELED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "cannot_update_bill_already_canceled", locale);
        }

        if (isBillStatusExist(currentBill, BillStatus.PAID_BY_SADAD) || isBillStatusExist(currentBill, BillStatus.PAID_BY_COMPANY)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "cannot_update_bill_already_paid", locale);
        }

//        if (isBillStatusExist(currentBill, BillStatus.PARTIALLY_PAID_BY_COMPANY) || isBillStatusExist(currentBill, BillStatus.PARTIALLY_PAID_BY_SADAD)) {
//            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "cannot_update_bill_already_partially_paid", locale);
//        }


        BigDecimal subAmountAfterDiscount = Utils.calculateSubAmountDiscount(detailedBillUpdateRqDto.getSubAmount(), detailedBillUpdateRqDto.getDiscount(), detailedBillUpdateRqDto.getDiscountType());

        BigDecimal totalAmount = Utils.calculateTotalAmountFromVatAndPreviousBalance(subAmountAfterDiscount, detailedBillUpdateRqDto.getVat(), detailedBillUpdateRqDto.getCustomerPreviousBalance());

        BigDecimal difference = totalAmount.subtract(currentBill.getTotalAmount());

        BigDecimal miniPartialAmount = detailedBillUpdateRqDto.getMiniPartialAmount();
        if (miniPartialAmount == null) {
            miniPartialAmount = BigDecimal.ZERO;
        }

        if (totalAmount.doubleValue() < miniPartialAmount.doubleValue()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "Total amount should not less than minimum partial amount", locale);
        }

        EntityActivity entityActivity = entityActivityRepo.findEntityActivityById(detailedBillUpdateRqDto.getEntityActivityId());

        if (entityActivity == null) {
            entityActivity = entityActivityRepo.findEntityActivity(detailedBillUpdateRqDto.getEntityActivityCode(), entityUser.getEntity());
        }

        if (entityActivity != null && entityActivity.getStatus().equals(Status.ACTIVE)) {

            LOGGER.info("************************************");
            LOGGER.info("totalAmount: " + totalAmount);

            if (entityActivity.getTheLowestAmountPerUploadedBill() != null) {
                if (totalAmount.compareTo(entityActivity.getTheLowestAmountPerUploadedBill()) == -1) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_amount_min", locale);
                }
            }

            if (entityActivity.getTheHighestAmountPerUploadedBill() != null) {
                //checking bill limit per bill
                if (totalAmount.compareTo(entityActivity.getTheHighestAmountPerUploadedBill()) == 1) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_amount_limit_per_bill", locale);
                }
            }

            if (entityActivity.getMonthlyHighestValueOfExpectedUploadedBills() != null) {
                //checking bill limit per month
                LocalDateTime startDayOfMonth = Utils.getStartDayOfCurrentMonth();
                LocalDateTime currentDay = Utils.getCurrentDateTime();

                LOGGER.info("startDayOfMonth: " + startDayOfMonth);
                LOGGER.info("currentDay: " + currentDay);

                BigDecimal totalAmountForCurrentMonth = billRepo.volumeByApprovedDate(startDayOfMonth, currentDay, entityActivity, entityUser.getEntity());
                if (totalAmountForCurrentMonth == null) {
                    totalAmountForCurrentMonth = new BigDecimal("0");
                }

                LOGGER.info("************************************");
                LOGGER.info("totalAmountForCurrentMonth: " + totalAmountForCurrentMonth);
                LOGGER.info("BillMaxAmountPerBill: " + entityActivity.getMonthlyHighestValueOfExpectedUploadedBills());

                if ((totalAmount.add(totalAmountForCurrentMonth)).compareTo(entityActivity.getMonthlyHighestValueOfExpectedUploadedBills()) == 1) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_amount_limit_per_month", locale);
                }
            }
        } else {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_entity_activity_id", locale);
        }

        Bill updatedBill = (Bill) detailedBillUpdateRqDto.toEntity(locale);

        currentBill.setTotalAmount(totalAmount);
        currentBill.setTotalRunningAmount(null);
        currentBill.setEntity(entityUser.getEntity());
        currentBill.setEntityUser(entityUser);
        currentBill.setCreateDate(LocalDateTime.now());

        switch (detailedBillUpdateRqDto.getVat()) {
            case "EXE":
                currentBill.setVat(new BigDecimal("0"));
                currentBill.setVatExemptedFlag(BooleanFlag.YES);
                currentBill.setVatNaFlag(BooleanFlag.NO);
                break;
            case "NA":
                currentBill.setVat(new BigDecimal("0"));
                currentBill.setVatExemptedFlag(BooleanFlag.NO);
                currentBill.setVatNaFlag(BooleanFlag.YES);
                break;
            default:
                currentBill.setVat(new BigDecimal(detailedBillUpdateRqDto.getVat()));
                currentBill.setVatExemptedFlag(BooleanFlag.NO);
                currentBill.setVatNaFlag(BooleanFlag.NO);
        }

        currentBill.setDiscount(detailedBillUpdateRqDto.getDiscount());
        currentBill.setDiscountType(updatedBill.getDiscountType());
        currentBill.setCampaignId(detailedBillUpdateRqDto.getCampaignId());

        if (updatedBill.getBillNumber() != null && !updatedBill.getBillNumber().isEmpty()) {

            Bill existBill = billRepo.findBill(updatedBill.getBillNumber(), entityUser.getEntity());
            if (existBill != null && !existBill.getId().equals(currentBill.getId())) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_already_exist", locale);
            }
            currentBill.setBillNumber(updatedBill.getBillNumber());
        }

        currentBill.setSubAmount(updatedBill.getSubAmount());
        currentBill.setServiceName(updatedBill.getServiceName());
        currentBill.setServiceDescription(updatedBill.getServiceDescription());
        currentBill.setIssueDate(updatedBill.getIssueDate());
        currentBill.setExpireDate(updatedBill.getExpireDate());
        currentBill.setCustomerFullName(updatedBill.getCustomerFullName());
        currentBill.setCustomerEmailAddress(updatedBill.getCustomerEmailAddress());
        currentBill.setCustomerMobileNumber(updatedBill.getCustomerMobileNumber());
        currentBill.setCustomerIdNumber(updatedBill.getCustomerIdNumber());
        currentBill.setEntityActivity(entityActivityRepo.findEntityActivityById(detailedBillUpdateRqDto.getEntityActivityId()));
        currentBill.setCustomerIdType(updatedBill.getCustomerIdType());
        currentBill.setCustomerPreviousBalance(updatedBill.getCustomerPreviousBalance());

        currentBill.setAdditionalNumber(detailedBillUpdateRqDto.getAdditionalNumber());
        currentBill.setBuildingNumber(detailedBillUpdateRqDto.getBuildingNumber());
        currentBill.setCity(cityRepo.findCityById(detailedBillUpdateRqDto.getCityId()));
        currentBill.setDistrict(districtRepo.findDistrictById(detailedBillUpdateRqDto.getDistrictId()));
        currentBill.setPostalCode(detailedBillUpdateRqDto.getPostalCode());
        currentBill.setStreet(detailedBillUpdateRqDto.getStreetName());
        currentBill.setOtherDistrict(detailedBillUpdateRqDto.getOtherDistrict());

        if (currentBill.getAccount() != null) {
            Account account = currentBill.getAccount();
            if (account instanceof IndividualAccount) {
                currentBill.setReferenceNumber(detailedBillUpdateRqDto.getReferenceNumber());
            } else {
                currentBill.setReferenceNumber(detailedBillUpdateRqDto.getContractNumber());
            }
        }

//        currentBill.setIsPartialAllowed(BooleanFlag.getValue(detailedBillUpdateRqDto.getIsPartialAllowed()));
//        if (currentBill.getIsPartialAllowed() != null && currentBill.getIsPartialAllowed().equals(BooleanFlag.YES)) {
//            currentBill.setBillCategory(BillCategory.ONE_OFF_PARTIAL);
//        } else {
//            currentBill.setBillCategory(BillCategory.INSTANTLY);
//            currentBill.setIsPartialAllowed(BooleanFlag.NO);
//        }
        currentBill.setMiniPartialAmount(detailedBillUpdateRqDto.getMiniPartialAmount());
        currentBill.setMaxPartialAmount(detailedBillUpdateRqDto.getMaxPartialAmount());

        if (totalAmount.compareTo(currentBill.getCurrentPaidAmount()) == -1) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "cannot update this invoice with amount less than paid amount", locale);
        }

        currentBill.setRemainAmount(totalAmount.subtract(currentBill.getCurrentPaidAmount()));


        billItemRepo.deleteAllByBill(currentBill);

        Integer line = 1;
        for (BillItemDetailedRqDto billItemDetailedRqDto : detailedBillUpdateRqDto.getBillItemList()) {
            BillItem billItem = (BillItem) billItemDetailedRqDto.toEntity(locale);
            billItem.setBill(currentBill);
            billItem.setNumber(line++);
            billItem = billItemRepo.save(billItem);
        }

        billCustomFieldRepo.deleteAllByBill(currentBill);

        if (detailedBillUpdateRqDto.getCustomFieldList() != null && !detailedBillUpdateRqDto.getCustomFieldList().isEmpty()) {
            for (BillCustomFieldRqDto billCustomFieldRqDto : detailedBillUpdateRqDto.getCustomFieldList()) {

                EntityField entityField = entityFieldRepo.findEntityFieldById(billCustomFieldRqDto.getFieldId());

                if (entityField == null) {
                    continue;
                }

                if (entityField.getStatus().equals(Status.DELETED)) {
                    continue;
                }

                if (entityField.getStatus().equals(Status.INACTIVE)) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "custom field inactive", locale);
                }

                if (!entityField.getEntity().getId().equals(entityUser.getEntity().getId())) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "custom field not related for current biller", locale);
                }

                if (!Utils.isValidCustomField(entityField, billCustomFieldRqDto.getFieldValue())) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "custom field value is not valid", locale);
                }

                BillCustomField billCustomField = new BillCustomField();
                billCustomField.setBill(currentBill);
                billCustomField.setEntityField(entityField);
                billCustomField.setValue(billCustomFieldRqDto.getFieldValue());

                billCustomFieldRepo.save(billCustomField);
            }
        }

        if (detailedBillUpdateRqDto.getCustomerEmailList() != null && !detailedBillUpdateRqDto.getCustomerEmailList().isEmpty()) {
            List<BillEmail> billEmailList = new ArrayList<>();

            billEmailRepo.deleteAllByBill(currentBill);

            for (String email : detailedBillUpdateRqDto.getCustomerEmailList()) {
                BillEmail billEmail = new BillEmail();
                billEmail.setBill(currentBill);
                billEmail.setEmail(email);
                billEmailList.add(billEmail);
            }

            billEmailRepo.saveAll(billEmailList);
        }

        if (detailedBillUpdateRqDto.getCustomerMobileList() != null && !detailedBillUpdateRqDto.getCustomerMobileList().isEmpty()) {
            List<BillMobileNumber> billMobileNumberList = new ArrayList<>();

            billMobileNumberRepo.deleteAllByBill(currentBill);

            for (String mobile : detailedBillUpdateRqDto.getCustomerMobileList()) {
                BillMobileNumber billMobileNumber = new BillMobileNumber();
                billMobileNumber.setBill(currentBill);
                billMobileNumber.setMobileNumber(Utils.formatMobileNumber(mobile));
                billMobileNumberList.add(billMobileNumber);
            }

            billMobileNumberRepo.saveAll(billMobileNumberList);
        }

        if (isBillStatusExist(currentBill, BillStatus.CHECKED)) {
            currentBill.setBillStatus(BillStatus.UPDATED_BY_COMPANY);

            BillTimeline updatedBillTimeline = new BillTimeline();
            updatedBillTimeline.setBillStatus(BillStatus.UPDATED_BY_COMPANY);
            updatedBillTimeline.setBill(currentBill);
            updatedBillTimeline.setActionDate(LocalDateTime.now());
            updatedBillTimeline.setEntityUser(entityUser);
            updatedBillTimeline.setUserReference(entityUser.getUsername());

            updatedBillTimeline = billTimelineRepo.save(updatedBillTimeline);

            if (currentBill.getDueDate() != null && currentBill.getDueDate().isBefore(LocalDateTime.now())) {
                BillTransaction billTransaction = new BillTransaction();
                billTransaction.setBill(currentBill);
                billTransaction.setPreviousAmount(currentBill.getAccount() != null ? currentBill.getAccount().getBalance() : BigDecimal.ZERO);
                billTransaction.setAccount(currentBill.getAccount());
                billTransaction.setAmount(difference);
                billTransaction.setDate(currentBill.getDueDate());
                billTransaction.setBalanceAmount(currentBill.getAccount() != null ? currentBill.getAccount().getBalance().add(difference) : BigDecimal.ZERO);
                billTransaction.setName("Updated Invoice");
                billTransaction.setDocumentNumber(currentBill.getBillNumber());

                billTransaction = billTransactionRepo.save(billTransaction);
            }


            currentBill.setBillStatus(BillStatus.APPROVED_BY_SADAD);

            boolean isSADADAprovedBill = isBillStatusExist(currentBill, BillStatus.APPROVED_BY_SADAD);

            if (!isSADADAprovedBill) {
                BillTimeline acceptedBillTimeline = new BillTimeline();
                acceptedBillTimeline.setBillStatus(BillStatus.APPROVED_BY_SADAD);
                acceptedBillTimeline.setBill(currentBill);
                acceptedBillTimeline.setActionDate(LocalDateTime.now());
                acceptedBillTimeline.setEntityUser(entityUser);
                acceptedBillTimeline.setUserReference(entityUser.getUsername());
                acceptedBillTimeline = billTimelineRepo.save(acceptedBillTimeline);

                currentBill.setApproveDateBySadad(acceptedBillTimeline.getActionDate());
            }

            boolean isViewedBill = isBillStatusExist(currentBill, BillStatus.VIEWED_BY_CUSTOMER);

            if (isViewedBill) {
                BillTimeline acceptedBillTimeline = new BillTimeline();
                acceptedBillTimeline.setBillStatus(BillStatus.VIEWED_BY_CUSTOMER);
                acceptedBillTimeline.setBill(currentBill);
                acceptedBillTimeline.setActionDate(LocalDateTime.now());
                acceptedBillTimeline.setEntityUser(entityUser);
                acceptedBillTimeline.setUserReference(entityUser.getUsername());
                acceptedBillTimeline = billTimelineRepo.save(acceptedBillTimeline);

                currentBill.setBillStatus(BillStatus.VIEWED_BY_CUSTOMER);
            }
        }


        //update bill to sadad if the bill was checked before:
        if (isBillStatusExist(currentBill, BillStatus.CHECKED) && !currentBill.getBillType().equals(BillType.RECURRING_BILL)) {
            Config config = configRepo.findConfigByKey("INTG_FLAG");

            if (config != null && config.getValue().equalsIgnoreCase("true")) {

                WebServiceResponse webServiceResponse = webServiceHelper.uploadBill(currentBill, "BillUpdated");

                if (!webServiceResponse.getStatus().equals(200)) {
                    Document documentXmlResponse = Utils.convertStringToXmlDocument(new JSONObject(webServiceResponse.getBody()).getString("data"));
                    String message = Utils.getElementValueByTagName(documentXmlResponse, "faultstring");
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, message, webServiceResponse.getBody(), locale);
                }
            }
        }

        if (!Utils.isMinimumPartialAmountAllowed(currentBill)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "minimum partial amount should not less than activity minimum amount", locale);
        }

        billRepo.save(currentBill);

        if (currentBill.getBillType().equals(BillType.RECURRING_BILL)) {
            recurringBillJob(currentBill.getAccount());
        }

        return currentBill;
    }

    public Bill saveTaxBill(DetailedBillMakeRqDto detailedBillMakeRqDto, EntityUser entityUser, BillSource billSource, Locale locale) {
        if (!entityUser.getEntityUserType().equals(EntityUserType.OFFICER) && entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "access_officer_user", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }


        if (detailedBillMakeRqDto.getBillNumber() != null && !detailedBillMakeRqDto.getBillNumber().isEmpty()) {
            Bill currentBill = billRepo.findBill(detailedBillMakeRqDto.getBillNumber(), entityUser.getEntity());

            if (currentBill != null) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_already_exist", locale);
            }
        }

        BigDecimal subAmountAfterDiscount = Utils.calculateSubAmountDiscount(detailedBillMakeRqDto.getSubAmount(), detailedBillMakeRqDto.getDiscount(), detailedBillMakeRqDto.getDiscountType());

        LOGGER.info("subAmountAfterDiscount: " + subAmountAfterDiscount);

        BigDecimal totalAmount = Utils.calculateTotalAmountFromVatAndPreviousBalance(subAmountAfterDiscount, detailedBillMakeRqDto.getVat(), detailedBillMakeRqDto.getCustomerPreviousBalance());

        LOGGER.info("totalAmount: " + totalAmount);

        EntityActivity entityActivity = entityActivityRepo.findEntityActivityById(detailedBillMakeRqDto.getEntityActivityId());

        if (entityActivity == null) {
            entityActivity = entityActivityRepo.findEntityActivity(detailedBillMakeRqDto.getEntityActivityCode(), entityUser.getEntity());
        }

        if (entityActivity != null && entityActivity.getStatus().equals(Status.ACTIVE)) {

            LOGGER.info("************************************");
            LOGGER.info("totalAmount: " + totalAmount);

            if (entityActivity.getTheLowestAmountPerUploadedBill() != null) {
                if (totalAmount.compareTo(entityActivity.getTheLowestAmountPerUploadedBill()) == -1) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_amount_min", locale);
                }
            }

            if (entityActivity.getTheHighestAmountPerUploadedBill() != null) {
                //checking bill limit per bill
                if (totalAmount.compareTo(entityActivity.getTheHighestAmountPerUploadedBill()) == 1) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_amount_limit_per_bill", locale);
                }
            }

            if (entityActivity.getMonthlyHighestValueOfExpectedUploadedBills() != null) {
                //checking bill limit per month
                LocalDateTime startDayOfMonth = Utils.getStartDayOfCurrentMonth();
                LocalDateTime currentDay = Utils.getCurrentDateTime();

                LOGGER.info("startDayOfMonth: " + startDayOfMonth);
                LOGGER.info("currentDay: " + currentDay);

                BigDecimal totalAmountForCurrentMonth = billRepo.volumeByApprovedDate(startDayOfMonth, currentDay, entityActivity, entityUser.getEntity());
                if (totalAmountForCurrentMonth == null) {
                    totalAmountForCurrentMonth = new BigDecimal("0");
                }

                LOGGER.info("************************************");
                LOGGER.info("totalAmountForCurrentMonth: " + totalAmountForCurrentMonth);
                LOGGER.info("BillMaxAmountPerBill: " + entityActivity.getMonthlyHighestValueOfExpectedUploadedBills());

                if ((totalAmount.add(totalAmountForCurrentMonth)).compareTo(entityActivity.getMonthlyHighestValueOfExpectedUploadedBills()) == 1) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_amount_limit_per_month", locale);
                }
            }
        } else {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_entity_activity_id", locale);
        }

        Bill detailedBill = (Bill) detailedBillMakeRqDto.toEntity(locale);

        detailedBill.setTotalAmount(totalAmount);
        detailedBill.setTotalRunningAmount(totalAmount);
        detailedBill.setBillType(BillType.TAX_BILL);
        detailedBill.setBillSource(billSource);
        detailedBill.setBillStatus(BillStatus.MAKED);
        detailedBill.setEntity(entityUser.getEntity());
        detailedBill.setEntityUser(entityUser);
        detailedBill.setCreateDate(LocalDateTime.now());
        detailedBill.setEntityActivity(entityActivityRepo.findEntityActivityById(detailedBillMakeRqDto.getEntityActivityId()));
        detailedBill.setCampaignId(detailedBillMakeRqDto.getCampaignId());
        detailedBill.setLogo(entityUser.getEntity().getLogo());
        detailedBill.setStamp(entityUser.getEntity().getStamp());

        detailedBill.setIsPartialAllowed(BooleanFlag.getValue(detailedBillMakeRqDto.getIsPartialAllowed()));
        if (detailedBill.getIsPartialAllowed() != null && detailedBill.getIsPartialAllowed().equals(BooleanFlag.YES)) {
            detailedBill.setBillCategory(BillCategory.ONE_OFF_PARTIAL);
        } else {
            detailedBill.setBillCategory(BillCategory.INSTANTLY);
            detailedBill.setIsPartialAllowed(BooleanFlag.NO);
        }

        detailedBill.setMiniPartialAmount(detailedBillMakeRqDto.getMiniPartialAmount());
        detailedBill.setMaxPartialAmount(detailedBillMakeRqDto.getMaxPartialAmount());
        detailedBill.setPaymentNumber(0);
        detailedBill.setSettlementNumber(0);

        detailedBill.setAdditionalNumber(detailedBillMakeRqDto.getAdditionalNumber());
        detailedBill.setBuildingNumber(detailedBillMakeRqDto.getBuildingNumber());
        detailedBill.setCity(cityRepo.findCityById(detailedBillMakeRqDto.getCityId()));
        detailedBill.setDistrict(districtRepo.findDistrictById(detailedBillMakeRqDto.getDistrictId()));
        detailedBill.setPostalCode(detailedBillMakeRqDto.getPostalCode());
        detailedBill.setStreet(detailedBillMakeRqDto.getStreetName());
        detailedBill.setOtherDistrict(detailedBillMakeRqDto.getOtherDistrict());

        if (detailedBill.getBillNumber() == null || detailedBill.getBillNumber().trim().isEmpty()) {
            if (entityUser.getEntity().getIsAutoGenerateBillNumber().equals(BooleanFlag.YES)) {
                String prefixBillNumber = Utils.generateBillNumber(entityUser.getEntity());
                Long sequence = (entityUser.getEntity().getAutoGenerationBillNumberSequence() + 1);
                entityRepo.increaseBillNumberCounter(sequence, entityUser.getEntity().getId());
                detailedBill.setBillNumber(prefixBillNumber + sequence);
            } else {
                detailedBill.setBillNumber(null);
            }
        } else {
            if (entityUser.getEntity().getIsAutoGenerateBillNumber().equals(BooleanFlag.YES)) {
                Long sequence = (entityUser.getEntity().getAutoGenerationBillNumberSequence() + 1);
                entityRepo.increaseBillNumberCounter(sequence, entityUser.getEntity().getId());
            }
        }

        detailedBill = billRepo.save(detailedBill);

        Integer line = 1;
        for (BillItemDetailedRqDto billItemDetailedRqDto : detailedBillMakeRqDto.getBillItemList()) {
            BillItem billItem = (BillItem) billItemDetailedRqDto.toEntity(locale);
            billItem.setBill(detailedBill);
            billItem.setNumber(line++);
            billItem = billItemRepo.save(billItem);
        }

        Integer runningLine = 1;
        for (BillItemDetailedRqDto billItemDetailedRqDto : detailedBillMakeRqDto.getBillItemList()) {
            RunningTaxBillItem billItem = (RunningTaxBillItem) billItemDetailedRqDto.toRunningTaxBillItemEntity(locale);
            billItem.setBill(detailedBill);
            billItem.setNumber(runningLine++);
            billItem = runningTaxBillItemRepo.save(billItem);
        }
        //----------------

        if (detailedBillMakeRqDto.getCustomFieldList() != null && !detailedBillMakeRqDto.getCustomFieldList().isEmpty()) {
            for (BillCustomFieldRqDto billCustomFieldRqDto : detailedBillMakeRqDto.getCustomFieldList()) {

                EntityField entityField = entityFieldRepo.findEntityFieldById(billCustomFieldRqDto.getFieldId());

                BillCustomField billCustomField = new BillCustomField();
                billCustomField.setBill(detailedBill);
                billCustomField.setEntityField(entityField);
                billCustomField.setValue(billCustomFieldRqDto.getFieldValue());

                billCustomFieldRepo.save(billCustomField);
            }
        }

        BillTimeline makedBillTimeline = new BillTimeline();
        makedBillTimeline.setBillStatus(BillStatus.MAKED);
        makedBillTimeline.setBill(detailedBill);
        makedBillTimeline.setActionDate(LocalDateTime.now());
        makedBillTimeline.setEntityUser(entityUser);
        makedBillTimeline.setUserReference(entityUser.getUsername());

        makedBillTimeline = billTimelineRepo.save(makedBillTimeline);

        if (detailedBillMakeRqDto.getCustomerEmailList() != null && !detailedBillMakeRqDto.getCustomerEmailList().isEmpty()) {
            List<BillEmail> billEmailList = new ArrayList<>();

            for (String email : detailedBillMakeRqDto.getCustomerEmailList()) {
                BillEmail billEmail = new BillEmail();
                billEmail.setBill(detailedBill);
                billEmail.setEmail(email);
                billEmailList.add(billEmail);
            }

            billEmailRepo.saveAll(billEmailList);
        }

        if (detailedBillMakeRqDto.getCustomerMobileList() != null && !detailedBillMakeRqDto.getCustomerMobileList().isEmpty()) {
            List<BillMobileNumber> billMobileNumberList = new ArrayList<>();

            for (String mobile : detailedBillMakeRqDto.getCustomerMobileList()) {
                BillMobileNumber billMobileNumber = new BillMobileNumber();
                billMobileNumber.setBill(detailedBill);
                billMobileNumber.setMobileNumber(Utils.formatMobileNumber(mobile));
                billMobileNumberList.add(billMobileNumber);
            }

            billMobileNumberRepo.saveAll(billMobileNumberList);
        }

        //------------------------- save contact for customer --------------------------------
        if ((detailedBillMakeRqDto.getSaveCustomer() != null) && (detailedBillMakeRqDto.getSaveCustomer().equals("YES"))) {

            //List<Customer> customerList = customerRepo.findCustomerByContactIdTypeAndIdNoAndStatus(detailedBill.getCustomerIdType(), detailedBill.getCustomerIdNumber(), Status.valueOf("ACTIVE"));
            List<Customer> customerList = null;
            if (detailedBill.getCustomerIdNumber() != null && detailedBill.getCustomerIdType() != null) {
                customerList = customerRepo.findCustomerByContactIdTypeAndIdNoAndStatusAndEntity(entityUser.getEntity(), detailedBill.getCustomerIdNumber(), detailedBill.getCustomerIdType(), Status.valueOf("ACTIVE"));
            } else {
                customerList = new ArrayList<>();
            }

            if (customerList == null || customerList.isEmpty()) {
                Customer customer = new Customer();
                customer.setCustomerName(detailedBill.getCustomerFullName());
                customer.setAddress(detailedBill.getCustomerAddress());
                customer.setIdNo(detailedBill.getCustomerIdNumber() != null ? detailedBill.getCustomerIdNumber() : "");
                customer.setEmail(detailedBill.getCustomerEmailAddress());
                customer.setPhoneNo(detailedBill.getCustomerMobileNumber());
                customer.setEntityUser(entityUser);
                customer.setStatus(Status.ACTIVE);
                customer.setTaxRegNo(detailedBill.getCustomerTaxNumber() != null ? detailedBill.getCustomerTaxNumber() : "");
                customer.setContactIdType(detailedBill.getCustomerIdType());
                customerRepo.save(customer);

                if (detailedBillMakeRqDto.getCustomFieldList() != null && !detailedBillMakeRqDto.getCustomFieldList().isEmpty()) {
                    for (BillCustomFieldRqDto billCustomFieldRqDto : detailedBillMakeRqDto.getCustomFieldList()) {

                        EntityField entityField = entityFieldRepo.findEntityFieldById(billCustomFieldRqDto.getFieldId());

                        CustomerCustomField customerCustomField = new CustomerCustomField();
                        customerCustomField.setCustomer(customer);
                        customerCustomField.setEntityField(entityField);
                        customerCustomField.setValue(billCustomFieldRqDto.getFieldValue());

                        customerCustomFieldRepo.save(customerCustomField);
                    }
                }
            } else {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "customer_exist", locale);
            }
        }
        //-------------------------


        if (entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN_PLUS)
                || entityUser.getEntityUserType().equals(EntityUserType.SUPER_ADMIN)
                || entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR_PLUS)) {
            this.checkBill(detailedBill.getId(), entityUser, locale);
        }


        return detailedBill;
    }

    public Bill updateTaxBill(DetailedBillUpdateRqDto detailedBillUpdateRqDto, EntityUser entityUser, Locale locale) {
        if (entityUser.getEntityUserType().equals(EntityUserType.OFFICER)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "update_is_not_allowed_for_officer", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }


        Bill currentBill = billRepo.findBillById(detailedBillUpdateRqDto.getBillId());

        if (currentBill == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
        }

        if (currentBill.getBillStatus().equals(BillStatus.DELETED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_already_deleted", locale);
        }

        if (isBillStatusExist(currentBill, BillStatus.CANCELED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "cannot_update_bill_already_canceled", locale);
        }

        if (isBillStatusExist(currentBill, BillStatus.PAID_BY_SADAD) || isBillStatusExist(currentBill, BillStatus.PAID_BY_COMPANY)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "cannot_update_bill_already_paid", locale);
        }


        BigDecimal subAmountAfterDiscount = Utils.calculateSubAmountDiscount(detailedBillUpdateRqDto.getSubAmount(), detailedBillUpdateRqDto.getDiscount(), detailedBillUpdateRqDto.getDiscountType());

        BigDecimal totalAmount = Utils.calculateTotalAmountFromVatAndPreviousBalance(subAmountAfterDiscount, detailedBillUpdateRqDto.getVat(), detailedBillUpdateRqDto.getCustomerPreviousBalance());

        EntityActivity entityActivity = entityActivityRepo.findEntityActivityById(detailedBillUpdateRqDto.getEntityActivityId());

        if (entityActivity == null) {
            entityActivity = entityActivityRepo.findEntityActivity(detailedBillUpdateRqDto.getEntityActivityCode(), entityUser.getEntity());
        }

        if (entityActivity != null && entityActivity.getStatus().equals(Status.ACTIVE)) {

            LOGGER.info("************************************");
            LOGGER.info("totalAmount: " + totalAmount);

            if (entityActivity.getTheLowestAmountPerUploadedBill() != null) {
                if (totalAmount.compareTo(entityActivity.getTheLowestAmountPerUploadedBill()) == -1) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_amount_min", locale);
                }
            }

            if (entityActivity.getTheHighestAmountPerUploadedBill() != null) {
                //checking bill limit per bill
                if (totalAmount.compareTo(entityActivity.getTheHighestAmountPerUploadedBill()) == 1) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_amount_limit_per_bill", locale);
                }
            }

            if (entityActivity.getMonthlyHighestValueOfExpectedUploadedBills() != null) {
                //checking bill limit per month
                LocalDateTime startDayOfMonth = Utils.getStartDayOfCurrentMonth();
                LocalDateTime currentDay = Utils.getCurrentDateTime();

                LOGGER.info("startDayOfMonth: " + startDayOfMonth);
                LOGGER.info("currentDay: " + currentDay);

                BigDecimal totalAmountForCurrentMonth = billRepo.volumeByApprovedDate(startDayOfMonth, currentDay, entityActivity, entityUser.getEntity());
                if (totalAmountForCurrentMonth == null) {
                    totalAmountForCurrentMonth = new BigDecimal("0");
                }

                LOGGER.info("************************************");
                LOGGER.info("totalAmountForCurrentMonth: " + totalAmountForCurrentMonth);
                LOGGER.info("BillMaxAmountPerBill: " + entityActivity.getMonthlyHighestValueOfExpectedUploadedBills());

                if ((totalAmount.add(totalAmountForCurrentMonth)).compareTo(entityActivity.getMonthlyHighestValueOfExpectedUploadedBills()) == 1) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_amount_limit_per_month", locale);
                }
            }
        } else {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "invalid_entity_activity_id", locale);
        }

        BigDecimal oldTotalAmount = currentBill.getTotalRunningAmount();

        BigDecimal difference = oldTotalAmount.subtract(totalAmount);

        Bill updatedBill = (Bill) detailedBillUpdateRqDto.toEntity(locale);

        currentBill.setTotalRunningAmount(totalAmount);
        currentBill.setEntity(entityUser.getEntity());
        currentBill.setEntityUser(entityUser);

        switch (detailedBillUpdateRqDto.getVat()) {
            case "EXE":
                currentBill.setVat(new BigDecimal("0"));
                currentBill.setVatExemptedFlag(BooleanFlag.YES);
                currentBill.setVatNaFlag(BooleanFlag.NO);
                break;
            case "NA":
                currentBill.setVat(new BigDecimal("0"));
                currentBill.setVatExemptedFlag(BooleanFlag.NO);
                currentBill.setVatNaFlag(BooleanFlag.YES);
                break;
            default:
                currentBill.setVat(new BigDecimal(detailedBillUpdateRqDto.getVat()));
                currentBill.setVatExemptedFlag(BooleanFlag.NO);
                currentBill.setVatNaFlag(BooleanFlag.NO);
        }

        currentBill.setCampaignId(detailedBillUpdateRqDto.getCampaignId());

        if (updatedBill.getBillNumber() != null && !updatedBill.getBillNumber().isEmpty()) {

            Bill existBill = billRepo.findBill(updatedBill.getBillNumber(), entityUser.getEntity());
            if (existBill != null && !existBill.getId().equals(currentBill.getId())) {
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_already_exist", locale);
            }
            currentBill.setBillNumber(updatedBill.getBillNumber());
        }

        currentBill.setServiceName(updatedBill.getServiceName());
        currentBill.setServiceDescription(updatedBill.getServiceDescription());
        currentBill.setIssueDate(updatedBill.getIssueDate());
        currentBill.setExpireDate(updatedBill.getExpireDate());
        currentBill.setCustomerFullName(updatedBill.getCustomerFullName());
        currentBill.setCustomerEmailAddress(updatedBill.getCustomerEmailAddress());
        currentBill.setCustomerMobileNumber(updatedBill.getCustomerMobileNumber());
        currentBill.setCustomerIdNumber(updatedBill.getCustomerIdNumber());
        currentBill.setEntityActivity(entityActivityRepo.findEntityActivityById(detailedBillUpdateRqDto.getEntityActivityId()));
        currentBill.setCustomerIdType(updatedBill.getCustomerIdType());
        currentBill.setCustomerPreviousBalance(updatedBill.getCustomerPreviousBalance());

        currentBill.setIsPartialAllowed(BooleanFlag.getValue(detailedBillUpdateRqDto.getIsPartialAllowed()));
        if (currentBill.getIsPartialAllowed() != null && currentBill.getIsPartialAllowed().equals(BooleanFlag.YES)) {
            currentBill.setBillCategory(BillCategory.ONE_OFF_PARTIAL);
        } else {
            currentBill.setBillCategory(BillCategory.INSTANTLY);
            currentBill.setIsPartialAllowed(BooleanFlag.NO);
        }

        currentBill.setMiniPartialAmount(detailedBillUpdateRqDto.getMiniPartialAmount());
        currentBill.setMaxPartialAmount(detailedBillUpdateRqDto.getMaxPartialAmount());

        if (isBillStatusExist(currentBill, BillStatus.CHECKED)) {
//            currentBill.setBillStatus(BillStatus.UPDATED_BY_COMPANY);
//
//            BillTimeline updatedBillTimeline = new BillTimeline();
//            updatedBillTimeline.setBillStatus(BillStatus.UPDATED_BY_COMPANY);
//            updatedBillTimeline.setBill(currentBill);
//            updatedBillTimeline.setActionDate(LocalDateTime.now());
//            updatedBillTimeline.setEntityUser(entityUser);
//            updatedBillTimeline.setUserReference(entityUser.getUsername());
//
//            updatedBillTimeline = billTimelineRepo.save(updatedBillTimeline);
//
//            currentBill.setBillStatus(BillStatus.APPROVED_BY_SADAD);
//
//            boolean isSADADAprovedBill = isBillStatusExist(currentBill, BillStatus.APPROVED_BY_SADAD);
//
//            if (!isSADADAprovedBill) {
//                BillTimeline acceptedBillTimeline = new BillTimeline();
//                acceptedBillTimeline.setBillStatus(BillStatus.APPROVED_BY_SADAD);
//                acceptedBillTimeline.setBill(currentBill);
//                acceptedBillTimeline.setActionDate(LocalDateTime.now());
//                acceptedBillTimeline.setEntityUser(entityUser);
//                acceptedBillTimeline.setUserReference(entityUser.getUsername());
//                acceptedBillTimeline = billTimelineRepo.save(acceptedBillTimeline);
//
//                currentBill.setApproveDateBySadad(acceptedBillTimeline.getActionDate());
//            }
        }

        if (totalAmount.compareTo(currentBill.getCurrentPaidAmount()) == -1) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "cannot update this invoice with amount less than paid amount", locale);
        }

        currentBill.setRemainAmount(totalAmount.subtract(currentBill.getCurrentPaidAmount()));


        //update bill to sadad if the bill was checked before:
        if (isBillStatusExist(currentBill, BillStatus.CHECKED)) {
            Config config = configRepo.findConfigByKey("INTG_FLAG");

            if (config != null && config.getValue().equalsIgnoreCase("true")) {

                WebServiceResponse webServiceResponse = webServiceHelper.uploadBill(currentBill, "BillUpdated");

                if (!webServiceResponse.getStatus().equals(200)) {
                    Document documentXmlResponse = Utils.convertStringToXmlDocument(new JSONObject(webServiceResponse.getBody()).getString("data"));
                    String message = Utils.getElementValueByTagName(documentXmlResponse, "faultstring");
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, message, webServiceResponse.getBody(), locale);
                }
            }
        }


        //save latest items

        List<RunningTaxBillItem> currentBillItemList = runningTaxBillItemRepo.findAllByBill(currentBill);

        runningTaxBillItemRepo.deleteAllByBill(currentBill);

        List<RunningTaxBillItem> runningTaxBillItemList = new ArrayList<>();

        Integer runningLine = 1;
        for (BillItemDetailedRqDto billItemDetailedRqDto : detailedBillUpdateRqDto.getBillItemList()) {
            RunningTaxBillItem billItem = (RunningTaxBillItem) billItemDetailedRqDto.toRunningTaxBillItemEntity(locale);

            billItem.setBill(currentBill);
            billItem.setNumber(runningLine++);

            if (billItem.getQuantity().doubleValue() == 0) {
                continue;
            }

            runningTaxBillItemList.add(billItem);
        }
        runningTaxBillItemRepo.saveAll(runningTaxBillItemList);
        //----------------

        if (Utils.isNullOrEmpty(detailedBillUpdateRqDto.getDocumentNumber())) {
            detailedBillUpdateRqDto.setDocumentNumber(Utils.generateDocumentNumber(entityUser.getEntity()));
        }

        List<BillNote> billNoteList = billNoteRepo.findAllByBill(detailedBillUpdateRqDto.getDocumentNumber(), entityUser.getEntity());
        if (billNoteList != null && !billNoteList.isEmpty()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "document number already exist", locale);
        }

        BillNote billNote = new BillNote();
        billNote.setBill(currentBill);
        billNote.setDocumentNumber(detailedBillUpdateRqDto.getDocumentNumber());
        billNote.setDate(LocalDateTime.now());
        billNote.setPreviousAmount(oldTotalAmount);
        billNote.setAmount(difference.multiply(new BigDecimal(-1)));
        if (billNote.getAmount().doubleValue() < 0) {
            billNote.setNoteType(NoteType.CREDIT_NOTE);
            billNote.setDescription("Credit Note");
        } else {
            billNote.setNoteType(NoteType.DEBIT_NOTE);
            billNote.setDescription("Debit Note");
        }

        billNote = billNoteRepo.save(billNote);

        List<RunningTaxBillItem> billItemList = new ArrayList<>(currentBillItemList);

        Map<String, RunningTaxBillItem> currentBillItemMap = new HashMap<>();
        //billItemList.stream().collect(Collectors.toMap(item -> item.getName(), item -> item));
        for (RunningTaxBillItem billItem : billItemList) {
            currentBillItemMap.put(billItem.getName(), billItem);
        }

        Map<String, BillItemDetailedRqDto> newBillItemMap = new HashMap<>();
        //detailedBillUpdateRqDto.getBillItemList().stream().collect(Collectors.toMap(item -> item.getName(), item -> item));
        for (BillItemDetailedRqDto billItem : detailedBillUpdateRqDto.getBillItemList()) {
            newBillItemMap.put(billItem.getName(), billItem);
        }

        if (billNote.getAmount().doubleValue() < 0) {
            //credit
            Integer line = 1;

            for (RunningTaxBillItem billItem : billItemList) {
                String itemName = billItem.getName();
                BillItemDetailedRqDto billItemDetailedRqDto = newBillItemMap.get(itemName);

                if (billItemDetailedRqDto == null) {
                    //deleted Item
                    BillNoteItem billNoteItem = new BillNoteItem();
                    billNoteItem.setName(billItem.getName());
                    billNoteItem.setQuantity(billItem.getQuantity());
                    billNoteItem.setUnitPrice(billItem.getUnitPrice());
                    billNoteItem.setTotalPrice(billItem.getUnitPrice().multiply(billItem.getQuantity()));
                    billNoteItem.setDiscount(billItem.getDiscount());
                    billNoteItem.setDiscountType(billItem.getDiscountType());
                    billNoteItem.setVat(billItem.getVat());
                    billNoteItem.setVatExemptedFlag(billItem.getVatExemptedFlag());
                    billNoteItem.setVatNaFlag(billItem.getVatNaFlag());
                    billNoteItem.setBillNote(billNote);
                    billNoteItem.setNumber(line++);

                    if (billNoteItem.getQuantity().doubleValue() == 0) {
                        continue;
                    }

                    billNoteItem = billNoteItemRepo.save(billNoteItem);
                } else {
                    //change quantity
                    BigDecimal quantityDefiance = billItemDetailedRqDto.getQuantity().subtract(billItem.getQuantity());

                    BillNoteItem billNoteItem = new BillNoteItem();
                    billNoteItem.setName(billItemDetailedRqDto.getName());
                    billNoteItem.setQuantity(quantityDefiance.abs());
                    billNoteItem.setUnitPrice(billItemDetailedRqDto.getUnitPrice());
                    billNoteItem.setTotalPrice(billItemDetailedRqDto.getUnitPrice().multiply(quantityDefiance.abs()));
                    billNoteItem.setDiscount(billItemDetailedRqDto.getDiscount());
                    billNoteItem.setDiscountType(billItemDetailedRqDto.getDiscountType());

                    switch (billItemDetailedRqDto.getVat()) {
                        case "EXE":
                            billNoteItem.setVat(new BigDecimal("0"));
                            billNoteItem.setVatExemptedFlag(BooleanFlag.YES);
                            billNoteItem.setVatNaFlag(BooleanFlag.NO);
                            break;
                        case "NA":
                            billNoteItem.setVat(new BigDecimal("0"));
                            billNoteItem.setVatExemptedFlag(BooleanFlag.NO);
                            billNoteItem.setVatNaFlag(BooleanFlag.YES);
                            break;
                        default:
                            billNoteItem.setVat(new BigDecimal(billItemDetailedRqDto.getVat()));
                            billNoteItem.setVatExemptedFlag(BooleanFlag.NO);
                            billNoteItem.setVatNaFlag(BooleanFlag.NO);
                    }

                    billNoteItem.setBillNote(billNote);
                    billNoteItem.setNumber(line++);

                    if (billNoteItem.getQuantity().doubleValue() == 0) {
                        continue;
                    }

                    billNoteItem = billNoteItemRepo.save(billNoteItem);
                }
            }

        } else {
            //debit
            Integer line = 1;
            for (BillItemDetailedRqDto billItemDetailedRqDto : detailedBillUpdateRqDto.getBillItemList()) {
                String itemName = billItemDetailedRqDto.getName();

                RunningTaxBillItem billItem = currentBillItemMap.get(itemName);
                if (billItem == null) {
                    //new Item
                    BillNoteItem billNoteItem = new BillNoteItem();
                    billNoteItem.setName(billItemDetailedRqDto.getName());
                    billNoteItem.setQuantity(billItemDetailedRqDto.getQuantity());
                    billNoteItem.setUnitPrice(billItemDetailedRqDto.getUnitPrice());
                    billNoteItem.setTotalPrice(billItemDetailedRqDto.getUnitPrice().multiply(billItemDetailedRqDto.getQuantity()));
                    billNoteItem.setDiscount(billItemDetailedRqDto.getDiscount());
                    billNoteItem.setDiscountType(billItemDetailedRqDto.getDiscountType());

                    switch (billItemDetailedRqDto.getVat()) {
                        case "EXE":
                            billNoteItem.setVat(new BigDecimal("0"));
                            billNoteItem.setVatExemptedFlag(BooleanFlag.YES);
                            billNoteItem.setVatNaFlag(BooleanFlag.NO);
                            break;
                        case "NA":
                            billNoteItem.setVat(new BigDecimal("0"));
                            billNoteItem.setVatExemptedFlag(BooleanFlag.NO);
                            billNoteItem.setVatNaFlag(BooleanFlag.YES);
                            break;
                        default:
                            billNoteItem.setVat(new BigDecimal(billItemDetailedRqDto.getVat()));
                            billNoteItem.setVatExemptedFlag(BooleanFlag.NO);
                            billNoteItem.setVatNaFlag(BooleanFlag.NO);
                    }

                    billNoteItem.setBillNote(billNote);
                    billNoteItem.setNumber(line++);

                    if (billNoteItem.getQuantity().doubleValue() == 0) {
                        continue;
                    }

                    billNoteItem = billNoteItemRepo.save(billNoteItem);

                } else {
                    //change quantity
                    BigDecimal quantityDefiance = billItemDetailedRqDto.getQuantity().subtract(billItem.getQuantity());

                    BillNoteItem billNoteItem = new BillNoteItem();
                    billNoteItem.setName(billItemDetailedRqDto.getName());
                    billNoteItem.setQuantity(quantityDefiance.abs());
                    billNoteItem.setUnitPrice(billItemDetailedRqDto.getUnitPrice());
                    billNoteItem.setTotalPrice(billItemDetailedRqDto.getUnitPrice().multiply(quantityDefiance.abs()));
                    billNoteItem.setDiscount(billItemDetailedRqDto.getDiscount());
                    billNoteItem.setDiscountType(billItemDetailedRqDto.getDiscountType());

                    switch (billItemDetailedRqDto.getVat()) {
                        case "EXE":
                            billNoteItem.setVat(new BigDecimal("0"));
                            billNoteItem.setVatExemptedFlag(BooleanFlag.YES);
                            billNoteItem.setVatNaFlag(BooleanFlag.NO);
                            break;
                        case "NA":
                            billNoteItem.setVat(new BigDecimal("0"));
                            billNoteItem.setVatExemptedFlag(BooleanFlag.NO);
                            billNoteItem.setVatNaFlag(BooleanFlag.YES);
                            break;
                        default:
                            billNoteItem.setVat(new BigDecimal(billItemDetailedRqDto.getVat()));
                            billNoteItem.setVatExemptedFlag(BooleanFlag.NO);
                            billNoteItem.setVatNaFlag(BooleanFlag.NO);
                    }

                    billNoteItem.setBillNote(billNote);
                    billNoteItem.setNumber(line++);

                    if (billNoteItem.getQuantity().doubleValue() == 0) {
                        continue;
                    }

                    billNoteItem = billNoteItemRepo.save(billNoteItem);
                }
            }
        }

        billRepo.save(currentBill);

        return currentBill;
    }

    public Bill checkBill(Long billId, EntityUser entityUser, Locale locale) {

        if (!entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR) && entityUser.getEntityUserType().equals(EntityUserType.OFFICER)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "cannot_access_for_officer", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }


        Bill bill = billRepo.findBillById(billId);

        if (bill == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
        }

        if (bill.getBillStatus().equals(BillStatus.DELETED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
        }

        if (!bill.getEntity().getId().equals(entityUser.getEntity().getId())) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_for_company", locale);
        }

        if (isBillStatusExist(bill, BillStatus.CHECKED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_already_checked", locale);
        }

        EntityActivity entityActivity = bill.getEntityActivity();
        if (entityActivity != null) {

            LOGGER.info("************************************");
            LOGGER.info("totalAmount: " + bill.getTotalAmount());
            LOGGER.info("BillMaxAmountPerBill: " + entityActivity.getTheHighestAmountPerUploadedBill());

            if (entityActivity.getTheHighestAmountPerUploadedBill() != null) {
                //checking bill limit per bill
                if (bill.getTotalAmount().compareTo(entityActivity.getTheHighestAmountPerUploadedBill()) == 1) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_amount_limit_per_bill", locale);
                }
            }

            if (entityActivity.getMonthlyHighestValueOfExpectedUploadedBills() != null) {
                //checking bill limit per month
                LocalDateTime startDayOfMonth = Utils.getStartDayOfCurrentMonth();
                LocalDateTime currentDay = Utils.getCurrentDateTime();

                LOGGER.info("startDayOfMonth: " + startDayOfMonth);
                LOGGER.info("currentDay: " + currentDay);

                BigDecimal totalAmountForCurrentMonth = billRepo.volumeByApprovedDate(startDayOfMonth, currentDay, entityActivity, entityUser.getEntity());
                if (totalAmountForCurrentMonth == null) {
                    totalAmountForCurrentMonth = new BigDecimal("0");
                }

                LOGGER.info("************************************");
                LOGGER.info("totalAmountForCurrentMonth: " + totalAmountForCurrentMonth);
                LOGGER.info("BillMaxAmountPerBill: " + entityActivity.getMonthlyHighestValueOfExpectedUploadedBills());

                if ((bill.getTotalAmount().add(totalAmountForCurrentMonth)).compareTo(entityActivity.getMonthlyHighestValueOfExpectedUploadedBills()) == 1) {
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_amount_limit_per_month", locale);
                }
            }
        }

        bill.setBillStatus(BillStatus.CHECKED);

        BillTimeline checkedBillTimeline = new BillTimeline();
        checkedBillTimeline.setBillStatus(BillStatus.CHECKED);
        checkedBillTimeline.setBill(bill);
        checkedBillTimeline.setActionDate(LocalDateTime.now());
        checkedBillTimeline.setEntityUser(entityUser);
        checkedBillTimeline.setUserReference(entityUser.getUsername());

        billTimelineRepo.save(checkedBillTimeline);

        if (bill.getEntityActivity() == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "activity_id_not_found", locale);
        }

        if (bill.getSadadNumber() == null) {
            String sadadNumber = (bill.getEntity().getCode()) + (bill.getEntityActivity().getCode()) + (bill.getEntity().getBillSequence() + 1);

            bill.setSadadNumber(sadadNumber);

            entityRepo.increaseSadadNumberCounter(bill.getEntity().getBillSequence() + 1, bill.getEntity().getId());
        }


        if (bill.getBillType().equals(BillType.RECURRING_BILL)) {

            recurringBillJob(bill.getAccount());

        } else {
            Config config = configRepo.findConfigByKey("INTG_FLAG");

            if (config != null && config.getValue().equalsIgnoreCase("true")) {

                WebServiceResponse webServiceResponse = webServiceHelper.uploadBill(bill, "BillNew");

                if (!webServiceResponse.getStatus().equals(200)) {
                    Document documentXmlResponse = Utils.convertStringToXmlDocument(new JSONObject(webServiceResponse.getBody()).getString("data"));
                    String message = Utils.getElementValueByTagName(documentXmlResponse, "faultstring");
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, message, webServiceResponse.getBody(), locale);
                }
            }
        }

        BillTimeline uploadedToSadadBillTimeline = new BillTimeline();
        uploadedToSadadBillTimeline.setBillStatus(BillStatus.UPLOADED_TO_SADAD);
        uploadedToSadadBillTimeline.setBill(bill);
        uploadedToSadadBillTimeline.setActionDate(LocalDateTime.now());
        uploadedToSadadBillTimeline.setEntityUser(entityUser);
        uploadedToSadadBillTimeline.setUserReference(entityUser.getUsername());
        billTimelineRepo.save(uploadedToSadadBillTimeline);

        BillTimeline approvedBySadadBillTimeline = new BillTimeline();
        approvedBySadadBillTimeline.setBillStatus(BillStatus.APPROVED_BY_SADAD);
        approvedBySadadBillTimeline.setBill(bill);
        approvedBySadadBillTimeline.setActionDate(LocalDateTime.now());
        approvedBySadadBillTimeline.setEntityUser(null);
        approvedBySadadBillTimeline.setUserReference("SADAD");
        billTimelineRepo.save(approvedBySadadBillTimeline);

        bill.setBillStatus(BillStatus.APPROVED_BY_SADAD);
        bill.setApproveDateBySadad(approvedBySadadBillTimeline.getActionDate());

        return bill;
    }

    public void checkRecurringBill(Bill bill, EntityUser entityUser, Account account, Locale locale) {

        if (!entityUser.getEntityUserType().equals(EntityUserType.SUPERVISOR) && entityUser.getEntityUserType().equals(EntityUserType.OFFICER)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "cannot_access_for_officer", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }


        if (bill == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
        }

        if (bill.getBillStatus().equals(BillStatus.DELETED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
        }

        if (!bill.getEntity().getId().equals(entityUser.getEntity().getId())) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_for_company", locale);
        }

        if (isBillStatusExist(bill, BillStatus.CHECKED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_already_checked", locale);
        }

        bill.setBillStatus(BillStatus.CHECKED);

        BillTimeline checkedBillTimeline = new BillTimeline();
        checkedBillTimeline.setBillStatus(BillStatus.CHECKED);
        checkedBillTimeline.setBill(bill);
        checkedBillTimeline.setActionDate(LocalDateTime.now());
        checkedBillTimeline.setEntityUser(entityUser);
        checkedBillTimeline.setUserReference(entityUser.getUsername());

        billTimelineRepo.save(checkedBillTimeline);

        if (bill.getEntityActivity() == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "activity_id_not_found", locale);
        }

        BillTimeline uploadedToSadadBillTimeline = new BillTimeline();
        uploadedToSadadBillTimeline.setBillStatus(BillStatus.UPLOADED_TO_SADAD);
        uploadedToSadadBillTimeline.setBill(bill);
        uploadedToSadadBillTimeline.setActionDate(LocalDateTime.now());
        uploadedToSadadBillTimeline.setEntityUser(entityUser);
        uploadedToSadadBillTimeline.setUserReference(entityUser.getUsername());
        billTimelineRepo.save(uploadedToSadadBillTimeline);

        BillTimeline approvedBySadadBillTimeline = new BillTimeline();
        approvedBySadadBillTimeline.setBillStatus(BillStatus.APPROVED_BY_SADAD);
        approvedBySadadBillTimeline.setBill(bill);
        approvedBySadadBillTimeline.setActionDate(LocalDateTime.now());
        approvedBySadadBillTimeline.setEntityUser(null);
        approvedBySadadBillTimeline.setUserReference("SADAD");
        billTimelineRepo.save(approvedBySadadBillTimeline);

        bill.setBillStatus(BillStatus.APPROVED_BY_SADAD);
        bill.setApproveDateBySadad(approvedBySadadBillTimeline.getActionDate());

        if (bill.getDueDate() != null && bill.getDueDate().isBefore(LocalDateTime.now())) {

            BillTransaction billTransaction = new BillTransaction();
            billTransaction.setBill(bill);
            billTransaction.setPreviousAmount(account.getBalance());
            billTransaction.setAccount(account);
            billTransaction.setAmount(bill.getTotalAmount());
            billTransaction.setDate(bill.getDueDate());
            billTransaction.setBalanceAmount(bill.getTotalAmount().add(account.getBalance()));
            billTransaction.setName("Issued Invoice");
            billTransaction.setDocumentNumber(bill.getBillNumber());

            billTransaction = billTransactionRepo.save(billTransaction);
        }

        recurringBillJob(account);
    }

    public Bill deleteBill(Long billId, EntityUser entityUser, Locale locale) {

//        if (!entityUser.getEntityUserType().equals(EntityUserType.OFFICER)) {
//            throw new HttpServiceException(HttpStatus.FORBIDDEN, "delete_is_not_allowed_for_officer", locale);
//        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }


        Bill bill = billRepo.findBillById(billId);

        if (bill == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
        }

        if (bill.getBillStatus().equals(BillStatus.DELETED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
        }

        if (!bill.getEntity().getId().equals(entityUser.getEntity().getId())) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_for_company", locale);
        }

        if (!bill.getBillStatus().equals(BillStatus.MAKED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "can_delete_maked_bill_only", locale);
        }

        bill.setBillStatus(BillStatus.DELETED);

        BillTimeline deletedBillTimeline = new BillTimeline();
        deletedBillTimeline.setBillStatus(BillStatus.DELETED);
        deletedBillTimeline.setBill(bill);
        deletedBillTimeline.setActionDate(LocalDateTime.now());
        deletedBillTimeline.setEntityUser(entityUser);
        deletedBillTimeline.setUserReference(entityUser.getUsername());

        billTimelineRepo.save(deletedBillTimeline);

        return bill;
    }

    public Bill cancelBill(Long billId, EntityUser entityUser, Locale locale) {

        if (entityUser.getEntityUserType().equals(EntityUserType.OFFICER)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "update_is_allowed_for_officer", locale);
        }

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }


        Bill bill = billRepo.findBillById(billId);

        if (bill == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
        }

        if (bill.getBillStatus().equals(BillStatus.DELETED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
        }

        if (!bill.getEntity().getId().equals(entityUser.getEntity().getId())) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_for_company", locale);
        }

        if (!isBillStatusExist(bill, BillStatus.CHECKED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "cannot_cancel_non_checked_bill", locale);
        }

        if (isBillStatusExist(bill, BillStatus.CANCELED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_already_canceled", locale);
        }

        if (isBillStatusExist(bill, BillStatus.PAID_BY_COMPANY) || isBillStatusExist(bill, BillStatus.PAID_BY_SADAD)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "cannot_cancel_paid_bill", locale);
        }

        if (isBillStatusExist(bill, BillStatus.PARTIALLY_PAID_BY_COMPANY) || isBillStatusExist(bill, BillStatus.PARTIALLY_PAID_BY_SADAD)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "cannot cancel partially paid bill", locale);
        }

        bill.setBillStatus(BillStatus.CANCELED);
        bill.setExpireDate(LocalDate.now().atStartOfDay());

        if (bill.getBillType().equals(BillType.RECURRING_BILL)) {

            if (bill.getDueDate() != null && bill.getDueDate().isBefore(LocalDateTime.now())) {
                BillTransaction billTransaction = new BillTransaction();
                billTransaction.setBill(bill);
                billTransaction.setPreviousAmount(bill.getAccount().getBalance());
                billTransaction.setAccount(bill.getAccount());
                billTransaction.setAmount(bill.getTotalAmount().multiply(BigDecimal.valueOf(-1)));
                billTransaction.setDate(bill.getDueDate());
                billTransaction.setBalanceAmount(bill.getTotalAmount().subtract(bill.getAccount().getBalance()).abs());
                billTransaction.setName("Canceled Invoice");
                billTransaction.setDocumentNumber(bill.getBillNumber());

                billTransaction = billTransactionRepo.save(billTransaction);
            }


            recurringBillJob(bill.getAccount());

        } else {
            Config config = configRepo.findConfigByKey("INTG_FLAG");

            if (config != null && config.getValue().equalsIgnoreCase("true")) {

                WebServiceResponse webServiceResponse = webServiceHelper.uploadBill(bill, "BillExpired");

                if (!webServiceResponse.getStatus().equals(200)) {
                    Document documentXmlResponse = Utils.convertStringToXmlDocument(new JSONObject(webServiceResponse.getBody()).getString("data"));
                    String message = Utils.getElementValueByTagName(documentXmlResponse, "faultstring");
                    throw new HttpServiceException(HttpStatus.BAD_REQUEST, message, webServiceResponse.getBody(), locale);
                }
            }
        }

        BillTimeline cancelledBillTimeline = new BillTimeline();
        cancelledBillTimeline.setBillStatus(BillStatus.CANCELED);
        cancelledBillTimeline.setBill(bill);
        cancelledBillTimeline.setActionDate(LocalDateTime.now());
        cancelledBillTimeline.setEntityUser(entityUser);
        cancelledBillTimeline.setUserReference(entityUser.getUsername());

        billTimelineRepo.save(cancelledBillTimeline);

        return bill;
    }

    public void cancelAllExpireBill() {

        LOGGER.info("Start Cancel Bill JOB: ");

        LocalDateTime localDateTime = LocalDate.now().atStartOfDay();

        LOGGER.info("localDateTime: " + localDateTime);

        List<Bill> billList = billRepo.findAllExpiredBill(localDateTime);

        LOGGER.info("billList: " + billList.size());

        Config config = configRepo.findConfigByKey("INTG_FLAG");

        List<BillTimeline> completeBillTimeLineList = new ArrayList<BillTimeline>();

        for (Bill bill : billList) {

            LOGGER.info("ID: " + bill.getId() + " expireDate " + bill.getExpireDate());

            bill.setBillStatus(BillStatus.CANCELED);
            bill.setExpireDate(LocalDate.now().atStartOfDay());

            BillTimeline cancelledBillTimeline = new BillTimeline();
            cancelledBillTimeline.setBillStatus(BillStatus.CANCELED);
            cancelledBillTimeline.setBill(bill);
            cancelledBillTimeline.setActionDate(LocalDateTime.now());
            cancelledBillTimeline.setEntityUser(null);
            cancelledBillTimeline.setUserReference("SYSTEM");

//            if (config != null && config.getValue().equalsIgnoreCase("true")) {
//
//                WebServiceResponse webServiceResponse = webServiceHelper.uploadBill(bill, "BillExpired");
//
//                if (!webServiceResponse.getStatus().equals(200)) {
//                    Document documentXmlResponse = Utils.convertStringToXmlDocument(new JSONObject(webServiceResponse.getBody()).getString("data"));
//                    String message = Utils.getElementValueByTagName(documentXmlResponse, "faultstring");
//                    continue;
//                }
//            }

            completeBillTimeLineList.add(cancelledBillTimeline);
        }

        billTimelineRepo.saveAll(completeBillTimeLineList);

        LOGGER.info("End Cancel Bill JOB: ");
    }

    public void deleteAllExpireBill() {
//        LOGGER.info("Start Delete Bill JOB: ");
//
//        LocalDateTime localDateTime = LocalDate.now().atStartOfDay();
//
//        LOGGER.info("localDateTime: " + localDateTime);

//        List<Bill> billList = billRepo.findAllExpiredBill(localDateTime);
//
//        LOGGER.info("billList: " + billList.size());
//
//        List<BillTimeline> completeBillTimeLineList = new ArrayList<BillTimeline>();
//
//        for (Bill bill : billList) {
//
//            LOGGER.info("ID: " + bill.getId() + " expireDate " + bill.getExpireDate());
//
//            bill.setBillStatus(BillStatus.DELETED);
//
//            BillTimeline cancelledBillTimeline = new BillTimeline();
//            cancelledBillTimeline.setBillStatus(BillStatus.DELETED);
//            cancelledBillTimeline.setBill(bill);
//            cancelledBillTimeline.setActionDate(LocalDateTime.now());
//            cancelledBillTimeline.setEntityUser(null);
//            cancelledBillTimeline.setUserReference("SYSTEM");
//
//            completeBillTimeLineList.add(cancelledBillTimeline);
//        }
//
//        billTimelineRepo.saveAll(completeBillTimeLineList);

        LOGGER.info("End Cancel Bill JOB: ");
    }

    public void recurringBillJob() {

        //find all accounts
        List<Account> accountList = accountRepo.findAll();

        LocalDateTime dueDate = LocalDateTime.now();

        //find all recurring bills that entered in due date and account is not null ....
        for (Account account : accountList) {

            BigDecimal totalRemainingBalance = billRepo.sumOfRemaningDueBillsTest(dueDate, account);

            LOGGER.info("totalRemainingBalance:: " + totalRemainingBalance);

            if (totalRemainingBalance != null) {

                BigDecimal totalPaymentBalance = sadadPaymentRepo.sumOfRemaningPayment(account);

                if (totalPaymentBalance == null) {
                    totalPaymentBalance = BigDecimal.ZERO;
                }

                Integer cycleNumber = account.getCycleNumber();
                cycleNumber = cycleNumber + 1;
                account.setCycleNumber(cycleNumber);
                accountRepo.increaseCycleNumberCounter(cycleNumber, account.getId());

                account.setBalance(totalRemainingBalance);
                accountRepo.updateBalance(totalRemainingBalance, account.getId());

                LOGGER.info("totalRemainingBalance: " + totalRemainingBalance + " for account: " + account.getAccountNumber());

                Bill bill = new Bill();
                bill.setBillCategory(BillCategory.RECURRING_ACCOUNT_PAR);
                bill.setSadadNumber(account.getAccountNumber());
                bill.setCycleNumber(cycleNumber);
                bill.setTotalRunningAmount(totalRemainingBalance.add(totalPaymentBalance));
                bill.setDueDate(LocalDateTime.now());

                WebServiceResponse webServiceResponse = webServiceHelper.uploadRecurringBill(bill, "BillCreate");

                LOGGER.info("Recurring webServiceResponse status: " + webServiceResponse.getStatus());
                LOGGER.info("Recurring webServiceResponse body: " + webServiceResponse.getBody());

            } else {
                totalRemainingBalance = BigDecimal.ZERO;

                Integer cycleNumber = account.getCycleNumber();
                cycleNumber = cycleNumber + 1;
                account.setCycleNumber(cycleNumber);
                accountRepo.increaseCycleNumberCounter(cycleNumber, account.getId());

                account.setBalance(totalRemainingBalance);
                accountRepo.updateBalance(totalRemainingBalance, account.getId());

                LOGGER.info("totalRemainingBalance: " + totalRemainingBalance + " for account: " + account.getAccountNumber());

                Bill bill = new Bill();
                bill.setBillCategory(BillCategory.RECURRING_ACCOUNT_PAR);
                bill.setSadadNumber(account.getAccountNumber());
                bill.setCycleNumber(cycleNumber);
                bill.setTotalRunningAmount(totalRemainingBalance);
                bill.setDueDate(LocalDateTime.now());

                WebServiceResponse webServiceResponse = webServiceHelper.uploadRecurringBill(bill, "BillCreate");

                LOGGER.info("Recurring webServiceResponse status: " + webServiceResponse.getStatus());
                LOGGER.info("Recurring webServiceResponse body: " + webServiceResponse.getBody());
            }
        }


    }

    public void recurringBillJob(Account account) {

        LocalDateTime dueDate = LocalDateTime.now();

        BigDecimal totalRemainingBalance = billRepo.sumOfRemaningDueBillsTest(dueDate, account);

        LOGGER.info("totalRemainingBalance:: " + totalRemainingBalance);

        if (totalRemainingBalance != null) {

            BigDecimal totalPaymentBalance = sadadPaymentRepo.sumOfRemaningPayment(account);

            if (totalPaymentBalance == null) {
                totalPaymentBalance = BigDecimal.ZERO;
            }

            Integer cycleNumber = account.getCycleNumber();
            cycleNumber = cycleNumber + 1;
            account.setCycleNumber(cycleNumber);
            accountRepo.increaseCycleNumberCounter(cycleNumber, account.getId());

            account.setBalance(totalRemainingBalance);
            accountRepo.updateBalance(totalRemainingBalance, account.getId());

            LOGGER.info("totalRemainingBalance: " + totalRemainingBalance + " for account: " + account.getAccountNumber());

            Bill bill = new Bill();
            bill.setBillCategory(BillCategory.RECURRING_ACCOUNT_PAR);
            bill.setSadadNumber(account.getAccountNumber());
            bill.setCycleNumber(cycleNumber);
            bill.setTotalRunningAmount(totalRemainingBalance.add(totalPaymentBalance));
            bill.setDueDate(LocalDateTime.now());
            bill.setMiniPartialAmount(account.getMiniPartialAmount() != null ? account.getMiniPartialAmount() : totalRemainingBalance.add(totalPaymentBalance));

            WebServiceResponse webServiceResponse = webServiceHelper.uploadRecurringBill(bill, "BillCreate");

            LOGGER.info("Recurring webServiceResponse status: " + webServiceResponse.getStatus());
            LOGGER.info("Recurring webServiceResponse body: " + webServiceResponse.getBody());

        } else {

            totalRemainingBalance = BigDecimal.ZERO;

            Integer cycleNumber = account.getCycleNumber();
            cycleNumber = cycleNumber + 1;
            account.setCycleNumber(cycleNumber);
            accountRepo.increaseCycleNumberCounter(cycleNumber, account.getId());

            account.setBalance(totalRemainingBalance);
            accountRepo.updateBalance(totalRemainingBalance, account.getId());

            LOGGER.info("totalRemainingBalance: " + totalRemainingBalance + " for account: " + account.getAccountNumber());

            Bill bill = new Bill();
            bill.setBillCategory(BillCategory.RECURRING_ACCOUNT_PAR);
            bill.setSadadNumber(account.getAccountNumber());
            bill.setCycleNumber(cycleNumber);
            bill.setTotalRunningAmount(totalRemainingBalance);
            bill.setDueDate(LocalDateTime.now());
            bill.setMiniPartialAmount(account.getMiniPartialAmount() != null ? account.getMiniPartialAmount() : totalRemainingBalance);

            WebServiceResponse webServiceResponse = webServiceHelper.uploadRecurringBill(bill, "BillCreate");

            LOGGER.info("Recurring webServiceResponse status: " + webServiceResponse.getStatus());
            LOGGER.info("Recurring webServiceResponse body: " + webServiceResponse.getBody());
        }

    }

    public void setApprovedDateBySadad() {

        List<Bill> billList = billRepo.findAll();

        for (Bill bill : billList) {
            List<BillTimeline> billTimeline = billTimelineRepo.findBillTimeline(bill, BillStatus.APPROVED_BY_SADAD);

            if (billTimeline == null || billTimeline.isEmpty()) {
                continue;
            }
            bill.setApproveDateBySadad(billTimeline.get(0).getActionDate());
        }
    }

    public boolean isBillStatusExist(Bill bill, BillStatus billStatus) {

        BillTimeline checkTimeline = billTimelineRepo.findFirstBillTimelineByBillAndBillStatus(bill, billStatus);

        if (checkTimeline == null) {
            return false;
        } else
            return true;
    }

    public BillResultTimelineDtoRs findBillTimeline(Long billId, Locale locale) {

        Bill bill = billRepo.findBillById(billId);

        if (bill == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
        }

        if (bill.getBillStatus().equals(BillStatus.DELETED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
        }

        List<BillTimeline> billTimelineList = billTimelineRepo.findBillTimelineByBill(bill);

        List<BillTimelineDtoRs> billTimelineDtoRsList = new ArrayList<>();
        billTimelineList.forEach(billTimeline -> {
            billTimelineDtoRsList.add((BillTimelineDtoRs) billTimeline.toDto(locale));
        });

        BillResultTimelineDtoRs billResultTimelineDtoRs = new BillResultTimelineDtoRs();

        billResultTimelineDtoRs.setBillTimeline(billTimelineDtoRsList);
        billResultTimelineDtoRs.setBillNumber(bill.getBillNumber());
        billResultTimelineDtoRs.setSadadNumber(bill.getSadadNumber());
        billResultTimelineDtoRs.setCustomerEmailAddress(bill.getCustomerEmailAddress());
        billResultTimelineDtoRs.setCustomerMobileNumber(bill.getCustomerMobileNumber());
        billResultTimelineDtoRs.setCustomerFullName(bill.getCustomerFullName());
        billResultTimelineDtoRs.setCustomerIdNumber(bill.getCustomerIdNumber());
        billResultTimelineDtoRs.setExpireDate(bill.getExpireDate());
        billResultTimelineDtoRs.setIssueDate(bill.getIssueDate());
        billResultTimelineDtoRs.setSubAmount(bill.getSubAmount());
        billResultTimelineDtoRs.setVat(bill.getVat());
        billResultTimelineDtoRs.setTotalAmount(bill.getTotalRunningAmount());
        billResultTimelineDtoRs.setDiscount(bill.getDiscount());
        billResultTimelineDtoRs.setServiceName(bill.getServiceName());
        billResultTimelineDtoRs.setServiceDescription(bill.getServiceDescription());

        billResultTimelineDtoRs.setPaymentNumber(bill.getPaymentNumber());
        billResultTimelineDtoRs.setSettlementNumber(bill.getSettlementNumber());
        billResultTimelineDtoRs.setRemainAmount(bill.getRemainAmount());
        billResultTimelineDtoRs.setCurrentPaidAmount(bill.getCurrentPaidAmount());

        billResultTimelineDtoRs.setIsPartialAllowed(bill.getIsPartialAllowed() != null ? bill.getIsPartialAllowed().name() : null);
        billResultTimelineDtoRs.setBillCategory(bill.getBillCategory() != null ? bill.getBillCategory().name() : null);

        List<SadadPayment> sadadPaymentList = sadadPaymentRepo.findSadadPaymentByBillOrderByIdAsc(bill);
        List<LocalPayment> localPaymentList = localPaymentRepo.findLocalPaymentByBillOrderByIdAsc(bill);


        BigDecimal totalOfflinePaidAmount = BigDecimal.ZERO;
        BigDecimal totalOnlinePaidAmount = BigDecimal.ZERO;

        List<PaymentDtoRs> paymentDtoRsList = new ArrayList<>();

        if (sadadPaymentList != null && !sadadPaymentList.isEmpty()) {
            for (SadadPayment sadadPayment : sadadPaymentList) {
                PaymentDtoRs paymentDtoRs = new PaymentDtoRs();
                paymentDtoRs.setBillNumber(bill.getBillNumber());
                paymentDtoRs.setSadadNumber(bill.getSadadNumber());
                paymentDtoRs.setSadadPaymentId(sadadPayment.getSadadPaymentId());
                paymentDtoRs.setPaymentDate(Utils.parseDateTime(sadadPayment.getPaymentDate(), "yyyy-MM-dd"));
                paymentDtoRs.setPaymentType("ONLINE");
                paymentDtoRs.setPaymentMethod("SADAD");
                paymentDtoRs.setPaymentAmount(sadadPayment.getPaymentAmount());
                paymentDtoRs.setRemainingAmount(sadadPayment.getRemainingAmount());
                paymentDtoRs.setPaymentStatus(sadadPayment.getPaymentStatus().name());
                paymentDtoRs.setUserReference("NA");
                paymentDtoRs.setPaymentImage(sadadPayment.getPaymentImage());
                paymentDtoRs.setTotalAmountAr(sadadPayment.getTotalAmountAr());
                paymentDtoRs.setTotalAmountEn(sadadPayment.getTotalAmountEn());
                paymentDtoRs.setPaymentAmountAr(sadadPayment.getPaymentAmountAr());
                paymentDtoRs.setPaymentAmountEn(sadadPayment.getPaymentAmountAr());
                paymentDtoRs.setBillAmount(bill.getTotalAmount());
                paymentDtoRs.setCustomerName(bill.getCustomerFullName());
                paymentDtoRs.setActivityCode(bill.getEntityActivity().getCode());
                paymentDtoRs.setVoucherNumber(sadadPayment.getVoucherNumber() + "");
                paymentDtoRs.setPaymentNumber(sadadPayment.getPaymentNumber());
                paymentDtoRs.setVoucherDate(sadadPayment.getPaymentDate());
                paymentDtoRsList.add(paymentDtoRs);

                totalOnlinePaidAmount = totalOnlinePaidAmount.add(sadadPayment.getPaymentAmount());
            }
        }

        if (localPaymentList != null && !localPaymentList.isEmpty()) {
            for (LocalPayment localPayment : localPaymentList) {
                PaymentDtoRs paymentDtoRs = new PaymentDtoRs();
                paymentDtoRs.setBillNumber(bill.getBillNumber());
                paymentDtoRs.setSadadNumber(bill.getSadadNumber());
                paymentDtoRs.setSadadPaymentId(localPayment.getSadadPaymentId());
                paymentDtoRs.setPaymentDate(Utils.parseDateTime(localPayment.getPaymentDate(), "yyyy-MM-dd"));
                paymentDtoRs.setPaymentType("OFFLINE");
                paymentDtoRs.setPaymentMethod(localPayment.getPaymentMethod() != null ? localPayment.getPaymentMethod().name() : null);
                paymentDtoRs.setPaymentAmount(localPayment.getPaymentAmount());
                paymentDtoRs.setRemainingAmount(localPayment.getRemainingAmount());
                paymentDtoRs.setPaymentStatus(localPayment.getPaymentStatus().name());
                paymentDtoRs.setUserReference(localPayment.getEntityUser() != null ? localPayment.getEntityUser().getUsername() : null);
                paymentDtoRs.setPaymentImage(localPayment.getPaymentImage());
                paymentDtoRs.setTotalAmountAr(localPayment.getTotalAmountAr());
                paymentDtoRs.setTotalAmountEn(localPayment.getTotalAmountEn());
                paymentDtoRs.setPaymentAmountAr(localPayment.getPaymentAmountAr());
                paymentDtoRs.setPaymentAmountEn(localPayment.getPaymentAmountAr());
                paymentDtoRs.setBillAmount(bill.getTotalAmount());
                paymentDtoRs.setCustomerName(bill.getCustomerFullName());
                paymentDtoRs.setActivityCode(bill.getEntityActivity().getCode());
                paymentDtoRs.setVoucherNumber(localPayment.getVoucherNumber() + "");
                paymentDtoRs.setPaymentNumber(localPayment.getPaymentNumber());
                paymentDtoRs.setVoucherDate(localPayment.getCreateDate());
                paymentDtoRsList.add(paymentDtoRs);

                totalOfflinePaidAmount = totalOfflinePaidAmount.add(localPayment.getPaymentAmount());
            }
        }

        Collections.sort(paymentDtoRsList);

        billResultTimelineDtoRs.setPaymentList(paymentDtoRsList);
        billResultTimelineDtoRs.setOnlinePaymentList(sadadPaymentList);
        billResultTimelineDtoRs.setOfflinePaymentList(localPaymentList);

        billResultTimelineDtoRs.setTotalOfflinePaidAmount(totalOfflinePaidAmount);
        billResultTimelineDtoRs.setTotalOnlinePaidAmount(totalOnlinePaidAmount);


        billResultTimelineDtoRs.setInvoiceLogo(bill.getLogo());
        billResultTimelineDtoRs.setInvoiceStamp(bill.getStamp());

        List<BillNote> billNoteList = billNoteRepo.findAllBillNote(bill);
        List<BillNoteDtoRs> billNoteDtoRsList = new ArrayList<>();

        BillNoteDtoRs billNoteDtoRs = new BillNoteDtoRs();

        billNoteDtoRs.setId(0L);
        billNoteDtoRs.setDocumentNumber(bill.getSadadNumber());
        billNoteDtoRs.setDate(bill.getCreateDate());
        billNoteDtoRs.setNoteType(NoteType.INVOICE_NOTE);
        billNoteDtoRs.setDescription(Utf8ResourceBundle.getString(billNoteDtoRs.getNoteType().name(), locale));
        billNoteDtoRs.setAmount(bill.getTotalAmount());
        billNoteDtoRs.setBalanceAmount(bill.getTotalAmount());
        billNoteDtoRs.setItemList(new ArrayList<>());

        billNoteDtoRsList.add(billNoteDtoRs);

        for (BillNote billNote : billNoteList) {

            BillNoteDtoRs billNoteDtoRsItem = new BillNoteDtoRs();

            billNoteDtoRsItem.setId(0L);
            billNoteDtoRsItem.setDocumentNumber(billNote.getDocumentNumber());
            billNoteDtoRsItem.setDate(billNote.getDate());
            billNoteDtoRsItem.setNoteType(billNote.getNoteType());
            billNoteDtoRsItem.setDescription(Utf8ResourceBundle.getString(billNote.getNoteType().name(), locale));
            billNoteDtoRsItem.setAmount(billNote.getAmount());
            billNoteDtoRsItem.setBalanceAmount(billNote.getBalanceAmount());
            billNoteDtoRsItem.setItemList(billNote.getItemList());

            billNoteDtoRsList.add(billNoteDtoRsItem);
        }

        for (PaymentDtoRs paymentDtoRs : paymentDtoRsList) {

            BillNoteDtoRs billNoteDtoRsItem = new BillNoteDtoRs();

            billNoteDtoRsItem.setId(0L);
            billNoteDtoRsItem.setDocumentNumber(paymentDtoRs.getVoucherNumber());
            billNoteDtoRsItem.setDate(paymentDtoRs.getVoucherDate());
            billNoteDtoRsItem.setNoteType(NoteType.PAYMENT_NOTE);
            billNoteDtoRsItem.setDescription(Utf8ResourceBundle.getString(billNoteDtoRsItem.getNoteType().name(), locale));
            billNoteDtoRsItem.setAmount(paymentDtoRs.getPaymentAmount().multiply(BigDecimal.valueOf(-1)));
            billNoteDtoRsItem.setBalanceAmount(paymentDtoRs.getRemainingAmount());
            billNoteDtoRsItem.setItemList(new ArrayList<>());

            billNoteDtoRsList.add(billNoteDtoRsItem);
        }

        if (bill.getBillType().equals(BillType.RECURRING_BILL)) {

            billResultTimelineDtoRs.setDueDate(bill.getDueDate());

            billResultTimelineDtoRs.setReferenceNumber(bill.getReferenceNumber());
            billResultTimelineDtoRs.setContractNumber(bill.getReferenceNumber());
            Account account = bill.getAccount();

            if (account instanceof IndividualAccount) {
                billResultTimelineDtoRs.setAccount("INDIVIDUAL");
            } else {
                billResultTimelineDtoRs.setAccount("BUSINESS");
            }

        }

        billResultTimelineDtoRs.setBillTypeText(Utf8ResourceBundle.getString(bill.getBillType().name(), locale));

        Collections.sort(billNoteDtoRsList);

        billResultTimelineDtoRs.setBillNoteList(billNoteDtoRsList);

        return billResultTimelineDtoRs;
    }

    public PageDtoRs searchBill(SearchBillRqDto searchBillRqDto, EntityUser entityUser, Locale locale) {

        if (searchBillRqDto.getPage() == null)
            searchBillRqDto.setPage(0);
        if (searchBillRqDto.getSize() == null)
            searchBillRqDto.setSize(10);

        Pageable pageable = PageRequest.of(searchBillRqDto.getPage(), searchBillRqDto.getSize(), Sort.Direction.DESC, "id");

        Page<Bill> billPage = billRepo.findAll(new Specification<Bill>() {


            public Predicate toPredicate(Root<Bill> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                List<Predicate> predicates = new ArrayList<>();

                if (!Utils.isNullOrEmpty(searchBillRqDto.getBillNumber())) {
                    predicates.add(cb.equal(cb.lower(root.get("billNumber")), searchBillRqDto.getBillNumber().toLowerCase()));
                }

                if (!Utils.isNullOrEmpty(searchBillRqDto.getSadadNumber())) {
                    predicates.add(cb.equal(cb.lower(root.get("sadadNumber")), searchBillRqDto.getSadadNumber().toLowerCase()));
                }

                if (!Utils.isNullOrEmpty(searchBillRqDto.getTotalAmount())) {
                    predicates.add(cb.equal(root.get("totalAmount"), searchBillRqDto.getTotalAmount()));
                }

                if (!Utils.isNullOrEmpty(searchBillRqDto.getServiceName())) {
                    predicates.add(cb.like(cb.lower(root.get("serviceName")), "%" + searchBillRqDto.getServiceName().toLowerCase() + "%"));
                }

                if (!Utils.isNullOrEmpty(searchBillRqDto.getCustomerFullName())) {
                    predicates.add(cb.like(cb.lower(root.get("customerFullName")), "%" + searchBillRqDto.getCustomerFullName().toLowerCase() + "%"));
                }

                if (!Utils.isNullOrEmpty(searchBillRqDto.getCustomerEmailAddress())) {
                    predicates.add(cb.like(cb.lower(root.get("customerEmailAddress")), "%" + searchBillRqDto.getCustomerEmailAddress().toLowerCase() + "%"));
                }

                if (!Utils.isNullOrEmpty((searchBillRqDto.getCustomerMobileNumber()))) {
                    predicates.add(cb.equal(cb.lower(root.get("customerMobileNumber")), Utils.formatMobileNumber(searchBillRqDto.getCustomerMobileNumber().toLowerCase())));
                }

                if (!Utils.isNullOrEmpty((searchBillRqDto.getCustomerIdNumber()))) {
                    predicates.add(cb.equal(cb.lower(root.get("customerIdNumber")), searchBillRqDto.getCustomerIdNumber().toLowerCase()));
                }

                if (Utils.isEnumValueExist(searchBillRqDto.getBillType(), BillType.class)) {
                    predicates.add(cb.equal(root.get("billType"), BillType.valueOf(searchBillRqDto.getBillType())));
                }

                if (entityUser != null && entityUser.getEntity() != null) {
                    predicates.add(cb.equal(root.get("entity"), entityUser.getEntity()));
                }

                Entity entity = null;
                if (searchBillRqDto.getEntityCode() != null && !searchBillRqDto.getEntityCode().isEmpty()) {
                    entity = entityRepo.findEntityByCode(searchBillRqDto.getEntityCode());
                    predicates.add(cb.equal(root.get("entity"), entity));
                }

                if (searchBillRqDto.getActivityCode() != null && !searchBillRqDto.getActivityCode().isEmpty() && entity != null) {
                    EntityActivity entityActivity = entityActivityRepo.findEntityActivity(searchBillRqDto.getActivityCode(), entity);
                    predicates.add(cb.equal(root.get("entityActivity"), entityActivity));
                }

                LocalDateTime issueDateFrom = Utils.parseDateFromString(searchBillRqDto.getIssueDateFrom(), "yyyy-MM-dd");

                if (issueDateFrom != null) {
                    predicates.add(cb.greaterThanOrEqualTo(root.get("issueDate"), issueDateFrom));
                }

                LocalDateTime issueDateTo = Utils.parseDateFromString(searchBillRqDto.getIssueDateTo(), "yyyy-MM-dd");

                if (issueDateTo != null) {
                    issueDateTo = issueDateTo.plusHours(24).minusMinutes(1);
                    predicates.add(cb.lessThanOrEqualTo(root.get("issueDate"), issueDateTo));
                }

                predicates.add(cb.notEqual(root.get("billStatus"), BillStatus.DELETED));

                List<Predicate> billStatusPredicateList = new ArrayList<>();

                if (searchBillRqDto.getBillStatusList() != null && !searchBillRqDto.getBillStatusList().isEmpty()) {

                    for (String billStatusCode : searchBillRqDto.getBillStatusList()) {

                        if (billStatusCode.equals("UNPAID")) {
                            Predicate billStatusPredicateAcceptedBySadad = cb.equal(root.get("billStatus"), BillStatus.APPROVED_BY_SADAD);
                            Predicate billStatusPredicateViewedByClient = cb.equal(root.get("billStatus"), BillStatus.VIEWED_BY_CUSTOMER);
                            billStatusPredicateList.add(billStatusPredicateAcceptedBySadad);
                            billStatusPredicateList.add(billStatusPredicateViewedByClient);
                        } else {
                            if (billStatusCode.equals("PAID")) {
                                Predicate billStatusPredicatePaidByEntity = cb.equal(root.get("billStatus"), BillStatus.PAID_BY_COMPANY);
                                billStatusPredicateList.add(billStatusPredicatePaidByEntity);

                                Predicate billStatusPredicatePaidBySadad = cb.equal(root.get("billStatus"), BillStatus.PAID_BY_SADAD);
                                billStatusPredicateList.add(billStatusPredicatePaidBySadad);

                                Predicate billStatusPredicateSettledBySadad = cb.equal(root.get("billStatus"), BillStatus.SETTLED_BY_SADAD);
                                billStatusPredicateList.add(billStatusPredicateSettledBySadad);
                            } else {
                                if (Utils.isEnumValueExist(billStatusCode, BillStatus.class)) {
                                    Predicate billStatusPredicate = cb.equal(root.get("billStatus"), BillStatus.valueOf(billStatusCode));
                                    billStatusPredicateList.add(billStatusPredicate);
                                }
                            }
                        }
                    }
                }

                Predicate andPredicate = cb.and(predicates.toArray(new Predicate[0]));

                Predicate statusPredicate = cb.or(billStatusPredicateList.toArray(new Predicate[0]));

                Predicate finalPredicate = null;
                if (!billStatusPredicateList.isEmpty()) {
                    finalPredicate = cb.and(andPredicate, statusPredicate);
                } else {
                    finalPredicate = cb.and(andPredicate);
                }
                return finalPredicate;
            }
        }, pageable);

        List<Bill> billList = billPage.getContent();

        List<SearchBillRsDto> searchBillRsDtoList = new ArrayList<>();

        billList.forEach(bill -> {
            SearchBillRsDto searchBillRsDto = new SearchBillRsDto();
            searchBillRsDto.setId(bill.getId());
            searchBillRsDto.setBillNumber(bill.getBillNumber());
            searchBillRsDto.setSadadNumber(bill.getSadadNumber());
            searchBillRsDto.setServiceName(bill.getServiceName());
            searchBillRsDto.setServiceDescription(bill.getServiceDescription());
            searchBillRsDto.setIssueDate(bill.getIssueDate());
            searchBillRsDto.setExpireDate(bill.getExpireDate());
            searchBillRsDto.setCreateDate(bill.getCreateDate());
            searchBillRsDto.setSubAmount(bill.getSubAmount());
            searchBillRsDto.setVat(bill.getVat());
            searchBillRsDto.setDiscount(bill.getDiscount());
            searchBillRsDto.setTotalAmount(bill.getTotalRunningAmount());
            searchBillRsDto.setCustomerFullName(bill.getCustomerFullName());
            searchBillRsDto.setCustomerMobileNumber(bill.getCustomerMobileNumber());
            searchBillRsDto.setCustomerEmailAddress(bill.getCustomerEmailAddress());
            searchBillRsDto.setCustomerIdNumber(bill.getCustomerIdNumber());
            searchBillRsDto.setBillStatus(bill.getBillStatus());
            searchBillRsDto.setBillStatusText(Utf8ResourceBundle.getString(bill.getBillStatus().name(), locale));
            searchBillRsDto.setBillType(bill.getBillType());
            searchBillRsDto.setBillTypeText(Utf8ResourceBundle.getString(bill.getBillType().name(), locale));
            searchBillRsDto.setBillSource(bill.getBillSource());
            searchBillRsDto.setBillSourceText(Utf8ResourceBundle.getString(bill.getBillSource().name(), locale));

            searchBillRsDto.setActivityId(bill.getEntityActivity().getId());
            searchBillRsDto.setActivityName(bill.getEntityActivity().getName(locale));
            searchBillRsDto.setPreviousBalance(bill.getCustomerPreviousBalance());
            searchBillRsDto.setCustomerTaxNumber(bill.getCustomerTaxNumber());
            searchBillRsDto.setCustomerAddress(bill.getCustomerAddress());
            searchBillRsDto.setEntityCode(bill.getEntity().getCode());
            searchBillRsDto.setEntityName(bill.getEntity().getBrandName(locale));
            searchBillRsDto.setActivityCode(bill.getEntityActivity().getCode());
            searchBillRsDto.setReferenceNumber(bill.getReferenceNumber());

            searchBillRsDto.setAccountNumber(bill.getAccount() != null ? bill.getAccount().getAccountNumber() : "");
            searchBillRsDto.setDueDate(bill.getDueDate());

            List<BillCustomField> billCustomFieldList = billCustomFieldRepo.findAllByBill(bill);
            List<BillCustomFieldRsDto> billCustomFieldRsDtoList = new ArrayList<>();

            if (billCustomFieldList != null && !billCustomFieldList.isEmpty()) {
                billCustomFieldList.forEach(billCustomField -> {
                    billCustomFieldRsDtoList.add((BillCustomFieldRsDto) billCustomField.toDto(locale));
                });
            }

            searchBillRsDto.setCustomFieldList(billCustomFieldRsDtoList);

            if (bill.getBillStatus().equals(BillStatus.PAID_BY_COMPANY)
                    || bill.getBillStatus().equals(BillStatus.PAID_BY_SADAD)
                    || bill.getBillStatus().equals(BillStatus.SETTLED_BY_SADAD)
                    || bill.getBillStatus().equals(BillStatus.SETTLED_BY_COMPANY)) {
                searchBillRsDto.setStatusAr("مدفوعة");
                searchBillRsDto.setStatusEn("Paid");
                searchBillRsDto.setPaymentFlag("PAID");
            } else {
                searchBillRsDto.setStatusAr("غير مدفوعة");
                searchBillRsDto.setStatusEn("Unpaid");
                searchBillRsDto.setPaymentFlag("UNPAID");
            }

            searchBillRsDto.setCampaignId(bill.getCampaignId());
            searchBillRsDto.setVatExemptedFlag(bill.getVatExemptedFlag());
            searchBillRsDto.setVatNaFlag(bill.getVatNaFlag());
            searchBillRsDto.setDiscountType(bill.getDiscountType());

            if (bill.getIsPartialAllowed() != null) {
                searchBillRsDto.setIsPartialAllowed(bill.getIsPartialAllowed().name());
            } else {
                searchBillRsDto.setIsPartialAllowed(BooleanFlag.NO.name());
            }

            if (bill.getBillCategory() != null) {
                searchBillRsDto.setBillCategory(bill.getBillCategory().name());
            }

            if (bill.getMiniPartialAmount() != null) {
                searchBillRsDto.setMiniPartialAmount(bill.getMiniPartialAmount());
            }

            if (bill.getMaxPartialAmount() != null) {
                searchBillRsDto.setMaxPartialAmount(bill.getMaxPartialAmount());
            }

            searchBillRsDto.setPaymentNumber(bill.getPaymentNumber());
            searchBillRsDto.setSettlementNumber(bill.getSettlementNumber());
            searchBillRsDto.setRemainAmount(bill.getRemainAmount());
            searchBillRsDto.setCurrentPaidAmount(bill.getCurrentPaidAmount());

            if (bill.getIsPartialAllowed().equals(BooleanFlag.YES)) {
                searchBillRsDto.setPaymentRule(PaymentRule.PARTIAL.name());
            } else {
                searchBillRsDto.setPaymentRule(PaymentRule.NONE.name());
            }

            searchBillRsDto.setCycle(bill.getCycleNumber() + "");

            searchBillRsDto.setAccountId(bill.getAccount() != null ? bill.getAccount().getId() : null);


            searchBillRsDtoList.add(searchBillRsDto);

        });

        return new PageDtoRs(billPage.getTotalElements(), billPage.getTotalPages(), searchBillRsDtoList);
    }

    public BillInfoDtoRs findBillDetail(Long billId, Locale locale) {

        Bill bill = billRepo.findBillById(billId);

        if (bill == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
        }

        if (bill.getBillStatus().equals(BillStatus.DELETED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
        }

        List<BillItem> billItemList = billItemRepo.findAllByBill(bill);
        List<BillItemDetailedRsDto> billItemDetailedRsDtoList = new ArrayList<>();
        if (billItemList != null && !billItemList.isEmpty()) {
            billItemList.forEach(billItem -> {
                billItemDetailedRsDtoList.add((BillItemDetailedRsDto) billItem.toDto(locale));
            });
        }

        List<BillCustomField> billCustomFieldList = billCustomFieldRepo.findAllByBill(bill);
        List<BillCustomFieldRsDto> billCustomFieldRsDtoList = new ArrayList<>();

        if (billCustomFieldList != null && !billCustomFieldList.isEmpty()) {
            billCustomFieldList.forEach(billCustomField -> {

                billCustomFieldRsDtoList.add((BillCustomFieldRsDto) billCustomField.toDto(locale));

            });
        }

        List<BillEmail> billEmailList = billEmailRepo.findAllBillEmail(bill);
        List<String> billEmailRsList = new ArrayList<>();
        if (billEmailList != null && !billEmailList.isEmpty()) {
            billEmailList.forEach(billEmail -> {
                billEmailRsList.add(billEmail.getEmail());
            });
        }

        List<BillMobileNumber> billMobileNumberList = billMobileNumberRepo.findAllBillMobileNumber(bill);
        List<String> billMobileNumberRsList = new ArrayList<>();
        if (billMobileNumberList != null && !billMobileNumberList.isEmpty()) {
            billMobileNumberList.forEach(billMobileNumber -> {
                billMobileNumberRsList.add(billMobileNumber.getMobileNumber());
            });
        }

        BillInfoDtoRs billInfoDtoRs = new BillInfoDtoRs();
        billInfoDtoRs.setId(bill.getId());
        billInfoDtoRs.setBillItemList(billItemDetailedRsDtoList);
        billInfoDtoRs.setBillNumber(bill.getBillNumber());
        billInfoDtoRs.setSadadNumber(bill.getSadadNumber());
        billInfoDtoRs.setCustomerEmailAddress(bill.getCustomerEmailAddress());
        billInfoDtoRs.setCustomerMobileNumber(bill.getCustomerMobileNumber());
        billInfoDtoRs.setCustomerFullName(bill.getCustomerFullName());
        billInfoDtoRs.setCustomerIdNumber(bill.getCustomerIdNumber());
        billInfoDtoRs.setExpireDate(bill.getExpireDate());
        billInfoDtoRs.setIssueDate(bill.getIssueDate());
        billInfoDtoRs.setSubAmount(bill.getSubAmount());
        billInfoDtoRs.setCustomerEmailList(billEmailRsList);
        billInfoDtoRs.setCustomerMobileList(billMobileNumberRsList);
        billInfoDtoRs.setVat(bill.getVat().toString());
        if (bill.getVatNaFlag().equals(BooleanFlag.YES)) {
            billInfoDtoRs.setVat("NA");
        }

        if (bill.getVatExemptedFlag().equals(BooleanFlag.YES)) {
            billInfoDtoRs.setVat("EXE");
        }

        billInfoDtoRs.setTotalAmount(bill.getTotalAmount());
        billInfoDtoRs.setDiscount(bill.getDiscount());
        billInfoDtoRs.setServiceName(bill.getServiceName());
        billInfoDtoRs.setServiceDescription(bill.getServiceDescription());
        billInfoDtoRs.setCustomFieldList(billCustomFieldRsDtoList);
        billInfoDtoRs.setStatusCode(bill.getBillStatus().name());
        billInfoDtoRs.setStatusText(Utf8ResourceBundle.getString(bill.getBillStatus().name(), locale));
        billInfoDtoRs.setCustomerAddress(bill.getCustomerAddress());
        billInfoDtoRs.setCustomerTaxNumber(bill.getCustomerTaxNumber());
        billInfoDtoRs.setCustomerPreviousBalance(bill.getCustomerPreviousBalance());
        billInfoDtoRs.setVatExemptedFlag(bill.getVatExemptedFlag());
        billInfoDtoRs.setVatNaFlag(bill.getVatNaFlag());
        billInfoDtoRs.setDiscountType(bill.getDiscountType());

        if (bill.getCampaignId() != null) {
            billInfoDtoRs.setCampaignId(bill.getCampaignId());
            ;
        }

        if (bill.getCustomerIdType() != null) {
            billInfoDtoRs.setCustomerIdType(bill.getCustomerIdType().name());
        }


        if (bill.getEntityActivity() != null) {
            billInfoDtoRs.setEntityActivityId(bill.getEntityActivity().getId());
        }

        if (bill.getIsPartialAllowed() != null) {
            billInfoDtoRs.setIsPartialAllowed(bill.getIsPartialAllowed().name());
        } else {
            billInfoDtoRs.setIsPartialAllowed(BooleanFlag.NO.name());
        }

        if (bill.getBillCategory() != null) {
            billInfoDtoRs.setBillCategory(bill.getBillCategory().name());
        }

        if (bill.getMiniPartialAmount() != null) {
            billInfoDtoRs.setMiniPartialAmount(bill.getMiniPartialAmount());
        }

        if (bill.getMaxPartialAmount() != null) {
            billInfoDtoRs.setMaxPartialAmount(bill.getMaxPartialAmount());
        }

        billInfoDtoRs.setPaymentNumber(bill.getPaymentNumber());
        billInfoDtoRs.setSettlementNumber(bill.getSettlementNumber());
        billInfoDtoRs.setRemainAmount(bill.getRemainAmount());
        billInfoDtoRs.setCurrentPaidAmount(bill.getCurrentPaidAmount());

        List<SadadPayment> sadadPaymentList = sadadPaymentRepo.findSadadPaymentByBillOrderByIdAsc(bill);
        List<LocalPayment> localPaymentList = localPaymentRepo.findLocalPaymentByBillOrderByIdAsc(bill);

        BigDecimal totalOfflinePaidAmount = BigDecimal.ZERO;
        BigDecimal totalOnlinePaidAmount = BigDecimal.ZERO;

        List<PaymentDtoRs> paymentDtoRsList = new ArrayList<>();

        if (sadadPaymentList != null && !sadadPaymentList.isEmpty()) {
            for (SadadPayment sadadPayment : sadadPaymentList) {
                PaymentDtoRs paymentDtoRs = new PaymentDtoRs();
                paymentDtoRs.setBillNumber(bill.getBillNumber());
                paymentDtoRs.setSadadNumber(bill.getSadadNumber());
                paymentDtoRs.setSadadPaymentId(sadadPayment.getSadadPaymentId());
                paymentDtoRs.setPaymentDate(Utils.parseDateTime(sadadPayment.getPaymentDate(), "yyyy-MM-dd"));
                paymentDtoRs.setPaymentType("ONLINE");
                paymentDtoRs.setPaymentMethod("SADAD");
                paymentDtoRs.setPaymentAmount(sadadPayment.getPaymentAmount());
                paymentDtoRs.setRemainingAmount(sadadPayment.getRemainingAmount());
                paymentDtoRs.setPaymentStatus(sadadPayment.getPaymentStatus().name());
                paymentDtoRs.setUserReference("NA");
                paymentDtoRs.setPaymentImage(sadadPayment.getPaymentImage());
                paymentDtoRs.setTotalAmountAr(NumberToWord.convertNumberToArabicWords(bill.getTotalAmount().intValue() + ""));
                paymentDtoRs.setTotalAmountEn(NumberToWord.convertNumberToEnglishWords(bill.getTotalAmount().intValue() + ""));
                paymentDtoRs.setPaymentAmountAr(NumberToWord.convertNumberToArabicWords(sadadPayment.getPaymentAmount().intValue() + ""));
                paymentDtoRs.setPaymentAmountEn(NumberToWord.convertNumberToEnglishWords(sadadPayment.getPaymentAmount().intValue() + ""));
                paymentDtoRs.setBillAmount(bill.getTotalAmount());
                paymentDtoRs.setCustomerName(bill.getCustomerFullName());
                paymentDtoRs.setVoucherNumber(sadadPayment.getVoucherNumber() + "");
                paymentDtoRs.setPaymentNumber(sadadPayment.getPaymentNumber());
                paymentDtoRs.setVoucherDate(sadadPayment.getPaymentDate());
                paymentDtoRsList.add(paymentDtoRs);

                totalOnlinePaidAmount = totalOnlinePaidAmount.add(sadadPayment.getPaymentAmount());
            }
        }

        if (localPaymentList != null && !localPaymentList.isEmpty()) {
            for (LocalPayment localPayment : localPaymentList) {
                PaymentDtoRs paymentDtoRs = new PaymentDtoRs();
                paymentDtoRs.setBillNumber(bill.getBillNumber());
                paymentDtoRs.setSadadNumber(bill.getSadadNumber());
                paymentDtoRs.setSadadPaymentId(localPayment.getSadadPaymentId());
                paymentDtoRs.setPaymentDate(Utils.parseDateTime(localPayment.getPaymentDate(), "yyyy-MM-dd"));
                paymentDtoRs.setPaymentType("OFFLINE");
                paymentDtoRs.setPaymentMethod(localPayment.getPaymentMethod() != null ? localPayment.getPaymentMethod().name() : null);
                paymentDtoRs.setPaymentAmount(localPayment.getPaymentAmount());
                paymentDtoRs.setRemainingAmount(localPayment.getRemainingAmount());
                paymentDtoRs.setPaymentStatus(localPayment.getPaymentStatus().name());
                paymentDtoRs.setUserReference(localPayment.getEntityUser() != null ? localPayment.getEntityUser().getUsername() : "");
                paymentDtoRs.setPaymentImage(localPayment.getPaymentImage());
                paymentDtoRs.setTotalAmountAr(NumberToWord.convertNumberToArabicWords(bill.getTotalAmount().intValue() + ""));
                paymentDtoRs.setTotalAmountEn(NumberToWord.convertNumberToEnglishWords(bill.getTotalAmount().intValue() + ""));
                paymentDtoRs.setPaymentAmountAr(NumberToWord.convertNumberToArabicWords(localPayment.getPaymentAmount().intValue() + ""));
                paymentDtoRs.setPaymentAmountEn(NumberToWord.convertNumberToEnglishWords(localPayment.getPaymentAmount().intValue() + ""));
                paymentDtoRs.setBillAmount(bill.getTotalAmount());
                paymentDtoRs.setCustomerName(bill.getCustomerFullName());
                paymentDtoRs.setVoucherNumber(localPayment.getVoucherNumber() + "");
                paymentDtoRs.setPaymentNumber(localPayment.getPaymentNumber());
                paymentDtoRs.setVoucherDate(localPayment.getCreateDate());
                paymentDtoRsList.add(paymentDtoRs);

                totalOfflinePaidAmount = totalOfflinePaidAmount.add(localPayment.getPaymentAmount());
            }
        }

        Collections.sort(paymentDtoRsList);

        billInfoDtoRs.setPaymentList(paymentDtoRsList);
        billInfoDtoRs.setOnlinePaymentList(sadadPaymentList);
        billInfoDtoRs.setOfflinePaymentList(localPaymentList);

        billInfoDtoRs.setTotalOfflinePaidAmount(totalOfflinePaidAmount);
        billInfoDtoRs.setTotalOnlinePaidAmount(totalOnlinePaidAmount);

        billInfoDtoRs.setAdditionalNumber(bill.getAdditionalNumber());
        billInfoDtoRs.setBuildingNumber(bill.getBuildingNumber());
        billInfoDtoRs.setDistrictId(bill.getDistrict() != null ? bill.getDistrict().getId() : 0L);
        billInfoDtoRs.setPostalCode(bill.getPostalCode());
        billInfoDtoRs.setStreetName(bill.getStreet());
        billInfoDtoRs.setCityId(bill.getCity() != null ? bill.getCity().getId() : null);
        billInfoDtoRs.setOtherDistrict(bill.getOtherDistrict());

        billInfoDtoRs.setEntityActivity(bill.getEntityActivity() != null ? bill.getEntityActivity().toDto(locale) : null);

        if (bill.getBillType().equals(BillType.RECURRING_BILL)) {
            billInfoDtoRs.setInstallmentAmount(bill.getTotalAmount());

            billInfoDtoRs.setDueDate(bill.getDueDate());

            billInfoDtoRs.setReferenceNumber(bill.getReferenceNumber());
            billInfoDtoRs.setContractNumber(bill.getReferenceNumber());
            Account account = bill.getAccount();

            if (account instanceof IndividualAccount) {
                billInfoDtoRs.setAccount("INDIVIDUAL");
            } else {
                billInfoDtoRs.setAccount("BUSINESS");
            }

            billInfoDtoRs.setAccountId(account.getId());
            billInfoDtoRs.setAccountInfo(account.toAccountDtoRs());
        }


        billInfoDtoRs.setInvoiceLogo(bill.getLogo());
        billInfoDtoRs.setInvoiceStamp(bill.getStamp());

        List<BillNote> billNoteList = billNoteRepo.findAllBillNote(bill);

        List<BillNoteDtoRs> billNoteDtoRsList = new ArrayList<>();

        BillNoteDtoRs billNoteDtoRs = new BillNoteDtoRs();

        billNoteDtoRs.setId(0L);
        billNoteDtoRs.setDocumentNumber(bill.getSadadNumber());
        billNoteDtoRs.setDate(bill.getCreateDate());
        billNoteDtoRs.setNoteType(NoteType.INVOICE_NOTE);
        billNoteDtoRs.setDescription(Utf8ResourceBundle.getString(billNoteDtoRs.getNoteType().name(), locale));
        billNoteDtoRs.setAmount(bill.getTotalAmount());
        billNoteDtoRs.setBalanceAmount(bill.getTotalAmount());
        billNoteDtoRs.setItemList(new ArrayList<>());

        billNoteDtoRsList.add(billNoteDtoRs);

        for (BillNote billNote : billNoteList) {

            BillNoteDtoRs billNoteDtoRsItem = new BillNoteDtoRs();

            billNoteDtoRsItem.setId(0L);
            billNoteDtoRsItem.setDocumentNumber(billNote.getDocumentNumber());
            billNoteDtoRsItem.setDate(billNote.getDate());
            billNoteDtoRsItem.setNoteType(billNote.getNoteType());
            billNoteDtoRsItem.setDescription(Utf8ResourceBundle.getString(billNote.getNoteType().name(), locale));
            billNoteDtoRsItem.setAmount(billNote.getAmount());
            billNoteDtoRsItem.setBalanceAmount(billNote.getBalanceAmount());
            billNoteDtoRsItem.setItemList(billNoteItemRepo.findAllBillNoteItem(billNote));

            billNoteDtoRsList.add(billNoteDtoRsItem);
        }

        for (PaymentDtoRs paymentDtoRs : paymentDtoRsList) {

            BillNoteDtoRs billNoteDtoRsItem = new BillNoteDtoRs();

            billNoteDtoRsItem.setId(0L);
            billNoteDtoRsItem.setDocumentNumber(paymentDtoRs.getVoucherNumber());
            billNoteDtoRsItem.setDate(paymentDtoRs.getVoucherDate());
            billNoteDtoRsItem.setNoteType(NoteType.PAYMENT_NOTE);
            billNoteDtoRsItem.setDescription(Utf8ResourceBundle.getString(billNoteDtoRsItem.getNoteType().name(), locale));
            billNoteDtoRsItem.setAmount(paymentDtoRs.getPaymentAmount().multiply(BigDecimal.valueOf(-1)));
            billNoteDtoRsItem.setBalanceAmount(paymentDtoRs.getRemainingAmount());
            billNoteDtoRsItem.setItemList(new ArrayList<>());

            billNoteDtoRsList.add(billNoteDtoRsItem);
        }

        Collections.sort(billNoteDtoRsList);

        billInfoDtoRs.setBillNoteList(billNoteDtoRsList);

        if (bill.getBillType().equals(BillType.TAX_BILL)) {
            List<RunningTaxBillItem> runningTaxBillItemList = runningTaxBillItemRepo.findAllByBill(bill);
            List<BillItemDetailedRsDto> runningTaxBillItemDetailedRsDtoList = new ArrayList<>();
            if (runningTaxBillItemList != null && !runningTaxBillItemList.isEmpty()) {
                runningTaxBillItemList.forEach(billItem -> {
                    runningTaxBillItemDetailedRsDtoList.add((BillItemDetailedRsDto) billItem.toDto(locale));
                });
            }
            billInfoDtoRs.setRunningBillItemList(runningTaxBillItemDetailedRsDtoList);
        } else {
            billInfoDtoRs.setRunningBillItemList(billItemDetailedRsDtoList);
        }


        String billNumberDecoded = aes256.encrypt(billInfoDtoRs.getSadadNumber());

        String qrString = "اسم المورد: " + bill.getEntity().getBrandNameAr() + "\n" +
                "الرقم الضريبي للمورد: " + bill.getEntity().getVatNumber() + "\n" +
                "الطابع الزمني: " + Utils.parseDateTime(bill.getCreateDate(), "yyyy-MM-dd hh:mm") + "\n" +
                "اجمالي الفاتورة: " + Utils.roundDecimal(bill.getTotalAmount()) + "\n" +
                "اجمالي الضريبة: " + Utils.roundDecimal(bill.getSubAmount().multiply(bill.getVat())) + "\n" +
                environment.getProperty("qr_url") + billNumberDecoded;

        billInfoDtoRs.setQr(qrString);

        billInfoDtoRs.setReferenceNumber(bill.getReferenceNumber());
        billInfoDtoRs.setContractNumber(bill.getReferenceNumber());

        Entity entity = bill.getEntity();

        EntityContactDetail entityContactDetail = entityContactDetailRepo.findAllByEntity(bill.getEntity());

        EntityBillInfoDtoRs entityBillInfoDtoRs = new EntityBillInfoDtoRs();

        entityBillInfoDtoRs.setId(entity.getId());
        entityBillInfoDtoRs.setLogo(entity.getLogo());
        entityBillInfoDtoRs.setStamp(entity.getStamp());
        entityBillInfoDtoRs.setCode(entity.getCode());
        entityBillInfoDtoRs.setBrandNameAr(entity.getBrandNameAr());
        entityBillInfoDtoRs.setBrandNameEn(entity.getBrandNameEn());
        entityBillInfoDtoRs.setCommercialNameAr(entity.getCommercialNameAr());
        entityBillInfoDtoRs.setCommercialNameEn(entity.getCommercialNameEn());
        entityBillInfoDtoRs.setVatNumber(entity.getVatNumber());
        entityBillInfoDtoRs.setVatNameAr(entity.getVatNameAr());
        entityBillInfoDtoRs.setCity(entity.getCity());
        entityBillInfoDtoRs.setOtherCity(entity.getOtherCity());
        entityBillInfoDtoRs.setDistrict(entity.getDistrict());
        entityBillInfoDtoRs.setOtherDistrict(entity.getOtherDistrict());
        entityBillInfoDtoRs.setStreetName(entity.getStreetName());
        entityBillInfoDtoRs.setEntityUnitNumber(entity.getEntityUnitNumber());
        entityBillInfoDtoRs.setZipCode(entity.getZipCode());
        entityBillInfoDtoRs.setAdditionalZipCode(entity.getAdditionalZipCode());
        entityBillInfoDtoRs.setIdNumber(entity.getIdNumber());

        entityBillInfoDtoRs.setPhoneNumber(entityContactDetail.getPhoneNumber());
        entityBillInfoDtoRs.setPhoneNumberCode(entityContactDetail.getPhoneNumberCode());
        entityBillInfoDtoRs.setMobileNumber(entityContactDetail.getMobileNumber());
        entityBillInfoDtoRs.setMobileNumberCode(entityContactDetail.getMobileNumberCode());
        entityBillInfoDtoRs.setUnifiedNumber(entityContactDetail.getUnifiedNumber());
        entityBillInfoDtoRs.setWebsite(entityContactDetail.getWebsite());
        entityBillInfoDtoRs.setEmail(entityContactDetail.getEmail());

        SettingStyleDtoRq settingStyleDtoRq = new SettingStyleDtoRq();
        settingStyleDtoRq.setColor(entity.getColor());
        settingStyleDtoRq.setStamp(entity.getStampWidth());
        settingStyleDtoRq.setLogo(entity.getLogoWidth());

        entityBillInfoDtoRs.setStyling(settingStyleDtoRq);

        billInfoDtoRs.setEntity(entityBillInfoDtoRs);

        billInfoDtoRs.setBillTypeText(Utf8ResourceBundle.getString(bill.getBillType().name(), locale));


        return billInfoDtoRs;
    }

    public BillInfoDtoRs findBillDetail(String sadadNumber, Locale locale) {

        LOGGER.info(sadadNumber);

        String[] sadadNumberArray = sadadNumber.split("_");

        List<Bill> billList = null;

        if (sadadNumberArray.length > 1) {

            Account account = accountRepo.findAccountByAccountNumber(sadadNumberArray[0]);

            if (account != null) {
                Integer cycleNumber = Utils.parseInt(sadadNumberArray[1]);

                LOGGER.info("sadadNumber" + sadadNumberArray[0] + "cycleNumber " + cycleNumber);

                Bill bill = billRepo.findBySadadNumberAndCycleNumber(sadadNumberArray[0], cycleNumber);

                if (bill != null) {
                    billList = new ArrayList<>();
                    billList.add(bill);
                }

            } else {
                billList = billRepo.findBySadadNumber(sadadNumberArray[0]);
            }
        } else {
            LOGGER.info("sadadNumberArray.length " + sadadNumberArray.length);
            LOGGER.info("sadadNumber" + sadadNumber);
            billList = billRepo.findBySadadNumber(sadadNumber);
        }

        Bill bill = null;

        if (billList != null && !billList.isEmpty()) {
            bill = billList.get(0);
        }

        if (bill == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
        }

        if (bill.getBillStatus().equals(BillStatus.DELETED)) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bill_not_found", locale);
        }

        List<BillItem> billItemList = billItemRepo.findAllByBill(bill);
        List<BillItemDetailedRsDto> billItemDetailedRsDtoList = new ArrayList<>();
        if (billItemList != null && !billItemList.isEmpty()) {
            billItemList.forEach(billItem -> {
                billItemDetailedRsDtoList.add((BillItemDetailedRsDto) billItem.toDto(locale));
            });
        }

        List<BillCustomField> billCustomFieldList = billCustomFieldRepo.findAllByBill(bill);
        List<BillCustomFieldRsDto> billCustomFieldRsDtoList = new ArrayList<>();

        if (billCustomFieldList != null && !billCustomFieldList.isEmpty()) {
            billCustomFieldList.forEach(billCustomField -> {

                billCustomFieldRsDtoList.add((BillCustomFieldRsDto) billCustomField.toDto(locale));

            });
        }

        List<BillEmail> billEmailList = billEmailRepo.findAllBillEmail(bill);
        List<String> billEmailRsList = new ArrayList<>();
        if (billEmailList != null && !billEmailList.isEmpty()) {
            billEmailList.forEach(billEmail -> {
                billEmailRsList.add(billEmail.getEmail());
            });
        }

        List<BillMobileNumber> billMobileNumberList = billMobileNumberRepo.findAllBillMobileNumber(bill);
        List<String> billMobileNumberRsList = new ArrayList<>();
        if (billMobileNumberList != null && !billMobileNumberList.isEmpty()) {
            billMobileNumberList.forEach(billMobileNumber -> {
                billMobileNumberRsList.add(billMobileNumber.getMobileNumber());
            });
        }

        BillInfoDtoRs billInfoDtoRs = new BillInfoDtoRs();
        billInfoDtoRs.setId(bill.getId());
        billInfoDtoRs.setBillItemList(billItemDetailedRsDtoList);
        billInfoDtoRs.setBillNumber(bill.getBillNumber());
        billInfoDtoRs.setSadadNumber(bill.getSadadNumber());
        billInfoDtoRs.setCustomerEmailAddress(bill.getCustomerEmailAddress());
        billInfoDtoRs.setCustomerMobileNumber(bill.getCustomerMobileNumber());
        billInfoDtoRs.setCustomerFullName(bill.getCustomerFullName());
        billInfoDtoRs.setCustomerIdNumber(bill.getCustomerIdNumber());
        billInfoDtoRs.setExpireDate(bill.getExpireDate());
        billInfoDtoRs.setIssueDate(bill.getIssueDate());
        billInfoDtoRs.setSubAmount(bill.getSubAmount());
        billInfoDtoRs.setCustomerEmailList(billEmailRsList);
        billInfoDtoRs.setCustomerMobileList(billMobileNumberRsList);
        billInfoDtoRs.setVat(bill.getVat().toString());
        if (bill.getVatNaFlag().equals(BooleanFlag.YES)) {
            billInfoDtoRs.setVat("NA");
        }

        if (bill.getVatExemptedFlag().equals(BooleanFlag.YES)) {
            billInfoDtoRs.setVat("EXE");
        }

        billInfoDtoRs.setTotalAmount(bill.getTotalAmount());
        billInfoDtoRs.setDiscount(bill.getDiscount());
        billInfoDtoRs.setServiceName(bill.getServiceName());
        billInfoDtoRs.setServiceDescription(bill.getServiceDescription());
        billInfoDtoRs.setCustomFieldList(billCustomFieldRsDtoList);
        billInfoDtoRs.setStatusCode(bill.getBillStatus().name());
        billInfoDtoRs.setStatusText(Utf8ResourceBundle.getString(bill.getBillStatus().name(), locale));
        billInfoDtoRs.setCustomerAddress(bill.getCustomerAddress());
        billInfoDtoRs.setCustomerTaxNumber(bill.getCustomerTaxNumber());
        billInfoDtoRs.setCustomerPreviousBalance(bill.getCustomerPreviousBalance());
        billInfoDtoRs.setVatExemptedFlag(bill.getVatExemptedFlag());
        billInfoDtoRs.setVatNaFlag(bill.getVatNaFlag());
        billInfoDtoRs.setDiscountType(bill.getDiscountType());

        if (bill.getCampaignId() != null) {
            billInfoDtoRs.setCampaignId(bill.getCampaignId());
            ;
        }

        if (bill.getCustomerIdType() != null) {
            billInfoDtoRs.setCustomerIdType(bill.getCustomerIdType().name());
        }


        if (bill.getEntityActivity() != null) {
            billInfoDtoRs.setEntityActivityId(bill.getEntityActivity().getId());
        }

        if (bill.getIsPartialAllowed() != null) {
            billInfoDtoRs.setIsPartialAllowed(bill.getIsPartialAllowed().name());
        } else {
            billInfoDtoRs.setIsPartialAllowed(BooleanFlag.NO.name());
        }

        if (bill.getBillCategory() != null) {
            billInfoDtoRs.setBillCategory(bill.getBillCategory().name());
        }

        if (bill.getMiniPartialAmount() != null) {
            billInfoDtoRs.setMiniPartialAmount(bill.getMiniPartialAmount());
        }

        if (bill.getMaxPartialAmount() != null) {
            billInfoDtoRs.setMaxPartialAmount(bill.getMaxPartialAmount());
        }

        billInfoDtoRs.setPaymentNumber(bill.getPaymentNumber());
        billInfoDtoRs.setSettlementNumber(bill.getSettlementNumber());
        billInfoDtoRs.setRemainAmount(bill.getRemainAmount());
        billInfoDtoRs.setCurrentPaidAmount(bill.getCurrentPaidAmount());

        List<SadadPayment> sadadPaymentList = sadadPaymentRepo.findSadadPaymentByBillOrderByIdAsc(bill);
        List<LocalPayment> localPaymentList = localPaymentRepo.findLocalPaymentByBillOrderByIdAsc(bill);


        BigDecimal totalOfflinePaidAmount = BigDecimal.ZERO;
        BigDecimal totalOnlinePaidAmount = BigDecimal.ZERO;

        List<PaymentDtoRs> paymentDtoRsList = new ArrayList<>();

        if (sadadPaymentList != null && !sadadPaymentList.isEmpty()) {
            for (SadadPayment sadadPayment : sadadPaymentList) {
                PaymentDtoRs paymentDtoRs = new PaymentDtoRs();
                paymentDtoRs.setBillNumber(bill.getBillNumber());
                paymentDtoRs.setSadadNumber(bill.getSadadNumber());
                paymentDtoRs.setSadadPaymentId(sadadPayment.getSadadPaymentId());
                paymentDtoRs.setPaymentDate(Utils.parseDateTime(sadadPayment.getPaymentDate(), "yyyy-MM-dd"));
                paymentDtoRs.setPaymentType("ONLINE");
                paymentDtoRs.setPaymentMethod("SADAD");
                paymentDtoRs.setPaymentAmount(sadadPayment.getPaymentAmount());
                paymentDtoRs.setRemainingAmount(sadadPayment.getRemainingAmount());
                paymentDtoRs.setPaymentStatus(sadadPayment.getPaymentStatus().name());
                paymentDtoRs.setUserReference("NA");
                paymentDtoRs.setPaymentImage(sadadPayment.getPaymentImage());
                paymentDtoRs.setTotalAmountAr(sadadPayment.getTotalAmountAr());
                paymentDtoRs.setTotalAmountEn(sadadPayment.getTotalAmountEn());
                paymentDtoRs.setPaymentAmountAr(sadadPayment.getPaymentAmountAr());
                paymentDtoRs.setPaymentAmountEn(sadadPayment.getPaymentAmountAr());
                paymentDtoRs.setBillAmount(bill.getTotalAmount());
                paymentDtoRs.setCustomerName(bill.getCustomerFullName());
                paymentDtoRs.setActivityCode(bill.getEntityActivity().getCode());
                paymentDtoRs.setVoucherNumber(sadadPayment.getVoucherNumber() + "");
                paymentDtoRs.setPaymentNumber(sadadPayment.getPaymentNumber());
                paymentDtoRs.setVoucherDate(sadadPayment.getPaymentDate());

                paymentDtoRsList.add(paymentDtoRs);

                totalOnlinePaidAmount = totalOnlinePaidAmount.add(sadadPayment.getPaymentAmount());
            }
        }

        if (localPaymentList != null && !localPaymentList.isEmpty()) {
            for (LocalPayment localPayment : localPaymentList) {
                PaymentDtoRs paymentDtoRs = new PaymentDtoRs();
                paymentDtoRs.setBillNumber(bill.getBillNumber());
                paymentDtoRs.setSadadNumber(bill.getSadadNumber());
                paymentDtoRs.setSadadPaymentId(localPayment.getSadadPaymentId());
                paymentDtoRs.setPaymentDate(Utils.parseDateTime(localPayment.getPaymentDate(), "yyyy-MM-dd"));
                paymentDtoRs.setPaymentType("OFFLINE");
                paymentDtoRs.setPaymentMethod(localPayment.getPaymentMethod() != null ? localPayment.getPaymentMethod().name() : null);
                paymentDtoRs.setPaymentAmount(localPayment.getPaymentAmount());
                paymentDtoRs.setRemainingAmount(localPayment.getRemainingAmount());
                paymentDtoRs.setPaymentStatus(localPayment.getPaymentStatus().name());
                paymentDtoRs.setUserReference(localPayment.getEntityUser() != null ? localPayment.getEntityUser().getUsername() : "");
                paymentDtoRs.setPaymentImage(localPayment.getPaymentImage());
                paymentDtoRs.setTotalAmountAr(localPayment.getTotalAmountAr());
                paymentDtoRs.setTotalAmountEn(localPayment.getTotalAmountEn());
                paymentDtoRs.setPaymentAmountAr(localPayment.getPaymentAmountAr());
                paymentDtoRs.setPaymentAmountEn(localPayment.getPaymentAmountAr());
                paymentDtoRs.setBillAmount(bill.getTotalAmount());
                paymentDtoRs.setCustomerName(bill.getCustomerFullName());
                paymentDtoRs.setActivityCode(bill.getEntityActivity().getCode());
                paymentDtoRs.setVoucherNumber(localPayment.getVoucherNumber() + "");
                paymentDtoRs.setPaymentNumber(localPayment.getPaymentNumber());
                paymentDtoRs.setVoucherDate(localPayment.getCreateDate());
                paymentDtoRsList.add(paymentDtoRs);

                totalOfflinePaidAmount = totalOfflinePaidAmount.add(localPayment.getPaymentAmount());
            }
        }

        Collections.sort(paymentDtoRsList);

        billInfoDtoRs.setPaymentList(paymentDtoRsList);
        billInfoDtoRs.setOnlinePaymentList(sadadPaymentList);
        billInfoDtoRs.setOfflinePaymentList(localPaymentList);

        billInfoDtoRs.setTotalOfflinePaidAmount(totalOfflinePaidAmount);
        billInfoDtoRs.setTotalOnlinePaidAmount(totalOnlinePaidAmount);

        billInfoDtoRs.setAdditionalNumber(bill.getAdditionalNumber());
        billInfoDtoRs.setBuildingNumber(bill.getBuildingNumber());
        billInfoDtoRs.setDistrictId(bill.getDistrict() != null ? bill.getDistrict().getId() : 0L);
        billInfoDtoRs.setPostalCode(bill.getPostalCode());
        billInfoDtoRs.setStreetName(bill.getStreet());
        billInfoDtoRs.setCityId(bill.getCity() != null ? bill.getCity().getId() : null);
        billInfoDtoRs.setOtherDistrict(bill.getOtherDistrict());
        billInfoDtoRs.setEntityActivity(bill.getEntityActivity() != null ? bill.getEntityActivity().toDto(locale) : null);

        if (bill.getBillType().equals(BillType.RECURRING_BILL)) {
            billInfoDtoRs.setInstallmentAmount(bill.getTotalAmount());

            billInfoDtoRs.setDueDate(bill.getDueDate());

            billInfoDtoRs.setReferenceNumber(bill.getReferenceNumber());
            billInfoDtoRs.setContractNumber(bill.getReferenceNumber());
            Account account = bill.getAccount();

            if (account instanceof IndividualAccount) {
                billInfoDtoRs.setAccount("INDIVIDUAL");
            } else {
                billInfoDtoRs.setAccount("BUSINESS");
            }

            billInfoDtoRs.setAccountInfo(account.toAccountDtoRs());
            billInfoDtoRs.setAccountId(account.getId());
        }

        billInfoDtoRs.setInvoiceLogo(bill.getLogo());
        billInfoDtoRs.setInvoiceStamp(bill.getStamp());

        List<BillNote> billNoteList = billNoteRepo.findAllBillNote(bill);

        List<BillNoteDtoRs> billNoteDtoRsList = new ArrayList<>();

        BillNoteDtoRs billNoteDtoRs = new BillNoteDtoRs();

        billNoteDtoRs.setId(0L);
        billNoteDtoRs.setDocumentNumber(bill.getSadadNumber());
        billNoteDtoRs.setDate(bill.getCreateDate());
        billNoteDtoRs.setNoteType(NoteType.INVOICE_NOTE);
        billNoteDtoRs.setDescription(Utf8ResourceBundle.getString(billNoteDtoRs.getNoteType().name(), locale));
        billNoteDtoRs.setAmount(bill.getTotalAmount());
        billNoteDtoRs.setBalanceAmount(bill.getTotalAmount());
        billNoteDtoRs.setItemList(new ArrayList<>());

        billNoteDtoRsList.add(billNoteDtoRs);

        for (BillNote billNote : billNoteList) {

            BillNoteDtoRs billNoteDtoRsItem = new BillNoteDtoRs();

            billNoteDtoRsItem.setId(0L);
            billNoteDtoRsItem.setDocumentNumber(billNote.getDocumentNumber());
            billNoteDtoRsItem.setDate(billNote.getDate());
            billNoteDtoRsItem.setNoteType(billNote.getNoteType());
            billNoteDtoRsItem.setDescription(Utf8ResourceBundle.getString(billNote.getNoteType().name(), locale));
            billNoteDtoRsItem.setAmount(billNote.getAmount());
            billNoteDtoRsItem.setBalanceAmount(billNote.getBalanceAmount());
            billNoteDtoRsItem.setItemList(billNoteItemRepo.findAllBillNoteItem(billNote));

            billNoteDtoRsList.add(billNoteDtoRsItem);
        }

        for (PaymentDtoRs paymentDtoRs : paymentDtoRsList) {

            BillNoteDtoRs billNoteDtoRsItem = new BillNoteDtoRs();

            billNoteDtoRsItem.setId(0L);
            billNoteDtoRsItem.setDocumentNumber(paymentDtoRs.getVoucherNumber());
            billNoteDtoRsItem.setDate(paymentDtoRs.getVoucherDate());
            billNoteDtoRsItem.setNoteType(NoteType.PAYMENT_NOTE);
            billNoteDtoRsItem.setDescription(Utf8ResourceBundle.getString(billNoteDtoRsItem.getNoteType().name(), locale));
            billNoteDtoRsItem.setAmount(paymentDtoRs.getPaymentAmount().multiply(BigDecimal.valueOf(-1)));
            billNoteDtoRsItem.setBalanceAmount(paymentDtoRs.getRemainingAmount());
            billNoteDtoRsItem.setItemList(new ArrayList<>());

            billNoteDtoRsList.add(billNoteDtoRsItem);
        }

        Collections.sort(billNoteDtoRsList);

        billInfoDtoRs.setBillNoteList(billNoteDtoRsList);

        String billNumberDecoded = aes256.encrypt(billInfoDtoRs.getSadadNumber());

        String qrString = "اسم المورد: " + bill.getEntity().getBrandNameAr() + "\n" +
                "الرقم الضريبي للمورد: " + bill.getEntity().getVatNumber() + "\n" +
                "الطابع الزمني: " + Utils.parseDateTime(bill.getCreateDate(), "yyyy-MM-dd hh:mm") + "\n" +
                "اجمالي الفاتورة: " + Utils.roundDecimal(bill.getTotalAmount()) + "\n" +
                "اجمالي الضريبة: " + Utils.roundDecimal(bill.getSubAmount().multiply(bill.getVat())) + "\n" +
                environment.getProperty("qr_url") + billNumberDecoded;

        billInfoDtoRs.setQr(qrString);

        billInfoDtoRs.setReferenceNumber(bill.getReferenceNumber());
        billInfoDtoRs.setContractNumber(bill.getReferenceNumber());


        Entity entity = bill.getEntity();

        EntityContactDetail entityContactDetail = entityContactDetailRepo.findAllByEntity(bill.getEntity());

        EntityBillInfoDtoRs entityBillInfoDtoRs = new EntityBillInfoDtoRs();

        entityBillInfoDtoRs.setId(entity.getId());
        entityBillInfoDtoRs.setLogo(entity.getLogo());
        entityBillInfoDtoRs.setStamp(entity.getStamp());
        entityBillInfoDtoRs.setCode(entity.getCode());
        entityBillInfoDtoRs.setBrandNameAr(entity.getBrandNameAr());
        entityBillInfoDtoRs.setBrandNameEn(entity.getBrandNameEn());
        entityBillInfoDtoRs.setCommercialNameAr(entity.getCommercialNameAr());
        entityBillInfoDtoRs.setCommercialNameEn(entity.getCommercialNameEn());
        entityBillInfoDtoRs.setVatNumber(entity.getVatNumber());
        entityBillInfoDtoRs.setVatNameAr(entity.getVatNameAr());
        entityBillInfoDtoRs.setCity(entity.getCity());
        entityBillInfoDtoRs.setOtherCity(entity.getOtherCity());
        entityBillInfoDtoRs.setDistrict(entity.getDistrict());
        entityBillInfoDtoRs.setOtherDistrict(entity.getOtherDistrict());
        entityBillInfoDtoRs.setStreetName(entity.getStreetName());
        entityBillInfoDtoRs.setEntityUnitNumber(entity.getEntityUnitNumber());
        entityBillInfoDtoRs.setZipCode(entity.getZipCode());
        entityBillInfoDtoRs.setAdditionalZipCode(entity.getAdditionalZipCode());
        entityBillInfoDtoRs.setIdNumber(entity.getIdNumber());

        entityBillInfoDtoRs.setPhoneNumber(entityContactDetail.getPhoneNumber());
        entityBillInfoDtoRs.setPhoneNumberCode(entityContactDetail.getPhoneNumberCode());
        entityBillInfoDtoRs.setMobileNumber(entityContactDetail.getMobileNumber());
        entityBillInfoDtoRs.setMobileNumberCode(entityContactDetail.getMobileNumberCode());
        entityBillInfoDtoRs.setUnifiedNumber(entityContactDetail.getUnifiedNumber());
        entityBillInfoDtoRs.setWebsite(entityContactDetail.getWebsite());
        entityBillInfoDtoRs.setEmail(entityContactDetail.getEmail());

        SettingStyleDtoRq settingStyleDtoRq = new SettingStyleDtoRq();
        settingStyleDtoRq.setColor(entity.getColor());
        settingStyleDtoRq.setStamp(entity.getStampWidth());
        settingStyleDtoRq.setLogo(entity.getLogoWidth());

        entityBillInfoDtoRs.setStyling(settingStyleDtoRq);

        billInfoDtoRs.setEntity(entityBillInfoDtoRs);

        billInfoDtoRs.setBillTypeText(Utf8ResourceBundle.getString(bill.getBillType().name(), locale));


        return billInfoDtoRs;
    }

    public String exportPdf(SearchBillRqDto searchBillRqDto, EntityUser entityUser, Locale locale) {

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }


        List<Bill> billList = billRepo.findAll(new Specification<Bill>() {

            public Predicate toPredicate(Root<Bill> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                List<Predicate> predicates = new ArrayList<>();

                if (!Utils.isNullOrEmpty(searchBillRqDto.getBillNumber())) {
                    predicates.add(cb.equal(cb.lower(root.get("billNumber")), searchBillRqDto.getBillNumber().toLowerCase()));
                }

                if (!Utils.isNullOrEmpty(searchBillRqDto.getSadadNumber())) {
                    predicates.add(cb.equal(cb.lower(root.get("sadadNumber")), searchBillRqDto.getSadadNumber().toLowerCase()));
                }

                if (!Utils.isNullOrEmpty(searchBillRqDto.getTotalAmount())) {
                    predicates.add(cb.equal(root.get("totalAmount"), searchBillRqDto.getTotalAmount()));
                }

                if (!Utils.isNullOrEmpty(searchBillRqDto.getServiceName())) {
                    predicates.add(cb.like(cb.lower(root.get("serviceName")), "%" + searchBillRqDto.getServiceName().toLowerCase() + "%"));
                }

                if (!Utils.isNullOrEmpty(searchBillRqDto.getCustomerFullName())) {
                    predicates.add(cb.like(cb.lower(root.get("customerFullName")), "%" + searchBillRqDto.getCustomerFullName().toLowerCase() + "%"));
                }

                if (!Utils.isNullOrEmpty(searchBillRqDto.getCustomerEmailAddress())) {
                    predicates.add(cb.like(cb.lower(root.get("customerEmailAddress")), "%" + searchBillRqDto.getCustomerEmailAddress().toLowerCase() + "%"));
                }

                if (!Utils.isNullOrEmpty((searchBillRqDto.getCustomerMobileNumber()))) {
                    predicates.add(cb.equal(cb.lower(root.get("customerMobileNumber")), Utils.formatMobileNumber(searchBillRqDto.getCustomerMobileNumber().toLowerCase())));
                }

                if (!Utils.isNullOrEmpty((searchBillRqDto.getCustomerIdNumber()))) {
                    predicates.add(cb.equal(cb.lower(root.get("customerIdNumber")), searchBillRqDto.getCustomerIdNumber().toLowerCase()));
                }

                if (Utils.isEnumValueExist(searchBillRqDto.getBillType(), BillType.class)) {
                    predicates.add(cb.equal(root.get("billType"), BillType.valueOf(searchBillRqDto.getBillType())));
                }

                if (entityUser != null && entityUser.getEntity() != null) {
                    predicates.add(cb.equal(root.get("entity"), entityUser.getEntity()));
                }

                LocalDateTime issueDateFrom = Utils.parseDateFromString(searchBillRqDto.getIssueDateFrom(), "yyyy-MM-dd");

                if (issueDateFrom != null) {
                    predicates.add(cb.greaterThanOrEqualTo(root.get("issueDate"), issueDateFrom));
                }

                LocalDateTime issueDateTo = Utils.parseDateFromString(searchBillRqDto.getIssueDateTo(), "yyyy-MM-dd");

                if (issueDateTo != null) {
                    issueDateTo = issueDateTo.plusHours(24).minusMinutes(1);
                    predicates.add(cb.lessThanOrEqualTo(root.get("issueDate"), issueDateTo.plusHours(24)));
                }

                predicates.add(cb.notEqual(root.get("billStatus"), BillStatus.DELETED));

                List<Predicate> billStatusPredicateList = new ArrayList<>();

                if (searchBillRqDto.getBillStatusList() != null && !searchBillRqDto.getBillStatusList().isEmpty()) {

                    for (String billStatusCode : searchBillRqDto.getBillStatusList()) {

                        if (Utils.isEnumValueExist(billStatusCode, BillStatus.class)) {
                            Predicate billStatusPredicate = cb.equal(root.get("billStatus"), BillStatus.valueOf(billStatusCode));
                            billStatusPredicateList.add(billStatusPredicate);
                        }
                    }
                }

                Predicate andPredicate = cb.and(predicates.toArray(new Predicate[0]));

                Predicate statusPredicate = cb.or(billStatusPredicateList.toArray(new Predicate[0]));

                Predicate finalPredicate = null;
                if (!billStatusPredicateList.isEmpty()) {
                    finalPredicate = cb.and(andPredicate, statusPredicate);
                } else {
                    finalPredicate = cb.and(andPredicate);
                }
                return finalPredicate;
            }
        });

        if (billList == null || billList.isEmpty()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "no_bills_found", locale);
        }

        JasperReport jasperReport = Utils.findReportResourceFile("BillReportEntity", locale);
        if (jasperReport == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "error_while_generating_report", locale);
        }

        List<BillReportEntityDtoRs> billReportEntityDtoRsList = Utils.fetchBillReportEntityList(billList, locale);

        JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(billReportEntityDtoRsList);

        Map<String, Object> parameters = new HashMap<>();

        parameters.put("entityName", entityUser.getEntity().getBrandName(locale));
        parameters.put("entityCode", entityUser.getEntity().getCode());
        parameters.put("userName", entityUser.getUsername());
        parameters.put("numberOfBills", billList.size() + "");
        parameters.put("volumeOfBills", Utils.fetchBillReportVolume(billList, locale));

        JasperPrint jasperPrint = Utils.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
        if (jasperPrint == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "error_while_generating_report", locale);
        }

        String reportName = System.currentTimeMillis() + ".pdf";

        String finalReportPath = environment.getProperty("report.archive.path") + reportName;

        try {
            JasperExportManager.exportReportToPdfFile(jasperPrint, finalReportPath);

            File reportFile = new File(finalReportPath);

            ftpHandler.uploadFile(reportFile);

            reportFile.delete();

        } catch (JRException e) {
            LOGGER.error(ExceptionUtils.getStackTrace(e), e);
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "error_while_generating_report", locale);
        } catch (Exception e) {
            LOGGER.error(ExceptionUtils.getStackTrace(e), e);
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "error_while_generating_report", locale);
        }


        return environment.getProperty("report.archive.url") + reportName;
    }

    public String exportExcel(SearchBillRqDto searchBillRqDto, EntityUser entityUser, Locale locale) {

        if (entityUser.getStatus().equals(Status.DELETED)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_deleted", locale);
        }

        if (entityUser.getStatus().equals(Status.INACTIVE)) {
            throw new HttpServiceException(HttpStatus.FORBIDDEN, "user_inactive", locale);
        }

        List<Bill> billList = billRepo.findAll(new Specification<Bill>() {

            public Predicate toPredicate(Root<Bill> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                List<Predicate> predicates = new ArrayList<>();

                if (!Utils.isNullOrEmpty(searchBillRqDto.getBillNumber())) {
                    predicates.add(cb.equal(cb.lower(root.get("billNumber")), searchBillRqDto.getBillNumber().toLowerCase()));
                }

                if (!Utils.isNullOrEmpty(searchBillRqDto.getSadadNumber())) {
                    predicates.add(cb.equal(cb.lower(root.get("sadadNumber")), searchBillRqDto.getSadadNumber().toLowerCase()));
                }

                if (!Utils.isNullOrEmpty(searchBillRqDto.getTotalAmount())) {
                    predicates.add(cb.equal(root.get("totalAmount"), searchBillRqDto.getTotalAmount()));
                }

                if (!Utils.isNullOrEmpty(searchBillRqDto.getServiceName())) {
                    predicates.add(cb.like(cb.lower(root.get("serviceName")), "%" + searchBillRqDto.getServiceName().toLowerCase() + "%"));
                }

                if (!Utils.isNullOrEmpty(searchBillRqDto.getCustomerFullName())) {
                    predicates.add(cb.like(cb.lower(root.get("customerFullName")), "%" + searchBillRqDto.getCustomerFullName().toLowerCase() + "%"));
                }

                if (!Utils.isNullOrEmpty(searchBillRqDto.getCustomerEmailAddress())) {
                    predicates.add(cb.like(cb.lower(root.get("customerEmailAddress")), "%" + searchBillRqDto.getCustomerEmailAddress().toLowerCase() + "%"));
                }

                if (!Utils.isNullOrEmpty((searchBillRqDto.getCustomerMobileNumber()))) {
                    predicates.add(cb.equal(cb.lower(root.get("customerMobileNumber")), Utils.formatMobileNumber(searchBillRqDto.getCustomerMobileNumber().toLowerCase())));
                }

                if (!Utils.isNullOrEmpty((searchBillRqDto.getCustomerIdNumber()))) {
                    predicates.add(cb.equal(cb.lower(root.get("customerIdNumber")), searchBillRqDto.getCustomerIdNumber().toLowerCase()));
                }

                if (Utils.isEnumValueExist(searchBillRqDto.getBillType(), BillType.class)) {
                    predicates.add(cb.equal(root.get("billType"), BillType.valueOf(searchBillRqDto.getBillType())));
                }

                if (entityUser != null && entityUser.getEntity() != null) {
                    predicates.add(cb.equal(root.get("entity"), entityUser.getEntity()));
                }

                LocalDateTime issueDateFrom = Utils.parseDateFromString(searchBillRqDto.getIssueDateFrom(), "yyyy-MM-dd");

                if (issueDateFrom != null) {
                    predicates.add(cb.greaterThanOrEqualTo(root.get("issueDate"), issueDateFrom));
                }

                LocalDateTime issueDateTo = Utils.parseDateFromString(searchBillRqDto.getIssueDateTo(), "yyyy-MM-dd");

                if (issueDateTo != null) {
                    issueDateTo = issueDateTo.plusHours(24).minusMinutes(1);
                    predicates.add(cb.lessThanOrEqualTo(root.get("issueDate"), issueDateTo.plusHours(24)));
                }

                predicates.add(cb.notEqual(root.get("billStatus"), BillStatus.DELETED));

                List<Predicate> billStatusPredicateList = new ArrayList<>();

                if (searchBillRqDto.getBillStatusList() != null && !searchBillRqDto.getBillStatusList().isEmpty()) {

                    for (String billStatusCode : searchBillRqDto.getBillStatusList()) {

                        if (Utils.isEnumValueExist(billStatusCode, BillStatus.class)) {
                            Predicate billStatusPredicate = cb.equal(root.get("billStatus"), BillStatus.valueOf(billStatusCode));
                            billStatusPredicateList.add(billStatusPredicate);
                        }
                    }
                }

                Predicate andPredicate = cb.and(predicates.toArray(new Predicate[0]));

                Predicate statusPredicate = cb.or(billStatusPredicateList.toArray(new Predicate[0]));

                Predicate finalPredicate = null;
                if (!billStatusPredicateList.isEmpty()) {
                    finalPredicate = cb.and(andPredicate, statusPredicate);
                } else {
                    finalPredicate = cb.and(andPredicate);
                }
                return finalPredicate;
            }
        });

        if (billList == null || billList.isEmpty()) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "no_bills_found", locale);
        }

        JasperReport jasperReport = Utils.findReportResourceFile("BillReportEntity", locale);
        if (jasperReport == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "error_while_generating_report", locale);
        }

        List<BillReportEntityDtoRs> billReportEntityDtoRsList = Utils.fetchBillReportEntityList(billList, locale);

        JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(billReportEntityDtoRsList);

        Map<String, Object> parameters = new HashMap<>();

        parameters.put("entityName", entityUser.getEntity().getBrandName(locale));
        parameters.put("entityCode", entityUser.getEntity().getCode());
        parameters.put("userName", entityUser.getUsername());
        parameters.put("numberOfBills", billList.size() + "");
        parameters.put("volumeOfBills", Utils.fetchBillReportVolume(billList, locale));

        JasperPrint jasperPrint = Utils.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
        if (jasperPrint == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "error_while_generating_report", locale);
        }

        String reportName = System.currentTimeMillis() + ".xlsx";

        String finalReportPath = environment.getProperty("report.archive.path") + reportName;

        try {
            JRXlsxExporter exporter = new JRXlsxExporter();

            SimpleXlsxReportConfiguration reportConfig
                    = new SimpleXlsxReportConfiguration();
            reportConfig.setSheetNames(new String[]{"Bills"});

            exporter.setConfiguration(reportConfig);
            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(finalReportPath));
            exporter.exportReport();

            File reportFile = new File(finalReportPath);

            ftpHandler.uploadFile(reportFile);

            reportFile.delete();

        } catch (JRException e) {
            LOGGER.error(ExceptionUtils.getStackTrace(e), e);
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "error_while_generating_report", locale);
        } catch (Exception e) {
            LOGGER.error(ExceptionUtils.getStackTrace(e), e);
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "error_while_generating_report", locale);
        }


        return environment.getProperty("report.archive.url") + reportName;
    }

    public void updateStatusToViewed(Long billId, Locale locale) {

        Bill bill = billRepo.findBillById(billId);
        if (bill != null) {
            if (isBillStatusExist(bill, BillStatus.VIEWED_BY_CUSTOMER)
                    || isBillStatusExist(bill, BillStatus.PAID_BY_COMPANY)
                    || isBillStatusExist(bill, BillStatus.PARTIALLY_PAID_BY_COMPANY)
                    || isBillStatusExist(bill, BillStatus.PARTIALLY_PAID_BY_SADAD)
                    || isBillStatusExist(bill, BillStatus.PAID_BY_SADAD)
                    || isBillStatusExist(bill, BillStatus.SETTLED_BY_COMPANY)
                    || isBillStatusExist(bill, BillStatus.SETTLED_BY_SADAD)) {
                return;
            }

            if (isBillStatusExist(bill, BillStatus.APPROVED_BY_SADAD)) {

                bill.setBillStatus(BillStatus.VIEWED_BY_CUSTOMER);

                BillTimeline viewedBillTimeline = new BillTimeline();
                viewedBillTimeline.setBillStatus(BillStatus.VIEWED_BY_CUSTOMER);
                viewedBillTimeline.setBill(bill);
                viewedBillTimeline.setActionDate(LocalDateTime.now());
                viewedBillTimeline.setEntityUser(null);
                viewedBillTimeline.setUserReference("Customer");

                billTimelineRepo.save(viewedBillTimeline);
            }
        }
    }

    public PageDtoRs findBillsByFileId(Long fileId, EntityUser entityUser, Integer page, Integer size, Locale locale) {

        if (page == null)
            page = (0);
        if (size == null)
            size = (10);

        BulkBill bulkBill = bulkBillRepo.findBulkBillById(fileId);

        if (bulkBill == null) {
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "bulk_not_found", locale);
        }

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.ASC, "id");

        Page<Bill> billPage = billRepo.findAllByBulkBill(bulkBill, pageable);

        List<Bill> billList = billPage.getContent();

        List<SearchBillRsDto> searchBillRsDtoList = new ArrayList<>();

        billList.forEach(bill -> {
            SearchBillRsDto searchBillRsDto = new SearchBillRsDto();
            searchBillRsDto.setId(bill.getId());
            searchBillRsDto.setBillNumber(bill.getBillNumber());
            searchBillRsDto.setSadadNumber(bill.getSadadNumber());
            searchBillRsDto.setServiceName(bill.getServiceName());
            searchBillRsDto.setServiceDescription(bill.getServiceDescription());
            searchBillRsDto.setIssueDate(bill.getIssueDate());
            searchBillRsDto.setExpireDate(bill.getExpireDate());
            searchBillRsDto.setCreateDate(bill.getCreateDate());
            searchBillRsDto.setSubAmount(bill.getSubAmount());
            searchBillRsDto.setVat(bill.getVat());
            searchBillRsDto.setDiscount(bill.getDiscount());
            searchBillRsDto.setTotalAmount(bill.getTotalRunningAmount());
            searchBillRsDto.setCustomerFullName(bill.getCustomerFullName());
            searchBillRsDto.setCustomerMobileNumber(bill.getCustomerMobileNumber());
            searchBillRsDto.setCustomerEmailAddress(bill.getCustomerEmailAddress());
            searchBillRsDto.setCustomerIdNumber(bill.getCustomerIdNumber());
            searchBillRsDto.setBillStatus(bill.getBillStatus());
            searchBillRsDto.setBillStatusText(Utf8ResourceBundle.getString(bill.getBillStatus().name(), locale));
            searchBillRsDto.setBillType(bill.getBillType());
            searchBillRsDto.setBillTypeText(Utf8ResourceBundle.getString(bill.getBillType().name(), locale));
            searchBillRsDto.setBillSource(bill.getBillSource());
            searchBillRsDto.setBillSourceText(Utf8ResourceBundle.getString(bill.getBillSource().name(), locale));
            searchBillRsDto.setActivityId(bill.getEntityActivity().getId());
            searchBillRsDto.setActivityName(bill.getEntityActivity().getName(locale));
            searchBillRsDto.setPreviousBalance(bill.getCustomerPreviousBalance());
            searchBillRsDto.setCustomerTaxNumber(bill.getCustomerTaxNumber());
            searchBillRsDto.setCustomerAddress(bill.getCustomerAddress());

            List<BillCustomField> billCustomFieldList = billCustomFieldRepo.findAllByBill(bill);
            List<BillCustomFieldRsDto> billCustomFieldRsDtoList = new ArrayList<>();

            if (billCustomFieldList != null && !billCustomFieldList.isEmpty()) {
                billCustomFieldList.forEach(billCustomField -> {
                    if (!billCustomField.getEntityField().getStatus().equals(Status.DELETED)) {
                        billCustomFieldRsDtoList.add((BillCustomFieldRsDto) billCustomField.toDto(locale));
                    }
                });
            }

            searchBillRsDto.setCustomFieldList(billCustomFieldRsDtoList);

            searchBillRsDtoList.add(searchBillRsDto);
        });


        return new PageDtoRs(billPage.getTotalElements(), billPage.getTotalPages(), searchBillRsDtoList);
    }

    public String findHtmlContentForBill(Bill bill, Locale locale) {
        String htmlContents = null;

        try {
            ConfigTemplate configTemplate = configTemplateRepo.findConfigTemplateByKey("TAX_INV_TEMP");

            String newFileName = System.currentTimeMillis() + ".ftl";

            String finalNewFilePath = environment.getProperty("report.archive.path") + newFileName;

            File tempFile = new File(finalNewFilePath);
            try (FileOutputStream fos = new FileOutputStream(tempFile)) {
                fos.write(configTemplate.getContent());
                fos.close();
            } catch (IOException e) {
                LOGGER.error(ExceptionUtils.getStackTrace(e), e);
                throw new HttpServiceException(HttpStatus.BAD_REQUEST, "error_while_fetch_content", locale);
            }

            Configuration cfg = new Configuration(Configuration.VERSION_2_3_22);

            cfg.setDirectoryForTemplateLoading(tempFile.getParentFile());
            cfg.setDefaultEncoding("UTF-8");
            cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
            cfg.setClassicCompatible(true);

            Template temp = cfg.getTemplate(tempFile.getName());

            Map<String, Object> adMap = adverstisementUtil.getAdvertisment(bill.getEntity(), bill.getCampaignId() != null ? bill.getCampaignId() : 0);

            Map<String, Object> paymentMap = new HashMap<>();
            if (bill.getBillStatus().equals(BillStatus.PAID_BY_COMPANY)
                    || bill.getBillStatus().equals(BillStatus.PAID_BY_SADAD)
                    || bill.getBillStatus().equals(BillStatus.SETTLED_BY_SADAD)
                    || bill.getBillStatus().equals(BillStatus.SETTLED_BY_COMPANY)) {

                paymentMap.put("statusAr", "مدفوعة");
                paymentMap.put("statusEn", "Paid");
                paymentMap.put("paymentFlag", "PAID");

                SadadPayment sadadPayment = null;
                if (sadadPayment != null) {
                    if (sadadPayment.getPaymentMethod() != null) {
                        paymentMap.put("paymentMethod", "سداد");
                        paymentMap.put("paymentMethodImg", "https://www.sadad.com/_layouts/inc/SADAD.Internet.Portal/img/sadad_logo_ar.png");
                    }
                    paymentMap.put("paymentDate", Utils.parseDateTime(sadadPayment.getPaymentDate(), "YYYY-MM-dd"));
                } else {
                    LocalPayment localPayment = null;
                    if (localPayment.getPaymentMethod() != null) {
                        paymentMap.put("paymentMethod", Utf8ResourceBundle.getString(localPayment.getPaymentMethod() != null ? localPayment.getPaymentMethod().name() : null, locale));
                        paymentMap.put("paymentMethodImg", null);
                        switch (localPayment.getPaymentMethod().name()) {
                            case "MADA":
                                paymentMap.put("paymentMethodImg", "https://www.alahli.com/en-us/about-us/csr/PublishingImages/mada-logo-474-Px.png");
                                break;
                            case "VISA":
                                paymentMap.put("paymentMethodImg", "https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/Visa.svg/1200px-Visa.svg.png");
                                break;
                            case "MASTER":
                                paymentMap.put("paymentMethodImg", "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Mastercard-logo.svg/200px-Mastercard-logo.svg.png");
                                break;
                            case "AMERICAN_EXP":
                                paymentMap.put("paymentMethodImg", "https://i.dlpng.com/static/png/6699838_preview.png");
                                break;
                            case "APPLEPAY":
                                paymentMap.put("paymentMethodImg", "https://seeklogo.com/images/A/apple-pay-logo-F68C9AC252-seeklogo.com.png");
                                break;
                            case "EWALLET":
                                paymentMap.put("paymentMethodImg", "https://cdn6.f-cdn.com/contestentries/92694/11771521/53da7de995a1a_thumb900.jpg");
                                break;
                        }
                    }
                    paymentMap.put("paymentDate", Utils.parseDateTime(localPayment.getPaymentDate(), "YYYY-MM-dd"));
                }
            } else {
                paymentMap.put("statusAr", "غير مدفوعة");
                paymentMap.put("statusEn", "Unpaid");
                paymentMap.put("paymentFlag", "UNPAID");
                paymentMap.put("paymentDate", null);
                paymentMap.put("paymentMethod", null);
                paymentMap.put("paymentMethodImg", null);
            }

            List<BillItem> billItemList = billItemRepo.findAllByBill(bill);

            List<BillCustomField> billCustomFieldList = billCustomFieldRepo.findAllByBill(bill);

            StringWriter stringWriter = new StringWriter();
            temp.process(Utils.fillDataForBillAdv(bill, billItemList, billCustomFieldList, adMap, paymentMap), stringWriter);

            htmlContents = stringWriter.toString();

            tempFile.delete();
        } catch (IOException | TemplateException e) {
            LOGGER.error(ExceptionUtils.getStackTrace(e), e);
            throw new HttpServiceException(HttpStatus.BAD_REQUEST, "error_while_generate_content", locale);
        }

        return htmlContents;
    }

    public String generateBillNumber(EntityUser entityUser, Locale locale) {
        String prefixBillNumber = Utils.generateBillNumber(entityUser.getEntity());
        Long sequence = (entityUser.getEntity().getAutoGenerationBillNumberSequence() + 1);

        String billNumber = (prefixBillNumber + sequence);

//        List<Bill> billList = billRepo.findBillByBillNumber(billNumber,entityUser.getEntity());
//        if(billList == null || billList.isEmpty()){
//            entityRepo.increaseBillNumberCounter(sequence, entityUser.getEntity().getId());
//        }

        return billNumber;
    }

    public List<String> generateBillNumber(Integer size, EntityUser entityUser, Locale locale) {

        List<String> invoiceNumberList = new ArrayList<>();

        Long sequence = (entityUser.getEntity().getAutoGenerationBillNumberSequence());

        for (int i = 0; i < size; i++) {
            String prefixBillNumber = Utils.generateBillNumber(entityUser.getEntity());
            sequence = sequence + 1;

            String billNumber = (prefixBillNumber + sequence);

            invoiceNumberList.add(billNumber);
        }


        return invoiceNumberList;
    }
}
