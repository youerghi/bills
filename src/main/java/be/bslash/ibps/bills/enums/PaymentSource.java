package be.bslash.ibps.bills.enums;

public enum PaymentSource {
    ENTITY_PAYMENT,
    SADAD_NOTIFICATION;


    public static PaymentSource getValue(String value) {
        try {
            return PaymentSource.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

