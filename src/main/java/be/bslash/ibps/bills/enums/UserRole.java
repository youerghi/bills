package be.bslash.ibps.bills.enums;

public enum UserRole {
    SUPER_ADMIN_PLUS,
    SUPER_ADMIN,
    COMPLIANCE_SUPERVISOR,
    COMPLIANCE_OFFICER,
    FINANCE_SUPERVISOR,
    FINANCE_OFFICER,
    SALES_SUPERVISOR,
    SALES_OFFICER;

    public static UserRole getValue(String value) {
        try {
            return UserRole.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

