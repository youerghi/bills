package be.bslash.ibps.bills.enums;


public enum BulkBillStatus {
    BULK_GENERATED,
    BULK_UNDER_PROCESSING,
    BULK_MAKED,
    BULK_DELETED,
    BULK_CHECKED,
    BULK_UPLOADED_TO_SADAD,
    BULK_APPROVED_BY_SADAD,
    BULK_REJECTED_BY_SADAD;


    public static BulkBillStatus getValue(String value) {
        try {
            return BulkBillStatus.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

