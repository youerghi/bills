package be.bslash.ibps.bills.enums;


public enum DurationType {
    DAY,
    WEEK,
    MONTH,
    YEAR;

    public static DurationType getValue(String value) {
        try {
            return DurationType.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

