package be.bslash.ibps.bills.enums;

public enum IdIssueAuthority {
    MCI,
    MOMRA,
    SAMA,
    OTH;


    public static IdIssueAuthority getValue(String value) {
        try {
            return IdIssueAuthority.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }

}

