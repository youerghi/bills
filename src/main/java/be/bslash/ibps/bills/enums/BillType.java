package be.bslash.ibps.bills.enums;


public enum BillType {
    MASTER_BILL,
    DETAIL_BILL,
    TAX_BILL,
    SIMPLE_BILL,
    RECURRING_BILL,
    ;

    public static BillType getValue(String value) {
        try {
            return BillType.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

