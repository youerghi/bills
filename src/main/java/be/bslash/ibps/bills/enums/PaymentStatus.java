package be.bslash.ibps.bills.enums;

public enum PaymentStatus {
    APPROVED,
    FAILED,
    REVERSED;

    public static PaymentStatus getValue(String value) {
        try {
            return PaymentStatus.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

