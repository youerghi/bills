package be.bslash.ibps.bills.enums;


public enum NotificationStatus {
    SUCCESS,
    FAILED,
    PENDING;

    public static NotificationStatus getValue(String value) {
        try {
            return NotificationStatus.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

