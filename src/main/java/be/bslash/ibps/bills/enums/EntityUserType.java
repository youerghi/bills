package be.bslash.ibps.bills.enums;

public enum EntityUserType {
    //    MAKER,
//    CHECKER,
//    ADMIN,
    SUPER_ADMIN_PLUS,
    SUPER_ADMIN,
    SUPERVISOR_PLUS,
    SUPERVISOR,
    OFFICER;


    public static EntityUserType getValue(String value) {
        try {
            return EntityUserType.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

