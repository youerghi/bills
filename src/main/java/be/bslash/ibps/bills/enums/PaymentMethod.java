package be.bslash.ibps.bills.enums;

public enum PaymentMethod {
    CASH,
    CCARD,
    VISA,
    MASTER,
    AMERICAN_EXP,
    MADA,
    EFT,
    ACTDEB,
    APPLEPAY,
    EWALLET,
    CHEQUE;


    public static PaymentMethod getValue(String value) {
        try {
            return PaymentMethod.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

