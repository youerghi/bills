package be.bslash.ibps.bills.enums;

public enum NotificationMedia {
    SMS,
    EMAIL;

    public static NotificationMedia getValue(String value) {
        try {
            return NotificationMedia.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }


}

