package be.bslash.ibps.bills.enums;


public enum BulkType {
    MASTER_BULK,
    DETAIL_BULK;

    public static BulkType getValue(String value) {
        try {
            return BulkType.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }

}

