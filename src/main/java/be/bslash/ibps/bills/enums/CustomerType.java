package be.bslash.ibps.bills.enums;


public enum CustomerType {
    IND,
    BUS;


    public static CustomerType getValue(String value) {
        try {
            return CustomerType.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

