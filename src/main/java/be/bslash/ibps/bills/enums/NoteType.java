package be.bslash.ibps.bills.enums;


public enum NoteType {
    CREDIT_NOTE,
    DEBIT_NOTE,
    PAYMENT_NOTE,
    INVOICE_NOTE;

    public static NoteType getValue(String value) {
        try {
            return NoteType.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

