package be.bslash.ibps.bills.enums;


public enum BillNotificationType {
    ISSUE,
    REMINDER,
    PAID;

    public static BillNotificationType getValue(String value) {
        try {
            return BillNotificationType.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

