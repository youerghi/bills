package be.bslash.ibps.bills.enums;

public enum AccessChannel {
    ATM,
    IVR,
    KIOSK,
    INTERNET,
    PORTAL,
    BTELLER,
    POS,
    CAM,
    CORP,
    CCC,
    PDA,
    SMS,
    MOBILE,
    USSD;


    public static AccessChannel getValue(String value) {
        try {
            return AccessChannel.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

