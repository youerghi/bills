package be.bslash.ibps.bills.enums;


public enum BillSource {
    USER_INTERFACE,
    FILE,
    API;

    public static BillSource getValue(String value) {
        try {
            return BillSource.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

