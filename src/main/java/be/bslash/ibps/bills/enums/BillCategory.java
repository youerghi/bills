package be.bslash.ibps.bills.enums;


public enum BillCategory {
    INSTANTLY("instantly"),
    ONE_OFF_PARTIAL("one-off-par"),
    RECURRING_ACCOUNT_INSTANT("rec-instant"),
    RECURRING_ACCOUNT_PAR("rec-par");

    private String value;

    BillCategory(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static BillCategory getValue(String value) {
        try {
            return BillCategory.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

