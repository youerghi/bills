package be.bslash.ibps.bills.enums;

public enum PaymentRule {
    NONE,
    PARTIAL,
    OVER,
    ADVANCE;

    public static PaymentRule getValue(String value) {
        try {
            return PaymentRule.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

