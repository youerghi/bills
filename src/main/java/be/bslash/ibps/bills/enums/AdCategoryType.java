package be.bslash.ibps.bills.enums;


public enum AdCategoryType {
    DEFAULT,
    TARGETED;

    public static AdCategoryType getValue(String value) {
        try {
            return AdCategoryType.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

