package be.bslash.ibps.bills.enums;

public enum IdType {
    NAT,
    PAS,
    IQA,
    CRR;

    public static IdType getValue(String value) {
        try {
            return IdType.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

