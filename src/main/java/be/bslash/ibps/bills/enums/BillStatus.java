package be.bslash.ibps.bills.enums;


public enum BillStatus {
    MAKED,
    DELETED,
    CHECKED,
    UPLOADED_TO_SADAD,
    APPROVED_BY_SADAD,
    REJECTED_BY_SADAD,
    CANCELED,
    UPDATED_BY_COMPANY,
    PAID_BY_SADAD,
    PARTIALLY_PAID_BY_SADAD,
    PAID_BY_COMPANY,
    PARTIALLY_PAID_BY_COMPANY,
    SETTLED_BY_SADAD,
    SETTLED_BY_COMPANY,
    VIEWED_BY_CUSTOMER;

    public static BillStatus getValue(String value) {
        try {
            return BillStatus.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

