package be.bslash.ibps.bills.enums;

public enum EntityType {
    GOV_ENT,
    INT_ENT,
    LIM_ENT,
    LIS_ENT,
    SOL_ENT,
    CHAR_ENT,
    CLS_ENT,
    OTH_ENT;

    public static EntityType getValue(String value) {
        try {
            return EntityType.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

