package be.bslash.ibps.bills.enums;


public enum SendNotificationType {
    SAMEDAY,
    PRIOR;

    public static SendNotificationType getValue(String value) {
        try {
            return SendNotificationType.valueOf(value);
        } catch (Exception ex) {
            return null;
        }
    }
}

